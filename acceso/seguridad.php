<?php
ini_set("session.cookie_lifetime","28800");
ini_set("session.gc_maxlifetime","28800"); 
error_reporting(E_ERROR);

session_start();

if(file_exists("./clases/bd.php")){

	include("./clases/bd.php");

	include("./clases/mysql.php");

}

else if(file_exists("../clases/bd.php")){

	include("../clases/bd.php");

	include("../clases/mysql.php");

}

else{

	include("../../clases/bd.php");

	include("../../clases/mysql.php");

}

$log=false;

if(isset($_SESSION['Autentificado']) && isset($_SESSION['Acceso']))

{		

		$iUsuarioId=$_SESSION['iUsuarioId'];		

		$iUsuarioEmpresaId=$_SESSION['iUsuarioEmpresaId'];

		$iEmpleadoId=$_SESSION['iEmpleadoId'];		

		$iCodigo=$_SESSION['iCodigo'];				

		

		$dato_usuario = seleccionar("usuario","sNombre,sPaterno,sMaterno,sBaseDatos,sServidor,iTipoUsuarioId,sUsuario","iId='".$iUsuarioId."'",false,false);

		$log=true;			

		$iTipoUsuarioId  	= $dato_usuario[0]['iTipoUsuarioId'];			

		$sNombre 			= $dato_usuario[0]['sNombre'];

		$sPaterno 			= $dato_usuario[0]['sPaterno'];			

		$sMaterno			= $dato_usuario[0]['sMaterno'];			

		$sBaseDatos			= $dato_usuario[0]['sBaseDatos'];			

		$sServidor			= $dato_usuario[0]['sServidor'];	

		$sUsuario			= $dato_usuario[0]['sUsuario'];

		$dato_empresa = seleccionar("usuarioempresa INNER JOIN empresa ON usuarioempresa.iEmpresaId = empresa.iId","empresa.iId,empresa.sLogotipo,empresa.sEmpresa,empresa.sRazonSocial,empresa.iTicket,empresa.iCantidadProductos,empresa.iSolicitaPrecio,empresa.fMinimoUtilidad,empresa.iComision,empresa.iGasolina","usuarioempresa.iId='".$iUsuarioEmpresaId."'",false,false);	

		$sEmpresa	= $dato_empresa[0]['sEmpresa'];	

		$iEmpresaId	= $dato_empresa[0]['iId'];	

		$iIncluyeTicket	= $dato_empresa[0]['iTicket'];	

		$iIncluyeCantidadProductos	= $dato_empresa[0]['iCantidadProductos'];	
		$iGasolina	= $dato_empresa[0]['iGasolina'];
		$iComision	= $dato_empresa[0]['iComision'];

		if(permiso_modulo("accioncambiarprecio",$iEmpleadoId) || strtolower($sUsuario[0])=='c') {
			$iIncluyeSolicitudPrecio	= $dato_empresa[0]['iSolicitaPrecio'];
		}
		else {
			$iIncluyeSolicitudPrecio=0;
		}
		$fMinimoUtilidad	= $dato_empresa[0]['fMinimoUtilidad'];
		$sRazonSocial	= $dato_empresa[0]['sRazonSocial'];	
		$sLogotipo	= $dato_empresa[0]['sLogotipo'];	
				

}

else if(isset($_SESSION['iEmpleadoId']) && isset($_SESSION['Autentificado'])){

	$iEmpleadoId=$_SESSION['iEmpleadoId'];		



	

	$dato_empleado = seleccionar("empleado","iCodigo,sNombre,sPaterno,sMaterno,iUsuarioEmpresaId","iId='".$iEmpleadoId."'",false,false);

	$log=true;			

	$iUsuarioEmpresaId	= $dato_empleado[0]['iUsuarioEmpresaId'];			

	$sNombre 			= $dato_empleado[0]['sNombre'];

	$sPaterno 			= $dato_empleado[0]['sPaterno'];			

	$sMaterno			= $dato_empleado[0]['sMaterno'];

	$iCodigo			= $dato_empleado[0]['iCodigo'];		

	$sUsuario			= $dato_empleado[0]['iCodigo'];
 
	$dato_empresa = seleccionar("usuarioempresa INNER JOIN empresa ON usuarioempresa.iEmpresaId = empresa.iId","empresa.iId,empresa.sEmpresa,empresa.sLogotipo,empresa.sRazonSocial,empresa.iTicket,empresa.iCantidadProductos,empresa.iSolicitaPrecio,empresa.fMinimoUtilidad,empresa.iComision,empresa.iGasolina","usuarioempresa.iId='".$iUsuarioEmpresaId."'",false,false);	

	$sEmpresa	= $dato_empresa[0]['sEmpresa'];	

	$iEmpresaId	= $dato_empresa[0]['iId'];		

	$iIncluyeTicket	= $dato_empresa[0]['iTicket'];	

	$iIncluyeCantidadProductos	= $dato_empresa[0]['iCantidadProductos'];	

	$iIncluyeSolicitudPrecio	= $dato_empresa[0]['iSolicitaPrecio'];	
	
	$iGasolina	= $dato_empresa[0]['iGasolina'];	
	
	$iComision	= $dato_empresa[0]['iComision'];	

	if(permiso_modulo("accioncambiarprecio",$iEmpleadoId) || strtolower($sUsuario[0])=='c') {
		$iIncluyeSolicitudPrecio	= $dato_empresa[0]['iSolicitaPrecio'];
	}
	else {
		$iIncluyeSolicitudPrecio=0;
	}

	$fMinimoUtilidad = $dato_empresa[0]['fMinimoUtilidad'];	

	$sRazonSocial	= $dato_empresa[0]['sRazonSocial'];	
$sLogotipo	= $dato_empresa[0]['sLogotipo'];
}

else {

	#mientras no se mande este parametro de "no Die" no ejecutar comando.

	if(!isset($_REQUEST['nd'])){
		echo '<script language="javascript">window.location="./acceso/"</script>;';
		die("No esta conectado");
		

	}

}





?>