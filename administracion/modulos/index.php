<?
$_tabla = "modulo";
$_formulario = "./administracion/modulos/formulario.php";
?>
<div class="row">
  <div class="col-lg-6">
	  	<button type="button" class="btn btn-primary" id="nuevo" titulo='Agregar <?=$_tabla;?>' formulario='<?=$_formulario;?>' tabla='<?=$_tabla;?>'>Agregar <?=$_tabla;?></button>
    
   		<div class="btn-group">
			<button type="button" class="btn btn-default">Otras Opciones</button>
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		    <span class="caret"></span>
	    	<span class="sr-only">-</span>
			</button>
			<ul class="dropdown-menu" role="menu"> 
				<li><a href="#" id='eliminar-seleccionados' tabla='<?=$_tabla;?>'>Eliminar seleccionados</a></li>    
			</ul>
		</div>
	</div> 
	<? include("./clases/exportacion.php"); ?>
</div>

<div class="row">
    <div class="col-lg-12">    		
      	<div id="div-tabla"></div>    
	</div>
</div>        

<script>
$(document).ready(function () {

var genera_tabla = {
  columnas: [ 
    {columna:'modulo.sNombreModulo', titulo: 'Nombre Modulo',class:false,widthpdf:'20%',visible:true},
    {columna:'modulo.sTituloModulo', titulo: 'Titulo',class:false,widthpdf:'60%',visible:true},
    {columna:'sistema.sNombreSistema', titulo: 'Sistema',class:false,widthpdf:'8%',visible:true},
    {columna:'modulo.iEstatus', titulo: 'Estatus',class:false,widthpdf:'8%',visible:true,label:"avanzado"}				
   ]   
};

$("#div-tabla").crear_tabla('<?=$_formulario;?>','modulo.iId','sistema INNER JOIN modulo ON modulo.iSistemaId = sistema.iId',genera_tabla,"modulo.iEstatus=0 && sistema.iEstatus=0",false);

$('#ventana-emergente').on('hidden.bs.modal', function (e) {
	$("#div-tabla").crear_tabla('<?=$_formulario;?>','modulo.iId','sistema INNER JOIN modulo ON modulo.iSistemaId = sistema.iId',genera_tabla,"modulo.iEstatus=0 && sistema.iEstatus=0",false,true);	
});

});

</script>
