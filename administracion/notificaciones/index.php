<?
$_tabla = "notificacion";
$_formulario = "./administracion/notificaciones/formulario.php";
?>
<div class="row">
  <div class="col-lg-6">
	  	<button type="button" class="btn btn-primary" id="nuevo" titulo='Agregar <?=$_tabla;?>' formulario='<?=$_formulario;?>' tabla='<?=$_tabla;?>' retorna_id="si">Agregar <?=$_tabla;?></button>
    
   		<div class="btn-group">
			<button type="button" class="btn btn-default">Otras Opciones</button>
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		    <span class="caret"></span>
	    	<span class="sr-only">-</span>
			</button>
			<ul class="dropdown-menu" role="menu"> 
				<li><a href="#" id='eliminar-seleccionados' tabla='<?=$_tabla;?>'>Eliminar seleccionados</a></li>    
			</ul>
		</div>
	</div> 
	<? include("./clases/exportacion.php"); ?>
</div>

<div class="row">
    <div class="col-lg-12">    		
      	<div id="div-tabla"></div>    
	</div>
</div>        

<script>
$(document).ready(function () {

		$("body").on("click","#id_de_retorno",function(e){
			var iNotificacionId = $("#id_de_retorno").val();
			var iUsuarioEmpresaIdSel = $("#iUsuarioEmpresaId").val();
			$.post("./administracion/notificaciones/asigna_usuarios_empresas.php",{iNotificacionId:iNotificacionId,iUsuarioEmpresaIdSel:iUsuarioEmpresaIdSel},function(data){
				if(data[1]>0) {
					$(elemento).correcto(data[0]);	
				}
				else
				$(elemento).error(data[0]);	
			});			
		}); 
 
var genera_tabla = {
  columnas: [ 
    {columna:'notificacion.sNotificacion', titulo: 'Notificacion',class:false,widthpdf:'20%',visible:true},
    {columna:'tiponotificacion.sTipo', titulo: 'Tipo',class:false,widthpdf:'60%',visible:true},
    {columna:'notificacion.dRegistro', titulo: 'Envio',class:false,widthpdf:'18%',visible:true}
    
   ]   
};

$("#div-tabla").crear_tabla('<?=$_formulario;?>','notificacion.iId','notificacion INNER JOIN tiponotificacion ON notificacion.iTipoNotificacionId = tiponotificacion.iId',genera_tabla,"notificacion.iEstatus=0 and tiponotificacion.iEstatus=0",false);

$('#ventana-emergente').on('hidden.bs.modal', function (e) {
	$("#div-tabla").crear_tabla('<?=$_formulario;?>','notificacion.iId','notificacion INNER JOIN tiponotificacion ON notificacion.iTipoNotificacionId = tiponotificacion.iId',genera_tabla,"notificacion.iEstatus=0 and tiponotificacion.iEstatus=0",false,true);	
});

});

</script>
