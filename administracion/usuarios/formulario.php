<? 
include("../../acceso/seguridad.php");
?>
<div id='ventana-formulario'>
<!-- Nav tabs -->
<ul class="nav nav-pills">
  <li class='active'><a href="#general" data-toggle="tab">General</a></li>
  <li class='hide'><a href="#servidor" data-toggle="tab">Servidor</a></li>
  <li class='hide'><a href="#licencias" data-toggle="tab">Licencias</a></li>  
</ul>

<!-- Tab panes --> 
<div class="tab-content">
  <div class="tab-pane active" id="general">
  <input type="text" class="form-control" name="dRegistro" value="CURRENT_DATE()" style='display:none;'>
  <input type="text" class="form-control" name="iId" style='display:none;'>
	<div class="row">
	  <div class="col-lg-4">
     		<label>Nombres </label><span><input obligatorio='si' type="text" class="form-control" name="sNombre"></span>
     		<label>Apellido Paterno</label> <input type="text" class="form-control" name="sPaterno">            
     		<label>Apellido Materno </label><input type="text" class="form-control" name="sMaterno">            
			<label>Direcci&oacute;n</label> <input type="text" class="form-control" name="sDireccion">            
      </div>
	  <div class="col-lg-4">
      		<label>Usuario </label><span><input disabled="disabled" type="text" class="form-control" name="sUsuario"></span>
     		<label>Clave </label><span><input obligatorio='si' type="password" class="form-control" name="sClave"></span>          
     		<label>Correo</label><span><input obligatorio='si' type="text" class="form-control" name="sCorreo"></span>           



		<label>Tipo de Usuario</label>
        <span>
        <select class="form-control" name='iTipoUsuarioId' obligatorio="si">
        	<option value="1">Administrador</option>
        	<option value="2">Cliente</option> 
        	<option value="3">Distribuidor</option>            
        </select>
        </span>                          
      
      </div>
	  <div class="col-lg-4">
          	 <label>Telefono</label><input type="tel" class="form-control " name="sTelefono">
     		<label>Celular </label><input type="text" class="form-control" name="sCelular">                        
      </div>
    </div>
      
  		
  </div>
  <div class="tab-pane" id="servidor">
	<div class="row">  
	   <div class="col-lg-4">
     		<label>Servidor</label> <input type="text" class="form-control" name="sServidor">
     		<label>Base de Datos </label><input type="text" class="form-control" name="sBaseDatos">                 		
      </div>
	 </div>      
  </div>
  
    <div class="tab-pane" id="licencias">
	<div class="row">  
	   <div class="col-lg-8">
     		<label>Seleccione licencia para asignar</label> 
                <select class="form-control" name='iLicenciaId' no_catalogo="no">
                <?
                $select = seleccionar("licencia","sNombreLicencia,iId","iEstatus=0",false, false);
                foreach($select as $dato) {
                ?>
                <option value="<?=$dato['iId'];?>"><?=$dato['sNombreLicencia'];?></option>
                <?
                }
                ?>
                </select>
		</div>                 
		<div class="col-lg-4">  
		        <label style='color:#fff;'>-</label>              
                <button type="button" class="btn btn-success" id="asignar_licencia" data-toggle="tooltip" titulo='Asignar Licencia'>Asignar</button>
                
      </div>                  
	 </div>      
     <div class="row">         
	   <div class="col-lg-12">
			<label style='margin-top:15px;'>Licencias registradas de usuario</label>       
          <div id="licencias_registradas">
			</div>
       </div>
     </div>
     
  </div>
  

</div>
</div>