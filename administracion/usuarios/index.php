<?
$_tabla = "usuario";
$_formulario = "./administracion/usuarios/formulario.php";
$_post_agregar = "./administracion/usuarios/genera_codigo.php";
?>
<div class="row"> 
  <div class="col-lg-6">
	  	<button type="button" class="btn btn-primary" id="nuevo" titulo='Agregar <?=$_tabla;?>' formulario='<?=$_formulario;?>' tabla='<?=$_tabla;?>' post_agregar='<?=$_post_agregar;?>' retorna_id="si">Agregar <?=$_tabla;?></button>
    
   		<div class="btn-group">
			<button type="button" class="btn btn-default">Otras Opciones</button>
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		    <span class="caret"></span>
	    	<span class="sr-only">-</span>
			</button>
			<ul class="dropdown-menu" role="menu"> 
   				<li><a href="#" id='asignar-licencia' tabla='<?=$_tabla;?>'>Asignar licencia</a></li>    
   				<li><a href="#" id='ver-licencia' tabla='<?=$_tabla;?>'>Licencias de usuario</a></li>                    
				<li class="divider"></li>                
				<li><a href="#" id='eliminar-seleccionados' tabla='<?=$_tabla;?>'>Eliminar seleccionados</a></li>    

			</ul>
		</div>
	</div> 
	<? include("./clases/exportacion.php"); ?>
</div>

<div class="row">
    <div class="col-lg-12">    		
      	<div id="div-tabla"></div>    
	</div>
</div>        

<script>
$(document).ready(function () {
	
	$("body").on("click","#id_de_retorno",function(e){
			var iUsuarioId = $("#id_de_retorno").val();
			if($(this).attr("tipo")=="usuario"){
				$("#ventana-formulario li.hide").addClass("show");
				$("#ventana-formulario [name='iId']").val(iUsuarioId);
			}
				
			
		}); 
var genera_tabla = {
  columnas: [ 
    {columna:'sNombre', titulo: 'Nombre',class:false,widthpdf:'17%',visible:true},
    {columna:'sPaterno', titulo: 'Ap. Paterno',class:"hidden-xs",widthpdf:'17%',visible:true},
    {columna:'sMaterno', titulo: 'Ap. Materno',class:"hidden-xs",widthpdf:'20%',visible:true},
    {columna:'sCorreo', titulo: 'Correo',class:false,widthpdf:'20%',visible:true},	
    {columna:'dRegistro', titulo: 'Registro',class:"hidden-xs",widthpdf:'10%',visible:true},
    {columna:'iEstatus', titulo: 'Estatus',class:"hidden-xs",widthpdf:'20%',visible:false,label:"avanzado"}				
   ]   
};

$("#div-tabla").crear_tabla('<?=$_formulario;?>','iId','<?=$_tabla;?>',genera_tabla,"iEstatus=0",false);

$('#ventana-emergente').on('hidden.bs.modal', function (e) {
	$("#div-tabla").crear_tabla('<?=$_formulario;?>','iId','<?=$_tabla;?>',genera_tabla,"iEstatus=0",false,true);	
});

$("body").on("click","[name='iId']",function(){
	var iUsuarioId = $(this).val();
	$.post("./administracion/usuarios/licencias_registradas.php",{iUsuarioId:iUsuarioId},function(data){
		$("#licencias_registradas").html(data);	
	});
});

$("body").on("click","#asignar_licencia",function(){
	var elemento = $(this);
	$(elemento).cargando("Asignando Licencia");
	var iLicenciaId=$("[name='iLicenciaId']").val();
	var iUsuarioId=$("[name='iId']").val();
	$.post("./administracion/usuarios/asigna_licencia.php",{iUsuarioId:iUsuarioId,iLicenciaId:iLicenciaId},function(data){
		data = eval(data);
		if(data[1]>0) {
			$(elemento).correcto(data[0]);	
			$.post("./administracion/usuarios/licencias_registradas.php",{iUsuarioId:iUsuarioId},function(data){
				$("#licencias_registradas").html(data);	
			});			
		}
		else
		$(elemento).error(data[0]);	
	});
});
$("body").on("click",".accion_pagar",function(){
	$("#detalle_pago").show();
	var iUsuarioLicenciaId = $(this).attr("href");
	$("#guardar_pago").attr("iUsuarioLicenciaId",iUsuarioLicenciaId);	
	return false;
});

$("body").on("click","#guardar_pago",function(){
	var sNoTransaccion = $("[name='sNoTransaccion']").val();
	var dtFechaPago = $("[name='dtFechaPago']").val();
	var fMonto = $("[name='fMonto']").val();
	var sComentario = $("[name='sComentario']").val();	
	var iUsuarioLicenciaId = $(this).attr("iUsuarioLicenciaId");		
	if(iUsuarioLicenciaId!="0") {
		$.post("./administracion/usuarios/registra_pago.php",{sNoTransaccion:sNoTransaccion,dtFechaPago:dtFechaPago,fMonto:fMonto,sComentario:sComentario,iUsuarioLicenciaId:iUsuarioLicenciaId},function(data){	
			var iUsuarioId=$("[name='iId']").val();
			$.post("./administracion/usuarios/licencias_registradas.php",{iUsuarioId:iUsuarioId},function(data){
				$("#licencias_registradas").html(data);	
			});		
			$("#detalle_pago").hide();
		});		
	}else console.log("no hay licencia seleccionada");
	return false;
});

$("body").on("click",".accion_cancelar",function(){
	var iUsuarioLicenciaId = $(this).attr("href");
	var elemento = $(this);
	$(elemento).confirmar("Seguro que desea cancelar licencia","Si","No",function(respuesta){
	if(respuesta) {
		if(iUsuarioLicenciaId!="0") {
			$.post("./administracion/usuarios/cancela_licencia.php",{iUsuarioLicenciaId:iUsuarioLicenciaId},function(data){	
				var iUsuarioId=$("[name='iId']").val();
				$.post("./administracion/usuarios/licencias_registradas.php",{iUsuarioId:iUsuarioId},function(data){
					$("#licencias_registradas").html(data);	
				});		
			});		
		}else console.log("no hay licencia seleccionada");
	}
	});
	return false;

});

$("body").on("click","[name='iId']",function(){
	var iUsuarioId = $(this).val();
	
	$("#ventana-formulario li.hide").addClass("show");
	$("#ventana-formulario [name='iId']").val(iUsuarioId);

});

});

</script>
