<? 
include("../../acceso/seguridad.php");
$iUsuarioId_ = $_REQUEST['iUsuarioId'];
?>
<div class="panel panel-default" id='detalle_pago' style='display:none;'>
  <div class="panel-heading">Detalle del pago</div>
  <div class="panel-body">
		<label>No Transaccion</label><span><input type="text" class="form-control" name="sNoTransaccion" no_catalogo="no"></span>
     	<label>Fecha y Hora Pago (dd/mm/aaaa hh:mm:ss)</label> <input type="datetime" class="form-control" name="dtFechaPago" no_catalogo="no">            
     	<label>Monto	</label><input type="text" class="form-control" name="fMonto" no_catalogo="no">            
     	<label>Comentario </label><input type="text" class="form-control" name="sComentario" no_catalogo="no">                    
         <button type="button" iUsuarioLicenciaId='0' class="btn btn-success" id="guardar_pago" data-toggle="tooltip" titulo='Almacena Pago' style='margin-top:2px;'>Registrar pago</button>
  </div>
</div>
 	<table class="table table-bordered table-striped" id="tabla_licencias">
              <thead>
                <th class="hidden-xs">
                	Licencia
                </th>
                <th>
                	Precio
                </th>
                <th>
                	Vigencia
                </th>
                <th class="hidden-xs">
                	Asignación
                </th>
                <th class="hidden-xs">
                	Pagada
                </th>
                <th class="hidden-xs">
                	Vigente
                </th>
				<th>
                	Fecha Pago
                </th>
                <th>
                	Opciones
                </th></thead>
		       <tbody>  
               <?
$datos = seleccionar("licenciausuario
					 INNER JOIN licencia ON licenciausuario.iLicenciaId = licencia.iId","
										licenciausuario.iId,
										licenciausuario.iLicenciaId,
										licenciausuario.dFechaAsignacion,
										licenciausuario.iEstatus,
										licenciausuario.iPagado,
										licenciausuario.iVigente,
										licenciausuario.dFechaActivacion,
										licencia.sNombreLicencia,
										licencia.iVigencia,
										licencia.fPrecio","licencia.iEstatus=0 and licenciausuario.iUsuarioId='".$iUsuarioId_."'",false, false);
										
			   foreach($datos as $renglones) {
				   echo '<tr>';
					echo '<td class="hidden-xs">'.$renglones['sNombreLicencia'].'</td>';
					echo '<td>'.$renglones['fPrecio'].'</td>';
					echo '<td>'.$renglones['iVigencia'].'</td>';
					echo '<td class="hidden-xs">'.$renglones['dFechaAsignacion'].'</td>';
					echo '<td class="hidden-xs">'.label($renglones['iPagado'],'sino').'</td>';
					echo '<td class="hidden-xs">'.label($renglones['iVigente'],'sino').'</td>';
					echo '<td>'.$renglones['dFechaActivacion'].'</td>';
					echo '<td>';
					?>
					  <div class="btn-group" style=''> 
                  <button type="button" class="btn btn-info dropdown-toggle btn-sm" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only"></span>
                  </button>
                  <ul class="dropdown-menu" role="menu" id='accion_seleccionada'>                   
                    <li><a href="<?=$renglones['iId'];?>" class='accion_pagar'>Pagar</a></li>
                    <li><a href="<?=$renglones['iId'];?>" class='accion_correo'>Enviar Correo</a></li>
					 <li class="divider"></li>
                    <li><a href="<?=$renglones['iId'];?>" class='accion_cancelar'>Cancelar</a></li>
                  </ul>
          </div>
					<?
					echo '</td>';										
															
				   echo '</tr>';
			   }
			   ?>                             
               </tbody>
               </table>
                             
