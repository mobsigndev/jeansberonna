<? include("../acceso/seguridad.php"); 
	
?>
<div id='ventana-formulario'>
<!-- Tab panes -->
<div class="tab-content">
  <input type="text" class="form-control" name="iUsuarioEmpresaId" value="<?=$iUsuarioEmpresaId;?>" style='display:none;'>
  <input type="text" class="form-control" value="<?=$iEmpresaId;?>" style='display:none;' id="iEmpresaId">
	<div class="row">	 
      <div class="col-lg-12">
     		<label>Contraseña actual</label><span><input obligatorio='si' type="password" value='' class="form-control" no_catalogo='no' name="sActual"></span>
     		<hr>
     		<label>Nueva contraseña</label> <span><input obligatorio='si' type="password" value='' class="form-control" no_catalogo='no' name="sNueva"></span>                
            <label>Repita nueva contraseña</label> <span><input obligatorio='si' type="password" value='' class="form-control" no_catalogo='no' name="sNuevaRepetida"></span>                


            <button type="button" id="cambiar_clave" class="btn btn-success" style='margin-top:5px;'>Cambiar Contraseña</button>
            <br />

      </div>
	 
  		
  </div> 
  
  
  
  

</div>
</div>
<script>
$(document).ready(function () {
	$("#cambiar_clave").click(function(data){
		var actual=$("[name='sActual']").val();
		var nueva=$("[name='sNueva']").val();
		var nuevarepetida=$("[name='sNuevaRepetida']").val();
		var elemento=$(this);
		$(elemento).cargando();
		$.post("./clases/cambio_clave_bd.php",{actual:actual,nueva:nueva,nuevarepetida:nuevarepetida},function(data){
			data = eval(data);
			if(data[1]>0){
				$(elemento).correcto(data[0]);
			}
			else {
				$(elemento).error(data[0]);
			}
		});
	});
	
   
});
</script>