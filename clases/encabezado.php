<?php
include("../acceso/seguridad.php");
  $titulo = $_REQUEST['titulo'];
  $datos = seleccionar("empresa","sEmpresa,sCalle,sColonia,sCiudad,sEstado,sPais,sNumeroExterior,sNumeroInterior,sCodigoPostal,sRazonSocial,sNumeroTelefono,sLogotipo,sCorreo","iId='$iEmpresaId'",false, false);        
?>
<style> 
	@page { margin: 0px 20px 0px 20px; color:#666;}
    #header {  text-align: center; border-bottom:2px solid #ccc;}
    #footer { position: fixed; left: 0px; bottom: 0px; right: 0px; height: 80px; width:785px; background:#ccc; font-size:10px;color:#666;}
	@media print {
  html, body {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
	color:#666;
  }
  #footer {
    position: absolute;
    bottom: 0;
	color:#666;
  }
}
</style>
 <!--ENCABEZADO -->
    <div style='width:780px; border-top:1px solid #fff;'>
    	
        <div style='width:200px; background-color:#fff; display:inline-block; border-right:2px solid #CCC;'>
           
            <div style='width:160px; padding-top:20px; margin-left:10px;color:#0082B4; display:inline-block; vertical-align:bottom;'>
                <img src="./logotipos/<?php echo $datos[0]["sLogotipo"]; ?>" width="160" alt="Logotipo" /> 
            </div>
        </div>
         
        <div style="width:380px; height:106px; display:inline-block; text-align:left; vertical-align:top;">
            <div style='margin-left:20px; margin-top:15px; font-size:24px; color:#444; vertical-align:top;'>
            <?=$titulo; ?>
			</div>
            <div style='margin-left:20px; margin-top:8px; font-size:13px; color:#444;vertical-align:top;'>
            <?php echo $sEmpresa; ?>
			</div>
        </div>
        
		<div style="width:180px; height:110px; display:inline-block; vertical-align:top">
         	<div style='margin-top:10px;margin-left:10px;padding-bottom:1px;color:#444;font-size:19px;text-align: left;'>
            <strong>Fecha:</strong><? echo date("d-m-Y"); ?>
            </div>       
		</div>
        
    </div>
    <!--ENCABEZADO#-->
    
    <!--PIE DE REPORTE -->
  <div id="footer">
    	<p class="page">
        <center>
        <div style='margin-top:7px;'><?php echo $sEmpresa; ?></div>
    	<div><?php echo utf8_encode($datos[0]["sCalle"])." ".$datos[0]["sNumeroExterior"]." Col.".$datos[0]["sColonia"]." ".$datos[0]["sCiudad"].", ".$datos[0]["sEstado"].", ".$datos[0]["sPais"]; ?></div>
        <div>Tel&eacute;fono <? echo $datos[0]["sNumeroTelefono"]; ?></div>
    	<div><? echo $datos[0]["sCorreo"]; ?></div>
        </center>
    	</p>
         
    </div>	
	<!--PIE DE REPORTE#-->