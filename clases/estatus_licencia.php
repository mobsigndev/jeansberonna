<?php 
include("../acceso/seguridad.php");
$sel = seleccionar("licenciausuario INNER JOIN usuarioempresa ON usuarioempresa.iUsuarioId = licenciausuario.iUsuarioId INNER JOIN empleado ON usuarioempresa.iId = empleado.iUsuarioEmpresaId INNER JOIN licencia ON licencia.iId = licenciausuario.iLicenciaId INNER JOIN sistema ON licencia.iSistemaId = sistema.iId","licenciausuario.iId,licenciausuario.dFechaActivacion,licencia.sNombreLicencia,sistema.sNombreSistema,licencia.iVigencia,DATEDIFF(CURRENT_DATE(),licenciausuario.dFechaActivacion) as DiasTranscurridos","empleado.iId=$iEmpleadoId and licenciausuario.iVigente=1 and iPagado=1",false,false);
?>
    <li>
        <a class="text-center" >
            <strong>Dias restantes de licencias</strong>
            <i class="fa fa-angle-right"></i>
        </a>
    </li><li class="divider"></li>
<?
$vigencia =false;
foreach($sel as $renglones) {
    $vigencia = true;
	$faltantes = $renglones['iVigencia']-$renglones['DiasTranscurridos'];
	$porcentaje = $faltantes*100/$renglones['iVigencia'];
	if($porcentaje>70 && $porcentaje <=100) { $tipo='success'; }
	if($porcentaje>30 && $porcentaje <=69) { $tipo='warning'; }	
	if($porcentaje>=0 && $porcentaje <=29) { $tipo='danger'; }	
    if($faltantes<0){
        $mensaje="<label class='label label-danger'>Vencida</label>";
    }
    else { 
        $mensaje = $faltantes." Dias";	
    }
	?>
	<li>
        <a href="#">
            <div>
                <p>
                    <strong><?=$renglones['sNombreLicencia'];?></strong>
                    <span class="pull-right text-muted"><?=$mensaje;?></span>
                </p>
                <div class="progress progress-striped active" style='margin-top:5px;'>
                    <div class="progress-bar progress-bar-<?=$tipo;?>" role="progressbar" aria-valuenow="<?=$porcentaje?>" aria-valuemin="0" aria-valuemax="100" style="width:<?=$porcentaje?>%">
                        <span class="sr-only"><?=$renglones['DiasTranscurridos'];?>Dias</span>
                    </div>
                </div>
            </div>
        </a>
    </li>
    <li class="divider"></li>
    <?
}
if(!$vigencia){
    ?>
        <li>
        <a href="#">
            <div>
                <p>
                    <strong>Licencias</strong>
                    <span class="pull-right text-muted">Sin Licencias Activas/Vigentes</span>
                </p>               
            </div>
        </a>
    </li>
    <?
}
	?>
    <li>
        <a class="text-center" href="#">
            <strong class="ver_licencias_usuario">Ver mis licencias</strong>
            <i class="fa fa-angle-right"></i>
        </a>
    </li>