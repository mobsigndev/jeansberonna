	<?php 
include("../acceso/seguridad.php");
if(($_REQUEST['badge'])=="si") $badge=true;
else $badge=false;
$html="";
$sel = seleccionar("licenciausuario INNER JOIN usuarioempresa ON usuarioempresa.iUsuarioId = licenciausuario.iUsuarioId INNER JOIN empleado ON usuarioempresa.iId = empleado.iUsuarioEmpresaId INNER JOIN licencia ON licencia.iId = licenciausuario.iLicenciaId INNER JOIN sistema ON licencia.iSistemaId = sistema.iId","licenciausuario.iId,licenciausuario.dFechaActivacion,licencia.sNombreLicencia,sistema.sNombreSistema,licencia.iVigencia,DATEDIFF(CURRENT_DATE(),licenciausuario.dFechaActivacion) as DiasTranscurridos","empleado.iId=$iEmpleadoId and licenciausuario.iVigente=1 and iPagado=1",false,false);
$tiene=false;
$cantidad=0;
$vigencia=false;
foreach($sel as $renglones) {
	$vigencia=true;
	$faltantes = $renglones['iVigencia']-$renglones['DiasTranscurridos'];
	if($faltantes<15) {	
		if($faltantes<0) {
			actualiza("licenciausuario","iVigente=0","iId='".$renglones['iId']."'");
		 	$mensaje = "Su Licencia ".$renglones['sNombreLicencia']." activada el dia ".$renglones['dFechaActivacion']." Se encuentra: <label class='label label-danger'>Vencida</label>"; 

		}
		else { $mensaje = 'Su Licencia "'.$renglones['sNombreLicencia'].'" activada el dia '.$renglones['dFechaActivacion'].' esta apunto de expirar, le restan '.$faltantes.' Dias'; }
	$cantidad++;
	$tiene= true;
$html .= '<li>
        <a href="#">
            <div>
                <strong>Administrador</strong>
                <span class="pull-right text-muted">
                    <em>Urgente</em>
                </span>
            </div>';
            

            $html .='<div>'.$mensaje.'</div>';
       

        $html.='</a>
    </li>
    <li class="divider"></li>';
	}
}

if(!$vigencia){
	$cantidad++;
	$html .='<li style="padding:4px;">
            <div class="alert alert-danger" style="margin:0px;">
                <div><strong>Licencias</strong>
                <span class="pull-right">
                    <em>AVISO</em>
                </span></div>
            <small>No cuenta con licencias activas/vigentes</small></div>
    </li>';
    $tiene=true;
}
//Mensajes desde Notificaciones
$sel = seleccionar("notificacion INNER JOIN tiponotificacion ON notificacion.iTipoNotificacionId = tiponotificacion.iId INNER JOIN notificacionusuario ON notificacion.iId =notificacionusuario.iNotificacionId","tiponotificacion.iId as iTipoNotificacion, tiponotificacion.sTipo,notificacionusuario.iLeido,notificacionusuario.iUsuarioEmpresaId,notificacion.sNotificacion,notificacion.dRegistro","notificacion.iEstatus='0' and notificacionusuario.iUsuarioEmpresaId='$iUsuarioEmpresaId' and notificacionusuario.iLeido=0",false,false);
foreach($sel as $renglones) {
	
	if($renglones['iLeido']=="0") {	
	$cantidad++;
	$tiene= true;
	if($renglones['iTipoNotificacion']=="1"){
		$tipo__="danger";
	}
	else if($renglones['iTipoNotificacion']=="2"){
		$tipo__="warning";
	}
	else if($renglones['iTipoNotificacion']=="3"){
		$tipo__="info";
	}
	else if($renglones['iTipoNotificacion']=="4"){
		$tipo__="success";

	}
	else {
		$tipo__="success";
	}
	$html .= '<li style="padding:4px;">
            <div class="alert alert-'.$tipo__.'" style="margin:0px;">
                <div><strong>'.$renglones['dRegistro'].'</strong>
                <span class="pull-right">
                    <em>'.$renglones['sTipo'].'</em>
                </span></div>
            <small>'.plainUrlToLink($renglones['sNotificacion']).'</small></div>
    </li>
    ';
	}
}



if(!$tiene) {
	$html .='<li style="padding:4px;"><div class="alert alert-success" role="alert" style="margin:0px;">No tiene ningún mensaje.</div></li>'; 
	}
	
	if($badge) {
      echo '<span class="badge">'.$cantidad.'</span>';
	}
	else {
		echo $html;
	}
	

?>


    
