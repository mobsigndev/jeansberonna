<? include("../acceso/seguridad.php"); 
	$iTransaccionId=$_REQUEST['iTransaccionId'];
	$datos =seleccionar("transaccion INNER JOIN cliente ON transaccion.iClienteId = cliente.iId","transaccion.iTipoTransaccion,cliente.sCorreo,cliente.sCorreoContacto,transaccion.iFolio,transaccion.sSerie","transaccion.iId='$iTransaccionId'",false, false, false);
	
	$correos = "";
	if($datos[0]['sCorreo']!="") {
		$correos.=$datos[0]['sCorreo'];
	}
	if($datos[0]['sCorreoContacto']!="" && $datos[0]['sCorreoContacto']!=$datos[0]['sCorreo']) {
		$correos.=",".$datos[0]['sCorreoContacto'];
	}
	
	$tipo		=	$datos[0]['iTipoTransaccion'];
	$folio		=	$datos[0]['iFolio'];	
	if($tipo=="1") { $tipo_ = "Venta"; $archivo="venta"; $serie=""; }
	if($tipo=="2") { $tipo_ = "Factura"; $archivo="factura"; $serie=$datos[0]['sSerie'];
	 $archivopdf=$iTransaccionId.".pdf";
	 $archivoxml="xml_".$iTransaccionId.".xml";
	}
	if($tipo=="3") { $tipo_ = "Cotización"; $archivo="cotizacion"; $serie=""; }	
	if($tipo=="4") { $tipo_ = "Tienda En Linea"; $archivo="tiendavirtual"; $serie=""; }	
	
	$nombre_de_archivo = $archivo."_".$serie.$folio;	

	$consulta= seleccionar("transaccion INNER JOIN usuarioempresa ON transaccion.iUsuarioEmpresaId = usuarioempresa.iId
INNER JOIN empresa ON empresa.iId = usuarioempresa.iEmpresaId","usuarioempresa.iUsuarioId, usuarioempresa.iEmpresaId,transaccion.iClienteId,transaccion.iEmpleadoId,transaccion.dFecha,empresa.sEmpresa,empresa.sRazonSocial,empresa.sRFC,empresa.sNumeroTelefono,empresa.sCorreo","transaccion.iId='$iTransaccionId'",false,false,false);

?>
<div id='ventana-formulario'>
<!-- Nav tabs -->
<ul class="nav nav-pills">
  <li class='active'><a href="#envio" data-toggle="tab">Configuración de Envio</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="envio">
  <input type="text" class="form-control" name="iUsuarioEmpresaId" value="<?=$iUsuarioEmpresaId;?>" style='display:none;'>
  <input type="text" class="form-control" value="<?=$iEmpresaId;?>" style='display:none;' id="iEmpresaId">
	<div class="row">	 
      <div class="col-lg-12">
     		<label>Destinatario</label><span><input obligatorio='si' type="text" value='<?=$correos;?>' class="form-control" no_catalogo='no' name="sDestinatario"></span>
     		<label>Asunto</label> <span><input obligatorio='si' type="text" value='Adminbox - Envio de <?=$tipo_;?> Folio <?=$folio;?>'class="form-control" no_catalogo='no' name="sAsunto"></span>                
            
            <label>Comentario</label><textarea type="text" class="form-control" name="sComentario" style="height:120px;"><? if(existe("detallefactura","iTransaccionId='".$iTransaccionId."'",$return = 'iId')){?>Se anexa el siguiente comprobante fiscal digital en su formato PDF: <?=$archivopdf;?> y en su formato XML: <?=$archivoxml;?></textarea>
            <? }else {?>Se adjunta archivo correspondiente a <?=$tipo_;?> con folio <?=$folio;?></textarea><? }?>


            <button type="button" id="enviar_archivo" class="btn btn-success" sDocumento='<?=$nombre_de_archivo;?>' sPdf='<?=$archivopdf?>' iTransaccionId='<?=$iTransaccionId;?>' style='margin-top:5px;'>Enviar Archivo</button>
            <br />

             		
                    <?
                     if (file_exists("../facturacionE/temp/".$iEmpresaId."/xml_".$iTransaccionId.".xml")) {
					  $timbrada=true; 
					 }else if(existe("detallefactura","iTransaccionId='".$iTransaccionId."'",$return = 'iId')){
					  $timbrada=true;
					 }
					 else {
					  $timbrada = false;
					 }
                    if($timbrada){
					?>
                    </br>
						<label class='label label-warning'><?=$archivopdf;?></label>
                        <label class='label label-warning'><?=$archivoxml;?></label>
                        <input type="hidden" value="<?=$archivoxml?>" id="xml_" />
                        <input type="hidden" value="<?=$archivopdf?>" id="pdf_" />
					<? } else {?>
                    <label class='label label-default'>Adjuntos</label> <label class='label label-warning'><?=$nombre_de_archivo;?>.pdf</label>
                    <? }?>
      </div>
	 
    </div>
      
  		
  </div> 
  
  
  
  

</div>
</div>