<?
function saldo($iClienteId){
	global $iUsuarioEmpresaId;

	$saldo = seleccionar("cliente","fLimite,sNombre,iCantidadPrendas","iId='".$iClienteId."'",false, false);
	$limite = $saldo[0]['fLimite'];
	$sNombre = $saldo[0]['sNombre'];
	$iCantidadPrendas = $saldo[0]['iCantidadPrendas'];
	$saldo=0;
	$total_ventas=0;
	$datos_transaccion = seleccionar("transaccion","transaccion.iEstatus as estado,transaccion.iId","transaccion.iUsuarioEmpresaId='".$iUsuarioEmpresaId."' and transaccion.iClienteId='".$iClienteId."' and transaccion.iEstatus=0",false, false);
	$procede=false;
	    foreach($datos_transaccion as $dato) {
	    	$procede=true;
	    $estado =$dato['iEstatus']; 
	    $iTransaccionId =$dato['iId'];  
		$totales_ = totalesTransaccion($dato['iId']);  
	    #Sumatoria para totales
	    $t_subtotal_+=$totales_["subtotal"];
	    $t_trasladados_+=($totales_["iva"]+$t_trasladados);
	    $t_total_+= $totales_["total"];
	    $t_descuento_ +=$totales_["descuento"];

	    $t_retenidos_ +=$t_retenidos_;

	    $saldo += limpia_formato(number_format($totales_["total"],2));
	    $total_ventas+=$saldo;
	//Abonos

		$datos_transaccion2 = seleccionar("pagotransaccion","dFecha,iEstatus,fPago","iTransaccionId='".$iTransaccionId."' and iEstatus<>1",false, false);
	    foreach($datos_transaccion2 as $dato2) {  
	      if($dato2["fPago"]>0) {
	        $saldo -= limpia_formato(number_format($dato2["fPago"],2));
	      }

	    }
	           
	  }

  	return array($saldo,$total_ventas,$limite,$sNombre,$procede,$iCantidadPrendas);                                   
}




function existe($tabla,$condicion,$return = 'iId') {
	$sel_ = mysql_query("select $return from $tabla where $condicion");
	if(mysql_num_rows($sel_)>0) {
		$sel = mysql_fetch_array($sel_);
		$ex_ = strpos($return, ".");
		if ($ex_!== false) {
			$return = explode(".",$return);
			$return = $return[1];
		}				
		return $sel[$return];
	}
	else return false;
}
function insertar($tabla,$campos,$valores,$id_registrado = false){
	$insert_ = "insert into $tabla ($campos) values ($valores)";
    //echo $insert_;
	$insert = mysql_query($insert_);
	if($insert) {
		if($id_registrado)
		return	mysql_insert_id();
		else 
		return true;
	} else return false;	
}
function eliminar($tabla,$id){
	$eliminar_ = "update $tabla set iEstatus=1 where iId='$id'";
	$eliminar = mysql_query($eliminar_);
	if($eliminar) {
		return true;
	} else return false;	
}
function eliminar_fisico($tabla,$condicion){
	$eliminar_ = "delete from $tabla where $condicion";
	$eliminar = mysql_query($eliminar_);
	if($eliminar) {
		return true;
	} else return false;	
}

function actualiza($tabla,$valores,$condicion = false){
	if($condicion && $condicion != "false") $w = " where $condicion";	
	$actualiza_ = "update $tabla set $valores $w";
	#echo $actualiza_;
	$actualiza = mysql_query($actualiza_);
	if($actualiza) {
		return true;
	} else return false;	
}
function query($query){
	
	$datos = mysql_query($query);
	return mysql_fetch_array($datos);
	
}
function select_query($query){
	$sel_ = mysql_query($query);
	#echo "select $campos from $tabla $w $g $o";
	if(mysql_num_rows($sel_)>0) {
		return ($sel_);
	}else
	return false;
}
function seleccionar($tabla,$campos = 'iId',$condicion = false,$order = false, $json = false,$group = false) {
	$o = ""; $w = ""; $g = "";
	if($order && $order != "false") $o = " order by $order";
	if($group && $group!= "false") $g = " group by $group";	
	if($condicion && $condicion != "false") $w = " where $condicion";
	$campos_ = explode(",",$campos);

	$sel_ = mysql_query("select $campos from $tabla $w $g $o ");
	#echo "select $campos from $tabla $w $g $o";
	if(mysql_num_rows($sel_)>0) {
		$datos = array();
		$dato = array();
		$c=0;
	while($sel = mysql_fetch_array($sel_)){
			foreach($campos_ as $campo_){
				#Quitar si fuera un distinct campo, se agarrara el campo
				
				$aliasd_ = strpos($campo_, "distinct ");
				if ($aliasd_!== false) {

				$campo_ = explode("distinct ",$campo_);
				$campo_ = $campo_[1];				
				}
								
				$ex_ = strpos($campo_, ".");
				if ($ex_!== false) {
				$campo_ = explode(".",$campo_);
				$campo_ = $campo_[1];
				}	
				$alias_ = strpos($campo_, " as ");
				if ($alias_!== false) {
				$campo_ = explode(" as ",$campo_);
				$campo_ = $campo_[1];
				}
				if($campo_[0]=="d" && trim($campo_)!="distinct ")  {					
					if($sel[$campo_]!=""){					
						$fecha= date_create($sel[$campo_]);
						if($campo_[1]=="t") { 
							$dato[$campo_]= date_format($fecha,"d/m/Y H:i:s");
						}
						else{
							$dato[$campo_]= date_format($fecha,"d/m/Y");
						}
					}
					else
						$dato[$campo_]= "-";
				}
				else
				$dato[$campo_]=$sel[$campo_];
			}
			array_push($datos,$dato);
		}		
		if($json)
		return json_encode($datos);
		else
		return $datos;
	}
	else return false;
}
//FuncionesAdicionales
function label($celda,$tipo) {
	switch($tipo) {
		case "basico": { $l_ = basico($celda); break; }
		case "avanzado": {  $l_ = avanzado($celda); break; }
		case "inventarioestatus": {  $l_ = inventarioestatus($celda); break; }
		case "baseimpuesto": {  $l_ = baseimpuesto($celda); break; }
		case "tipoimpuesto": {  $l_ = tipoimpuesto($celda); break; }
		case "nivelimpuesto": {  $l_ = nivelimpuesto($celda); break; }		
		case "movimiento": {  $l_ = movimiento($celda); break; }
		case "porcentajeprenda": { $l_ =porcentajeprenda($celda); break; }
		case "numero": {  $l_ = number_format($celda,2); break; }		
		case "estado_factura": {  $l_ = estado_factura($celda); break; }
		case "sino": {  $l_ = sino($celda); break; }		
		case "beronna": {  $l_ = beronna($celda); break; }	
		case "cobranza": {  $l_ = cobranza($celda); break; }	
		default: { $l_ = "Desconocido"; break; }
	}
	return $l_;		
}

function cobranza($valor){	
	if($valor>=0&&$valor<=29) { $l_ = '<h4 style="margin:0;"><span class="label label-warning">15 Dias</span></h4>'; }
	else if($valor>=30&&$valor<45) { $l_ = '<h4 style="margin:0;"><span class="label label-warning">30 Dias</span></h4>'; }
	else if($valor>=45&&$valor<60) { $l_ = '<h4 style="margin:0;"><span class="label label-warning">45 Dias</span></h4>'; }
	else if($valor>=60&&$valor<90) { $l_ = '<h4 style="margin:0;"><span class="label label-warning">60 Dias</span></h4>'; }
	else if($valor>=90&&$valor<=120) { $l_ = '<h4 style="margin:0;"><span class="label label-warning">90 Dias</span></h4>'; }
	else if($valor>=120) { $l_ = '<h4 style="margin:0;"><span class="label label-warning">120 Dias</span></h4>'; }
	else {  $l_ = '<h4 style="margin:0;"><span class="label label-warning">+120 Dias</span></h4>'; }				
	return $l_;	
}
function beronna($valor){	
	if($valor<0) { $l_ = '<h4 style="margin:0;"><span class="label label-warning">Cerrado</span></h4>'; }
	else if($valor<=15) { $l_ = '<h4 style="margin:0;"><span class="label label-success">En Tiempo</span></h4>'; }
	else if($valor>15) { $l_ = '<h4 style="margin:0;"><span class="label label-danger">Corte</span></h4>'; }
	return $l_;	
}
function basico($estatus){
	switch($estatus) {
		case "0": { $l_ = '<span class="label label-success">Activo</span>'; break; }
		case "1": { $l_ = '<span class="label label-danger">Eliminado</span>';	break; }
		default: { $l_ = '<span class="label label-default">N/A</span>'; break; }
	}
	return $l_;		
}
function estado_factura($estatus){
	switch($estatus) {
		case "2": { $l_ = '<span class="label label-success">Pagada</span>'; break; }
		case "1": { $l_ = '<span class="label label-danger">Cancelada</span>';	break; }
		default: { $l_ = '<span class="label label-default">Pendiente de Pago</span>'; break; }
	}
	return $l_;		
}
function sino($estatus){
	switch($estatus) {
		case "0": { $l_ = '<span class="label label-danger">No</span>'; break; }
		case "1": { $l_ = '<span class="label label-success">Si</span>';	break; }
		default: { $l_ = '<span class="label label-default">N/A</span>'; break; }
	}
 
	return $l_;		
}
function baseimpuesto($estatus){
	if($estatus=="0") return "Subtotal"; 
	else {
 		$select_ = seleccionar("impuesto","sNombre","iId='".$estatus."'",false, false); 
 		$l_ = $select_[0]['sNombre'];
		return $l_;		
	}
}
function tipoimpuesto($estatus){
	switch($estatus) {
		case "0": { $l_ = '<span class="label label-danger">Retenido</span>'; break; }
		case "1": { $l_ = '<span class="label label-success">Trasladado</span>';	break; }
		default: { $l_ = '<span class="label label-default">N/A</span>'; break; }
	}
	return $l_;		
}
function nivelimpuesto($estatus){
	switch($estatus) {
		case "1": { $l_ = '<span class="label label-danger">Estatal</span>'; break; }
		case "0": { $l_ = '<span class="label label-success">Federal</span>';	break; }
		default: { $l_ = '<span class="label label-default">N/A</span>'; break; }
	}
	return $l_;		
}
function avanzado($estatus){
	switch($estatus) {
		case "0": { $l_ = '<span class="label label-success">Vigente</span>'; break; }
		case "1": { $l_ = '<span class="label label-danger">Cancelado</span>';	break; }
		default: { $l_ = '<span class="label label-default">N/A</span>'; break; }
	}
	return $l_;		
}
function inventarioestatus($estatus){
	switch($estatus) {
		case "0": { $l_ = '<span class="label label-warning">Pendiente</span>'; break; }
		case "1": { $l_ = '<span class="label label-danger">Cancelado</span>';	break; }
		case "2": { $l_ = '<span class="label label-success">Aceptado</span>'; break; }
		default: { $l_ = '<span class="label label-default">N/A</span>'; break; }
	}
	return $l_;		
}



function movimiento($valor){	
	if($valor>0) { $l_ = '<span class="label label-warning">Entrada</span>'; }
	else { $l_ = '<span class="label label-success">Salida</span>';  }
	return $l_;	
}
function inventario($valor,$minimo){	
	if($valor<=$minimo) { $l_ = '<h3 style="margin:0;"><span class="label label-danger">'.$valor.'</span></h3>'; }
	else if(($valor>$minimo) && $valor<($minimo+10)) { $l_ = '<h3 style="margin:0;"><span class="label label-warning">'.$valor.'</span></h3>'; }
	else { $l_ = '<h3 style="margin:0;"><span class="label label-success">'.$valor.'</span></h3>';  }
	return $l_;	
}
function porcentajeprenda($valor){	
	if($valor>=60) { $l_ = '<h4 style="margin:0;"><span class="label label-success">'.number_format($valor,2).'%</span></h4>'; }
	else if(($valor>35) && $valor<60) { $l_ = '<h4 style="margin:0;"><span class="label label-warning">'.number_format($valor,2).'%</span></h4>'; }
	else { $l_ = '<h4 style="margin:0;"><span class="label label-danger">'.number_format($valor,2).'%</span></h4>';  }
	return $l_;	
}

function cargoabono($valor,$tipo){	
	if($tipo=="1") { $l_ = '<span class="label label-success">'.$valor.'</span>'; }
	if($tipo=="2") { $l_ = '<span class="label label-warning">'.$valor.'</span>'; }
	if($tipo=="3") { $l_ = '<span class="label label-danger">'.$valor.'</span>'; }	
	return $l_;	
}
function transaccion($valor){	
	if($valor=="1") { $l_ = '<h5 style="margin:0;"><span class="label label-success">Venta</span></h5>'; }
	if($valor=="2") { $l_ = '<h5 style="margin:0;"><span class="label label-info">Factura</span></h5>'; }
	if($valor=="3") { $l_ = '<h5 style="margin:0;"><span class="label label-warning">Cotización</span></h5>'; }	
	
	
	
	if($valor=="4") { $l_ = '<h5 style="margin:0;"><span class="label label-primary">Tienda En Linea</span></h5>'; }	
	return $l_;	
}
function existencia($iProductoId,$matriz=0) {
	global $iUsuarioEmpresaId;
	if($matriz=="1"){
		$us="0";
	}else {
		$us= $iUsuarioEmpresaId;
	}

	$sel = seleccionar("inventario","sum(fCantidad) as fCantidad","iProductoId='".$iProductoId."' and iUsuarioEmpresaId='$us'",false, false);
	if($sel[0]['fCantidad']!="")
	return $sel[0]['fCantidad'];
	else
	return 0;
}
function existenciaMatriz($iProductoId) {
	$sel = seleccionar("inventariomatriz","sum(fCantidad) as fCantidad","iProductoId='".$iProductoId."'",false, false);
	if($sel[0]['fCantidad']!="")
	return $sel[0]['fCantidad'];
	else
	return 0;
}
function abonos($iTransaccionId) {
	$pago_ = array();
	$sel = seleccionar("pagotransaccion","sum(fPago) as fPago","iTransaccionId='".$iTransaccionId."' and iEstatus<>1",false, false);
	if($sel[0]['fPago']!="") {
		$pago_['pago']=$sel[0]['fPago'];
		$pago_['pagof']=number_format($sel[0]['fPago'],2);
	} else {
		$pago_['pago']="0";
		$pago_['pagof']=number_format("0",2);
	}
	return $pago_;
}
function calcular($precio,$cantidad=1,$iva,$descuento=0){
	$cantidadDescuento=0.0;
	if($descuento>=0 && $descuento<=100){
		$cantidadDescuento=($precio*$cantidad*$descuento)/100;
	}
	//$subtotal=($precio*$cantidad)-$cantidadDescuento;
	$subtotal=($precio*$cantidad);
	if($iva>=0){
		$iva=$iva/100;
		$operaciones["iva"]=($subtotal-$cantidadDescuento)*$iva;
		$operaciones["ivaf"]=number_format(($subtotal-$cantidadDescuento)*$iva,2);
	}
	$operaciones["subtotal"]=$subtotal;
	$operaciones["descuento"]=$cantidadDescuento;
	$operaciones["total"]=($subtotal+$operaciones["iva"]-$cantidadDescuento);
	$operaciones["subtotalf"]=number_format(($subtotal),2);
	$operaciones["descuentof"]=number_format($cantidadDescuento,2);
	#$operaciones["totalf"]=($subtotal*$iva)+$subtotal;	
	$operaciones["totalf"]=number_format((($subtotal*$iva)+$subtotal-$cantidadDescuento),2);
	#$subtotal=number_format($subtotal,2);
	#$iva=number_format($subtotal,2);
	 
	return $operaciones;
}
function calculosVenta($pedido,$impuestos=false){
	$operaciones=array();
	$operaciones["subtotal"]=0.0;
	$operaciones["iva"]=0.0;
	$operaciones["descuento"]=0.0;
	$operaciones["total"]=0.0;
	$retenidos=0.0;
	$trasladados=0.0;
	foreach ($pedido as $key => $value) 
	{ 
		$valores=calcular($value["fPrecioVenta"],$value["cantidad"],$value["fIva"],$value["descuento"]);
		$operaciones["subtotal"]+=$valores["subtotal"];
		$operaciones["iva"]+=$valores["iva"];
		$operaciones["total"]+=$valores["total"];
		$operaciones["descuento"]+=$valores["descuento"];
		$total=number_format($operaciones["total"],2);
		#echo "Total: ".$total."<br>";
	}
	if($impuestos!=false){
		foreach ($impuestos as $key => $value){ 
			if($value["tipo"]==0){
				$retenidos+=$value["cantidad"];
				$t__retenidos += limpia_formato($value["cantidad"]);
			}
	
			if($value["tipo"]==1){
				$trasladados+=$value["cantidad"];
				$t__trasladados += limpia_formato($value["cantidad"]);
			}
		}	
	}
	
	
	$operaciones["subtotalf"]=number_format($operaciones["subtotal"],2);
	$operaciones["ivaf"]=number_format($operaciones["iva"],2);
	//$operaciones["totalf"]=number_format($operaciones["total"]-$retenidos+$trasladados,2);
	$operaciones["totalf"]=number_format(limpia_formato($operaciones["subtotal"])+limpia_formato($operaciones["iva"])-$t__retenidos+$t__trasladados-limpia_formato($operaciones["descuento"]),2);
	$operaciones["descuentof"]=number_format($operaciones["descuento"],2);	
	return $operaciones;
}
function limpia_formato($numero){
	$numero  = str_replace("$","",$numero);
	$numero = str_replace(",", "", $numero);
	$numero = number_format($numero,2);
	$numero = str_replace(",", "", $numero);
	return $numero;
}
function totalesTransaccion($iTransaccionId) {
	$datos_ = array();
	$transaccion= array();
	$datos = seleccionar("detalletransaccion","sConcepto,fCantidad,fPrecio,fIva,fDescuento","iTransaccionId='$iTransaccionId'",false, false);                            
   foreach($datos as $renglones) {
      	$datos_['fPrecioVenta']	= 	$renglones['fPrecio'];
      	$datos_['cantidad']		= 	$renglones['fCantidad'];       
        $datos_['fIva']			=	$renglones['fIva'];
        $datos_['descuento']	=	$renglones['fDescuento'];		
		array_push($transaccion,$datos_);
   }
   $impuestos = ImpuestosTransaccion($iTransaccionId);
   return calculosVenta($transaccion,$impuestos);
}
function ImpuestosTransaccion($iTransaccionId) {
	$datos_ = array();
	$transaccion= array();
	$datos = seleccionar("transaccionimpuesto","iTipo,fCantidad","iTransaccionId='$iTransaccionId'",false, false);
	if($datos){
		foreach($datos as $renglones) {
	      	$datos_['tipo']	= 	$renglones['iTipo'];
	      	$datos_['cantidad']		= 	$renglones['fCantidad'];       
			array_push($transaccion,$datos_);
	   }
	}
   return $transaccion;
}
function datosLinea($iTransaccionId){
    $datoss = seleccionar("transaccionlinea","sNombre,sTelefono,sCorreo,sDireccion,sNoCliente","iPedidoId='$iTransaccionId'",false, false);                            
     return $datoss[0];
}
function imagen($iProductoId){
	  $datoss = seleccionar("producto","sImagen1","iId='$iProductoId'",false, false);                            
  	 return $datoss[0]['sImagen1'];
}
function conceptosTransaccion($iTransaccionId,$conimagen = false,$concomision = false,$concategoria = false) {
	
   $registros="";
   $datos = seleccionar("detalletransaccion","sConcepto,fCantidad,fPrecio,fIva,fDescuento,sUnidad,iProductoId,iTipo","iTransaccionId='$iTransaccionId'",false, false);                            
   foreach($datos as $renglones) {
    $registros.="<tr>";
    
    if($conimagen==true || $conimagen=="true"){
    	$imagen_ = imagen($renglones['iProductoId']);
    	if($imagen_!=""){ 
    		if(file_exists("../../imagenes/".$imagen_)) {
    			$registros.="<td><img src='../../imagenes/$imagen_' width='50'></td>";    
    		}	else {
    			$registros.="<td>$imagen_</td>";    
    		}	
    	}
    	else $registros.="<td>N/A</td>";
	}
	if($concategoria==true || $concategoria=="true"){
		$nombreCategoria = nombre_categoria($renglones['iProductoId'],$renglones['iTipo']);
		$registros.="<td>".$nombreCategoria."</td>";
	}
	$registros.="<td>".$renglones['sConcepto']."</td>";
	$registros.="<td align='center'>".$renglones['sUnidad']."</td>";
	$registros.="<td align='right'>".number_format($renglones['fPrecio'],2)."</td>";
	#$registros.="<td align='right'>".number_format($renglones['fIva'],2)."</td>";
	#$registros.="<td align='right'>".number_format($renglones['fIva']*$renglones['fPrecio']/100,2)."</td>";
	$registros.="<td align='center'>".$renglones['fCantidad']."</td>";
	$valores=calcular($renglones['fPrecio'],$renglones['fCantidad'],$renglones['fIva']);
	$registros.="<td align='right'>".number_format($valores["subtotal"],2)."</td>";
	if($concomision==true || $concomision=="true"){
		 if($renglones['iTipo']=="2"){
                  $comision_ = seleccionar("servicio","fComision","iId='".$renglones['iProductoId']."'",false,false,false);
                  foreach($comision_ as $renglones3){
                    $comision__ = $renglones3["fComision"];
                 }
              }
              $comision = ($valores["subtotal"]*$comision__)/100;
		$registros.="<td align='right'>".number_format($comision,2)."</td>";
	}
    $registros.="</tr>";
   }
   return $registros;
}
function nombre_categoria($iProductoId,$iTipo){
	if($iTipo=="1"){
	  $datos = seleccionar("producto","iCategoriaId","iId='$iProductoId'",false, false);                            
   	  $iCategoriaId = $datos[0]['iCategoriaId'];

   	}
   	if($iTipo=="2"){
	  $datos = seleccionar("servicio","iCategoriaId","iId='$iProductoId'",false, false);                            
   	  $iCategoriaId = $datos[0]['iCategoriaId'];
   	}

   	  $datos = seleccionar("categoria","sNombre","iId='$iCategoriaId'",false, false);                            
   	  $nombreCategoria = $datos[0]['sNombre'];
   	  return $nombreCategoria;
}
function datos_transaccion($iTransaccionId) {
	/*Regresa:
	iId
	dFecha
	iTipo
	iFolio
	sComentarioTransaccion
	sComentarioDescuento
	iTipoTransaccion
	sNombre
	sEmpresa
	sLogotipo
	iEstatus
	sCalle
	sNumeroExterior
	sCiudad
	sEstado
	sPais
	sTelefonoContacto
	sCorreo
	sColonia 
	iTipoTransaccion 
	sEnriquecido
	dLimitePago
	fInteresPagare,
	sRazonSocial	,
	sSerie,
	iTipoPago,
	iEmpleadoId,
	dtFechaEntrega
	*/
	$info_transaccion=array();
   $datos = seleccionar("transaccion INNER JOIN cliente ON transaccion.iClienteId = cliente.iId INNER JOIN usuarioempresa ON transaccion.iUsuarioEmpresaId = usuarioempresa.iId INNER JOIN empresa ON usuarioempresa.iEmpresaId = empresa.iId","transaccion.iId,transaccion.dFecha,transaccion.iTipo,transaccion.iFolio,transaccion.sComentarioTransaccion,transaccion.sComentarioDescuento,transaccion.iTipoTransaccion,cliente.sNombre,empresa.sEmpresa,empresa.sLogotipo,transaccion.iEstatus,empresa.sCalle,empresa.sNumeroExterior,empresa.sNumeroInterior,empresa.sCiudad,empresa.sEstado,empresa.sPais,empresa.sTelefonoContacto,empresa.sCorreo,empresa.sColonia,transaccion.iTipoTransaccion,transaccion.sEnriquecido,transaccion.dLimitePago,transaccion.fInteresPagare,empresa.sRazonSocial,transaccion.sSerie,transaccion.iTipoPago,transaccion.iEmpleadoId,transaccion.dtFechaEntrega","transaccion.iId='$iTransaccionId'",false, false);                            
	if($datos[0]['iId']!="") 
		return $datos[0];
	else
	false;
}


function conceptosaltainventario($iTransaccionId) {
	
   $registros="";
   $datos = seleccionar("detallealtainventario","fCantidad,iProductoId","iAltaInventarioId='$iTransaccionId'",false, false);                            
   foreach($datos as $renglones) {
   	 $datoss = seleccionar("producto","sUnidad,sNombre,sDescripcion","iId='".$renglones['iProductoId']."'",false, false);                            
  	$unidad= $datoss[0]['sUnidad'];
  	$nombre = $datoss[0]['sNombre'];
  	$sDescripcion = $datoss[0]['sDescripcion'];
    $registros.="<tr>";
    $registros.="<td align='center'>".$renglones['fCantidad']."</td>";
    $registros.="<td align='center'>".$unidad."</td>";
	$registros.="<td>".$nombre." ".$sDescripcion."</td>";
	$total_ += $renglones['fCantidad'];

    $registros.="</tr>";
   }
   $registros .="<tfoot><tr><td  style='text-align:center; font-weight:bold; font-size:14px;'>$total_</td><td colspan='2'></td></tr></tfoot>";

   return $registros;
}
function datos_altainventario($iTransaccionId) {	
	$info_transaccion=array();
	$datos = seleccionar("altainventario INNER JOIN empleado ON altainventario.iEmpleadoId = empleado.iId INNER JOIN usuarioempresa ON altainventario.iUsuarioEmpresaId = usuarioempresa.iId INNER JOIN empresa ON usuarioempresa.iEmpresaId = empresa.iId","altainventario.iEstatus,altainventario.iId,altainventario.iEmpleadoId,altainventario.dtFechaAlta,altainventario.sReferenciaFactura,altainventario.sComentarios,altainventario.dFechaFactura,empleado.sNombre as Empleado,altainventario.iFolio,altainventario.iUsuarioEmpresaId,altainventario.iUsuarioEmpresaIdPara,empresa.sEmpresa,empresa.sLogotipo,empresa.sCalle,empresa.sNumeroExterior,empresa.sCiudad,empresa.sEstado,empresa.sPais,empresa.sTelefonoContacto,empresa.sCorreo,empresa.sColonia","altainventario.iId='$iTransaccionId'",false, false);                              
	if($datos[0]['iId']!="") 
		return $datos[0];
	else
	false;
}
function permiso($sistema,$iUsuarioEmpresaId) {
	Global $iEmpleadoId;
	$sel = seleccionar("usuarioempresa INNER JOIN licenciausuario ON usuarioempresa.iUsuarioId = licenciausuario.iUsuarioId INNER JOIN licencia ON licenciausuario.iLicenciaId = licencia.iId INNER JOIN sistema ON licencia.iSistemaId = sistema.iId","licenciausuario.iId,usuarioempresa.iEmpresaId","usuarioempresa.iId=$iUsuarioEmpresaId and sistema.sNombreSistema='$sistema' and licenciausuario.iVigente=1 and iPagado=1",false,false);
	if($sel[0]['iId']>0) { 
			$iLicenciaId=$sel[0]['iId'];

			if($iUsuarioEmpresaId==7) {
				$sel = seleccionar("licenciausuario INNER JOIN usuarioempresa ON usuarioempresa.iUsuarioId = licenciausuario.iUsuarioId INNER JOIN empleado ON usuarioempresa.iId = empleado.iUsuarioEmpresaId INNER JOIN licencia ON licencia.iId = licenciausuario.iLicenciaId INNER JOIN sistema ON licencia.iSistemaId = sistema.iId","licenciausuario.iId,licenciausuario.dFechaActivacion,licencia.sNombreLicencia,sistema.sNombreSistema,licencia.iVigencia,DATEDIFF(CURRENT_DATE(),licenciausuario.dFechaActivacion) as DiasTranscurridos","empleado.iId=$iEmpleadoId and licenciausuario.iVigente=1 and iPagado=1 and licenciausuario.iId='$iLicenciaId'",false,false);
				foreach($sel as $renglones) {
					$faltantes = $renglones['iVigencia']-$renglones['DiasTranscurridos'];
					if($faltantes<0) {

					return false;	
					}
				}	
			}
			$sel_ = seleccionar("empresa","sClavePrivada","iId='".$sel[0]['iEmpresaId']."'",false);
			if(strtolower($sistema)=="facturacion"){
				if($sel_[0]['sClavePrivada']!="") {return true;}
				else return false;
			}
			else {
				return true; 
			}
	}
	else return false; 
}
function permiso_modulo($modulo,$iEmpleadoId,$sistema=false) {	
  if(!$sistema)
  $condicion = "modulo.sNombreModulo='$modulo' and moduloempleado.iEmpleadoId='$iEmpleadoId' and moduloempleado.iEstatus=0 and modulo.iEstatus=0"; 
  else
  	$condicion = "moduloempleado.iEmpleadoId='$iEmpleadoId' and sistema.sNombreSistema='$sistema'"; 
	$sel = seleccionar("modulo INNER JOIN moduloempleado ON moduloempleado.iModuloId = modulo.iId INNER JOIN sistema ON modulo.iSistemaId = sistema.iId","moduloempleado.iEmpleadoId,moduloempleado.iModuloId,sistema.sNombreSistema,modulo.sNombreModulo","$condicion",false,false);
	if($sel[0]['iModuloId']>0) { 		
		return true; 		
	}
	else return false; 
}
function datos_nomina($iNominaId) {
	$info_transaccion=array();
  $datos = seleccionar("nomina
INNER JOIN empleado ON empleado.iId = nomina.iEmpleadoId
INNER JOIN usuarioempresa ON usuarioempresa.iId = empleado.iUsuarioEmpresaId
INNER JOIN empresa ON empresa.iId = usuarioempresa.iEmpresaId
INNER JOIN banco ON banco.iId = nomina.iBanco
INNER JOIN regimencontratacion ON regimencontratacion.iId = nomina.iTipoRegimen","nomina.iId,
nomina.iEmpleadoId,nomina.sRfc,nomina.sEstado,nomina.sRegistroPatronal,nomina.sNumeroEmpleado,
nomina.sCurp,nomina.iTipoRegimen,nomina.sSeguroSocial,nomina.sDepartamento,nomina.sClabe,nomina.iBanco,nomina.dFechaInicioLaboral,nomina.sPuesto,nomina.sTipoContrato,nomina.sTipoJornada,nomina.sPeriodicidadPago,nomina.fSalarioBase,
nomina.iRiesgoPuesto,nomina.fSalarioDiarioIntegrado,nomina.dFecha,nomina.iUsuarioEmpresaId,empleado.sNombre,
empleado.sPaterno,empleado.sMaterno,empresa.sEmpresa,empresa.sCalle,empresa.sNumeroExterior,empresa.sNumeroInterior,
empresa.sCodigoPostal,empresa.sRazonSocial,empresa.sRFC,empresa.sCiudad,empresa.sEstado,empresa.sPais,
empresa.sColonia,empresa.sMunicipio,banco.sNombre as banco,regimencontratacion.sDescripcion as regimen,empresa.sLogotipo,
empresa.sCorreo,empresa.sTelefonoContacto",$condicion,false, false);                    
	if($datos[0]['iId']!="") 
	return $datos[0];
	else
	false;
}
function conceptonomina($iTransaccionId) {
   $registros="";
   $datos = seleccionar("detalletransaccion","sConcepto,fCantidad,fPrecio,fIva,fDescuento,sUnidad","iTransaccionId='$iTransaccionId'",false, false);                            
   foreach($datos as $renglones) {
    $registros.="<tr>";
	$registros.="<td>".$renglones['sConcepto']."</td>";
	$registros.="<td align='center'>".$renglones['sUnidad']."</td>";
	$registros.="<td align='right'>".number_format($renglones['fPrecio'],2)."</td>";
	$registros.="<td align='right'>".number_format($renglones['fIva'],2)."</td>";
	$registros.="<td align='center'>".$renglones['fCantidad']."</td>";
	$valores=calcular($renglones['fPrecio'],$renglones['fCantidad'],$renglones['fIva']);
	$registros.="<td align='right'>".number_format($valores["totalf"],2)."</td>";
    $registros.="</tr>";
   }
   return $registros;
}
function getSubString($string, $length=NULL)
{
    //Si no se especifica la longitud por defecto es 50
    if ($length == NULL)
        $length = 50;
    //Primero eliminamos las etiquetas html y luego cortamos el string
    $stringDisplay = substr(strip_tags($string), 0, $length);
    //Si el texto es mayor que la longitud se agrega puntos suspensivos
    if (strlen(strip_tags($string)) > $length)
        $stringDisplay .= ' ...';
    return $stringDisplay;
}

function empleado($iEmpleadoId){
    $datoss = seleccionar("empleado","sNombre,sPaterno,sMaterno","iId='$iEmpleadoId'",false, false);                            
     return $datoss[0];
}

function detalleabonos($iTransaccionId,$iId) {
	
   $registros="";
	   $datos = seleccionar("pagotransaccion","dFecha,sComentario,fPago,iEmpleadoId,dtFechaOperacion","iTransaccionId='$iTransaccionId' and (iEstatus=0 or iEstatus=2)  and iId<=$iId",false, false);                            
   foreach($datos as $renglones) {
    $registros.="<tr>";
    
  	$registros.="<td style='border:none;font-weight:bold;'>".$renglones['dFecha']."</td>";
	$registros.="<td align='center' style='border-bottom:1px solid #999;'>$".number_format($renglones['fPago'],2)."</td>";
	$registros.="</tr>";
   	
   }
   $datos2 = seleccionar("pagotransaccion","sum(fPago) as fPago","iTransaccionId='$iTransaccionId' and iId<$iId and iEstatus<>1",false, false);                               
   $saldo_anterior = $datos2[0]['fPago'];
   return (array($registros,number_format($saldo_anterior,2)));
}
function verabono($iId) {
	
   $registros="";
   $datos = seleccionar("pagotransaccion","dFecha,sComentario,fPago,iEmpleadoId,dtFechaOperacion,iTransaccionId","iId='$iId'",false, false);                            
   return $datos[0];
}
function plainUrlToLink($txt_content,$target="_blank") {       
    $regex = "/(http|https)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,6}(\/\S*)+?/";
    if(preg_match($regex, $txt_content,$matches)) {
       return preg_replace($regex, "<a href='".$matches[0]."' target='".$target."'>".$matches[0]."</a> ", $txt_content);
    } 
    else {
       return $txt_content;
    }
}

function neto_pagar_nomina_empleado($iEmpleadoId){

	$total_deducciones=0;
	$datos = seleccionar("empleadodeduccion","empleadodeduccion.iTipoDeduccionId,empleadodeduccion.sTipo,empleadodeduccion.iId,
							empleadodeduccion.fImporteGravadoDeduccion,
							empleadodeduccion.fImporteExentoDeduccion","empleadodeduccion.iEstatus=0 and empleadodeduccion.iEmpleadoId='".$iEmpleadoId."'",false, false);
	foreach($datos as $renglones) {
	   $total_deducciones+=$renglones['fImporteGravadoDeduccion']+$renglones['fImporteExentoDeduccion'];
	}

	$total_percepciones=0;
	$datos = seleccionar("empleadopercepcion","empleadopercepcion.iId,empleadopercepcion.iTipoPercepcionId,empleadopercepcion.sTipo,
							empleadopercepcion.fImporteGravadoPercepcion,
							empleadopercepcion.fImporteExentoPercepcion","empleadopercepcion.iEstatus=0 and empleadopercepcion.iEmpleadoId='".$iEmpleadoId."'",false, false);
	foreach($datos as $renglones) {
	   $total_percepciones+=$renglones['fImporteGravadoPercepcion']+$renglones['fImporteExentoPercepcion'];
	}

	$total = $total_percepciones-$total_deducciones;
	return number_format($total,2);
}

function neto_pagar_nomina($iNominaId){

  $total_deducciones=0;
  $datos = seleccionar("nominadeduccion","nominadeduccion.iTipoDeduccionId,nominadeduccion.sTipo,nominadeduccion.iId,
              nominadeduccion.fImporteGravado,
              nominadeduccion.fImporteExento","nominadeduccion.iNominaId='".$iNominaId."'",false, false);
  foreach($datos as $renglones) {
     $total_deducciones+=$renglones['fImporteGravado']+$renglones['fImporteExento'];
  }

  $total_percepciones=0;
  $datos = seleccionar("nominapercepcion","nominapercepcion.iId,nominapercepcion.iTipoPercepcionId,nominapercepcion.sTipo,
              nominapercepcion.fImporteGravado,
              nominapercepcion.fImporteExento","nominapercepcion.iNominaId='".$iNominaId."'",false, false);
  foreach($datos as $renglones) {
     $total_percepciones+=$renglones['fImporteGravado']+$renglones['fImporteExento'];
  }

  $total = $total_percepciones-$total_deducciones;
  return number_format($total,2);
}

//Tintoreria
function prendasdevueltas($iProductoId,$iTipo,$iDetalleTransaccionId) {
	$sel = seleccionar("detalleentregatransaccion","sum(fCantidad) as fCantidad","iDetalleTransaccionId='".$iDetalleTransaccionId."' and iProductoId='".$iProductoId."' and iTipo='".$iTipo."'",false, false);
	if($sel[0]['fCantidad']!="")
	return $sel[0]['fCantidad'];
	else
	return "0";
}
function prendasrealizadas($iProductoId,$iTipo,$iDetalleTransaccionId,$iEmpleadoId=false) {
	if($iEmpleadoId){
		$w = " and iEmpleadoId='$iEmpleadoId'";
	}
	$sel = seleccionar("tintoreria_detalleactividadestransaccion","sum(fCantidad) as fCantidad","iDetalleTransaccionId='".$iDetalleTransaccionId."' and iProductoId='".$iProductoId."' and iTipo='".$iTipo."' $w",false, false);
	if($sel[0]['fCantidad']!="")
	return $sel[0]['fCantidad'];
	else
	return "0";
}


function fechahora($fechahora){
	$v = explode(" ",$fechahora);	
	$v2 = explode("-",$v[0]);
	$v1 = explode(":",$v[1]);
	$d__ = date("Y-m-d H:i:s", mktime($v1[0],$v1[1],$v1[2],$v2[1],$v2[2],$v2[0]));
	return $d__;
}
function fechahora2($fechahora){
	$v = explode(" ",$fechahora);	
	$v2 = explode("/",$v[0]);
	$v1 = explode(":",$v[1]);
	//orden de mktime h i s M d Y
	$d__ = date("Y-m-d H:i:s", mktime($v1[0],$v1[1],$v1[2],$v2[1],$v2[0],$v2[2]));
	return $d__;
}

function minimoutilidad($compra,$vende,$porcentaje_utilidad){
	$porcentaje = (($vende*100)/$compra)-100;
	if($porcentaje>=$porcentaje_utilidad) return true;
	else return false;
}
?>