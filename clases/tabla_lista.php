<?
include("../acceso/seguridad.php");
$tabla 		= $_REQUEST['tabla'];
$iId		= $_REQUEST['iId'];
if($iId=="" || $iId=="false") $iId = "";

$condicion 	= stripslashes($_REQUEST['condicion']);
$order_by  	= $_REQUEST['order_by'];
$json 		=  $_REQUEST['genera_tabla'];
$campos	 	= array();

echo '<table class="table table-bordered table-striped" id="tabla_lista">
          <thead>';
		  if($iId!="") {
	          echo '<th no_visible="no" width="50px"></th>';
		  }

$implementar_clase = array();			
$nombre_clase = array();		
$implementar_label = array();
$nombre_label = array();

  if($iId!="") {
array_push($implementar_clase,0);
array_push($nombre_clase,"");
  }

foreach($json as $columnas => $j) {
	$l=1;
	foreach($j as $columna) {
			if($columna['label']) {
				 array_push($implementar_label,$l);		
			}

			 array_push($nombre_label,$columna['label']);	
  			 array_push($campos,$columna['columna']);
	         echo '<th';

			 if($columna['class']) {
				 echo ' class="'.$columna['class'].'"';
 				 array_push($implementar_clase,$l);							 
			 }

   		     array_push($nombre_clase,$columna['class']);	

			 if($columna['widthpdf']!="")
			 echo ' widthpdf="'.$columna['widthpdf'].'"';
			 
			// if($columna['width']!="")
			// echo ' width="'.$columna['width'].'"';
			 
			 if(!$columna['visible'])			 
			 echo ' no_visible="no"';			 
			 echo '>';
			 echo $columna['titulo'];
			 echo '</th>';		
	$l++;
	}
}
echo '</thead>
       <tbody>';

$campos_=implode(",",$campos);
if($iId!="") { $c__ = $iId.",".$campos_; }
else $c__ = $campos_;
$datos = seleccionar($tabla,$c__,$condicion,$order_by, false);
foreach($datos as $renglones) {	
	echo "<tr>";
	$c=0;
	if($iId!="") {
			$iId_ = $renglones["$iId"];
		echo '<td class="check">
                <label><input name="sel-'.$tabla.'" type="checkbox" value="option1" iId="'.$iId_.'" data-dismiss="modal"><span></span></label>
          </td>';	
	}else { $c=1; }
	foreach($renglones as $celda) {	
		 if($c>0){
    		 echo '<td';
 			 if(in_array($c,$implementar_clase)) 
				echo " class='".$nombre_clase[$c]."' ";		
			 echo '>';
			 if(in_array($c,$implementar_label)) 
				echo label($celda,$nombre_label[$c-1]);		
			 else
				 echo $celda;
			 echo '</td>';	 	
		 }		
		 $c++;
	}

echo "</tr>";
}
?>