<? include("../../acceso/seguridad.php"); 
	$iTransaccionId=$_REQUEST['iTransaccionId'];
  $tipo_boton =$_REQUEST['tipo'];

  $renglones=conceptosTransaccion((int)$iTransaccionId);
  
  $operaciones=totalesTransaccion((int)$iTransaccionId);
  $pagos=abonos((int)$iTransaccionId);
  
  $total_limpio = limpia_formato($operaciones["totalf"]);
  $abonos_limpio = limpia_formato($pagos["pagof"]);

  $restante = $total_limpio - $abonos_limpio;
  $restante_limpio= limpia_formato($restante);
	$datos =seleccionar("transaccion","transaccion.iFolio,transaccion.iTipoTransaccion","transaccion.iId='$iTransaccionId'",false, false, false);
	$tipo		=	$datos[0]['iTipoTransaccion'];
	$folio		=	$datos[0]['iFolio'];
	$info=seleccionar("pagotransaccion","iEstatus,sComentario,date_format(dFecha, '%d-%m-%Y') as fecha","pagotransaccion.iTransaccionId=$iTransaccionId",false,false);	
	$fecha		=	$info[0]['fecha'];
	$estado		=	$info[0]['iEstatus'];
	if($estado==0)$est="Vigente";
	if($estado==2)$est="Pagada";
	if($estado==1)$est="Cancelada";
  if($tipo=="2") $transaccion= "Factura";
  if($tipo=="1") $transaccion = "Venta";
?>
<div id='ventana-formulario'>
<!-- Nav tabs -->
<ul class="nav nav-pills">
  <li <? if($tipo_boton=="1" || $tipo_boton=="2") echo "class='active'";?>><a href="#envio" data-toggle="tab"><?=$transaccion;?> Folio <?=$folio?></a></li>
  <li <? if($tipo_boton=="3") echo "class='active'";?>><a href="#historial" data-toggle="tab">Historial de Pago de transacción</a></li>
</ul>



<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane <? if($tipo_boton=="1" || $tipo_boton=="2") echo "active";?>" id="envio">
<div id="totales"></div>
    <div class="clear"></div>


  <input type="text" class="form-control" name="iUsuarioEmpresaId" value="<?=$iUsuarioEmpresaId;?>" style='display:none;'>
  <input type="text" class="form-control" value="<?=$iEmpresaId;?>" style='display:none;' id="iEmpresaId">
    <input type="text" class="form-control" value="<?=$iTransaccionId;?>" style='display:none;' id="iTransaccionId">
      <input type="text" class="form-control" value="<?=$estado;?>" style='display:none;' id="est">


	<div class="row">	 
      <div class="col-lg-6">
      	<label>Seleccione Abono o Liquidación</label>
		<select class="form-control" name='iEstatus' id="estado">       			
              <option value="-1">Seleccione</option> 
        	 	  <option value="0">Abonar</option> 
              <option value="2">Liquidar</option>  
                
		</select>
        
        <div id="monto" style="display:none;">
            <label id="texto_abonoliquida"></label>
                 <input class="form-control" value="" id="fMonto" name="fMonto" size="14" type="text">
        </div>

        <div id="fecha" style="display:none;">
            <label>Fecha</label>
                 <input class="form-control" data-date-format="dd-mm-yyyy" value="<?=$fecha;?>" placeholder="Fecha" id="dFecha" name="dFecha" size="14" type="text">
        </div>
        </div> 
        </div>
   <div class="row">  
    <div class="col-lg-8">
        <label>Comentario</label>
        <textarea id="comentario" class="form-control" rows="3"><?=$comentario;?></textarea>
  		</div>   
  </div>
    <button type="button" id="enviar_estado" class="btn btn-success" iTransaccionId='<?=$iTransaccionId;?>' style='margin-top:5px;'>Guardar</button>
            <br />
      
  		
  </div>  
 
 
  <div class="tab-pane <? if($tipo_boton=="3") echo "active";?>" id="historial">
    

  </div>
   

</div>
</div>
<script>
$(document).ready(function () {
$("body").on("click","#cancelar_abono",function(e){  

      var elemento_ = $(this);

      var id = $(this).attr("href");
    
        $(elemento_).confirmar("Seguro que desea cancelar pago","Si","No",function(respuesta){

        if(respuesta) {
          
          $(elemento_).cargando("Cancelando...");
          
          
            $.post("./ventas/reportefacturas/cancela.php",{id:id},function(data){
              data = eval(data);
              if(data[1]==1) {              
                actualiza();
                $(elemento_).cerrar_cargando();                         
              }else if(data[1]==0) {
                $(elemento_).error(data[0]);            
              }     
            });
          }

      }); 
      e.preventDefault();
});
function actualiza(){
  var iTransaccionId=$("#iTransaccionId").val();
  $("#totales").html(labelcargando());
  $("#historial").html(labelcargando());
  $.post("./ventas/reportefacturas/totales.php",{iTransaccionId:iTransaccionId},function(data){
    $("#totales").html(data);

            <? if($tipo_boton=="1") {
          ?>
        $("#estado").val("0");
        cambio("0");
          <?
        }?>
        <? if($tipo_boton=="2") {
          ?>
          console.log("dos");
        $("#estado").val("2");
        cambio("2");

          <?
        }?>
        <? if($tipo_boton=="3") {
          ?>
        $("#estado").val("-1");
        cambio("-1");

          <?
        }?>
  });
  $.post("./ventas/reportefacturas/historial.php",{iTransaccionId:iTransaccionId},function(data){
    $("#historial").html(data);
  });
}
actualiza();
$('[name="dFecha"]').datepicker({
			 		todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true
			 });
			 

 $('[name="fMonto"]').on("change",function(e){  
    var monto_a_pagar = $(this).val();
    monto_a_pagar = monto_a_pagar*1;

        var total_=$("#total_").attr("valor");
  var abonos_=$("#abonos_").attr("valor");
  total_=total_*1;
  abonos_=abonos_*1;
  var monto_= number_format(total_-abonos_,2);


    if(monto_a_pagar>monto_){
      $(this).error("No puede pagar mas de la deuda","left");
      $(this).val("");
    }
 });       
 $('[name="iEstatus"]').on("change",function(e){  
  var valor=$(this).val();
  cambio(valor);
 });		

 function cambio(valor)	 {
  
  if(valor=="0"){
//Abonar
  $("#fecha").toggle(true);
  $("#monto").toggle(true); 
  $("#fMonto").val("");
  $("#fMonto").focus();
  $("#texto_abonoliquida").html("Abono ( Especifique monto )");
  $("#fMonto").removeAttr("disabled");
}
else if(valor=="2"){
  //Liquidar
  var total_=$("#total_").attr("valor");
  var abonos_=$("#abonos_").attr("valor");
  total_=total_*1;
  abonos_=abonos_*1;
  var monto_= number_format(total_-abonos_,2);
  console.log(abonos_+"-"+total_);
  
  $("#fecha").toggle(true);
  $("#monto").toggle(true); 
  $("#texto_abonoliquida").html("Liquide la cantidad de:");
  $("#fMonto").attr("disabled","disabled");

  $("#fMonto").val(monto_);
}else {
    $("#fecha").toggle(false);
  $("#monto").toggle(false); 
}
 }


 $('#enviar_estado').click(function(e){ 
  var elemento = $(this);
  $(elemento).cargando();
 var iTransaccionId=$("#iTransaccionId").val();
 var estado=$('#estado').val();
 var fecha=$("#dFecha").val();	
 var comentario=$("#comentario").val();
 var pago =$("#fMonto").val();
	$.post("./ventas/reportefacturas/guardar_estado.php",{iTransaccionId:iTransaccionId,estado:estado,fecha:fecha,comentario:comentario,pago:pago},function(data){
		data =eval(data);
    if(data[1]=="1") {
      $(elemento).correcto(data[0]);
      actualiza();
    }
    else
      $(elemento).error(data[0]);
	});
 
 
 });

});
</script>