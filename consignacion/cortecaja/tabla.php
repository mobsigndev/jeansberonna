<style>
@media print {
  table {
    width:100%;
    border:1px solid #ccc;
    border-collapse: collapse;
  }
  td,th,tr{
    border:1px solid #ccc;
    border-collapse: collapse;
  }
  h3,h4,h5{
    padding:0px;
    margin:2px;
  }
}
</style>
<?

include("../../acceso/seguridad.php");
$t_total_="0.00";
$tabla = $_REQUEST['tabla'];
$condicion_ = stripslashes($_REQUEST['condicion']);
$condicion = "transaccion.iUsuarioEmpresaId='".$iUsuarioEmpresaId."' and transaccion.iEstatus='0' and (transaccion.iTipoTransaccion='1' or transaccion.iTipoTransaccion='2')";
if($condicion_!="" &&$condicion_!="false")
  $condicion = $condicion_." and ".$condicion;

?>
<br>
<div>
  <h5>Corte de Caja del dia <span style="font-weight:bold; text-decoration:underline;"><?=$_REQUEST['fecha'];?></span></h5>
</div>
<div>
  <h5>Empleado: <span style="font-weight:bold; text-decoration:underline;"><?
  if($_REQUEST['empleado']=="0") echo "Todos";
  else {

    $datos = seleccionar("empleado","sNombre","iId='".$_REQUEST['empleado']."'",false,false, "iId");
    echo $datos[0]['sNombre'];
  }
  ?>
  </span></h5>
</div>
<table class="table table-bordered table-striped" id="<?=$tabla?>">

  <thead>
    <th style="text-align:left;">

        <h4>Forma de Pago</h4>

    </th>

  <th style="text-align:center; width:220px;">

        <h4>Total</h4>

    </th>

  </thead>

   <tbody>   

   <?

//Anterior
//$datos = seleccionar("transaccion INNER JOIN cliente ON transaccion.iClienteId = cliente.iId INNER JOIN empleado ON transaccion.iEmpleadoId = empleado.iId LEFT JOIN detallefactura ON detallefactura.iTransaccionId = transaccion.iId LEFT JOIN pagotransaccion ON pagotransaccion.iTransaccionId = transaccion.iId
//","transaccion.iTipoPago,sum(pagotransaccion.fPago) as pago,transaccion.iId,transaccion.iClienteId,transaccion.iEmpleadoId,transaccion.sSerie,transaccion.dFecha,transaccion.iEstatus,transaccion.iTipoTransaccion,transaccion.sComentarioTransaccion,transaccion.sComentarioDescuento,cliente.sNombre,empleado.sNombre as Empleado,transaccion.iFolio,detallefactura.sTUUID,pagotransaccion.iEstatus as estado,max(pagotransaccion.dFecha) as FechaPago",$condicion,false,false, "transaccion.iId");
//echo "select transaccion.iId,transaccion.iClienteId,transaccion.iEmpleadoId,transaccion.dFecha,transaccion.iEstatus,transaccion.iTipoTransaccion,transaccion.sComentarioTransaccion,transaccion.sComentarioDescuento,cliente.sNombre,empleado.sNombre as Empleado,transaccion.iFolio,detallefactura.sTUUID,pagotransaccion.iEstatus as estado from transaccion INNER JOIN cliente ON transaccion.iClienteId = cliente.iId INNER JOIN empleado ON transaccion.iEmpleadoId = empleado.iId LEFT JOIN detallefactura ON detallefactura.iTransaccionId = transaccion.iId LEFT JOIN pagotransaccion ON pagotransaccion.iTransaccionId = transaccion.iId where $condicion";                            

$datos = seleccionar("transaccion INNER JOIN empleado ON transaccion.iEmpleadoId = empleado.iId LEFT JOIN pagotransaccion ON pagotransaccion.iTransaccionId =transaccion.iId","transaccion.iId",$condicion,false,false, "transaccion.iId");
   foreach($datos as $renglones) {
    $totales_ = totalesTransaccion($renglones['iId']);  
    $t_total_+= $totales_["total"];
   }  
 

    $total_general=0;
      $totales_al_dia = seleccionar("transaccion INNER JOIN empleado ON transaccion.iEmpleadoId = empleado.iId LEFT JOIN pagotransaccion ON pagotransaccion.iTransaccionId =transaccion.iId","transaccion.iId,pagotransaccion.iTipoPago,sum(pagotransaccion.fPago) as total",$condicion,false,false,"iTipoPago");
      foreach($totales_al_dia as $total_dia){
        $tipo_pago = $total_dia['iTipoPago'];
        //Acumular totales dependiendo tipo de pagos.
        if($tipo_pago == "1" || $tipo_pago=="7") { $t_efectivo_+= $total_dia['total']; }
        else if($tipo_pago == "2" || $tipo_pago == "10" || $tipo_pago =="13" || $tipo_pago =="15" || $tipo_pago=="16") { $t_tarjetas_+= $total_dia['total']; }
        else if($tipo_pago == "3") { $t_tarjetas_+= $total_dia['total']; }
        else if($tipo_pago == "4" || $tipo_pago == "8") { $t_cheques_ += $total_dia['total']; }
        else if($tipo_pago == "5" || $tipo_pago == "9") { $t_transferencias_+= $total_dia['total']; }
        else if($tipo_pago == "6" || $tipo_pago == "17") { $t_noidentificados_+=$total_dia['total']; }        
          else if($tipo_pago == "12") { $t_dineroele_+=$total_dia['total']; }        
            else if($tipo_pago == "11") { $t_monederos_+=$total_dia['total']; }  
               else if($tipo_pago == "14") { $t_vales_+=$total_dia['total']; }          
        $total_general+= $total_dia['total'];
      } 
      $t_creditos_=limpia_formato($t_total_)-$total_general;
   ?>                             
    <tr>
    <td>
      <h4><i class="fa fa-money"></i> Efectivo</h4>
    </td> 
    

    <td align="center"><h4>$<?=number_format($t_efectivo_,2);?></h4></td>
    </tr>  
    <td><h4><i class="fa fa-book"></i> Crédito</h4></td> 
    <td align="center"><h4>$<?=number_format($t_creditos_,2);?></h4></td>
    <tr>
    <td><h4><i class="fa fa-credit-card"></i> Tarjetas</h4></td> 
    <td align="center"><h4>$<?=number_format($t_tarjetas_,2);?></h4></td>
    </tr>
      <tr>
    <td><h4><i class="fa fa-certificate"></i> Cheques</h4></td> 
    <td align="center"><h4>$<?=number_format($t_cheques_,2);?></h4></td>
    </tr>
      <tr>
    <td><h4><i class="fa fa-upload"></i> Transferencias</h4></td> 
    <td align="center"><h4>$<?=number_format($t_transferencias_,2);?></h4></td>
    </tr>
    <tr>
    <tr>
    <td><h4><i class="fa fa-dot-circle-o"></i> Monedero Electrónico</h4></td> 
    <td align="center"><h4>$<?=number_format($t_monederos_,2);?></h4></td>
    </tr>
    <tr>
    <td><h4><i class="fa fa-dot-circle-o"></i> Dinero Electrónico</h4></td> 
    <td align="center"><h4>$<?=number_format($t_dineroele_,2);?></h4></td>
    </tr>
    <tr>
    <td><h4><i class="fa fa-dot-circle-o"></i> Vales de Despensa</h4></td> 
    <td align="center"><h4>$<?=number_format($t_vales_,2);?></h4></td>
    </tr>
    <td><h4><i class="fa fa-dot-circle-o"></i> No Identificados / Otros</h4></td> 
    <td align="center"><h4>$<?=number_format($t_noidentificados_,2);?></h4></td>
    </tr>

      
</tbody>
<tfoot>
<tr>
    <th style="text-align:right;">

      <h4>TOTALES</h4>

    </th>
    <th style="text-align:center;">
    <h4>$<?=number_format(limpia_formato($t_total_),2);?></h4>
    </th>
</tr>
  </tfoot>
</table>

<script>
$(document).ready(function () {
  $("body").on("click",".detalles_t i,#t0",function(){
    var id_ = $(this).attr("id");
    var datos_ = $(this).attr("datos");
    $(this).informativo(datos_);
  }); 
  


});

</script>