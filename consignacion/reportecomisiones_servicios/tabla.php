<?

include("../../acceso/seguridad.php");

$tabla = $_REQUEST['tabla'];

$condicion_ = stripslashes($_REQUEST['condicion']);

$condicion = "transaccion.iUsuarioEmpresaId='".$iUsuarioEmpresaId."'";



if($condicion_!="" &&$condicion_!="false")

  $condicion = $condicion_." and ".$condicion;



?>

<table class="table table-bordered table-striped" id="<?=$tabla?>">

  <thead>

    <th>

        Tipo      

    </th>

    <th>

        Folio

    </th>

    <th class="hidden-xs">

        Fecha

    </th>      

   
    <th  class="hidden-xs">

      Cliente

    </th>

    

    <th>

       Total

    </th>

  <th >

      Comision

    </th>
    <th>

      Estatus 

    </th>
    
<th>

       

    </th>

  </thead>

   <tbody>   

   <?



$t_subtotal_=0;
$t_total_ =0;
$t_descuento_=0;
$t_trasladados_=0;
$t_retenidos_=0;



$datos = seleccionar("transaccion INNER JOIN cliente ON transaccion.iClienteId = cliente.iId INNER JOIN empleado ON transaccion.iEmpleadoId = empleado.iId LEFT JOIN detallefactura ON detallefactura.iTransaccionId = transaccion.iId","transaccion.iEstatus,transaccion.iId,transaccion.iClienteId,transaccion.iEmpleadoId,transaccion.dFecha,transaccion.iEstatus,transaccion.iTipoTransaccion,transaccion.sComentarioTransaccion,transaccion.sComentarioDescuento,cliente.sNombre,empleado.sNombre as Empleado,transaccion.sSerie,transaccion.iFolio,detallefactura.sTUUID",$condicion,false, false);                            

   foreach($datos as $renglones) {
      $t_retenidos=0;
      $t_trasladados=0;
      $comision=0;
      $t_comision=0;

      $conceptos = seleccionar("detalletransaccion","iProductoId,fPrecio,fCantidad,iProductoId,fIva,iTipo","iTransaccionId='".$renglones['iId']."'",false,false,false);
      foreach($conceptos as $renglones2){        
        $valores=calcular($renglones2['fPrecio'],$renglones2['fCantidad'],$renglones2['fIva']);
        $subtotal = $valores["subtotal"];
              if($renglones2['iTipo']=="2"){
                  $comision_ = seleccionar("servicio","fComision","iId='".$renglones2['iProductoId']."'",false,false,false);
                  foreach($comision_ as $renglones3){
                    $comision__ = $renglones3["fComision"];
                 }
              }

          $comision += ($subtotal*$comision__)/100;

          
      }


      $impuestos = seleccionar("transaccionimpuesto","sum(fCantidad) as cantidad,iTipo","iTransaccionId='".$renglones['iId']."'",false,false,"iTipo");
      foreach($impuestos as $t_impuestos){
        if($t_impuestos['iTipo']=='0') $t_retenidos = $t_impuestos['cantidad'];
        if($t_impuestos['iTipo']=='1') $t_trasladados = $t_impuestos['cantidad'];

      }
   $totales_ = totalesTransaccion($renglones['iId']);  

   #Sumatoria para totales
    $t_subtotal_+=$totales_["subtotal"];
    $t_trasladados_+=($totales_["iva"]+$t_trasladados);
    $t_total_+= $totales_["total"];
    $t_descuento_ +=$totales_["descuento"];
    $t_retenidos_ +=$t_retenidos_;
     $t_comision_ += $comision;
   #Termina sumatoria



     $iEstatus=$renglones['iEstatus'];      

     $subtotal=$totales_['subtotalf'];

     $iva=$totales_['ivaf'];     

     $t_trasladados = number_format((str_replace(",", "", $iva)+$t_trasladados),2);
     $t_retenidos = number_format($t_retenidos,2);

     $total=$totales_['totalf'];     

     $descuento=$totales_['descuentof'];   
     
     //$foliofiscal=$totales_['sTUUID'];



       echo '<tr>';

        echo '<td>'.transaccion($renglones['iTipoTransaccion']).'</td>';

        echo '<td>'.$renglones['sSerie'].''.$renglones['iFolio'].'</td>';

        echo '<td>'.$renglones['dFecha'].'</td>';

        

        echo '<td>'.$renglones['sNombre'].'</td>';                                    

        echo '<td>'.$total.'</td>';

        echo '<td>'.number_format($comision,2).'</td>';  
        

        echo '<td>'.label($iEstatus,"avanzado").'</td>';    
                 echo '<td>';
     ?>

         <div class="btn-group" style=''>

                  <button type="button" class="btn btn-info btn-sm">Acciones</button>

                  <button type="button" class="btn btn-info dropdown-toggle btn-sm" data-toggle="dropdown">

                    <span class="caret"></span>

                    <span class="sr-only"></span>

                  </button>

                  <ul class="dropdown-menu" role="menu" id='accion_seleccionada'>                   

                
                   <li><a href="./ventas/reportecomisiones/detalle_comision.php?iId=<?=$renglones['iId'];?>" target="_blank" class='detalle_comision'>Detalle Comision</a></li>
                            
          </ul>

          </div>

          <?    

    echo '</td>';                               

       echo '</tr>';

   }

   ?>                             

</tbody>
<tfoot>
<tr>
    <th colspan="4">

      Totales

    </th>

    <th>

       <?=number_format($t_total_,2);?>

    </th>

    <th>

      <?=number_format($t_comision_,2);?> 

    </th>
    
  <th no_visible="no">

      

    </th> <th></th>
</tr>
  </tfoot>

</table>