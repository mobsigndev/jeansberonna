<?
include("../../acceso/seguridad.php");
$iTransaccionId=$_REQUEST['iTransaccionId'];
$estado=$_REQUEST['estado'];
$fecha=$_REQUEST['fecha'];
$formateada=date_format ( new DateTime($fecha) , 'Y-m-d' );
$comentario=$_REQUEST['comentario'];
$pago=limpia_formato($_REQUEST['pago']);
$iTipoPago=$_REQUEST['iTipoPago'];
if($iTipoPago!="0"){

  if($pago>0){
  $campos="iTransaccionId,iEstatus,dFecha,sComentario,fPago,iEmpleadoId,dtFechaOperacion,iTipoPago";
  $valores="'$iTransaccionId','$estado','$formateada','$comentario','$pago','$iEmpleadoId',CURRENT_TIMESTAMP(),'$iTipoPago'";
  $id = insertar("pagotransaccion",$campos,$valores,"iId");

    if($id>0 ) { 
    	


      
      $operaciones=totalesTransaccion((int)$iTransaccionId);
      $pagos=abonos((int)$iTransaccionId);
      
      $total_limpio = limpia_formato($operaciones["totalf"]);
      $abonos_limpio = limpia_formato($pagos["pagof"]);

      $restante = $total_limpio - $abonos_limpio;
      $restante_limpio= limpia_formato($restante);
      if($restante_limpio<=0) {  	
    		$liquida = actualiza(pagotransaccion,'iEstatus=2',"iTransaccionId ='$iTransaccionId' and iEstatus<>1");
    		if($liquida) {
    			echo json_encode(array("Pago almacenado, Deuda Liquidada",1));
    		}
      }
      else
    	echo json_encode(array("Pago almacenado",1));  		

    }
    else echo json_encode(array("Error almacenando pago",0));
  }
  else echo json_encode(array("No hay cantidad de pago",0));
}
else echo json_encode(array("Seleccione tipo de pago",0));
?>