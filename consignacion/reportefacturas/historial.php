<? include("../../acceso/seguridad.php"); 
$iTransaccionId=$_REQUEST['iTransaccionId'];
?>

<table class="table table-bordered table-striped" style='margin-top:10px;'>
    <thead><tr><th>Fecha</th><th>Monto</th><th>Empleado</th><th>Comentario</th><th>Recibo</th></tr></thead>
    <tbody>
   <?
    $info2=seleccionar("pagotransaccion","iId,iEstatus,iEmpleadoId,sComentario,date_format(dFecha, '%d-%m-%Y') as fecha,fPago","pagotransaccion.iTransaccionId=$iTransaccionId and pagotransaccion.iEstatus<>1",false,false);  
    
    foreach($info2 as $info_) {
      
      $fecha    = $info_['fecha'];
      $monto    = number_format($info_['fPago'],2);
      $estado   = $info_['iEstatus'];
      $ipagotransaccion   = $info_['iId'];
      $comentario   = $info_['sComentario'];
      $empleado   = empleado($info_['iEmpleadoId']);
      $nombre_empleado = $empleado['sNombre'].$empleado['sPaterno'];           
       $recibo ="<a class='btn btn-info' href='./clases/a_pdf.php?url=../ventas/reporteventas/reporte_abono.php?iId=$ipagotransaccion'>Recibo</a>";
        $cancelar ="<a class='btn btn-danger' href='$ipagotransaccion' id='cancelar_abono'>Cancelar</a>";
       echo "<tr><td>".$fecha."</td><td>".$monto."</td><td>".$nombre_empleado."</td><td>".$comentario."</td><td style='font-size:10px;'>".$recibo." ".$cancelar."</td></tr>";
 
    }
    ?>
     

    </tbody>

</table>