<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

if(file_exists("../acceso/seguridad.php")){
include("../acceso/seguridad.php");
$p = "..";
}
if(file_exists("../../acceso/seguridad.php")){
include("../../acceso/seguridad.php");
$p = "../..";
}

if(file_exists("./acceso/seguridad.php")){
include("./acceso/seguridad.php");
$p = ".";
}


$id=$_REQUEST['iId'];
$renglones=conceptosTransaccion((int)$id);
$operaciones=totalesTransaccion((int)$id);
$datos=datos_transaccion((int)$id);
if($datos["iTipoTransaccion"]=="1"){
 $sTipoTransaccion= "Venta";
}
else if($datos['iTipoTransaccion'] == "2" ) {
 $sTipoTransaccion="Factura sin timbrar";
}
else if($datos['iTipoTransaccion'] == "3" ) {
 $sTipoTransaccion="Cotización";
}
if($datos["iEstatus"]==1)
	$estatus = "<div><label style='color:red;'>Cancelado</label></div>";
else 
	$estatus = "";

?>
<html>

<head> 
	<link rel="stylesheet" type="text/css" href="../css/estilo_cotizacion.css" />
</head>
<style> 
@page { margin: 160px 20px 120px 20px; }
    #header { position: fixed; left: 0px; top: -140px; right: 0px; height: 140px; text-align: center; border-bottom:2px solid #ccc;}
    #footer { position: fixed; left: 0px; bottom: -110px; right: 0px; height: 80px; background:#ccc; font-size:10px;}
</style>

<div style='font-family:Verdana, Geneva, sans-serif; color:#666;'>
	
    <!--ENCABEZADO -->
    <div style='width:780px; border-top:1px solid #fff;' id="header">
    	
        <div style='width:200px; background-color:#fff; display:inline-block; border-right:2px solid #CCC;'>
           
            <div style='width:160px; height:90px;margin-left:20px; margin-top:10px; font-size:24px; color:#0082B4;'>
				<?php if(file_exists("$p/logotipos/".$datos["sLogotipo"])) { ?>
                <img src="<?=$p;?>/logotipos/<?php echo $datos["sLogotipo"]; ?>" width="160" alt="Logotipo" />
				<?php } ?>                
            </div>
        </div>
         
        <div style="width:380px; height:106px; display:inline-block; text-align:left;">
            <div style='margin-left:20px; margin-top:15px; font-size:24px; color:#444; vertical-align:top;'>
            <?php echo $sTipoTransaccion; ?>
			</div>
            <div style='margin-left:20px; margin-top:8px; font-size:13px; color:#444;vertical-align:top;'>
            De: <?php echo $sEmpresa; ?>
			</div>
            <div style='margin-left:20px; margin-top:8px; font-size:13px; color:#444;vertical-align:top;'>
            Para: <? echo $datos["sNombre"]; ?>
            </div>
        </div>
        
		<div style="width:180px; height:110px; border:2px solid #ccc; display:inline-block;">
         	<div style='margin-top:10px;margin-left:10px;padding-bottom:1px;color:#444;font-size:19px;text-align: left;'>
            <strong>Folio:</strong><? echo $datos["iFolio"]; ?>
            </div>
            <div style='margin-left:10px; margin-top:8px; font-size:12px; color:#444;vertical-align:top; text-align:left;'>
            <strong>Fecha:</strong> <? echo $datos["dFecha"]; ?>
            </div>
            <div style='margin-left:10px; margin-top:8px; font-size:12px; color:#444;vertical-align:top; text-align:left;'>
            <strong>Lugar:</strong> <? echo $datos["sCiudad"].", ".$datos["sEstado"].", ".$datos["sPais"]; ?>
            </div>
		</div>
        <?= $estatus; ?>
    </div>
    <!--ENCABEZADO#-->
    
        <!--PIE DE REPORTE -->
  <div id="footer">
    	<p class="page">
        <center>
        <div style='margin-top:7px;'><?php echo $sEmpresa; ?></div>
    	<div><?php echo utf8_encode($datos["sCalle"])." ".$datos["sNumeroExterior"]." Col.".$datos["sColonia"]." ".$datos["sCiudad"].", ".$datos["sEstado"].", ".$datos["sPais"]; ?></div>
        <div>Tel&eacute;fono <? echo $datos["sTelefonoContacto"]; ?></div>
    	<div><? echo $datos["sCorreo"]; ?></div>
        </center>
    	</p>
         
    </div>	
	<!--PIE DE REPORTE#-->
    
    
    <!--CUERPO DE REPORTE -->
    <div style="width:780px;padding-bottom:30px;background-color:#fff;">    	
      <h4 style='width:780px; margin-top:10px;	margin-left:10px;	padding-bottom:1px;	background-color:#;	font-size:15px;text-align:justify;	color:#333;'><?php echo $datos["sComentarioTransaccion"]; ?></h4>                        
     
      <table border="1" style='border:1px solid #999; width:780px; font-size:11px; border-collapse:collapse;'>
          <tr> 
            <th width="440px"><strong>CONCEPTO</strong></th>
        	<th width="60px" ><strong>UNIDAD</strong></th>
            <th width="70px"><strong>PRECIO</strong></th>
            <!--<th width="50px"><strong>IVA</strong></th>-->
            <th width="50px"><strong>CANT</strong>.</th>
            <th width="140px"><strong>TOTAL</strong></th>
          </tr>
          <?php echo $renglones; ?>
      </table>

      <br><br>    
      <div style='width:300px; margin-left:470px; font-weight:bold;margin-top:10px;'>
          <div style='width:160px; display:inline-block; text-align:right; '>Subtotal</div>
          <div style='width:140px;  display:inline-block; text-align:right;border-bottom:1px solid #ccc;'>$<?php echo $operaciones["subtotalf"]; ?></div>
		<br>
          <div style='width:160px;display:inline-block; text-align:right;'>Descuento</div>
          <div style='width:140px;  display:inline-block; text-align:right;border-bottom:1px solid #ccc;'>-$<?php echo $operaciones["descuentof"]; ?></div>
		<br>
          <div style='width:160px;display:inline-block; text-align:right;'>I.V.A.</div>
          <div style='width:140px;  display:inline-block; text-align:right;border-bottom:1px solid #ccc;'>$<?php echo $operaciones["ivaf"]; ?></div>
		<br>
          <div style='width:160px;display:inline-block; text-align:right;'>TOTAL</div>          
          <div style='width:140px;  display:inline-block;text-align:right;border-bottom:1px solid #ccc;'>$<?php echo $operaciones["totalf"]; ?></div>
     </div>
      
  </div>
    <!--CUERPO DE REPORTE#-->
    
    
    </div>
    


</div>
</html>