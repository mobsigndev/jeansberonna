<?
require_once("../../clases/bd.php");
require_once("../../clases/mysql.php");
require_once('../../plugins/phpmailer/class.phpmailer.php');
require_once("../../plugins/dompdf/dompdf_config.inc.php");
ob_start();
$sAsunto= "Pedido realizado en la Tienda en línea de Adminbox";
$sDestinatario = $_REQUEST['correo'];
$sComentario= "Ha realizado un pedido desde la Tienda en l&iacute;nea de Adminbox. <br>Se adjunta copia de su pedido";
$sDocumento= $_REQUEST['id'];
$iTransaccionId=$_REQUEST['id'];
$url = $_REQUEST['url'];;

$u_ = explode("?",$url);
$u__ = explode("=",$u_[1]);
$campo = $u__[0];
$valor= $u__[1];
$_REQUEST["$campo"] = $valor;
include($u_[0]);

$content = ob_get_clean();
$dompdf = new DOMPDF();
$dompdf->load_html($content);
$dompdf->set_paper("letter", "portrait" );
$dompdf->render();
$output = $dompdf->output();


$consulta= seleccionar("transaccion INNER JOIN usuarioempresa ON transaccion.iUsuarioEmpresaId = usuarioempresa.iId
INNER JOIN empresa ON empresa.iId = usuarioempresa.iEmpresaId","usuarioempresa.iUsuarioId, usuarioempresa.iEmpresaId,transaccion.iClienteId,transaccion.iEmpleadoId,transaccion.dFecha,empresa.sEmpresa,empresa.sRazonSocial,empresa.sRFC,empresa.sNumeroTelefono,empresa.sCorreo","transaccion.iId='$iTransaccionId'",false,false,false);

$comprador= seleccionar("transaccionlinea","sNombre,sCorreo,sDireccion,sTelefono","transaccionlinea.iPedidoId='$iTransaccionId'",false,false,false);

$mail             = new PHPMailer();
$mail->Subject =$sAsunto;
$mail->IsSMTP(); // telling the class to use SMTP
$mail->Host       = "www.adminbox.com.mx"; // SMTP server
$mail->SMTPDebug  = false;                     // enables SMTP debug information (for testing)
$mail->SMTPSecure = "ssl"; 										   
$mail->SMTPAuth   = true;                  // enable SMTP authentication
$mail->Host       = "mail.adminbox.com.mx"; // sets the SMTP server
$mail->Port       = 465;                    // set the SMTP port for the GMAIL server
$mail->Username   = "contacto@adminbox.com.mx"; // SMTP account username
$mail->Password   = "Adminbox05";        // SMTP account password
$mail->SetFrom('contacto@adminbox.com.mx', 'Adminbox');
//$mail->AddReplyTo('contacto@mobsign.om', 'Daniel');
//$mail->Subject    = "Prueba de envio";
//$mail->AltBody    = "Para ver el mail usa la compatibilidad con html!"; // optional, comment out and test
$body = $sComentario."<br>";
$body.="<br>Su Informaci&oacute;n</br>
            <strong>Nombre:</strong> ".$comprador[0]['sNombre']."</br>
            <strong>Correo:</strong> ".$comprador[0]['sCorreo']."</br>
            <strong>Direccion:</strong> ".$comprador[0]['sDireccion']."</br>
            <strong>Tel&eacute;fono:</strong> ".$comprador[0]['sTelefono']."</br>";
           
$body.="<br>Informaci&oacute;n del vendedor</br>
            <strong>Empresa:</strong> ".$consulta[0]['sEmpresa']."</br>
            <strong>RazonSocial:</strong> ".$consulta[0]['sRazonSocial']."</br>
            <strong>RFC:</strong> ".$consulta[0]['sRFC']."</br>
            <strong>Tel&eacute;fono:</strong> ".$consulta[0]['sNumeroTelefono']."</br>
            <strong>Email:</strong> ".$consulta[0]['sCorreo']."</br>";
			
$body.="<br>
Adminbox, Mobsign Developments S.A de C.V.<br>
Calle Morelia No. 139 Local B, entre Ignacio Ram&iacute;rez y Narbona,<br>
Colonia Centro, Hermosillo, Sonora. <br>
Tel:(662)2 12 32 63<br>
www.adminbox.mx";
			
$mail->MsgHTML($body);
$sDestinatario = $sDestinatario.",";

$destinatarios = explode(",",$sDestinatario);
$envio =false;
foreach($destinatarios as $address) {	
if($address!="") {
	
	$envio=true;
	$mail->AddAddress($address, $sDocumento);
	}
}
if($envio){

	$mail->AddStringAttachment($output,$sDocumento.'.pdf','base64','application/pdf');
	
	//$mail->AddAttachment("images/phpmailer.gif");      // attachment
	if(!$mail->Send()) {
	  echo "Error Enviando Documento: " . $mail->ErrorInfo;
	} else {
	  #echo $address." [Documento Enviado]";
	  echo "Documento Enviado";
	}
}



?>