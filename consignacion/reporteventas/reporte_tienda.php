<?php
require_once("./clases/bd.php");
require_once("./clases/mysql.php");
$p="."
$id=$_REQUEST['iId'];
$renglones=conceptosTransaccion((int)$id);
$operaciones=totalesTransaccion((int)$id);
$datos=datos_transaccion((int)$id);
if($datos["iTipoTransaccion"]=="1"){
 $sTipoTransaccion= "Nota de Venta";
}
else if($datos['iTipoTransaccion'] == "2" ) {
 $sTipoTransaccion="Nota de Venta";
}
else if($datos['iTipoTransaccion'] == "3" ) {
 $sTipoTransaccion="Cotizaci&oacute;n";
}
if($datos["iEstatus"]==1)
	$estatus = "<div><label style='color:red;'>Cancelado</label></div>";
else 
	$estatus = "";

?>
<html>

<head> 
	<link rel="stylesheet" type="text/css" href="../css/estilo_cotizacion.css" />
</head>
<style> 
@page { margin: 160px 20px 120px 20px; }
    #header { position: fixed; left: 0px; top: -140px; right: 0px; height: 140px; text-align: center; border-bottom:2px solid #ccc;}
    #footer { position: fixed; left: 0px; bottom: -110px; right: 0px; height: 80px; background:#ccc; font-size:10px;}
</style>

<div style='font-family:Verdana, Geneva, sans-serif; color:#666;'>
	
    <!--ENCABEZADO -->
    <div style='width:780px; border-top:1px solid #fff;' id="header">
    	
        <div style='width:220px; background-color:#fff; display:inline-block; border-right:2px solid #CCC;'>
           
            <?php if($datos["sLogotipo"]!=""){
                if(file_exists("$p/logotipos/".$datos["sLogotipo"])) { 
                
                	list($anchura, $altura, $type, $attr) = getimagesize("$p/logotipos/".$datos["sLogotipo"]);
					
					if($anchura>$altura) {
						#Mas ancho       
						if(($altura*3.33)>$anchura) { $yy=60; $xx=0; }
						else { 
							$xx=200; $yy=0; 
				
						}
					}
					else {
					   
					   if(($anchura/3.33)>$altura) { $xx=200; $yy=0; }
					   else { $xx=0; $yy=60; }
					}
						
				  ?>
                  <img src="<?=$p;?>/logotipos/<?php echo $datos["sLogotipo"]; ?>" width="<?=$xx?>" height="<?=$yy?>" alt="Logotipo" />
                  
				<? } 
			}
        ?>                
          
        </div>
         
        <div style="width:360px; height:106px; display:inline-block; text-align:left;">
            <div style='margin-left:20px; margin-top:15px; font-size:24px; color:#444; vertical-align:top;'>
            <?php echo $sTipoTransaccion; ?>
			</div>
            <div style='margin-left:20px; margin-top:8px; font-size:13px; color:#444;vertical-align:top;'>
            De: <?php echo $datos['sRazonSocial']; ?>
			</div>
            <div style='margin-left:20px; margin-top:8px; font-size:13px; color:#444;vertical-align:top;'>
            Para: <? echo $datos["sNombre"]; ?>
            </div>
        </div>
        
		<div style="width:180px; height:110px; border:2px solid #ccc; display:inline-block;">
         	<div style='margin-top:10px;margin-left:10px;padding-bottom:1px;color:#444;font-size:19px;text-align: left;'>
            <strong>Folio:</strong><? echo $datos["sSerie"].$datos["iFolio"]; ?>
            </div>
            <div style='margin-left:10px; margin-top:8px; font-size:12px; color:#444;vertical-align:top; text-align:left;'>
            <strong>Fecha:</strong> <? echo $datos["dFecha"]; ?>
            </div>
            <div style='margin-left:10px; margin-top:8px; font-size:12px; color:#444;vertical-align:top; text-align:left;'>
            <strong>Lugar:</strong> <? echo $datos["sCiudad"].", ".$datos["sEstado"].", ".$datos["sPais"]; ?>
            </div>
		</div>
        <?= $estatus; ?>
    </div>
    <!--ENCABEZADO#-->
    
        <!--PIE DE REPORTE -->
  <div id="footer">
    	<p class="page">
        <center>
        <div style='margin-top:7px;'><?php echo $datos['sRazonSocial']; ?></div>
    	<div><?php echo utf8_encode($datos["sCalle"])." ".$datos["sNumeroExterior"]." Col.".$datos["sColonia"]." ".$datos["sCiudad"].", ".$datos["sEstado"].", ".$datos["sPais"]; ?></div>
        <div>Tel&eacute;fono <? echo $datos["sTelefonoContacto"]; ?></div>
    	<div><? echo $datos["sCorreo"]; ?></div>
        </center>
    	</p>
         
    </div>	
	<!--PIE DE REPORTE#-->
    
    
    <!--CUERPO DE REPORTE -->
    <div style="width:780px;padding-bottom:30px;background-color:#fff;">    	
      <h4 style='width:780px; margin-top:10px;	margin-left:10px;	padding-bottom:1px;	background-color:#;	font-size:15px;text-align:justify;	color:#333;'><?php echo $datos["sComentarioTransaccion"]; ?></h4>                        
     
      <table border="1" style='border:1px solid #999; width:780px; font-size:11px; border-collapse:collapse;'>
          <tr> 
            <th width="390px"><strong>CONCEPTO</strong></th>
        	<th width="60px" ><strong>UNIDAD</strong></th>
            <th width="70px"><strong>PRECIO</strong></th>
            <th width="50px"><strong>IVA</strong></th>
            <th width="50px"><strong>CANT</strong>.</th>
            <th width="140px"><strong>TOTAL</strong></th>
          </tr>
          <?php echo utf8_decode($renglones); ?>
      </table>

      <br><br>    
      <div style='width:300px; margin-left:470px; font-weight:bold;margin-top:10px;'>
          <div style='width:160px; display:inline-block; text-align:right; '>Subtotal</div>
          <div style='width:140px;  display:inline-block; text-align:right;border-bottom:1px solid #ccc;'>$<?php echo $operaciones["subtotalf"]; ?></div>
	    <? if($operaciones["descuento"]>0){ ?>
          <br>
          <div style='width:160px;display:inline-block; text-align:right;'>Descuento</div>
          <div style='width:140px;  display:inline-block; text-align:right;border-bottom:1px solid #ccc;'>-$<?php echo $operaciones["descuentof"]; ?></div>
      <? } ?>
		<br>
          <div style='width:160px;display:inline-block; text-align:right;'>I.V.A.</div>
          <div style='width:140px;  display:inline-block; text-align:right;border-bottom:1px solid #ccc;'>$<?php echo $operaciones["ivaf"]; ?></div>
		<br>
        
       		<? 
			$datos_ = array();
            $transaccion= array();
            $datos2 = seleccionar("transaccionimpuesto","iTipo,fCantidad,sImpuesto,fPorcentaje","iTransaccionId='$id'",false, false);
			   if($datos2){
         foreach($datos2 as $renglones) {
					$datos_['sImpuesto']	= 	$renglones['sImpuesto'];
					$datos_['cantidad']		= 	number_format($renglones['fCantidad'],2);  
					$datos_['porcentaje']		= 	number_format($renglones['fPorcentaje'],2);       
					array_push($transaccion,$datos_);
					?>
						<div style='width:160px;display:inline-block; text-align:right; font-size:14px;'><?php echo $datos_["sImpuesto"]." (".$datos_['porcentaje'].")%"; ?></div>
						<div style='width:140px;  display:inline-block; text-align:right;border-bottom:1px solid #ccc;'>$<?php echo $datos_["cantidad"]; ?></div>
						<br> <?
			   }
       }
           ?>
        
          <div style='width:160px;display:inline-block; text-align:right;'>TOTAL</div>          
          <div style='width:140px;  display:inline-block;text-align:right;border-bottom:1px solid #ccc;'>$<?php echo $operaciones["totalf"]; ?></div>
          
          
     </div>
     
     <? if($datos['dLimitePago']!="" && $datos['fInteresPagare']!="" && $datos['fInteresPagare']>0 ){ ?>
     <div style='font-size:8px; margin-top:15px; text-align:justify;'><? echo utf8_decode("Debo(emos) y pagare(mos) incondicionalmente, a la vista y a la orden de ".$datos['sRazonSocial']." la cantidad anotada en esta factura el dia ".$datos['dLimitePago'].", en la ciudad de ".$datos["sCiudad"].", si no fuere pagado satisfactoriamente este pagaré me(nos) obligo(amos) a pagar durante todo el tiempo que permaneciera total o parcialmente insoluto, intereses moratorios a razón del ".$datos['fInteresPagare']."% mensual sin que por esto considere prorrogado el plazo fijado para cumplir esta obligación. LA FIRMA DE ESTE COMPROBANTE DARA COMO ACEPTADO ESTE PAGARE."); ?></div>
      <? } ?>


  </div>
    <!--CUERPO DE REPORTE#-->
    
    
    
    </div>
    <?
     if($datos['iTipoTransaccion'] == "3" ) {
		 echo utf8_decode($datos['sEnriquecido']);
	 }
	?>


</div>
</html>