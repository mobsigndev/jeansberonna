<?

include("../../acceso/seguridad.php");

$tabla = $_REQUEST['tabla'];

$condicion_ = stripslashes($_REQUEST['condicion']);

$condicion = "transaccion.iUsuarioEmpresaId='".$iUsuarioEmpresaId."'";



if($condicion_!="" &&$condicion_!="false")

	$condicion = $condicion_." and ".$condicion;



?>

<table class="table table-bordered table-striped" id="<?=$tabla?>">

  <thead>

    <th class="hide">

        Tipo      

    </th>

    <th>

        Folio

    </th>

    <th class="hidden-xs">

        Fecha

    </th>      

   
    <th  class="hidden-xs">

    	Cliente

    </th>

    <th class="hide">

        Subtotal

    </th>

    <th class="hide">

       Imp. Trasladados

    </th>

    <th class="hide">

       Imp. Retenidos

    </th>

    <th class="hide">

       Descuento

    </th>

    <th>

     	 Total

    </th>

    <th>

	    Estatus	

    </th>
    
	<th no_visible="no">

    	

    </th>

	</thead>

   <tbody>   

   <?



$t_subtotal_=0;
$t_total_ =0;
$t_descuento_=0;
$t_trasladados_=0;
$t_retenidos_=0;



$datos = seleccionar("transaccion INNER JOIN cliente ON transaccion.iClienteId = cliente.iId INNER JOIN empleado ON transaccion.iEmpleadoId = empleado.iId LEFT JOIN detallefactura ON detallefactura.iTransaccionId = transaccion.iId","transaccion.iEstatus,transaccion.iId,transaccion.iClienteId,transaccion.iEmpleadoId,transaccion.dFecha,transaccion.iEstatus,transaccion.iTipoTransaccion,transaccion.sComentarioTransaccion,transaccion.sComentarioDescuento,cliente.sNombre,empleado.sNombre as Empleado,transaccion.sSerie,transaccion.iFolio,detallefactura.sTUUID",$condicion,false, false);                            

   foreach($datos as $renglones) {
      $t_retenidos=0;
      $t_trasladados=0;
      $impuestos = seleccionar("transaccionimpuesto","sum(fCantidad) as cantidad,iTipo","iTransaccionId='".$renglones['iId']."'",false,false,"iTipo");
      foreach($impuestos as $t_impuestos){
        if($t_impuestos['iTipo']=='0') $t_retenidos = $t_impuestos['cantidad'];
        if($t_impuestos['iTipo']=='1') $t_trasladados = $t_impuestos['cantidad'];

      }
   $totales_ = totalesTransaccion($renglones['iId']);  

   #Sumatoria para totales
    $t_subtotal_+=$totales_["subtotal"];
    $t_trasladados_+=($totales_["iva"]+$t_trasladados);
    $t_total_+= $totales_["total"];
    $t_descuento_ +=$totales_["descuento"];
    $t_retenidos_ +=$t_retenidos_;
   #Termina sumatoria



 	   $iEstatus=$renglones['iEstatus'];	    

	   $subtotal=$totales_['subtotalf'];

	   $iva=$totales_['ivaf'];	   

     $t_trasladados = number_format((str_replace(",", "", $iva)+$t_trasladados),2);
     $t_retenidos = number_format($t_retenidos,2);

	   $total=$totales_['totalf'];	   

	   $descuento=$totales_['descuentof'];	 
	   
	   //$foliofiscal=$totales_['sTUUID'];



       echo '<tr>';

        echo '<td class="hide">'.transaccion($renglones['iTipoTransaccion']).'</td>';

        echo '<td>'.$renglones['sSerie'].''.$renglones['iFolio'].'</td>';

        echo '<td>'.$renglones['dFecha'].'</td>';

        

        echo '<td>'.$renglones['sNombre'].'</td>';

        echo '<td class="hide">'.$subtotal.'</td>';

        echo '<td class="hide">'.$t_trasladados.'</td>';

        echo '<td class="hide">'.$t_retenidos.'</td>';

        echo '<td class="hide">'.$descuento.'</td>';                                                

        echo '<td>'.$total.'</td>'; 

        echo '<td>'.label($iEstatus,"avanzado").'</td>'; 		
	
		echo '<td>';
 if (file_exists("../../facturacionE/temp/".$iEmpresaId."/xml_".$renglones['iId'].".xml")) {
  $timbrada=true; 
 }else if(existe("detallefactura","iTransaccionId='".$renglones['iId']."'",$return = 'iId')){
  $timbrada=true;
 }
 else {
  $timbrada = false;
 }
		 ?>

         <div class="btn-group" style=''>

                  <button type="button" class="btn btn-info btn-sm">Acciones</button>

                  <button type="button" class="btn btn-info dropdown-toggle btn-sm" data-toggle="dropdown">

                    <span class="caret"></span>

                    <span class="sr-only"></span>

                  </button>

                  <ul class="dropdown-menu" role="menu" id='accion_seleccionada'>                   


                    <li><a href="./ventas/reporteventas/reporte.php?iId=<?=$renglones['iId'];?>" target="_blank">Imprimir</a></li>


                    <li><a href="#" id_transaccion="<?=$renglones['iId'];?>" id='imprimir_ticket'>Ticket</a></li>

                    <li><a href="<?=$renglones['iId'];?>" class='enviar_correo'>Enviar por correo</a></li>
                    
                    <li><a href="./clases/a_pdf.php?url=../ventas/reporteventas/reporte_pdf.php?iId=<?=$renglones['iId'];?>" target="_blank">PDF</a></li>
                    <?	if($renglones['iEstatus']==0) { ?>

                                <li class="divider"></li>

            					<li><a href="<?=$renglones['iId'];?>" class='enviar_devoluciones'>Devoluciones</a></li>
                                

                                <li><a href="<?=$renglones['iId'];?>" tipo="<?=$renglones['iTipoTransaccion'];?>" timbrada="<?php echo $timbrada; ?>" class='enviar_cancelar'>Cancelar</a></li>
                      <? } ?>



                  </ul>

          </div>

          <?		

		echo '</td>';                                               		

       echo '</tr>';

   }

   ?>                             

</tbody>
<tfoot>
<tr>
    <th colspan="3">

      Totales

    </th>

    <th class="hide">

        <?=number_format($t_subtotal_,2);?>

    </th>

    <th class="hide">

      <?=number_format($t_trasladados_,2);?>

    </th>

    <th class="hide">

      <?=number_format($t_retenidos_,2);?>

    </th>

    <th class="hide">

       <?=number_format($t_descuento_,2);?>

    </th>

    <th>

       <?=number_format($t_total_,2);?>

    </th>

    <th>

       

    </th>
    
  <th no_visible="no">

      

    </th>
</tr>
  </tfoot>

</table>