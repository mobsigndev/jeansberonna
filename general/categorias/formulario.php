<? include("../../acceso/seguridad.php"); ?>
<div id='ventana-formulario'>
<!-- Nav tabs -->
<ul class="nav nav-pills">
  <li class='active'><a href="#general" data-toggle="tab">General</a></li>
  <li class="hide"><a href="#otro" data-toggle="tab">Tienda en Línea</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="general">
  <input type="text" class="form-control" name="dRegistro" value="CURRENT_DATE()" style='display:none;'>
  <input type="text" class="form-control" name="iId" style='display:none;'>
	<div class="row">
	 
      <div class="col-lg-8">
      	  <input type="text" class="form-control" name="iUsuarioEmpresaId" value="<?=$iUsuarioEmpresaId;?>" style='display:none;'>
     		<label>Código </label><span><input obligatorio='si' type="text" class="form-control" name="sCodigo"></span>
     		<label>Nombre de Categoría</label> <span><input obligatorio='si' type="text" class="form-control" name="sNombre"></span>            
     		<label class="hide">Tipo de Categoría</label>
               <select class="form-control hide" name='iTipoCategoria'>             
	                <option value="1" selected="selected">Para productos</option>
                    <option value="2">Para servicios</option>
                </select> 
      </div>
      	 
    </div>
      
  		
  </div> 
    <? if(permiso("Tienda",$iUsuarioEmpresaId)) { ?>
  <div class="tab-pane" id="otro">
	<div class="row">  
    <div class="col-lg-6">
       <label>Mostrar ésta categoria en WEB</label><span><select obligatorio="si" class="form-control" name='iMostrarEnWeb'>
                    <option value="1" selected="selected">SI</option>   
                    <option value="0">NO</option>                   
                </select></span>
    </div>
    <div class="col-lg-6">
       <h3><label>ID Categoria</label> <label class="label label-danger" id="id_categoria"></label></h3>
    </div>
    </div>     
  </div>
  <? } ?>
  

</div>
</div>
<script>
$(document).ready(function () {
  

   var iCategoriaId=null;
  $("body").on('click','input[name=iId]',function(){
    iCategoriaId=$("input[name=iId]").val();
    $("#id_categoria").html(iCategoriaId);
  
  });

});
  </script>