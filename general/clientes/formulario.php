<? 
include("../../acceso/seguridad.php");
?>
<div id='ventana-formulario'>
<!-- Nav tabs -->
<ul class="nav nav-pills">
  <li class='active'><a href="#general" data-toggle="tab">General</a></li>
  <li><a href="#contacto" data-toggle="tab">Contacto</a></li>
    <li><a href="#contratos" data-toggle="tab">Solicitud de Crédito</a></li>
     <li><a href="#aval" data-toggle="tab">Aval</a></li>
  
  <li><a href="#credito" data-toggle="tab">Límite de Crédito</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="general">
  <input type="text" class="form-control" name="iUsuarioEmpresaId" value="<?=$iUsuarioEmpresaId;?>" style='display:none;'>
  <input type="text" class="form-control" name="dRegistro" value="CURRENT_DATE()" style='display:none;'>
  <input type="text" class="form-control" name="iId" style='display:none;'>
	<div class="row">
	  <div class="col-lg-4">
    <label>No. Cliente </label><span><input disabled="disabled" type="text" class="form-control" no_catalogo="no" name="iFolio"></span>
     		<label>Cliente </label><span><input obligatorio='si' type="text" class="form-control" name="sNombre"></span>
     		<label>RFC</label> <span><input type="text" class="form-control" name="sRFC"></span>            
     		<label>Raz&oacute;n Social</label><span><input type="text" class="form-control" name="sRazonSocial"></span> 
            <label>Tipo de Cliente</label><span>
            <select class="form-control " name="sRegimenFiscal"><option value="credito">Crédito</option><option value="contado">Contado</option></select> 

            
			<label>Calle</label> <span><input  type="text" class="form-control" name="sCalle"></span>  
      </div>
	  <div class="col-lg-4">
  			<label>No. Exterior </label><span><input  type="text" class="form-control" name="sNumeroExterior"></span>                      

      		<label>No. Interior </label><input type="text" class="form-control" name="sNumeroInterior">          
            <label>Colonia</label><span><input  type="text" class="form-control" name="sColonia"></span>           
     		<label>Cod. Postal</label><span><input  type="text" class="form-control" name="sCodigoPostal"></span>				
            <label>Municipio</label><span><input  type="tel" class="form-control " name="sMunicipio">
       		         	                        
      
      </div>
	  <div class="col-lg-4">   
		    <label>Ciudad</label><span><input  type="tel" class="form-control " name="sCiudad"></span>      	           	<label>Estado</label><span><input  type="tel" class="form-control " name="sEstado"></span>
     		<label>Pais </label><span><input  type="text" class="form-control" name="sPais"></span>   
			<label>Tel&eacute;fono </label><input type="text" class="form-control" name="sNumeroTelefono">               
			<label>Correo </label><input type="text" class="form-control" name="sCorreo">               
     		                                    
      </div>
    </div>
      
  		
  </div>
  <div class="tab-pane" id="contacto">
	<div class="row">  
	   <div class="col-lg-4">
     		<label>Nombre </label><input type="tel" class="form-control " name="sNombreContacto">      	    
            <label>Teléfono </label><input type="tel" class="form-control " name="sTelefonoContacto">
     		<label>Correo </label><input type="text" class="form-control" name="sCorreoContacto">                  		
      </div>
	 </div>      
  </div>



  <div class="tab-pane" id="contratos">
  <div class="row">  
      <div class="col-lg-4"> 
          <label>Adjunte Solicitud de Crédito</label>
                <div class="fileupload fileupload-exists" data-provides="fileupload" data-name="myimage">
                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                 <input type="text"  class="form-control" name="sSolicitudCredito" disabled="disabled" style=''> 
                 
                 </div>
                <div>
                 <span class="btn btn-default btn-file"><span class="fileupload-new">Selecciona</span><span class="fileupload-exists">Cargar</span><input id="uploadcontrato" type="file" name="files[]" data-url="clases/uploadcontratocliente.php" no_catalogo="no" multiple> </span>
                  <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" id="eliminacontrato">Eliminar</a>
                </div>
              </div>   
              
              <label>Adjunte IFE</label>
              <div class="fileupload fileupload-exists" data-provides="fileupload" data-name="myimage">
                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                
                  <input type="text" class="form-control" name="sIfe" disabled="disabled" style=''>
                  
                </div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Selecciona</span><span class="fileupload-exists">Cargar</span><input id="uploadife" type="file" name="files[]" data-url="clases/uploadifecliente.php" no_catalogo="no" multiple> </span>
                  <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" id="eliminarife">Eliminar</a>
                </div>
              </div> 

               <label>Adjunte Comprobante de domicilio</label>
              <div class="fileupload fileupload-exists" data-provides="fileupload" data-name="myimage">
                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                
                  <input type="text" class="form-control" name="sComprobante" disabled="disabled" style=''>
                  
                </div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Selecciona</span><span class="fileupload-exists">Cargar</span><input id="uploadcomprobante" type="file" name="files[]" data-url="clases/uploadcomprobantecliente.php" no_catalogo="no" multiple> </span>
                  <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" id="eliminacomprobante">Eliminar</a>
                </div>
              </div> 
              
              
       </div>
  <div class="col-lg-8"> 
  <div><label>Documentos:</label></div>
  <div id="descargacontrato"></div>
  <div id="descargaIfe"></div>
  <div id="descargacomprobante"></div>
  </div>
   </div>      
     
        
  </div>



  <div class="tab-pane" id="aval">
  <div class="row">  
      <div class="col-lg-4"> 
                       
              <label>Adjunte IFE</label>
              <div class="fileupload fileupload-exists" data-provides="fileupload" data-name="myimage">
                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                
                  <input type="text" class="form-control" name="sIfeAval" disabled="disabled" style=''>
                  
                </div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Selecciona</span><span class="fileupload-exists">Cargar</span><input id="uploadifeaval" type="file" name="files[]" data-url="clases/uploadifeclienteaval.php" no_catalogo="no" multiple> </span>
                  <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" id="eliminarifeaval">Eliminar</a>
                </div>
              </div> 

               <label>Adjunte Comprobante de domicilio</label>
              <div class="fileupload fileupload-exists" data-provides="fileupload" data-name="myimage">
                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                
                  <input type="text" class="form-control" name="sComprobanteAval" disabled="disabled" style=''>
                  
                </div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Selecciona</span><span class="fileupload-exists">Cargar</span><input id="uploadcomprobanteaval" type="file" name="files[]" data-url="clases/uploadcomprobanteclienteaval.php" no_catalogo="no" multiple> </span>
                  <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" id="eliminacomprobanteaval">Eliminar</a>
                </div>
              </div> 
              
              
       </div>
  <div class="col-lg-8"> 
  <div><label>Documentos:</label></div>
  <div id="descargaIfeaval"></div>
  <div id="descargacomprobanteaval"></div>
  </div>
   </div>      
     
        
  </div>


   <div class="tab-pane" id="credito">
  <div class="row">  
     <div class="col-lg-4">
        <label>Límite de crédito disponible </label><input type="number" class="form-control " name="fLimite">           
      </div>
      <div class="col-lg-8">
        <label>Cantidad máxima de prendas </label><input type="number" class="form-control " name="iCantidadPrendas">           
      </div>
   </div>  

    <div class="row">  
     <div class="col-lg-4">
        <label>Saldo Pendiente</label><input type="text" class="form-control " name="fSaldoPendiente">           
      </div>
      
   </div>   


   <? if ( strtolower($sUsuario[0])=='c' ) { ?>    
     <div class="row">  
     <div class="col-lg-4">
        <label>¿AUTORIZAR CRÉDITO? </label><select class="form-control " name="iCreditoAutorizado"><option value="0">NO</option><option value="1">SI</option></select>           
      </div>
      <div class="col-lg-8">
      </div>
   </div> 

   <? } ?>    


      
  </div>
  

  

</div>
</div>
<script src="js/jquery.ui.widget.js"></script>
<script src="js/jquery.iframe-transport.js"></script>
<script src="js/jquery.fileupload.js"></script>

<script>
$(document).ready(function () {

  var idEmpresa=null;
  $("body").on('click','input[name=iId]',function(){
    idEmpresa=$("input[name=iId]").val();
  var contrato=$("input[name=sSolicitudCredito]").val();
  var ife=$("input[name=sIfe]").val();
  var comprobante=$("input[name=sComprobante]").val();
  
  var ifeaval=$("input[name=sIfeAval]").val();
  var comprobanteaval=$("input[name=sComprobanteAval]").val();

  if(contrato!="")
  $("#descargacontrato").html("<a target='_blank' href='./contratos/"+contrato+"'>Descargar contrato</a>");
  if(ife!="")
  $("#descargaIfe").html("<a target='_blank' href='./comprobantes/"+ife+"'>Descargar IFE</a>");
  if(comprobante!="")
  $("#descargacomprobante").html("<a target='_blank' href='./comprobantes/"+comprobante+"'>Descargar comprobante de domicilio</a>");
  if(ifeaval!="")
  $("#descargaIfeaval").html("<a target='_blank' href='./comprobantes/"+ifeaval+"'>Descargar IFE de Aval</a>");
  if(comprobanteaval!="")
  $("#descargacomprobanteaval").html("<a target='_blank' href='./comprobantes/"+comprobanteaval+"'>Descargar comprobante de domicilio de Aval</a>");

  $("#eliminarife").attr("href","./clases/eliminar_ife.php?nombre="+ife+"&tipo=2"); 
  $("#eliminacontrato").attr("href","./clases/eliminar_contrato.php?nombre="+contrato);      
  $("#eliminacomprobante").attr("href","./clases/eliminar_comprobante.php?nombre="+comprobante+"&tipo=2"); 
 $("#eliminaifeaval").attr("href","./clases/eliminar_ife_aval.php?nombre="+contrato+"&tipo=2");      
  $("#eliminacomprobanteaval").attr("href","./clases/eliminar_comprobante_aval.php?nombre="+comprobante+"&tipo=2");    

  });



$("body").on('click','#eliminacontrato',function(e){
  e.preventDefault();
  var url=$(this).attr("href");
  $.get(url, function( data ) {
    $("input[name=sSolicitudCredito]").val("");
  });

});

$("body").on('click','#eliminacomprobante',function(e){
  e.preventDefault();
  var url=$(this).attr("href");
  $.get(url, function( data ) {
    $("input[name=sComprobante]").val("");
  });

});

$("body").on('click','#eliminarife',function(e){
  e.preventDefault();
  var url=$(this).attr("href");
  $.get(url, function( data ) {
    $("input[name=sIfe]").val("");
  });

});

$("body").on('click','#eliminacomprobanteaval',function(e){
  e.preventDefault();
  var url=$(this).attr("href");
  $.get(url, function( data ) {
    $("input[name=sComprobanteAval]").val("");
  });

});

$("body").on('click','#eliminarifeaval',function(e){
  e.preventDefault();
  var url=$(this).attr("href");
  $.get(url, function( data ) {
    $("input[name=sIfeAval]").val("");
  });

});


  
  $('#uploadcomprobante').bind('fileuploadsubmit', function (e, data) {
    data.formData = {idEmpresa: idEmpresa};
 }); 
  
  $('#uploadcomprobante').fileupload({
    dataType: 'json',
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        $("input[name=sComprobante]").val(file.name);
        $("#eliminacomprobante").attr("href","./clases/eliminar_comprobante.php?nombre="+file.name+"&tipo=2");
      });
    }
  });
  
  $('#uploadcontrato').bind('fileuploadsubmit', function (e, data) {
    data.formData = {idEmpresa: idEmpresa};
 }); 

   $('#uploadcontrato').fileupload({
    dataType: 'json',
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        $("input[name=sSolicitudCredito]").val(file.name);
        $("#eliminacontrato").attr("href","./clases/eliminar_contrato.php?nombre="+file.name);
      });
    }
  });


  $('#uploadife').bind('fileuploadsubmit', function (e, data) {
    data.formData = {idEmpresa: idEmpresa};
 }); 

   $('#uploadife').fileupload({
    dataType: 'json',
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        $("input[name=sIfe]").val(file.name);
        $("#eliminarife").attr("href","./clases/eliminar_ife.php?nombre="+file.name+"&tipo=2");
      });
    }
  });
  

$('#uploadifeaval').bind('fileuploadsubmit', function (e, data) {
    data.formData = {idEmpresa: idEmpresa};
 }); 

   $('#uploadifeaval').fileupload({
    dataType: 'json',
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        $("input[name=sIfeAval]").val(file.name);
        $("#eliminarifeaval").attr("href","./clases/eliminar_ife_aval.php?nombre="+file.name+"&tipo=2");
      });
    }
  });


  $('#uploadcomprobanteaval').bind('fileuploadsubmit', function (e, data) {
    data.formData = {idEmpresa: idEmpresa};
 }); 
  
  $('#uploadcomprobanteaval').fileupload({
    dataType: 'json',
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        $("input[name=sComprobanteAval]").val(file.name);
        $("#eliminacomprobanteaval").attr("href","./clases/eliminar_comprobante_aval.php?nombre="+file.name+"&tipo=2");
      });
    }
  });

var re = /^[0-9A-z]*$/;
$("input[name=sRFC]").on("change",function(){
	var elemento=$(this);
	var valor=$(this).val();
	if (!re.test(valor)) {
	$(elemento).error('Solo se pueden utilizar números y letras','right');
	$("input[name=sRFC]").val("");
 	} 
});

});
</script>




