<?

$_tabla = "cliente";
$_formulario = "./general/clientes/formulario.php";
$_pre_agregar = "./general/clientes/verifica_existencias.php";
$_post_agregar = "";
?>
<div class="row">
  <div class="col-lg-6">
	  	<button type="button" class="btn btn-primary" id="nuevo" titulo='Agregar <?=$_tabla;?>' formulario='<?=$_formulario;?>' tabla='<?=$_tabla;?>'>Agregar <?=$_tabla;?></button>
     	<button type="button" class="btn btn-primary" id="importar" titulo='Importar <?=$_tabla;?>' pre_agregar='<?=$_pre_agregar;?>' post_agregar='<?=$_post_agregar;?>' formulario='<?=$_formulario;?>' tabla='<?=$_tabla;?>'>Importar <?=$_tabla;?></button>       
   		<div class="btn-group">
			<button type="button" class="btn btn-default">Otras Opciones</button>
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		    <span class="caret"></span>
	    	<span class="sr-only">-</span>
			</button>
			<ul class="dropdown-menu" role="menu"> 
				<li class="divider"></li>                
				<li><a href="#" id='eliminar-seleccionados' tabla='<?=$_tabla;?>'>Eliminar seleccionados</a></li>    

			</ul>
		</div>
	</div> 
	<? include("./clases/exportacion.php"); ?>
</div>

<div class="row">
    <div class="col-lg-12">    		
      	<div id="div-tabla"></div>    
	</div>
</div>        

<script>
$(document).ready(function () {

var genera_tabla = {
  columnas: [ 
    {columna:'sNombre', titulo: 'Nombre',class:false,widthpdf:'23%',visible:true},
    {columna:'sCalle', titulo: 'Dirección',class:"hidden-xs",widthpdf:'28%',visible:true},
    {columna:'sNumeroExterior', titulo: 'No. Ext.',class:false,widthpdf:'13%',visible:true},
    {columna:'sNumeroTelefono', titulo: 'Teléfono',class:false,widthpdf:'10%',visible:true},	
    {columna:'fLimite', titulo: 'Límite Crédito',class:false,label:"numero",widthpdf:'15%',visible:true},	
    {columna:'dRegistro', titulo: 'Registro',class:"hidden-xs",widthpdf:'10%',visible:true},
    {columna:'iEstatus', titulo: 'Estatus',class:"hidden-xs",widthpdf:'20%',visible:false,label:"avanzado"}				
   ]   
};
 
$("#div-tabla").crear_tabla('<?=$_formulario;?>','iId','<?=$_tabla;?>',genera_tabla,"iEstatus=0 and iUsuarioEmpresaId=<?=$iUsuarioEmpresaId;?>",false,false,'<?=$_post_agregar;?>');

$('#ventana-emergente').on('hidden.bs.modal', function (e) {
	$("#div-tabla").crear_tabla('<?=$_formulario;?>','iId','<?=$_tabla;?>',genera_tabla,"iEstatus=0 and iUsuarioEmpresaId=<?=$iUsuarioEmpresaId;?>",false,true,'<?=$_post_agregar;?>');	
});
$("body").on("click","#importar",function(){ 
	var titulo=$(this).attr("titulo");
	var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>"
		+"<button type='button' class='btn btn-disabled' data-dismiss='modal' id='importarClientes' >Guardar</button>";
	$.post("./general/clientes/importar.php",null,function(data){				
    	vermodal(titulo,data,botones);
	});
});

});

</script>
