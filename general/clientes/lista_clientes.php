<?
include("../../acceso/seguridad.php");
$_tabla = "cliente";
$_formulario = "./general/clientes/formulario.php";
?>
<div class="row">
  	<div class="col-lg-6 col-xs-6 col-md-6 col-sm-6">	  	
	  	<button type="button" class="btn btn-primary" id="nuevo" titulo='Agregar Nuevo <?=$_tabla;?>' formulario='<?=$_formulario;?>' tabla='<?=$_tabla;?>' retorna_id="si">Agregar <?=$_tabla;?></button>
	</div> 
	<? 
	$_REQUEST['sin_exportar'] = "sin";
	include("../../clases/exportacion.php"); ?>
</div>

<div class="row">
    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">    		
      	<div id="div-tabla-lista"></div>    
	</div>
</div>        

<script>
$(document).ready(function () {



var genera_tabla = {
  columnas: [ 
    {columna:'iFolio', titulo: 'No. Cliente',widthpdf:'10%',visible:true},
    {columna:'sNombre', titulo: 'Nombre',class:false,widthpdf:'17%',visible:true},
    {columna:'sRazonSocial', titulo: 'Razon Social',class:"hidden-xs",widthpdf:'17%',visible:true}    
   ]   
};
 
$("#div-tabla-lista").crear_tabla_lista('<?=$_tabla;?>',"iId",genera_tabla,"iEstatus=0 and iUsuarioEmpresaId=<?=$iUsuarioEmpresaId;?>",false);

});

</script>
