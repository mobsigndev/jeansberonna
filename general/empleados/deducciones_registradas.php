<? 
include("../../acceso/seguridad.php");
$iEmpleadoId = $_REQUEST['iEmpleadoId'];
?>
 	<table class="table table-bordered table-striped" id="tabla_licencias">
              <thead>
                <th class="hidden-xs">
                	Clave
                </th>
                <th>
                	Concepto
                </th>
                <th>
                	Importe Gravado
                </th>
                <th class="hidden-xs">
                	Importe Exento
                </th>
               
                <th>
                	Opciones
                </th></thead>
		       <tbody>  
               <?
               $total=0;
$datos = seleccionar("empleadodeduccion","empleadodeduccion.iTipoDeduccionId,empleadodeduccion.sTipo,empleadodeduccion.iId,
										empleadodeduccion.fImporteGravadoDeduccion,
										empleadodeduccion.fImporteExentoDeduccion","empleadodeduccion.iEstatus=0 and empleadodeduccion.iEmpleadoId='".$iEmpleadoId."'",false, false);
			   foreach($datos as $renglones) {
			   	$dato =  return_($renglones['sTipo'],$renglones['iTipoDeduccionId']);
				   echo '<tr>';
					echo '<td class="hidden-xs">'.$dato[0].'</td>';
					echo '<td>'.$dato[1].'</td>';
					echo '<td>'.number_format($renglones['fImporteGravadoDeduccion'],2).'</td>';
					echo '<td class="hidden-xs">'.number_format($renglones['fImporteExentoDeduccion'],2).'</td>';
					echo '<td>';
					?>
			          <a href="<?=$renglones['iId'];?>" class='btn btn-danger btn-xs accion_cancelar_deducciones'>Cancelar</a>
					<?
					echo '</td>';										
															
				   echo '</tr>';
				   $total+=$renglones['fImporteGravadoDeduccion']+$renglones['fImporteExentoDeduccion'];
			   }
			      function return_($tipo,$id){
			   		if($tipo=="default") {
				   		$datos = seleccionar("tipodeduccion","tipodeduccion.sClave,tipodeduccion.sDescripcion","tipodeduccion.iEstatus=0 and tipodeduccion.iId='".$id."'",false, false);
				   		return array($datos[0]['sClave'],$datos[0]['sDescripcion']);
			   		}
			   		else{
			   			$datos = seleccionar("tipopercepciondeduccionotros","sClave,sNombre","iEstatus=0 and iTipo=2 and iId='".$id."'",false, false);
				   		return array($datos[0]['sClave'],$datos[0]['sNombre']);
			   		}

			   	}
			   ?>                             
               </tbody>
               </table>
                             
   <h2><label class="label label-success">Total Deducciones $<?=number_format($total,2);?></label></h2>