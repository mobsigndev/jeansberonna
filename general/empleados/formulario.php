<? include("../../acceso/seguridad.php"); ?>

<div id='ventana-formulario'>
<!-- Nav tabs -->
<ul class="nav nav-pills">
  <li class='active'><a href="#general" data-toggle="tab">General</a></li>
   <li><a href="#permisos" data-toggle="tab">Permiso a Módulos</a></li>

</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="general">
  <input type="text" class="form-control" name="dRegistro" value="CURRENT_DATE()" style='display:none;'>
  <input type="text" class="form-control" name="iUsuarioEmpresaId" value="<?=$iUsuarioEmpresaId;?>" style='display:none;'>
  <input type="text" class="form-control" name="iId" style='display:none;'>
	<div class="row">
	 
      <div class="col-lg-4">
     		<label>Nombre</label> <span><input obligatorio='si' type="text" class="form-control" name="sNombre"></span>            
     		<label>Ap. Paterno</label> <input type="text" class="form-control" name="sPaterno">		     			
            <label>Ap. Materno</label> <input type="text" class="form-control" name="sMaterno">                        
      </div>
      
       <div class="col-lg-4">     		
       		<label>Dirección</label><input type="text" class="form-control" name="sDireccion"> 
            <label>Teléfono</label><input type="tel" class="form-control" name="sTelefono">           
			<label>Correo</label> <input type="email" class="form-control" name="sCorreo">  
       </div>
         
     	<div class="col-lg-4">     	
          	<label>Código de Empleado</label><input type="text" class="form-control" name="iCodigo" disabled="disabled">
       		<label>Clave de Acceso al Sistema</label><input type="text" class="form-control" name="sClave"> 
     	</div>         
	 
    </div>
  </div> 
  
  <div class="tab-pane" id="nomina">
	<div class="row">  
	   <div class="col-lg-4">
		<label>*RFC</label><input type="text" class="form-control" name="sRfc"> 
        <label>*Estado</label><select class="form-control" name="sEstado"> <option value="Activo">Activo</option> <option value="Baja">Baja</option> </select>
		<label>Registro Patronal</label> <input type="text" class="form-control" name="sRegistroPatronal">  
        <label>*No. de empleado</label> <input type="text" class="form-control" name="sNumeroEmpleado">  	         		
      </div> 
      <div class="col-lg-4">
		<label>*CURP</label><input type="text" class="form-control" name="sCurp"> 
        <label>Tipo de r&eacute;gimen</label> 
                <select class="form-control" name='iTipoRegimen'>
                <?
                $select = seleccionar("regimencontratacion","sDescripcion,iId","iEstatus=0",false, false);
                foreach($select as $dato) {
                ?>
                <option value="<?=$dato['iId'];?>"><?=$dato['sDescripcion'];?></option>
                <?
                }
                ?>
                </select>
        <label>No. Seguro Social</label> <input type="text" class="form-control" name="sSeguroSocial">  
        <label>Departamento</label> <input type="text" class="form-control" name="sDepartamento">  
		</div> 
        
        <div class="col-lg-4">
            <label>CLABE</label><input type="text" class="form-control" name="sClabe"> 
            <label>Banco</label> 
                <select class="form-control" name='iBanco'>
                <?
                $select = seleccionar("banco","sNombre,iId","iEstatus=0",false, false);
                foreach($select as $dato) {
                ?>
                <option value="<?=$dato['iId'];?>"><?=$dato['sNombre'];?></option>
                <?
                }
                ?>
                </select>         
            <label>Fecha de Inicio Laboral:</label> <input type="text" data-date-format="dd/mm/yyyy" size="14" class="form-control" name="dFechaInicioLaboral">  
            <label>Puesto</label> <input type="text" class="form-control" name="sPuesto">  	         		
        </div>                        
		
        <div class="col-lg-4">
            <label>Tipo de Contrato</label><input type="text" class="form-control" name="sTipoContrato"> 
            <label>Tipo de Jornada</label><input type="text" class="form-control" name="sTipoJornada">	         		
        </div>
        
        <div class="col-lg-4">
            <label>*Periodicidad de Pago</label> <select class="form-control" name="sPeriodicidadPago"> <option value="Semanal">Semanal</option><option value="Catorcenal">Catorcenal</option> <option value="Quincenal">Quincenal</option><option value="Mensual">Mensual</option> </select>
            <label>Salario Base Cot Apor</label> <input type="number" class="form-control" name="fSalarioBase">  	         		
        </div>
       
        <div class="col-lg-4">
             
            <label>Riesgo del Puesto</label> 
                <select class="form-control" name='iRiesgoPuesto'>
                <?
                $select = seleccionar("riesgopuesto","sDescripcion,iId","iEstatus=0",false, false);
                foreach($select as $dato) {
                ?>
                <option value="<?=$dato['iId'];?>"><?=$dato['sDescripcion'];?></option>
                <?
                }
                ?>
                </select>         
            <label>Salario Diario Integrado:</label> <input type="number" class="form-control" name="fSalarioDiarioIntegrado">  
                  		
        </div>       
        	         		
      </div> 
      </div> 
      
      <div class="tab-pane" id="percepciones">
		<div class="row">  
           <div class="col-lg-4">
            <label>Tipo de Percepci&oacute;n</label> 
                    <select class="form-control" name='iTipoPercepcionId'  no_catalogo="no">
                 
                    </select> 	         		
          </div>
           <div class="col-lg-4">
              <label>Importe Gravado</label><input type="number" class="form-control" no_catalogo="no" name="fImporteGravadoPercepcion"> 
          </div>
        
          <div class="col-lg-4">
              <label>Importe Exento</label><input type="number" class="form-control" no_catalogo="no" name="fImporteExentoPercepcion"> 
          </div>

      </div>

      <div class="row">
          <div class="col-lg-8"></div>
            <div class="col-lg-4" style='padding-top:5px;'>  
  		                      
                  <button type="button" class="btn btn-success" id="agregar_percepcion"  data-toggle="tooltip" titulo='Agregar Percepci&oacute;n'>Agregar Percepci&oacute;n</button>
            </div>
      </div>
      		
            <div class="row">         
               <div class="col-lg-12">
                    <label style='margin-top:15px;'>Percepciones registradas de empleado</label>       
                  <div id="percepciones_registradas">
                    </div>
               </div>
            </div>
            
	        
  	</div>
    
    
    <div class="tab-pane" id="deducciones">
		<div class="row">  
           <div class="col-lg-4">
            <label>Tipo de Deducci&oacute;n</label> 
                    <select class="form-control" name='iTipoDeduccionId' no_catalogo="no">
                  
                    </select> 	         		
          </div>
           <div class="col-lg-4">
              <label>Importe Gravado</label><input type="number" class="form-control" no_catalogo="no" name="fImporteGravadoDeduccion"> 
          </div>
        
          <div class="col-lg-4">
              <label>Importe Exento</label><input type="number" class="form-control" no_catalogo="no" name="fImporteExentoDeduccion"> 
          </div>
         </div>
          <div class="row">
          <div class="col-lg-8"></div>
          <div class="col-lg-4" style="padding-top:5px;">  
		                  
                <button type="button" class="btn btn-success" id="agregar_deduccion" no_catalogo="no" data-toggle="tooltip" titulo='Agregar Deducci&oacute;n'>Agregar Deducci&oacute;n</button>
          </div>
        </div>
      	<div class="row">         
               <div class="col-lg-12">
                    <label style='margin-top:15px;'>Deducciones registradas de empleado</label>       
                  <div id="deducciones_registradas">
                    </div>
               </div>
            </div>
      	
	        
  	</div>
    
    
    <div class="tab-pane" id="incapacidad">
		<div class="row">  
           <div class="col-lg-4">
            <label>Tipo de Incapacidad</label> 
                    <select class="form-control" name='iTipoIncapacidadId' no_catalogo="no">
                    <?
                    $select = seleccionar("tipoincapacidad","sDescripcion,iId","iEstatus=0",false, false);
                    foreach($select as $dato) {
                    ?>
                    <option value="<?=$dato['iId'];?>"><?=$dato['sDescripcion'];?></option>
                    <?
                    }
                    ?>
                    </select> 	         		
          </div>
           <div class="col-lg-4">
              <label>D&iacute;as</label><input type="number" class="form-control" no_catalogo="no" name="fDiasIncapacidad"> 
          </div>
        
          <div class="col-lg-4">
              <label>Descuento</label><input type="number" class="form-control" no_catalogo="no" name="fDescuento"> 
          </div>
           </div>
           <div class="row">
           <div class="col-lg-8"></div>
          <div class="col-lg-4" style="padding-top:5px;">  
		                  
                <button type="button" class="btn btn-success" id="agregar_incapacidad" data-toggle="tooltip" titulo='Agregar Incapacidad'>Agregar Incapacidad</button>
          </div>
          </div>
      
      			<div class="row">         
               <div class="col-lg-12">
                    <label style='margin-top:15px;'>Incapacidades registradas de empleado</label>       
                  <div id="incapacidades_registradas">
                    </div>
               </div>
            </div>
            
	       
  	</div>
    
    
     <div class="tab-pane" id="horas">
		<div class="row">  
           <div class="col-lg-4">
            <label>Tipo de Horas</label> 
                    <select class="form-control" name='sTipoHoras' no_catalogo="no" obligatorio="si">
                        <option value="Dobles">Dobles</option>
                        <option value="Triples">Triples</option> 
                    </select>         		 
          </div>
          </div>
          <div class="row">
           <div class="col-lg-4">
              <label>D&iacute;as</label><input type="number" class="form-control" no_catalogo="no" name="iDias"> 
          </div>
        
          <div class="col-lg-4">
              <label>Horas Extra</label><input type="number" class="form-control" no_catalogo="no" name="iHorasExtra"> 
          </div>
          <div class="col-lg-4">
              <label>Importe Pagado</label><input type="number" class="form-control" no_catalogo="no" name="fImportePagado"> 
          </div>
          </div>
          <div class="row">
          <div class="col-lg-8"></div>
          <div class="col-lg-4" style="padding-top:5px;">  
		                   
                <button type="button" class="btn btn-success" id="agregar_horas" data-toggle="tooltip" titulo='Agregar Horas Extra'>Agregar Horas Extra</button>
          </div>
          
          </div>
          <div class="row">         
               <div class="col-lg-12">
                    <label style='margin-top:15px;'>Tipos de horas  registradas</label>       
                  <div id="horas_registradas">
                    </div>
               </div>
            </div>
      
	        
  	</div>
   
   <div class="tab-pane" id="direccion">
   <div class="row">
	 
      <div class="col-lg-4">
     		<label>Calle</label> <span><input type="text" class="form-control" name="sCalle"></span>            
     		<label>Numero Interior</label> <input type="text" class="form-control" name="sNumeroInterior">		     			
            <label>Numero Exterior</label> <input type="text" class="form-control" name="sNumeroExterior">                        
      </div>
      
       <div class="col-lg-4">     		
       		<label>Colonia</label><input type="text" class="form-control" name="sColonia">
            <label>Codigo Postal</label><input type="text" class="form-control" name="sCodigoPostal"> 
            <label>Municipio</label><input type="text" class="form-control" name="sMunicipio">           
			
       </div>
         
     	<div class="col-lg-4">     	
          	<label>Ciudad</label><input type="text" class="form-control" name="sCiudad" >
       		<label>Estado</label> <input type="text" class="form-control" name="sEstadoD">  
            <label>*Pais</label><input type="text" class="form-control" name="sPais"> 
     	</div>         
	 
    </div>
    </div>
    


    
 <div class="tab-pane" id="permisos">
  <div class="row">  
     <div class="col-lg-8">
        <label>Seleccione Permisos para asignar</label> 
                <select class="form-control" name='iModuloId' no_catalogo="no">
                <?
                $select = seleccionar("modulo","sTituloModulo,iId","iEstatus=0",false, false);
                foreach($select as $dato) {
                ?>
                <option value="<?=$dato['iId'];?>"><?=$dato['sTituloModulo'];?></option>
                <?
                }
                ?>
                </select>
    </div>                 
    <div class="col-lg-4">  
            <label style='color:#fff;'>-</label>              
                <button type="button" class="btn btn-success" id="asignar_modulo" data-toggle="tooltip" titulo='Asignar Modulo'>Asignar</button>
                
      </div>                  
   </div>      
     <div class="row">         
     <div class="col-lg-12">
      <label style='margin-top:15px;'>Modulos asignados a usuario</label>       
          <div id="modulos_asignados">
      </div>
       </div>
     </div>
     
  </div>
  

</div><!-- tab-content -->
</div><!-- ventana-formulario --> 
<script>
$(document).ready(function () {
	 $('[name="dFechaInicioLaboral"]').datepicker({
			  todayBtn: "linked",
	  language: "es",   	 
	  autoclose: true,
	  todayHighlight: true
	});
   

  function RFCValida(rfcStr) {
  var strCorrecta;
  strCorrecta = rfcStr; 
  if (rfcStr.length == 12){
  var valid = '^(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
  }else{
  var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
  }
  var validRfc=new RegExp(valid);
  var matchArray=strCorrecta.match(validRfc);
  if (matchArray==null) {

    return false;
  }
  else
  {
    return true;
  }
  
}


    $('[name="sRfc"]').change(function(){
      var rfc=$(this).val();
      var valor = RFCValida(rfc);
      if(valor){
       
      }else{
        $(this).error('RFC Incorrecto');
        $(this).val('');
      }
    });

    $('[name="sCurp"]').change(function(){
      var curp=$(this).val();
      if(curp.match(/^([a-z]{4})([0-9]{6})([a-z]{6})([0-9]{2})$/i)){//AAAA######AAAAAA##
       
      }else{
        $(this).error('CURP Incorrecta');
        $(this).val('');
      }
    });



});
</script>