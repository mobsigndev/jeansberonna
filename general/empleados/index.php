<?
$_tabla = "empleado";
$_formulario = "./general/empleados/formulario.php";
$_pre_agregar = "./general/empleados/verifica_existencias.php";
$_post_agregar = "./general/empleados/genera_codigo.php";
?>
<div class="row">
  <div class="col-lg-6">
	  	<button type="button" class="btn btn-primary" id="nuevo" titulo='Agregar <?=$_tabla;?>' pre_agregar='<?=$_pre_agregar;?>' post_agregar='<?=$_post_agregar;?>' formulario='<?=$_formulario;?>' tabla='<?=$_tabla;?>' retorna_id="si">Agregar <?=$_tabla;?></button>
    
   		<div class="btn-group">
			<button type="button" class="btn btn-default">Otras Opciones</button>
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		    <span class="caret"></span>
	    	<span class="sr-only">-</span>
			</button>
			<ul class="dropdown-menu" role="menu"> 
				<li><a href="#" id='eliminar-seleccionados' tabla='<?=$_tabla;?>'>Eliminar seleccionados</a></li>    
			</ul>
		</div>
	</div> 
	<? include("./clases/exportacion.php"); ?>
</div>

<div class="row">
    <div class="col-lg-12">    		
      	<div id="div-tabla"></div>    
	</div>
</div>        

<script>
$(document).ready(function () {

	$("body").on("click","#id_de_retorno",function(e){
			var iEmpleadoId = $("#id_de_retorno").val();
			if($(this).attr("tipo")=="empleado"){
				$("#ventana-formulario li.hide").addClass("show");
				$("#ventana-formulario [name='iId']").val(iEmpleadoId);
			}
				
			
		}); 

var genera_tabla = {
  columnas: [ 
    {columna:'iCodigo', titulo: 'Código',class:false,widthpdf:'13%',visible:true},
    {columna:'sNombre', titulo: 'Nombre',class:false,widthpdf:'15%',visible:true},
    {columna:'sPaterno', titulo: 'Ap. Paterno',class:"hidden-xs",widthpdf:'20%',visible:true},
    {columna:'sMaterno', titulo: 'Ap. Materno',class:"hidden-xs",widthpdf:'15%',visible:true},	
    {columna:'sCorreo', titulo: 'Correo',class:"hidden-xs",widthpdf:'15%',visible:true},	
    {columna:'sTelefono', titulo: 'Telefono',class:false,widthpdf:'12%',visible:true}	
   ]   
};

$("#div-tabla").crear_tabla('<?=$_formulario;?>','iId','<?=$_tabla;?>',genera_tabla,"iEstatus=0 and iUsuarioEmpresaId=<?=$iUsuarioEmpresaId;?>",false,false,'<?=$_post_agregar;?>');

$('#ventana-emergente').on('hidden.bs.modal', function (e) {
	$("#div-tabla").crear_tabla('<?=$_formulario;?>','iId','<?=$_tabla;?>',genera_tabla,"iEstatus=0 and iUsuarioEmpresaId=<?=$iUsuarioEmpresaId;?> ",false,true,'<?=$_post_agregar;?>');	
});


$("body").on("click","#agregar_percepcion",function(){
	var elemento = $(this);
	$(elemento).cargando("Agregando percepcion");
	var iTipoPercepcionId=$("[name='iTipoPercepcionId']").val();
	var sTipo=$("[name='iTipoPercepcionId']  option:selected").attr("tipo");
	var iEmpleadoId=$("[name='iId']").val();
	var fImporteGravadoPercepcion=$("[name='fImporteGravadoPercepcion']").val();
	var fImporteExentoPercepcion=$("[name='fImporteExentoPercepcion']").val();
	$.post("./general/empleados/asigna_percepcion.php",{sTipo:sTipo,iEmpleadoId:iEmpleadoId,iTipoPercepcionId:iTipoPercepcionId,fImporteGravadoPercepcion:fImporteGravadoPercepcion,fImporteExentoPercepcion:fImporteExentoPercepcion},function(data){
		data = eval(data);
		if(data[1]>0) {
			$(elemento).correcto(data[0]);	
			$.post("./general/empleados/percepciones_registradas.php",{iEmpleadoId:iEmpleadoId},function(data){
				$("#percepciones_registradas").html(data);	
			});			
		}
		else
		$(elemento).error(data[0]);	
	});
});

$("body").on("click","#agregar_deduccion",function(){
	var elemento = $(this);
	$(elemento).cargando("Agregando deduccion");
	var iTipoDeduccionId=$("[name='iTipoDeduccionId']").val();
	var iEmpleadoId=$("[name='iId']").val();
	var fImporteGravadoDeduccion=$("[name='fImporteGravadoDeduccion']").val();
	var fImporteExentoDeduccion=$("[name='fImporteExentoDeduccion']").val();
	var sTipo=$("[name='iTipoDeduccionId'] option:selected").attr("tipo");
	$.post("./general/empleados/asigna_deduccion.php",{sTipo:sTipo,iEmpleadoId:iEmpleadoId,iTipoDeduccionId:iTipoDeduccionId,fImporteGravadoDeduccion:fImporteGravadoDeduccion,fImporteExentoDeduccion:fImporteExentoDeduccion},function(data){
		data = eval(data);
		if(data[1]>0) {
			$(elemento).correcto(data[0]);	
			$.post("./general/empleados/deducciones_registradas.php",{iEmpleadoId:iEmpleadoId},function(data){
				$("#deducciones_registradas").html(data);	
			});			
		}
		else
		$(elemento).error(data[0]);	
	});
});

$("body").on("click","#agregar_incapacidad",function(){
	var elemento = $(this);
	$(elemento).cargando("Agregando incapacidad");
	var iTipoIncapacidadId=$("[name='iTipoIncapacidadId']").val();
	var iEmpleadoId=$("[name='iId']").val();
	var fDiasIncapacidad=$("[name='fDiasIncapacidad']").val();
	var fDescuento=$("[name='fDescuento']").val();
	$.post("./general/empleados/asigna_incapacidad.php",{iEmpleadoId:iEmpleadoId,iTipoIncapacidadId:iTipoIncapacidadId,fDiasIncapacidad:fDiasIncapacidad,fDescuento:fDescuento},function(data){
		data = eval(data);
		if(data[1]>0) {
			$(elemento).correcto(data[0]);	
			$.post("./general/empleados/incapacidades_registradas.php",{iEmpleadoId:iEmpleadoId},function(data){
				$("#incapacidades_registradas").html(data);	
			});			
		}
		else
		$(elemento).error(data[0]);	
	});
});


$("body").on("click","#agregar_horas",function(){
	var elemento = $(this);
	$(elemento).cargando("Agregando horas extra");
	var sTipoHoras=$("[name='sTipoHoras']").val();
	var iEmpleadoId=$("[name='iId']").val();
	var iDias=$("[name='iDias']").val();
	var iHorasExtra=$("[name='iHorasExtra']").val();
	var fImportePagado=$("[name='fImportePagado']").val();
	$.post("./general/empleados/asigna_horas.php",{iEmpleadoId:iEmpleadoId,sTipoHoras:sTipoHoras,iDias:iDias,iHorasExtra:iHorasExtra,fImportePagado:fImportePagado},function(data){
		data = eval(data);
		if(data[1]>0) {
			$(elemento).correcto(data[0]);	
			$.post("./general/empleados/horas_registradas.php",{iEmpleadoId:iEmpleadoId},function(data){
				$("#horas_registradas").html(data);	
			});			
		}
		else
		$(elemento).error(data[0]);	
	});
});


$("body").on("click","#asignar_modulo",function(){
	var elemento = $(this);
	$(elemento).cargando("Asignando Modulo");
	var iModuloId=$("[name='iModuloId']").val();
	var iEmpleadoId=$("[name='iId']").val();
	$.post("./general/empleados/asigna_modulo.php",{iEmpleadoId:iEmpleadoId,iModuloId:iModuloId},function(data){
		data = eval(data);
		if(data[1]>0) {
			$(elemento).correcto(data[0]);	
			$.post("./general/empleados/modulos_asignados.php",{iEmpleadoId:iEmpleadoId},function(data){
				$("#modulos_asignados").html(data);	
			});			
		}
		else
		$(elemento).error(data[0]);	
	});
});
function carga_deducciones(id_default){
	$.post("./general/empleados/lista_tipodeduccion.php",{id_default:id_default},function(data){
		$("[name='iTipoDeduccionId']").html(data);	
	});
}
function carga_percepciones(id_default){
	$.post("./general/empleados/lista_tipopercepcion.php",{id_default:id_default},function(data){
		$("[name='iTipoPercepcionId']").html(data);	
	});
}
$("body").on("click","[name='iId']",function(){
	var iEmpleadoId = $(this).val();
	$(".nomina_").show();
	$("#ventana-formulario [name='iId']").val(iEmpleadoId);

	carga_percepciones(0);
	carga_deducciones(0);

	$.post("./general/empleados/modulos_asignados.php",{iEmpleadoId:iEmpleadoId},function(data){
		$("#modulos_asignados").html(data);	
	});
	
	$.post("./general/empleados/percepciones_registradas.php",{iEmpleadoId:iEmpleadoId},function(data){
		$("#percepciones_registradas").html(data);	
	});

	$.post("./general/empleados/deducciones_registradas.php",{iEmpleadoId:iEmpleadoId},function(data){
		$("#deducciones_registradas").html(data);	
	});

	$.post("./general/empleados/horas_registradas.php",{iEmpleadoId:iEmpleadoId},function(data){
		$("#horas_registradas").html(data);	
	});

	$.post("./general/empleados/incapacidades_registradas.php",{iEmpleadoId:iEmpleadoId},function(data){
		$("#incapacidades_registradas").html(data);	
	});

});

	$("body").on("click",".accion_cancelar_percepcion",function(){
	var iEmpleadoPercepcionId = $(this).attr("href");
	var elemento = $(this);
	$(elemento).confirmar("Seguro que desea cancelar percepcion","Si","No",function(respuesta){
	if(respuesta) {
		if(iEmpleadoPercepcionId!="0") {
			$.post("./general/empleados/cancela_percepcion.php",{iEmpleadoPercepcionId:iEmpleadoPercepcionId},function(data){	
				var iEmpleadoId=$("[name='iId']").val();
				$.post("./general/empleados/percepciones_registradas.php",{iEmpleadoId:iEmpleadoId},function(data){
					$("#percepciones_registradas").html(data);	
				});		
			});		
		}else console.log("no hay percepcion seleccionada");
	}
	});
	return false;
 
});
$("body").on("click",".accion_cancelar",function(){
	var iModuloEmpleadoId = $(this).attr("href");
	var elemento = $(this);
	$(elemento).confirmar("Seguro que desea cancelar permiso a modulo","Si","No",function(respuesta){
	if(respuesta) {
		if(iModuloEmpleadoId!="0") {
			$(elemento).cargando("cancelando...");
			$.post("./general/empleados/cancela_modulo.php",{iModuloEmpleadoId:iModuloEmpleadoId},function(data){	
				var iEmpleadoId=$("[name='iId']").val();
				$.post("./general/empleados/modulos_asignados.php",{iEmpleadoId:iEmpleadoId},function(data){
					$("#modulos_asignados").html(data);	
				});		
			});		
		}else console.log("no hay modulos seleccionados");
	}
	});
	return false;

});
	$("body").on("click",".accion_cancelar_deducciones",function(){
		var iEmpleadoDeduccionId = $(this).attr("href");
		var elemento = $(this);
		$(elemento).confirmar("Seguro que desea cancelar deduccion","Si","No",function(respuesta){
		if(respuesta) {
			if(iEmpleadoDeduccionId!="0") {
				$.post("./general/empleados/cancela_deduccion.php",{iEmpleadoDeduccionId:iEmpleadoDeduccionId},function(data){	
					var iEmpleadoId=$("[name='iId']").val();
					$.post("./general/empleados/deducciones_registradas.php",{iEmpleadoId:iEmpleadoId},function(data){
						$("#deducciones_registradas").html(data);	
					});		
				});		
			}else console.log("no hay deduccion seleccionada");
		}
		});
		return false;
	
	});
	
	$("body").on("click",".accion_cancelar_incapacidades",function(){
		var iEmpleadoIncapacidadId = $(this).attr("href");
		var elemento = $(this);
		$(elemento).confirmar("Seguro que desea cancelar incapacidad","Si","No",function(respuesta){
		if(respuesta) {
			if(iEmpleadoIncapacidadId!="0") {
				$.post("./general/empleados/cancela_incapacidad.php",{iEmpleadoIncapacidadId:iEmpleadoIncapacidadId},function(data){	
					var iEmpleadoId=$("[name='iId']").val();
					$.post("./general/empleados/incapacidades_registradas.php",{iEmpleadoId:iEmpleadoId},function(data){
						$("#incapacidades_registradas").html(data);	
					});		
				});		
			}else console.log("no hay incapacidad seleccionada");
		}
		});
		return false;
	
	});
	
	$("body").on("click",".accion_cancelar_horas",function(){
		var iEmpleadoHorasId = $(this).attr("href");
		var elemento = $(this);
		$(elemento).confirmar("Seguro que desea cancelar tipo de horas","Si","No",function(respuesta){
		if(respuesta) {
			if(iEmpleadoHorasId!="0") {
				$.post("./general/empleados/cancela_hora.php",{iEmpleadoHorasId:iEmpleadoHorasId},function(data){	
					var iEmpleadoId=$("[name='iId']").val();
					$.post("./general/empleados/horas_registradas.php",{iEmpleadoId:iEmpleadoId},function(data){
						$("#horas_registradas").html(data);	
					});		
				});		
			}else console.log("no hay hora seleccionada");
		}
		});
		return false;
	
	});

	//Registrar nuevos percepciones o deducciones en "OTROS"
$("body").on("change","[name='iTipoPercepcionId']",function(){
  var elemento=$(this);
  var valor= $(this).val();
  if(valor=="14"){ 
    var input_="<h4 style='margin:0px; padding:0px; border-bottom:1px solid #fcfcfc;'>Registre los datos</h4>";
    input_+="<label>Clave:</label><input type='text' id='clave_' class='form-control' style='width:80px;'><br>";
    input_+="<label>Percepción:</label><input type='text' id='percepcion_' class='form-control' style='width:200px;'>";
    input_+="<div style='margin-top:4px;'><button class='btn btn-success' id='ok_percep'>OK</button> <button class='btn btn-default' id='cerrar_mini'>Cerrar</button></div>";
    $(elemento).minipopup(input_,"right");
    $("#cerrar_mini").click(function(){
       $(elemento).popover('destroy');  
       $("[name='iTipoPercepcionId']").val("1");
    });
     $("#ok_percep").click(function(){
        var clave_=$("#clave_").val();
        var percepcion_=$("#percepcion_").val();
       if(clave_!="" && percepcion_!=""){
        $("#ok_percep").cargando("Registrando...");
          $.post("./general/empleados/otros.php",{clave_:clave_,percepcion_:percepcion_,tipo_:"1"},function(data){
            data = eval(data);
            if(data[1]==0)
              $("#ok_percep").error(data[0],"right");
            else {
				carga_percepciones(data[1]);
              $(elemento).correcto(data[0],"right");

            }
          });
       }

    });
  }
  return false;
});


$("body").on("change","[name='iTipoDeduccionId']",function(){
  var elemento=$(this);
  var valor= $(this).val();
  if(valor=="4"){ 
    var input_="<h4 style='margin:0px; padding:0px; border-bottom:1px solid #fcfcfc;'>Registre los datos</h4>";
    input_+="<label>Clave:</label><input type='text' id='clave_' class='form-control' style='width:80px;'><br>";
    input_+="<label>Deducción:</label><input type='text' id='percepcion_' class='form-control' style='width:200px;'>";
    input_+="<div style='margin-top:4px;'><button class='btn btn-success' id='ok_percep'>OK</button> <button class='btn btn-default' id='cerrar_mini'>Cerrar</button></div>";
    $(elemento).minipopup(input_,"right");
    $("#cerrar_mini").click(function(){
       $(elemento).popover('destroy');  
       $("[name='iTipoDeduccionId']").val("1");
    });
     $("#ok_percep").click(function(){
        var clave_=$("#clave_").val();
        var percepcion_=$("#percepcion_").val();
       if(clave_!="" && percepcion_!=""){
        $("#ok_percep").cargando("Registrando...");
          $.post("./general/empleados/otros.php",{clave_:clave_,percepcion_:percepcion_,tipo_:"2"},function(data){
            data = eval(data);
            if(data[1]==0)
              $("#ok_percep").error(data[0],"right");
            else {
            	carga_deducciones(data[1]);
              $(elemento).correcto(data[0],"right");

            }
          });
       }

    });
  }
  return false;
});
	
});

</script>
