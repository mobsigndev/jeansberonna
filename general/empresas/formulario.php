<? include("../../acceso/seguridad.php"); 
#die("Sistema en mantenimiento");
?>
<div id='ventana-formulario'>
<!-- Nav tabs -->
<ul class="nav nav-pills">
  <li class='active'><a href="#fiscal" data-toggle="tab">Info. Fiscal</a></li>
  <li class="hide"><a href="#logotipo" data-toggle="tab">Logotipo</a></li>
  <li><a href="#contacto" data-toggle="tab">Info. Contacto</a></li>
   <li><a href="#contratos" data-toggle="tab">Contrato</a></li>
   <li><a href="#aval" data-toggle="tab">Aval</a></li>
   <li class="hide"><a href="#credito" data-toggle="tab">Límite de Crédito </a></li>
   <li><a href="#config" data-toggle="tab">Configuración</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="fiscal">
  <input type="text" class="form-control" name="dRegistro" value="CURRENT_DATE()" style='display:none;'>
  <input type="text" class="form-control" name="iId" style='display:none;'>
	<div class="row">
	  <div class="col-lg-4">
     		<label>*Distribuidor </label><span><input obligatorio='si' type="text" class="form-control" name="sEmpresa"></span>
     		<label>RFC</label> <span><input  type="text" class="form-control" name="sRFC"></span>            
     		<label>Raz&oacute;n Social</label><span><input type="text" class="form-control" name="sRazonSocial"></span> 
            <label>Regimen Fiscal </label><span><input type="text" class="form-control" name="sRegimenFiscal"></span>           
			<label>Calle</label> <span><input type="text" class="form-control" name="sCalle"></span>  
      </div>
	  <div class="col-lg-4">
  			<label>No. Exterior </label><span><input type="text" class="form-control" name="sNumeroExterior"></span>                      

      		<label>No. Interior </label><input type="text" class="form-control" name="sNumeroInterior">          
            <label>Colonia</label><span><input  type="text" class="form-control" name="sColonia"></span>           
     		<label>Cod. Postal</label><span><input type="text" class="form-control" name="sCodigoPostal"></span>				
            <label>Municipio</label><span><input  type="tel" class="form-control " name="sMunicipio">
       		         	                        
      
      </div>
	  <div class="col-lg-4">   
		    <label>Ciudad</label><span><input type="tel" class="form-control " name="sCiudad"></span>      	           	<label>Estado</label><span><input type="tel" class="form-control " name="sEstado"></span>
     		<label>Pais </label><span><input type="text" class="form-control" name="sPais"></span>   
			<label>Tel&eacute;fono </label><input type="text" class="form-control" name="sNumeroTelefono">               
			<label>Correo </label><input type="text" class="form-control" name="sCorreo">               
     		                                    
      </div>
    </div>
      
  		
  </div>
  <div class="tab-pane" id="logotipo">
	<div class="row">  
	   <div class="col-lg-4">
        <div class="fileupload fileupload-exists" data-provides="fileupload" data-name="myimage">
          <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
          <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
            <input type="text"  name="sLogotipo" style='display:none;'>
            <img src="./logotipos/no_logo.gif">
          </div>
          <div>
            <span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Cambiar</span><input id="fileupload" type="file" name="files[]" data-url="clases/upload.php" no_catalogo="no" multiple> </span>
            <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" id="eliminarLogo">Eliminar</a>
          </div>
        </div>      
      </div>
	 </div>      
  </div>
  
  <div class="tab-pane" id="contacto">
	<div class="row">  
	   <div class="col-lg-4">
     		<label>Nombre </label><input type="tel" class="form-control " name="sNombreContacto">      	    
        <label>Teléfono </label><input type="tel" class="form-control " name="sTelefonoContacto">
     		<label>Correo </label><input type="text" class="form-control" name="sCorreoContacto">   
			         		
      </div>
	 </div>      
  </div>
  
  <div class="tab-pane" id="contratos">
	<div class="row">  
     	<div class="col-lg-4"> 
        	<label>Adjunte Contrato</label>
              	<div class="fileupload fileupload-exists" data-provides="fileupload" data-name="myimage">
                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                 <input type="text"  class="form-control" name="sContrato" disabled="disabled" style=''> 
                 
                 </div>
                <div>
                 <span class="btn btn-default btn-file"><span class="fileupload-new">Selecciona</span><span class="fileupload-exists">Cargar</span><input id="uploadcontrato" type="file" name="files[]" data-url="clases/uploadcontrato.php" no_catalogo="no" multiple> </span>
                  <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" id="eliminacontrato">Eliminar</a>
                </div>
              </div>   
              
              <label>Adjunte IFE</label>
              <div class="fileupload fileupload-exists" data-provides="fileupload" data-name="myimage">
                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                
                  <input type="text" class="form-control" name="sIfe" disabled="disabled" style=''>
                  
                </div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Selecciona</span><span class="fileupload-exists">Cargar</span><input id="uploadife" type="file" name="files[]" data-url="clases/uploadife.php" no_catalogo="no" multiple> </span>
                  <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" id="eliminarife">Eliminar</a>
                </div>
              </div> 

               <label>Adjunte Comprobante de domicilio</label>
              <div class="fileupload fileupload-exists" data-provides="fileupload" data-name="myimage">
                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                
                  <input type="text" class="form-control" name="sComprobante" disabled="disabled" style=''>
                  
                </div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Selecciona</span><span class="fileupload-exists">Cargar</span><input id="uploadcomprobante" type="file" name="files[]" data-url="clases/uploadcomprobante.php" no_catalogo="no" multiple> </span>
                  <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" id="eliminacomprobante">Eliminar</a>
                </div>
              </div> 
              
              
       </div>
  <div class="col-lg-8"> 
  <div><label>Documentos:</label></div>
  <div id="descargacontrato"></div>
  <div id="descargaIfe"></div>
  <div id="descargacomprobante"></div>
  </div>
	 </div>      
     
     		
  </div>


  <div class="tab-pane" id="aval">
  <div class="row">  
      <div class="col-lg-4"> 
              <label>Adjunte IFE Aval</label>
              <div class="fileupload fileupload-exists" data-provides="fileupload" data-name="myimage">
                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                
                  <input type="text" class="form-control" name="sIfeAval" disabled="disabled" style=''>
                  
                </div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Selecciona</span><span class="fileupload-exists">Cargar</span><input id="uploadifeaval" type="file" name="files[]" data-url="clases/uploadifeaval.php" no_catalogo="no" multiple> </span>
                  <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" id="eliminarifeaval">Eliminar</a>
                </div>
              </div> 

               <label>Adjunte Comprobante de domicilio</label>
              <div class="fileupload fileupload-exists" data-provides="fileupload" data-name="myimage">
                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                
                  <input type="text" class="form-control" name="sComprobanteAval" disabled="disabled" style=''>
                  
                </div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Selecciona</span><span class="fileupload-exists">Cargar</span><input id="uploadcomprobanteaval" type="file" name="files[]" data-url="clases/uploadcomprobanteaval.php" no_catalogo="no" multiple> </span>
                  <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" id="eliminacomprobanteaval">Eliminar</a>
                </div>
              </div> 
              
              
       </div>
  <div class="col-lg-8"> 
  <div><label>Documentos:</label></div>
  <div id="descargaIfeaval"></div>
  <div id="descargacomprobanteaval"></div>
  </div>
   </div>      
     
        
  </div>

   <div class="tab-pane" id="credito">
  <div class="row">  
     <div class="col-lg-4">
      <label class="label label-warning">Límite de Crédito: </label>
        <label>Configure total de crédito </label><input type="text" class="form-control " name="fLimite">           
     </div>
      
      </div>
   </div>   
   
    <div class="tab-pane" id="config">
  		<div class="row">  
             <div class="col-lg-4">
             <label class="label label-danger">Especifique las siguientes configuraciones: </label>
             <div class="divider"></div>
                <label>¿Se pagará comisión?</label><select type="text" class="form-control " name="iComision">  <option value="0">NO</option><option value="1">SI</option> </select>
                <label>¿Apoyo a gasolina?</label><select type="text" class="form-control " name="iGasolina">  <option value="0">NO</option><option value="1">SI</option> </select>
             </div>
        </div>
    </div>
    
      
  </div>
  
  

</div>
</div>

<script src="js/jquery.ui.widget.js"></script>
<script src="js/jquery.iframe-transport.js"></script>
<script src="js/jquery.fileupload.js"></script>
<script>
$(function () {	

  var idEmpresa=null;
  $("body").on('click','input[name=iId]',function(){
    idEmpresa=$("input[name=iId]").val();
  var logo=$("input[name=sLogotipo]").val();
	var contrato=$("input[name=sContrato]").val();
	var ife=$("input[name=sIfe]").val();
  var comprobante=$("input[name=sComprobante]").val();
    var ifeaval=$("input[name=sIfeAval]").val();
  var comprobanteaval=$("input[name=sComprobanteAval]").val();
    if(logo==""){
        logo="no_logo.gif";
    }
if(contrato!="")
  $("#descargacontrato").html("<a target='_blank' href='./contratos/"+contrato+"'>Descargar contrato</a>");
  if(ife!="")
  $("#descargaIfe").html("<a target='_blank' href='./ifes/"+ife+"'>Descargar IFE</a>");
  if(comprobante!="")
    $("#descargacomprobante").html("<a target='_blank' href='./comprobantes/"+comprobante+"'>Descargar comprobante de domicilio</a>");
    if(ifeaval!="")
  $("#descargaIfeaval").html("<a target='_blank' href='./ifes/"+ifeaval+"'>Descargar IFE</a>");
  if(comprobanteaval!="")
  $("#descargacomprobanteaval").html("<a target='_blank' href='./comprobantes/"+comprobanteaval+"'>Descargar comprobante de domicilio</a>");

  $(".fileupload-preview").children('img').attr("src","./logotipos/"+logo);
  $("#eliminarLogo").attr("href","./clases/eliminar_logo.php?nombre="+logo);  
	$("#eliminarife").attr("href","./clases/eliminar_ife.php?nombre="+ife+"&tipo=1"); 
	$("#eliminacontrato").attr("href","./clases/eliminar_contrato.php?nombre="+contrato);      
  $("#eliminacomprobante").attr("href","./clases/eliminar_comprobante.php?nombre="+comprobante+"&tipo=1");   
  $("#eliminarifeaval").attr("href","./clases/eliminar_ife_aval.php?nombre="+ife+"&tipo=1"); 
  $("#eliminacomprobanteaval").attr("href","./clases/eliminar_comprobante_aval.php?nombre="+comprobante+"&tipo=1");   
  });

 $('#fileupload').bind('fileuploadsubmit', function (e, data) {
    data.formData = {idEmpresa: idEmpresa};
 }); 
$("body").on('click','#eliminarLogo',function(e){
  e.preventDefault();
  var url=$(this).attr("href");
  $.get(url, function( data ) {
    $(".fileupload-preview").children('img').attr("src","./logotipos/no_logo.gif");
  });

});

$("body").on('click','#eliminacontrato',function(e){
  e.preventDefault();
  var url=$(this).attr("href");
  $.get(url, function( data ) {
    $("input[name=sContrato]").val("");
   // $(".fileupload-preview").children('img').attr("src","./logotipos/no_logo.gif");
  });

});

$("body").on('click','#eliminacomprobante',function(e){
  e.preventDefault();
  var url=$(this).attr("href");
  $.get(url, function( data ) {
    $("input[name=sComprobante]").val("");
  //  $(".fileupload-preview").children('img').attr("src","./logotipos/no_logo.gif");
  });

});

$("body").on('click','#eliminarife',function(e){
  e.preventDefault();
  var url=$(this).attr("href");
  $.get(url, function( data ) {
    $("input[name=sIfe]").val("");
    //$(".fileupload-preview").children('img').attr("src","./logotipos/no_logo.gif");
  });

});

$("body").on('click','#eliminacomprobanteaval',function(e){
  e.preventDefault();
  var url=$(this).attr("href");
  $.get(url, function( data ) {
    $("input[name=sComprobanteAval]").val("");
  //  $(".fileupload-preview").children('img').attr("src","./logotipos/no_logo.gif");
  });

});

$("body").on('click','#eliminarifeaval',function(e){
  e.preventDefault();
  var url=$(this).attr("href");
  $.get(url, function( data ) {
    $("input[name=sIfeAval]").val("");
    //$(".fileupload-preview").children('img').attr("src","./logotipos/no_logo.gif");
  });

});

$('#fileupload').fileupload({
    dataType: 'json',
    done: function (e, data) {      
      $.each(data.result.files, function (index, file) {
        $("input[name=sLogotipo]").val(file.name);
        $(".fileupload-preview").children('img').attr("src","./logotipos/"+file.name);
        $("#eliminarLogo").attr("href","./clases/eliminar_logo.php?nombre="+file.name);
      });
    }
  });
  
  $('#uploadcomprobante').bind('fileuploadsubmit', function (e, data) {
    data.formData = {idEmpresa: idEmpresa};
 }); 
  
  $('#uploadcomprobante').fileupload({
    dataType: 'json',
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        $("input[name=sComprobante]").val(file.name);
        $("#eliminacomprobante").attr("href","./clases/eliminar_comprobante.php?nombre="+file.name+"&tipo=1");
      });
    }
  });
  
  $('#uploadcontrato').bind('fileuploadsubmit', function (e, data) {
    data.formData = {idEmpresa: idEmpresa};
 }); 

   $('#uploadcontrato').fileupload({
    dataType: 'json',
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        $("input[name=sContrato]").val(file.name);
        $("#eliminacontrato").attr("href","./clases/eliminar_contrato.php?nombre="+file.name);
      });
    }
  });


  $('#uploadife').bind('fileuploadsubmit', function (e, data) {
    data.formData = {idEmpresa: idEmpresa};
 }); 

   $('#uploadife').fileupload({
    dataType: 'json',
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        $("input[name=sIfe]").val(file.name);
        $("#eliminarife").attr("href","./clases/eliminar_ife.php?nombre="+file.name+"&tipo=1");
      });
    }
  });
  
  
  $('#uploadcomprobanteaval').bind('fileuploadsubmit', function (e, data) {
    data.formData = {idEmpresa: idEmpresa};
 }); 
  
  $('#uploadcomprobanteaval').fileupload({
    dataType: 'json',
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        $("input[name=sComprobanteAval]").val(file.name);
        $("#eliminacomprobanteaval").attr("href","./clases/eliminar_comprobante_aval.php?nombre="+file.name+"&tipo=1");
      });
    }
  });
$('#uploadifeaval').bind('fileuploadsubmit', function (e, data) {
    data.formData = {idEmpresa: idEmpresa};
 }); 

   $('#uploadifeaval').fileupload({
    dataType: 'json',
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        $("input[name=sIfeAval]").val(file.name);
        $("#eliminarifeaval").attr("href","./clases/eliminar_ife_aval.php?nombre="+file.name+"&tipo=1");
      });
    }
  });

});
</script>
