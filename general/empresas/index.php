<?

require_once(dirname(__FILE__)."/../../acceso/seguridad.php");

$_tabla = "empresa";

$_formulario = "./general/empresas/formulario.php";

$_pre_agregar = "./general/empresas/verifica_existencias.php";

$_post_agregar = "./general/empresas/agrega_relacional.php";

?>

<div class="row">

  <div class="col-lg-6">

	  	<button type="button" class="btn btn-primary" id="nuevo" titulo='Agregar Distribuidor' pre_agregar='<?=$_pre_agregar;?>' post_agregar='<?=$_post_agregar;?>' formulario='<?=$_formulario;?>' tabla='<?=$_tabla;?>'>Agregar Distribuidor</button>

    

   		<div class="btn-group">

			<button type="button" class="btn btn-default">Otras Opciones</button>

			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">

		    <span class="caret"></span>

	    	<span class="sr-only">-</span>

			</button>

			<ul class="dropdown-menu" role="menu"> 

				<li><a href="#" id='eliminar-seleccionados' tabla='<?=$_tabla;?>'>Eliminar seleccionados</a></li>    

			</ul>

		</div>

	</div> 

	<? require_once("./clases/exportacion.php"); ?>

</div>



<div class="row">

    <div class="col-lg-12">    		

      	<div id="div-tabla"></div>    

	</div>

</div>        



<script>

$(document).ready(function () {



var genera_tabla = {

  columnas: [ 

    {columna:'sEmpresa', titulo: 'Distribuidor',class:"hidden-xs",widthpdf:'18%',visible:true},

    {columna:'sNumeroTelefono', titulo: 'No. Teléfono',class:false,widthpdf:'15%',visible:true},	

    {columna:'sCorreo', titulo: 'Correo',class:"hidden-xs",widthpdf:'12%',visible:true},	

    {columna:'sCiudad', titulo: 'Ciudad',class:"hidden-xs",widthpdf:'12%',visible:true}		

   ]   

};



$("#div-tabla").crear_tabla('<?=$_formulario;?>','empresa.iId','empresa INNER JOIN usuarioempresa ON empresa.iId = usuarioempresa.iEmpresaId',genera_tabla,"empresa.iEstatus=0 and usuarioempresa.iUsuarioId=<?=$iUsuarioId;?>",false,false);



$('#ventana-emergente').on('hidden.bs.modal', function (e) {

	$("#div-tabla").crear_tabla('<?=$_formulario;?>','empresa.iId','empresa INNER JOIN usuarioempresa ON empresa.iId = usuarioempresa.iEmpresaId',genera_tabla,"empresa.iEstatus=0 and usuarioempresa.iUsuarioId=<?=$iUsuarioId;?>",false,true);	

});



});



</script>

