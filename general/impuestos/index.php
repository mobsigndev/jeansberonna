<?
$_tabla = "impuesto";
$_formulario = "./general/impuestos/formulario.php";
$_pre_agregar = "./general/impuestos/verifica_existencias.php";
$_post_agregar = "";
?>
<div class="row">
  <div class="col-lg-6">
	  	<button type="button" class="btn btn-primary" id="nuevo" titulo='Agregar <?=$_tabla;?>' pre_agregar='<?=$_pre_agregar;?>' post_agregar='<?=$_post_agregar;?>' formulario='<?=$_formulario;?>' tabla='<?=$_tabla;?>'>Agregar <?=$_tabla;?></button>
    
   		<div class="btn-group">
			<button type="button" class="btn btn-default">Otras Opciones</button>
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		    <span class="caret"></span>
	    	<span class="sr-only">-</span>
			</button>
			<ul class="dropdown-menu" role="menu"> 
				<li><a href="#" id='eliminar-seleccionados' tabla='<?=$_tabla;?>'>Eliminar seleccionados</a></li>    
			</ul>
		</div>
	</div> 
	<? include("./clases/exportacion.php"); ?>
</div>

<div class="row">
    <div class="col-lg-12">    		
      	<div id="div-tabla"></div>    
	</div>
</div>        

<script>
$(document).ready(function () {

var genera_tabla = {
  columnas: [ 
    {columna:'sNombre', titulo: 'Impuesto',class:false,widthpdf:'18%',visible:true},
    {columna:'fPorcentaje', titulo: 'Porcentaje',class:false,widthpdf:'55%',visible:true},
	{columna:'iBaseGravable', titulo: 'Base Gravable',class:false,widthpdf:'20%',visible:true,label:"baseimpuesto"},
    {columna:'iNivel', titulo: 'Nivel',class:false,widthpdf:'20%',visible:true,label:"nivelimpuesto"},
	{columna:'iTipo', titulo: 'Tipo de Impuesto',class:false,widthpdf:'20%',visible:true,label:"tipoimpuesto"},

   ]   
};

$("#div-tabla").crear_tabla('<?=$_formulario;?>','iId','<?=$_tabla;?>',genera_tabla,"iEstatus=0 and iUsuarioEmpresaId=<?=$iEmpresaId?>",false,false,'<?=$_post_agregar;?>');

$('#ventana-emergente').on('hidden.bs.modal', function (e) {
	$("#div-tabla").crear_tabla('<?=$_formulario;?>','iId','<?=$_tabla;?>',genera_tabla,"iEstatus=0 and iUsuarioEmpresaId=<?=$iEmpresaId?>",false,true,'<?=$_post_agregar;?>');	
});

});

</script>
