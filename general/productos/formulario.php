<? 
include("../../acceso/seguridad.php");
?>
<div id='ventana-formulario'>
<!-- Nav tabs -->
<ul class="nav nav-pills">
  <li class='active'><a href="#general" data-toggle="tab">General</a></li>
  <li><a href="#inventario" data-toggle="tab">Inventario</a></li>
  <? if(permiso("Tienda",$iUsuarioEmpresaId)) { ?><li><a href="#detalle" data-toggle="tab">Tienda en Linea</a></li><? } ?>
  <li class="hide"><a href="#aduana" data-toggle="tab">Pedimento</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="general">
    <input type="text" class="form-control" name="iUsuarioEmpresaId" value="<?=$iUsuarioEmpresaId;?>" style='display:none;'>
  <input type="text" class="form-control" name="dRegistro" value="CURRENT_DATE()" style='display:none;'>
  <input type="text" class="form-control" name="iId" style='display:none;'>
	<div class="row">
	  <div class="col-lg-4">
     		<label>Codigo </label><span><input obligatorio='si' type="text" class="form-control" name="sCodigo"></span>
     		<label>Nombre</label> <span><input obligatorio='si' type="text" class="form-control" name="sNombre"></span>            
     		<label>Descripción</label><input type="text" class="form-control" name="sDescripcion"> 
            <label>Precio de Compra</label><input type="text" class="form-control" name="fPrecioCompra">           
			<label>Precio de Venta</label> <span><input obligatorio='si' type="text" class="form-control" name="fPrecioVenta"></span>  
      </div>
	  <div class="col-lg-4">
      		<label>Unidad</label><input type="text" obligatorio='si' class="form-control" name="sUnidad">          
            <label>Proveedor</label>        
            	<span><select obligatorio="si" class="form-control" name='iProveedorId'>
                <option value="0">Sin Proveedor</option>
                <?
                $select = seleccionar("proveedor","sNombre,iId,sCodigo","iEstatus=0",false, false);
                foreach($select as $dato) {
                ?>
                <option value="<?=$dato['iId'];?>"><?=$dato['sCodigo']."-".$dato['sNombre'];?></option>
                <?
                }
                ?>
                </select></span>           	
                
                
           <label>Categoria</label><span><select obligatorio="si" class="form-control" name='iCategoriaId'>
                <option value="0">Ninguna</option>           
                <?
                $select = seleccionar("categoria","sNombre,iId,sCodigo","iEstatus=0 and iTipoCategoria=1",false, false);				
                foreach($select as $dato) {
                ?>
                <option value="<?=$dato['iId'];?>"><?=$dato['sCodigo']."-".$dato['sNombre'];?></option>
                <?
                }
                ?>
                </select></span>     				       		
                <label>I.V.A.</label><span><input type="number" min="0" max="100" class="form-control" name="fIva">    
                </span>                      
      </div>
	
    </div>
      
  		
  </div>
  <div class="tab-pane" id="inventario">
	<div class="row">  
	   <div class="col-lg-4">
	       <label>Control de Inventario</label><span><select obligatorio="si" class="form-control" name='iControlInventario'>
                <option value="0" selected="selected">NO</option>   
                <option value="1">SI</option>                   
            </select></span>
      		<label>Existencia Mínima</label><input type="text" class="form-control" name="iExistenciaMin">          
      		<label>Ubicación Física</label><input type="text" class="form-control" name="sUbicacionFisica">          
                        

      </div>
      	   <div class="col-lg-4 hide">
      		<label>Existencia Real (En Inventario)</label><input type="text" class="form-control" name="ExistenciaReal"  no_catalogo="no" value=''>                                   

      </div>
	 </div>      
  </div>
  

  <? if(permiso("Tienda",$iUsuarioEmpresaId)) { ?>
  <div class="tab-pane" id="detalle">
  <!-- IMAGEN 1-->
	<div class="row">  
<div class="col-lg-6">
   <label>Mostrar éste producto en WEB</label><span><select obligatorio="si" class="form-control" name='iMostrarEnWeb'>
                <option value="1" selected="selected">SI</option>   
                <option value="0">NO</option>                   
            </select></span>
</div>
<div class="col-lg-6">
   <h3><label>ID Producto</label> <label class="label label-danger" id="id_producto"></label></h3>
</div>
</div>
<br>
      <div class="row">  
	   <div class="col-lg-6">
        <div class="fileupload fileupload-exists" data-provides="fileupload" data-name="myimage">
          <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
          <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;" id="1">
            <input type="text"  name="sImagen1" style='display:none;'>
            <img src="./imagenes/no_logo.gif">
          </div>
          <div>
            <span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Cambiar</span><input id="fileupload" type="file" name="files[]" data-url="clases/uploadimagen.php" no_catalogo="no" multiple> </span>
            <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" id="eliminarimagen1">Eliminar</a>
          </div>
        </div>      
      </div>
	 <!-- IMAGEN 2-->
	   <div class="col-lg-6">
        <div class="fileupload fileupload-exists" data-provides="fileupload" data-name="myimage">
          <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" />
          </div>
          <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;" id="2">
            <input type="text"  name="sImagen2" style='display:none;'>
            <img src="./imagenes/no_logo.gif">
          </div>
          <div>
            <span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Cambiar</span><input id="fileupload2" type="file" name="files[]" data-url="clases/uploadimagen2.php" no_catalogo="no" multiple> </span>
            <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload2" id="eliminarimagen2">Eliminar</a>
          </div>
        </div>      
      </div>
	 </div>
     <div class="row"> 
     	<div class="col-lg-12">
      		<!--<label>Categoria 1</label><input type="text" class="form-control" name="sCategoria1"  value=''>       
            <label>Categoria 2</label><input type="text" class="form-control" name="sCategoria2"  value=''>       
            <label>Categoria 3</label><input type="text" class="form-control" name="sCategoria3"  value=''>       
            <label>Categoria 4</label><input type="text" class="form-control" name="sCategoria4"  value=''>
            -->
            <?
			$select = seleccionar("empresa
INNER JOIN usuarioempresa ON usuarioempresa.iEmpresaId = empresa.iId","sCategoria1,sCategoria2,sCategoria3,sCategoria4"," usuarioempresa.iId='".$iUsuarioEmpresaId."'",false, false);	
			
			foreach($select as $dato) {
				if($dato['sCategoria1']!=""){
					echo '<label>'.$dato['sCategoria1'].'</label><input type="text" class="form-control" name="sCategoria1"  value=""> ';
				}
				if($dato['sCategoria2']!=""){
					echo '<label>'.$dato['sCategoria2'].'</label><input type="text" class="form-control" name="sCategoria2"  value=""> ';
				}
				if($dato['sCategoria3']!=""){
					echo '<label>'.$dato['sCategoria3'].'</label><input type="text" class="form-control" name="sCategoria3"  value=""> ';
				}
				if($dato['sCategoria4']!=""){
					echo '<label>'.$dato['sCategoria4'].'</label><input type="text" class="form-control" name="sCategoria4"  value=""> ';
				}
			
			}
			?>                                   
      </div>
     </div>
  </div>   
  <? } ?>
	
  
  <div class="tab-pane" id="aduana">
	<div class="row">  
	   <div class="col-lg-4">
			       <label>No. Pedimento </label><span><input type="text" class="form-control" name="sNoPedimento"></span>
          <label>Aduana</label> <span><input type="text" class="form-control" name="sAduana"></span>            
        <label>Fecha</label> <span><input type="text" class="form-control" name="dFechaPedimento" data-date-format="dd/mm/yyyy" ></span>            

      </div>
	 </div>      
  </div>
  
  

</div>
</div>

<script src="js/jquery.ui.widget.js"></script>
<script src="js/jquery.iframe-transport.js"></script>
<script src="js/jquery.fileupload.js"></script>
<script>
$(document).ready(function(){
	
$(document).ready(function () {
   $('[name="dFechaPedimento"]').datepicker({
        todayBtn: "linked",
    language: "es",      
    autoclose: true,
    todayHighlight: true
  });
});

	 var iProductoId=null;
  $("body").on('click','input[name=iId]',function(){
    iProductoId=$("input[name=iId]").val();
    $("#id_producto").html(iProductoId);
    var sImagen1=$("input[name=sImagen1]").val();
	var sImagen2=$("input[name=sImagen2]").val();
    if(sImagen1==""){
        sImagen1="no_logo.gif";
    }
	if(sImagen2==""){
        sImagen2="no_logo.gif";
    }
    $("#1").children('img').attr("src","./imagenes/"+sImagen1);
	$("#2").children('img').attr("src","./imagenes/"+sImagen2);
    $("#eliminarimagen1").attr("href","./clases/eliminar_imagen.php?nombre="+sImagen1+"&numero=1");  
	$("#eliminarimagen2").attr("href","./clases/eliminar_imagen.php?nombre="+sImagen2+"&numero=2");     
  });
  
 $('#fileupload').bind('fileuploadsubmit', function (e, data) {
	 var elemento = $(this);
	iProductoId=$("input[name=iId]").val();
    data.formData = {iProductoId:iProductoId};
	$(elemento).cargando("Cargando");
 }); 
 
 $("body").on('click','#eliminarimagen1',function(e){
		var elemento = $(this);
		e.preventDefault();
		$(elemento).cargando("Eliminando");
		var url=$(this).attr("href");
		$.get(url, function( data ) {
		$("#1").children('img').attr("src","./imagenes/no_logo.gif");
		$(elemento).cerrar_cargando();
	  });
	
});

$('#fileupload').fileupload({
	
    dataType: 'json',
    done: function (e, data) {      
      $.each(data.result.files, function (index, file) {
        $("input[name=sImagen1]").val(file.name);
        $("#1").children('img').attr("src","./imagenes/"+file.name);
        $("#eliminarimagen1").attr("href","./clases/eliminar_imagen.php?nombre="+file.name+"&numero=1");
      });
    }
  });
  
$('#fileupload2').bind('fileuploadsubmit', function (e, data) {
	var elemento = $(this);
	iProductoId=$("input[name=iId]").val();
    data.formData = {iProductoId: iProductoId};
	$(elemento).cargando("Cargando");
}); 
 
$('#fileupload2').fileupload({
    dataType: 'json',
    done: function (e, data) {      
      $.each(data.result.files, function (index, file) {
        $("input[name=sImagen2]").val(file.name);
        $("#2").children('img').attr("src","./imagenes/"+file.name);
        $("#eliminarimagen2").attr("href","./clases/eliminar_imagen.php?nombre="+file.name+"&numero=2");
      });
    }
 });
  
$("body").on('click','#eliminarimagen2',function(e){
  e.preventDefault();
  var url=$(this).attr("href");
  $.get(url, function( data ) {
    $("#2").children('img').attr("src","./imagenes/no_logo.gif");
  });

});
	$("[name='iId']").click(function(){
		var id_sel=$(this).val();
		$("[name='ExistenciaReal']").cargando("Cargando","right");
		$.post("./clases/existencias.php",{iProductoId:id_sel},function(data){
			data = eval(data);
			$("[name='ExistenciaReal']").cerrar_cargando();
			$("[name='ExistenciaReal']").val(data[0]);
		});
		
	});
	

    $("[name='fPrecioCompra']").change(function(){
      var elemento = $(this);
      var compra = $("[name='fPrecioCompra']").val();
      var venta = $("[name='fPrecioVenta']").val();
      if(compra != "" && venta !="") {
        $.post("./clases/verifica_utilidad.php",{compra:compra,venta:venta,tipo:2},function(data2){
              data2 = eval(data2);
              if(data2[1]==1){
              }
              else {
                $(elemento).val("");
                $(elemento).error(data2[0]);  
              }
        });
      }
    });
    $("[name='fPrecioVenta']").change(function(){
        var elemento = $(this);
        var compra = $("[name='fPrecioCompra']").val();
        var venta = $("[name='fPrecioVenta']").val();
        if(compra != "" && venta !="") {
          $.post("./clases/verifica_utilidad.php",{compra:compra,venta:venta,tipo:2},function(data2){
                data2 = eval(data2);
                if(data2[1]==1){
                }
                else {
                  $(elemento).val("");
                  $(elemento).error(data2[0]);  
                }
          });
        }
    });

		$("[name='ExistenciaReal']").change(function(){
		var ExistenciaReal=$(this).val();
		if(ExistenciaReal!=""){
			var id_sel=$("[name='iId']").val();
	
			$("[name='ExistenciaReal']").cargando("Actualizando","right");
			$.post("./general/productos/modifica_existencia.php",{iProductoId:id_sel,ExistenciaReal:ExistenciaReal},function(data){
				data = eval(data);
				if(data[1]>0)
				$("[name='ExistenciaReal']").correcto(data[0],"bottom");
				else
				$("[name='ExistenciaReal']").error(data[0],"bottom");
			});
		}
		
	});
	
});
</script>