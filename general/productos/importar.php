<html>

<head>

</head>

<body>

<div class="row">  

<div class="col-lg-12">

	<div class="fileupload fileupload-exists" data-provides="fileupload" data-name="myimage">

	  <div>

	    <span class="btn btn-default btn-file"><span class="fileupload-exists">Seleccionar CSV</span><input id="fileupload" type="file" name="files[]" data-url="clases/uploadcsv.php" no_catalogo="no" multiple> </span>

	    <span> <a href="./Productos.csv">Descargar plantilla</a> </span>

	  </div>

	</div>      

</div>

</div>

<div class="row">  

<div class="col-lg-12">

   <div class="table-responsive" id="divCsv" style="overflow: auto">

   </div>

</div>

</div>

</body>

<script type="text/javascript" src="js/jquery.csvToTable.js"></script>

<script src="js/jquery.ui.widget.js"></script>

<script src="js/jquery.iframe-transport.js"></script>

<script src="js/jquery.fileupload.js"></script>

<script>

$(document).ready(function () {

	$('#fileupload').fileupload({

	    dataType: 'json',

	    done: function (e, data) {

	      $.each(data.result.files, function (index, file) {

	        $("input[name=sLogotipo]").val(file.name);

	        $(".fileupload-preview").children('span').text(file.name);

	        //$('#divCsv').csv2table("./importar/"+file.name);

	        $('#divCsv').CSVToTable("./importar/"+file.name, { 

	        	loadingImage: 'images/loading.gif', 

	        	startLine: 0,

	        	tableClass:"table table-bordered table-striped",

	        	loadingText:"Cargando datos..." 

	        }).bind("loadComplete",function() { 

			        $("#importarProductos").removeClass();

				    $("#importarProductos").addClass('btn btn-default');

				    $("#importarProductos").removeAttr('disabled');

			 });

	      });

	    }

	  });

	$("body #ventana-emergente").on("click","#importarProductos",function(e){

		var columnas;

		var datos;

		var tabla="producto";

		obtener_columnas(function(cols){

			columnas=cols;

		    obtener_datos(function(dat){

				datos=dat;

				$.post("./clases/importar.php",{columnas:columnas,datos:datos,tabla:tabla},function(data){



				});

		    });

		});

    });

    function obtener_columnas(listo){

    	var columnas=[];

		$("#divCsv table thead tr").find('th').each (function() {

		  columnas.push($(this).text());

		});

	   listo(columnas);

    }

    function obtener_datos(listo){

    	var datos=[];

    	var renglon=[];

		$('#divCsv table > tbody > tr').each(function() {

			$(this).find('td').each (function() {

			  renglon.push($(this).text());

			});

			datos.push(renglon);

			renglon=[];

		});	

		listo(datos);

    } 

   });       	

</script>