<?
$_tabla = "producto";
$_formulario = "./general/productos/formulario.php";
$_pre_agregar = "./general/productos/verifica_existencias.php";
$_post_agregar = "./general/productos/agrega_existencia.php";
?>
<div class="row">
  <div class="col-lg-6">
	  	<button type="button" class="btn btn-primary" id="nuevo" titulo='Agregar <?=$_tabla;?>' pre_agregar='<?=$_pre_agregar;?>' post_agregar='<?=$_post_agregar;?>' formulario='<?=$_formulario;?>' tabla='<?=$_tabla;?>'>Agregar <?=$_tabla;?></button>
 	  	<button type="button" class="btn btn-primary" id="importar" titulo='Importar <?=$_tabla;?>' pre_agregar='<?=$_pre_agregar;?>' post_agregar='<?=$_post_agregar;?>' formulario='<?=$_formulario;?>' tabla='<?=$_tabla;?>'>Importar <?=$_tabla;?></button>   
   		<div class="btn-group">
			<button type="button" class="btn btn-default">Otras Opciones</button>
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		    <span class="caret"></span>
	    	<span class="sr-only">-</span>
			</button>
			<ul class="dropdown-menu" role="menu"> 
				<li><a href="#" id='eliminar-seleccionados' tabla='<?=$_tabla;?>'>Eliminar seleccionados</a></li>    
			</ul>
		</div>
	</div> 
	<? include("./clases/exportacion.php"); ?>
</div>

<div class="row">
    <div class="col-lg-12">    		
      	<div id="div-tabla"></div>    
	</div>
</div>        

<script>
$(document).ready(function () {

	var genera_tabla = {
	  columnas: [ 
	    {columna:'sCodigo', titulo: 'Código',class:false,widthpdf:'10%',visible:true},
	    {columna:'sNombre', titulo: 'Nombre',class:false,widthpdf:'25%',visible:true},
	    {columna:'sDescripcion', titulo: 'Descripción',class:"hidden-xs",widthpdf:'35%',visible:false},
	    {columna:'fPrecioCompra', titulo: 'Precio Compra',class:false,widthpdf:'10%',visible:true, tipo:'input'},	
	    {columna:'fPrecioVenta', titulo: 'Precio Venta',class:"hidden-xs",widthpdf:'10%',visible:true, tipo:'input'},	
		{columna:'iExistenciaMin', titulo: 'Existencia Minima',class:"hidden-xs",widthpdf:'8%',visible:true},	
		{columna:'iControlInventario', titulo: 'Inventariado',class:"hidden-xs",widthpdf:'8%',visible:true,label:"sino"},		
	   ]   
	};
	$("body").on("change","input[rel='input_catalogo']",function(){
		var elemento = $(this);
		var id_ = $(this).attr("id_registro");
		var registro_ = $(this).attr("registro");
		var value_ = $(this).val();
		var datos = registro_+"="+value_;
		$(elemento).cargando("Almacenando","top");


		$.post("./clases/verifica_utilidad.php",{producto:id_,valor:value_,campo:registro_,tipo:1},function(data2){
			data2 = eval(data2);
			if(data2[1]==1){
					$.post("./clases/actualiza.php",{datos:datos,tabla:'<?=$_tabla;?>',iId:id_},function(data){

							data = eval(data);

							if(data[1]==1) {

								$(elemento).correcto(data[0]);


							}else if(data[1]==0) {
								
								$(elemento).error(data[0]);						

							}

						});
				}
				else {
					$(elemento).val("");
					$(elemento).error(data2[0]);		
				}
			});
	});
	$("#div-tabla").crear_tabla('<?=$_formulario;?>','iId','<?=$_tabla;?>',genera_tabla,"iEstatus=0",false,false,'<?=$_post_agregar;?>');

	$('#ventana-emergente').on('hidden.bs.modal', function (e) {
		$("#div-tabla").crear_tabla('<?=$_formulario;?>','iId','<?=$_tabla;?>',genera_tabla,"iEstatus=0",false,true,'<?=$_post_agregar;?>');	
	});
	$("body").on("click","#importar",function(){ 
		var titulo=$(this).attr("titulo");
		var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>"
			+"<button type='button' class='btn btn-disabled' data-dismiss='modal' id='importarProductos' >Guardar</button>";
		$.post("./general/productos/importar.php",null,function(data){				
	    	vermodal(titulo,data,botones);
		});
	});	
});

</script>
