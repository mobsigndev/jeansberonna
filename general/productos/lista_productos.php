<?
include("../../acceso/seguridad.php");
$_tabla = "producto";
$_formulario = "./general/productos/formulario.php";
?>
<div class="row">
  	<div class="col-lg-6">	  	
	  	<button type="button" class="btn btn-primary" id="nuevo" titulo='Agregar Nuevo <?=$_tabla;?>' formulario='<?=$_formulario;?>' tabla='<?=$_tabla;?>' retorna_id="si">Agregar <?=$_tabla;?></button>
	</div> 
	<? 
	$_REQUEST['sin_exportar'] = "sin";
	include("../../clases/exportacion.php"); ?>
</div>

<div class="row">
    <div class="col-lg-12">    		
      	<div id="div-tabla-lista"></div>    
	</div>
</div>        

<script>
$(document).ready(function () {



var genera_tabla = {
  columnas: [ 
    {columna:'sCodigo', titulo: 'Código',class:"hidden-xs",widthpdf:'17%',visible:true},
    {columna:'sNombre', titulo: 'Nombre',class:false,widthpdf:'17%',visible:true},
    {columna:'fPrecioVenta', titulo: 'Precio Venta',class:false,widthpdf:'20%',visible:true},
   ]   
};
 
$("#div-tabla-lista").crear_tabla_lista('<?=$_tabla;?>',"iId",genera_tabla,"iEstatus=0 and iUsuarioEmpresaId=<?=$iUsuarioEmpresaId;?>",false);

});

</script>
