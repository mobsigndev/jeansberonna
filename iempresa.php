<? include("./acceso/seguridad.php"); ?>
<!DOCTYPE html>
<html>
<head> 
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AdminBox V1.0</title>
    <!-- Core CSS - Include with every page -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- SB Admin CSS - Include with every page -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">   
    <link href="css/style.css" rel="stylesheet">   
    <link href="js/plugins/datepicker/css/datepicker3.css" rel="stylesheet">       
    <link href="js/plugins/timepicker/timepicker.css" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="css/jHtmlArea.css" />
	<script src="js/jquery-1.10.2.js"></script>
    <script src="js/jHtmlArea-0.8.min.js" type="text/javascript" ></script>
	<!-- Core Scripts - Include with every page -->    
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>  
    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>    
    <script src="js/plugins/bootstrap.confirmation.js"></script> 
    <script src="js/plugins/timepicker/timepicker.js"></script>    
    <script src="js/plugins/datepicker/js/bootstrap-datepicker.js"></script>    
    <script src="js/plugins/datepicker/js/locales/bootstrap-datepicker.es.js"></script> 
    <script src="js/typeahead.min.js"></script>   
    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>
    <!-- AdminBox Scripts - Include with every page -->
    <script src="js/adminbox.js"></script> 
     <!-- Texto enriquecido -->
<!--    <script src="js/jHtmlArea.ColorPickerMenu-0.8.min.js" type="text/javascript"></script>     
--></head>

<body>
    <div id="wrapper">
    <form action="#" id="form_pdf" method="post" target="_blank" style='display:none;'>
	    <textarea id="t_codigo" name='t_codigo'></textarea>
    </form>
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">
                   <img src="imagen/admin_box_100_transparente.png" width="100" height="46" alt="AdminBox">                   
                </a>
             
                
            </div>
                   <div  class="nav navbar-default navbar-left">
				 <div class="well well-sm" style='margin-bottom:0px; margin-top:4px; padding:5px;'>
				 <? echo "<div><strong><i class='fa fa-user'></i>  Empleado </strong> - <u>".$sNombre." ".$sPaterno." ".$sMaterno."</u></div>";
				 echo "<div><strong><i class='fa fa-building-o'></i>  Empresa </strong> - <u>".$sEmpresa."</u></div>";
				 ?>
                 </div>
             	
             </div> 
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right">
           
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">                  
                        <li><a href="./acceso/salir.php"><i class="fa fa-sign-out fa-fw"></i> Desconectar</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

        </nav>
        <!-- /.navbar-static-top -->

       

        <div id="page-wrapper" style='margin:0 0 0 0;' >
            <div class="row">
                <input type="hidden" id='id_de_retorno' value=""/>
                <? if($_REQUEST['s']=="venta") { ?>
                <div class="col-lg-12">
                        <div class="btn-group" style='margin-top:5px;'>
                          <button type="button" class="btn btn-success btn-lg" id='tipo_transaccion_' tipo='1'>Venta</button>
                          <button type="button" class="btn btn-success dropdown-toggle btn-lg" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" id='tipo_transaccion'>
                            <li><a href="1">Venta</a></li>
                            <? if(permiso("Facturacion",$iUsuarioEmpresaId)) { ?> <li><a href="2">Factura</a></li> <? } ?>
                           <!-- <li><a href="2">Factura</a></li>-->
                            <li><a href="3">Cotización</a></li>
                          </ul>
                        </div>
                 
                	<div class="btn-group" style='margin-top:5px;'>
                          <button type="button" class="btn btn-default btn-lg"><i class="fa fa-user"></i> <span iClienteId='0' id='iCliente'></span></button>
                          <button type="button" class="btn btn-default dropdown-toggle btn-lg" id='seleccionar-cliente' data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only"></span>
                          </button>                       
                       </div>

                 </div>  
                        
				<? } else { ?>
                	<div class="col-lg-12">
                    <h1 class="page-header"><?=$_REQUEST['s'];?></h1>
                    </div>
				<? } ?>                    
               
                <!-- /.col-lg-12 -->
            </div>
           
            <!-- /.row -->
            <div class="row">
                
                <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                <?
				if(isset($_REQUEST['m']) && isset($_REQUEST['s']))
					include("./".$_REQUEST['m']."/".$_REQUEST['s']."/index.php");
				else if(isset($_REQUEST['m']))
					include("./".$_REQUEST['m']."/index.php");
				else 	
					include("./escritorio.php");
                ?>              
                
                <!-- Split button -->
    
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    

    <!-- Tooltip -->    
 	   <div class="tooltip">
    	<div class="tooltip-inner">
	    </div>
		<div class="tooltip-arrow"></div>
	</div>
	
     
    <!-- Modal -->
    <div class="modal fade" id="ventana-emergente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
          </div>
          <div class="modal-body">
            ...
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

  
    
</body>

</html>
