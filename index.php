<? include("./acceso/seguridad.php"); 
//if(!isset($_REQUEST['develop']))
//die("Sistema en Mantenimiento");
?>
<!DOCTYPE html>
<html>
<head> 
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jeans Beronna</title>
    <!-- Core CSS - Include with every page -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- SB Admin CSS - Include with every page -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">   
    <link href="js/plugins/datepicker/css/datepicker3.css" rel="stylesheet">       
    <link href="js/plugins/timepicker/timepicker.css" rel="stylesheet">           
    <link href="css/style.css" rel="stylesheet">   
    <link rel="stylesheet" type="text/css" href="css/jHtmlArea.css" />

	<script src="js/jquery-1.10.2.js"></script>
    <script src="js/jHtmlArea-0.8.min.js" type="text/javascript" ></script>
	<!-- Core Scripts - Include with every page -->    
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jasny-bootstrap.min.js"></script>    
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>  

    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>        
    <script src="js/plugins/bootstrap.confirmation.js"></script> 
    <script src="js/plugins/timepicker/timepicker.js"></script>    
    <script src="js/plugins/datepicker/js/bootstrap-datepicker.js"></script>    
    <script src="js/plugins/datepicker/js/locales/bootstrap-datepicker.es.js"></script>             
    <script src="js/typeahead.min.js"></script>   

    
    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>
    <!-- AdminBox Scripts - Include with every page -->
    <script src="js/adminbox.js?v=1"></script>      
    <!-- Texto enriquecido -->
<!--    <script src="js/jHtmlArea.ColorPickerMenu-0.8.min.js" type="text/javascript"></script>
-->
    
    <script src="js/prettify.js"></script>
    <script src="js/hotkeys.js"></script>
    <script src="js/bootstrap-wysiwyg.js"></script>  

</head>
<body>

  <div id='tabla___'></div> 
    <div id="wrapper">
    <form action="#" id="form_pdf" method="post" target="_blank" style='display:none;'>
	    <textarea id="t_codigo" name='t_codigo'></textarea>
    </form>
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a  href="index.php">
                   <img src="./imagen/logotipo.jpg" height="50" alt="Jeans">
                </a>                              
            </div>

             <div class="nav navbar-default navbar-left">
                <!-- /.navbar-header -->
                <ul class="nav navbar-top-links">     
                    <li class="dropdown" id="t_empresa">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">    
                        <?
                             if($sEmpresa=="") { $texto_empresa="Seleccione  <i class='fa fa-caret-down'></i>"; }
                             else { $texto_empresa="<h4 style='margin:0px;'>$sEmpresa <br><small>$sNombre $sPaterno $sMaterno</small> <i class='fa fa-caret-down'></i></h4>"; }
                        ?>                  
                            <span id="empresa_seleccionada"><?=$texto_empresa;?></span> 
                        </a>                    
                        <div class="dropdown-menu" style='padding:0px;'>
                            <?       
                            include("./empresasv2.php"); ?>
                        </div>
                    </li>
                    <!-- /.dropdown -->
                </ul>
            </div> 

            
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right">     
            <? if(strtolower($sUsuario[0])=='c') 
            echo "<li><label class='label label-success'>CONECTADO COMO ADMINISTRADOR</label></li>"; ?>
				<li class="dropdown hide">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    	<? $_REQUEST['badge']="si";
							include("./clases/estatus_mensajes.php"); 
						?>    
                       <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>                    
                    <ul class="dropdown-menu dropdown-messages">
                		<? 		 $_REQUEST['badge']="no";
						include("./clases/estatus_mensajes.php"); ?>
                    </ul>
                </li>
   <!-- /.dropdown -->
                <li class="dropdown hide">
                    <a class="dropdown-toggle hidden-xs hidden-sm" data-toggle="dropdown" href="#">
                        <i class="fa fa-tags fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-licencias">
                       <? include("./clases/estatus_licencia.php"); ?>
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">          
                        <li class="hide"><a href="./licencias.php" class="ver_licencias_usuario"><i class="fa fa-tags fa-fw"></i> Licencias </a>
                        <li><a href="./clases/cambio_clave.php" class="cambio_clave"><i class="fa fa-key"></i> Cambio de contraseña </a>
                        <li><a href="./acceso/salir.php"><i class="fa fa-sign-out fa-fw"></i> Desconectar</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
        </nav>
        <!-- /.navbar-static-top -->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
             <div class="hide"><? include("./empresas.php");?></div>
                <ul class="nav" id="side-menu">                                 
                    <li>
                        <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Escritorio</a>                       
                    </li>                    
	<? if(strtolower($sUsuario[0])=='a') { ?>
                      <li <?if($_REQUEST['m']=="administracion") echo "class='active'";?>>
                        <a href="#"><i class="fa fa-plus-square-o fa-fw"></i> Administraci&oacute;n<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                        	<li>
                                <a href="index.php?m=administracion&s=usuarios">Usuarios</a>
                            </li>
                            <li>
                                <a href="index.php?m=administracion&s=sistemas">Sistemas</a>
                            </li>
                            <li>
                                <a href="index.php?m=administracion&s=modulos">Modulos</a>
                            </li>
                            <li>
                                <a href="index.php?m=administracion&s=licencias">Licencias</a>
                            </li>
                             <li>
                                <a href="index.php?m=administracion&s=notificaciones">Notificaciones</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
	<? } 
                 if((permiso("Inventario",$iUsuarioEmpresaId) || (permiso("Inventario",$iUsuarioEmpresaId) && permiso_modulo(false,$iEmpleadoId,"Inventario"))) && strtolower($sUsuario[0])!='a') {
                ?>
                    <li <?if($_REQUEST['m']=="ventas") echo "class='active'";?>>
                        <a href="#"><i class="fa fa-plus-square-o fa-fw"></i> Ventas<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">

                            <? if(permiso_modulo("venta",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                            <li>
                                <a href="index.php?m=ventas&s=venta">Venta/Edo. Cuenta</a>
                            </li>
                            <? } if(permiso_modulo("reporteventas",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                            <li>
                                <a href="index.php?m=ventas&s=reporteventas">Cobranza</a>
                            </li>
                            <? } 
                            if(permiso_modulo("concentradoventas",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                            <li>
                                <a href="index.php?m=ventas&s=concentradoventas">Concentrado Ventas</a>
                            </li>
                            <? } if(permiso_modulo("auxiliarclientes",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                            <li>
                                <a href="index.php?m=ventas&s=auxiliarclientes">Auxiliar Clientes</a>
                            </li>
                            <? } 
if(permiso_modulo("cortecaja",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                             <li>
                                <a href="index.php?m=ventas&s=cortecaja">Corte de Caja</a>
                            </li>
                               <? }
                            /*if(permiso_modulo("reportefacturas",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                             <li>
                                <a href="index.php?m=ventas&s=reportefacturas">Control de Operaciones</a>
                            </li>
                             <? } if(permiso_modulo("cortecaja",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                             <li>
                                <a href="index.php?m=ventas&s=cortecaja">Corte de Caja</a>
                            </li>
                               <? } if(permiso_modulo("facturapublicogeneral",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                             <li>
                                <a href="index.php?m=ventas&s=facturapublicogeneral">Factura Ventas Mostrador</a>
                            </li>
                            <? } */?>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>  
                  <? }  
/*
                  
                if((permiso("Inventario",$iUsuarioEmpresaId) || (permiso("Inventario",$iUsuarioEmpresaId) && permiso_modulo(false,$iEmpleadoId,"Inventario"))) && strtolower($sUsuario[0])!='a') {
                ?>
                      <li <?if($_REQUEST['m']=="reportes") echo "class='active'";?>>
                        <a href="#"><i class="fa fa-plus-square-o fa-fw"></i> Reportes<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">

                            <? if(permiso_modulo("utilidad",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                            <li>
                                <a href="index.php?m=reportes&s=utilidad">Utilidad</a>
                            </li>                           
                            <? }  ?>

                            <? if(permiso_modulo("desplazamiento",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                            <li>
                                <a href="index.php?m=reportes&s=desplazamiento">Desplazamiento de Producto</a>
                            </li>                           
                            <? }  ?>
                            
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>  
                  <? } */ ?>
                      <li id="cat_menu" <? if($iUsuarioEmpresaId<=0 || $_REQUEST['m']=="general") {  echo 'class="active"'; } ?>>
                        <a href="#"><i class="fa fa-plus-square-o fa-fw"></i> Generales<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                     
                         <?  if(permiso_modulo("empresas",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                        	<li>
                                <a href="index.php?m=general&s=empresas" id="n_empresa">Distribuidores</a>
                            </li>
                          
                             <?  if(permiso_modulo("servicios",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                              <li class="hide">
                                <a href="index.php?m=general&s=servicios">Servicios</a>
                            </li>  
                             <? } if(permiso_modulo("proveedores",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?>                           
                            <li>
                                <a href="index.php?m=general&s=proveedores">Proveedores</a>
                            </li>
                             <? } if(permiso_modulo("categorias",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?>                             
                            <li>
                                <a href="index.php?m=general&s=categorias">Categorias</a>
                            </li>  
                             <? } ?>
                             <? if(permiso_modulo("productos",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                            <li>
                                <a href="index.php?m=general&s=productos">Productos</a>
                            </li> 
                             <? } 
  if(permiso_modulo("ajustematriz",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                            <li class="hide">
                                <a href="index.php?m=inventario&s=ajustematriz">Inventario en Matriz</a>
                            </li>
                             <? }
                             } ?> 
                           
                            <? if($iUsuarioEmpresaId>0 && strtolower($sUsuario[0])!='a') { ?>                                                   
                             
                           
                             <?  if(permiso_modulo("impuestos",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                            <li class="hide">
                                <a href="index.php?m=general&s=impuestos">Impuestos</a>
                            </li> 
                            <? } } ?>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>     
                    <? if(  (permiso("Inventario",$iUsuarioEmpresaId) || (permiso("Inventario",$iUsuarioEmpresaId) &&  permiso_modulo(false,$iEmpleadoId,"Inventario")) )  && strtolower($sUsuario[0])!='a' ) { ?>              
                     <li <?if($_REQUEST['m']=="inventario") echo "class='active'";?>>
                        <a href="#"><i class="fa fa-plus-square-o fa-fw"></i> Cuenta<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                             <? if(permiso_modulo("ajuste",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                        	<li>
                                <a href="index.php?m=inventario&s=ajuste">Inventario en Cuenta</a>
                            </li>
                             <?   }
                           

                             if(permiso_modulo("enviomatriz",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                            <li class="hide">
                                <a href="index.php?m=inventario&s=enviomatriz">Envio a Matriz</a>
                            </li>
                             <? }


                              if(permiso_modulo("movimientos",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                            <li class="hide">
                                <a href="index.php?m=inventario&s=movimientos">Movimientos</a>
                            </li>        
                             <? } if(permiso_modulo("capitalactivo",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?>                     
                             <li class="hide">
                                <a href="index.php?m=inventario&s=capitalactivo">Capital Activo</a>
                            </li> 
                            <? } if(permiso_modulo("altas",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?>                    
                             <li>
                                <a href="index.php?m=inventario&s=altas">Transferencia de mercancía</a>
                            </li>
                            <? } if(permiso_modulo("reportealtas",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?>          
                             <li>
                                <a href="index.php?m=inventario&s=reportealtas">Reporte envíos</a>
                            </li>      
                            <? }  ?>    

                               <? if(permiso_modulo("clientes",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?>                         
                            <li>
                                <a href="index.php?m=general&s=clientes">Clientes</a>
                            </li>   
                             <? } if(permiso_modulo("empleados",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?>                          
                            <li>
                                <a href="index.php?m=general&s=empleados">Accesos de Cuenta</a>
                            </li> 
                             <? } ?>


                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
	<? }  ?>


    <? /* if((permiso("Tienda",$iUsuarioEmpresaId) || (permiso("Tienda",$iUsuarioEmpresaId) &&  permiso_modulo(false,$iEmpleadoId,"Tienda"))) && strtolower($sUsuario[0])!='a') { ?>
                     <li <?if($_REQUEST['m']=="paginaweb") echo "class='active'";?> >
                        <a href="#"><i class="fa fa-plus-square-o fa-fw"></i> Página Web<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
        <? if(permiso_modulo("entradas",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?>                    
                            <li>
                                <a href="index.php?m=paginaweb&s=entradas">Entradas</a>
                            </li>
        <? } if(permiso_modulo("listaentradas",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?>                    
                            <li>
                                <a href="index.php?m=paginaweb&s=listaentradas">Lista de entrada</a>
                            </li>          
        <? }  ?>                    
                                                     
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
    <? } */ ?>



                   <? if((permiso("tintoreria",$iUsuarioEmpresaId) || (permiso("tintoreria",$iUsuarioEmpresaId) &&  permiso_modulo(false,$iEmpleadoId,"tintoreria"))) && strtolower($sUsuario[0])!='a') { ?>
                      <li <?if($_REQUEST['m']=="ventas") echo "class='active'";?>>
                        <a href="#"><i class="fa fa-plus-square-o fa-fw"></i> Tintoreria<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">

                            <? if(permiso_modulo("actividadesdiarias",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                            <li>
                                <a href="index.php?m=tintoreria&s=actividadesdiarias">Registrar actividad</a>
                            </li>
                            <? } ?>

                            <? if(permiso_modulo("entregaproducto",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                            <li>
                                <a href="index.php?m=tintoreria&s=entregaproducto">Entrega de prendas</a>
                            </li>
                            <? } ?>

                             <? if(permiso_modulo("reportecomisiones",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                            <li>
                                <a href="index.php?m=tintoreria&s=reportecomisiones">Reporte de comisiones</a>
                            </li>
                            <? } ?>
                            <? if(permiso_modulo("reporteactividadentrega",$iEmpleadoId) || strtolower($sUsuario[0])=='c') { ?> 
                            <li>
                                <a href="index.php?m=tintoreria&s=reporteactividadentrega">Reporte General</a>
                            </li>
                            <? } ?>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>  
                  <? } ?>    

                    <!--Reestructuracion de modulo de creditos
      	 				<li <?if($_REQUEST['m']=="credito") echo "class='active'";?>>
                        <a href="#"><i class="fa fa-plus-square-o fa-fw"></i> Crédito / Abonos<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                        	<li>
                                <a href="index.php?m=credito&s=cargoabono">Cargo / Abono</a>
                            </li>
                            <li>
                                <a href="index.php?m=credito&s=estadocuenta">Estado de Cuenta</a>
                            </li>                           
                        </ul>
                        
                    </li>-->

                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->
        <div id="page-wrapper">
            <div class="row"> 
                <input type="hidden" id='id_de_retorno' value=""/>
                <? if(($_REQUEST['m']=="ventas" && ($_REQUEST['s']!="cortecaja" && $_REQUEST['s']!="concentradoventas"))|| ($_REQUEST['m']=="tintoreria")|| ($_REQUEST['m']=="reportes")) { ?>
                <div class="col-lg-8 form-inline">
                        <div class="form-group btn-group" style='margin-top:5px;'>
					      <? if($_REQUEST['s']=="reporteventas" || $_REQUEST['s']=="auxiliarclientes" || $_REQUEST['s']=="reportefacturas"|| $_REQUEST['s']=="utilidad") { echo '<button type="button" class="hide btn btn-success btn-lg" id="tipo_transaccion_" tipo="0">Todos los clientes</button>'; } 
                            //else if($_REQUEST['s']=="reportefacturas") { echo '<button type="button" class="btn btn-success btn-lg" id="tipo_transaccion_" tipo="2">Factura</button>'; }
                          else { ?>
                            <h3>Seleccione cliente </h3>
                          <button type="button" class="btn btn-success btn-lg hide" id="tipo_transaccion_" tipo="1">Venta</button>
                          <? } ?>
                          <button type="button" class="btn btn-success dropdown-toggle btn-lg hide" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" id='tipo_transaccion'>
                           	<? 	if($_REQUEST['s']=="reporteventas"|| $_REQUEST['s']=="utilidad" || $_REQUEST['s']=="desplazamiento") { echo '<li><a href="0">Todos</a></li>'; 

								if(permiso("Tienda",$iUsuarioEmpresaId)) { ?> <li><a href="4">Tienda en Linea</a></li> <? }

							}

                           	if($_REQUEST['s']=="reportefacturas") { ?>

                            <? if(permiso("Facturacion",$iUsuarioEmpresaId)) { ?> <li><a href="2">Factura</a></li> <? } 
                                if(permiso("Inventario",$iUsuarioEmpresaId)) { ?> <li><a href="1">Venta</a></li> <? } 
								} else {
                            	if(permiso("Inventario",$iUsuarioEmpresaId)) { ?> <li><a href="1">Venta</a></li> <? } 
								if(permiso("Facturacion",$iUsuarioEmpresaId)) { ?> <li><a href="2">Factura</a></li> <? }
								if(permiso("Cotizacion",$iUsuarioEmpresaId)) { ?> <li><a href="3">Cotización</a></li> <? }
						   		
                            	}
                            ?>
                          </ul>

                        </div>     
                                 
                	   <div class="form-group btn-group" style='margin-top:5px;'>
                          <button type="button" class="btn btn-default btn-lg"><i class="fa fa-user"></i> <span iClienteId='0' id='iCliente'>  <? if($_REQUEST['s']=="reporteventas" || $_REQUEST['s']=="auxiliarclientes"|| $_REQUEST['s']=="reportefacturas" || $_REQUEST['s']=="utilidad" || $_REQUEST['s']=="desplazamiento" || $_REQUEST['s']=="reportecomisiones" || $_REQUEST['s']=="actividadesdiarias" || $_REQUEST['s']=="entregaproducto" || $_REQUEST['s']=="reporteactividadentrega" ) { echo 'Todos los clientes'; }?></span></button>
                          <button type="button" class="btn btn-default dropdown-toggle btn-lg" id='seleccionar-cliente' data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only"></span>
                          </button>       

                       </div>   


                     
                            
                 </div>                     
   <div id="saldo_limite" class="col-lg-4"></div>
				<? } 				        
			
				else if ($_REQUEST['s']=="reportenomina"){?>
					<div class="col-lg-12">
                    <h1 class="page-header">Reporte de Nóminas</h1>
                    </div>
				<?				
				}else if ($_REQUEST['s']=="cortecaja"){?>
                    <div class="col-lg-12">
                    <h1 class="page-header">Corte de Caja</h1>
                    </div>
                <? }else if ($_REQUEST['s']=="concentradoventas"){?>
                    <div class="col-lg-12">
                    <h1 class="page-header">Concentrado de Ventas</h1>
                    </div>
                <? }else if ($_REQUEST['s']=="enviomatriz"){?>
                    <div class="col-lg-12">
                    <h1 class="page-header">Envio a Matriz</h1>
                    </div>
                <? }else if ($_REQUEST['s']=="generar"){?>
					<div class="col-lg-12">
                    <h1 class="page-header">Generar nómina</h1>
                    </div>
				<? }
                else if ($_REQUEST['s']=="entradas") {?>
                <div class="col-lg-12">
                    <h1 class="page-header">Añadir Nueva Entrada</h1>
                </div>
                <?
                    }
                    else if ($_REQUEST['s']=="listaentradas") {?>
                <div class="col-lg-12">
                    <h1 class="page-header">Lista de Entradas</h1>
                </div>                                
                <?
                    }
                    else if ($_REQUEST['s']=="ajustematriz") {?>
                <div class="col-lg-12">
                    <h1 class="page-header">Inventario en Matriz</h1>
                </div>                                
                <? }  else if ($_REQUEST['s']=="capitalactivo") {?>
                <div class="col-lg-12">
                    <h1 class="page-header">Capital Activo</h1>
                </div>     
                      <? }       else if ($_REQUEST['s']=="devolucionclienteanterior") { ?>
                <div class="col-lg-12">
                    <h1 class="page-header">Devolución de clientes con saldo pendiente</h1>
                </div>                                
                                           
                <? }       else if ($_REQUEST['s']=="altas") {?>
                <div class="col-lg-12">
                    <h1 class="page-header">Envio de Mercancia a 
                    <?
                    echo "<select style='width:200px;' id='empresapara'>";
                    $select = seleccionar("empresa","empresa.sEmpresa,empresa.iId","empresa.iEstatus=0 and iId!=$iEmpresaId",false, false);
                        foreach($select as $dato) {
                            echo "<option value='".$dato['iId']."'>".$dato['sEmpresa']."</option>";
                        }
                    echo "</select>";
                    ?></h1>
                </div>                                
                <? }      else if ($_REQUEST['s']=="ajuste") {?>
                <div class="col-lg-12">
                    <h1 class="page-header">Inventario actual en Cuenta <?php echo $sEmpresa; ?></h1>
                </div>                                
                <? } else if ($_REQUEST['s']=="empresas") {?>
                <div class="col-lg-12">
                    <h1 class="page-header">Distribuidores</h1>
                </div>                                
                <? }
                else if ($_REQUEST['s']=="reportealtas") {?>
                <div class="col-lg-12">
                    <h1 class="page-header">Reporte de Altas en Inventario</h1>
                </div>                                
                <? }
                  else if ($_REQUEST['s']=="utilidad") {?>
                <div class="col-lg-12">
                    <h1 class="page-header">Reporte de utilidad</h1>
                </div>                                
                <? }
                else if ($_REQUEST['s']=="desplazamiento") {?>
                <div class="col-lg-12">
                    <h1 class="page-header">Desplazamiento de Producto</h1>
                </div>                                
                <? }
                   else if ($_REQUEST['s']=="empleados") {?>
                <div class="col-lg-12">
                    <h1 class="page-header">Accesos de Cuenta</h1>
                </div>                                
                <? }
                
                else if ($_REQUEST['s']=="facturapublicogeneral") {?>
                <div class="col-lg-12">
                    <h1 class="page-header">Seleccione ventas para realizar factura a mostrador</h1>
                </div>                                
                <? }else { ?>
                	<div class="col-lg-12">
                    <h1 class="page-header"><?=$_REQUEST['s'];?></h1>
                    </div>
				<? } ?>                                   
                <!-- /.col-lg-12 -->
            </div>           
            <!-- /.row -->
            <div class="row">                
                <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                <?
				if(isset($_REQUEST['m']) && isset($_REQUEST['s']))
					include("./".$_REQUEST['m']."/".$_REQUEST['s']."/index.php");
				else if(isset($_REQUEST['m']))
					include("./".$_REQUEST['m']."/index.php");
				else 	
					include("./escritorio.php");
				?>                              
                <!-- Split button -->    
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->    
    <!-- Tooltip -->    
 	   <div class="tooltip">
    	<div class="tooltip-inner">
	    </div>
		<div class="tooltip-arrow"></div>
	</div>	     
    <!-- Modal -->
    
    <div class="modal fade" id="ventana-emergente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-refresh="true" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
          </div>
          <div class="modal-body">
            ...
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->    
</body>
</html>