<?

include_once("./acceso/seguridad.php");

$_tabla_general = "./inventario/ajuste/tabla.php";

?>

<div class="row">

  <div class="col-lg-3">



          <label>Categoría:</label>

	<select class="form-control" name='iCategoriaId' no_catalogo="no">

		<option value="0">Todas</option>    

		<?

		$select = seleccionar("categoria","iId,sCodigo,sNombre","iEstatus=0 and iTipoCategoria=1",false, false);

		foreach($select as $dato) {

			?>

			<option value="<?=$dato['iId'];?>"><?=$dato['sCodigo']."-".$dato['sNombre'];?></option>

			<?

		}

		?>

	</select>

  </div> 

   <div class="col-lg-3"></div>

	<? include("./clases/exportacion.php"); ?>

</div>



<div class="row">

    <div class="col-lg-12">    		

      	<div id="div-tabla"></div>    

	</div>

</div>        



<script>

$(document).ready(function () {

	$("#div-tabla").crear_tabla_general("tabla_ajuste","<?=$_tabla_general;?>",false,false);

	

	$("[name='iCategoriaId']").change(function(){

		if($(this).val()!="0") {

			var condicion = "iCategoriaId="+$(this).val();

			$("#div-tabla").crear_tabla_general("tabla_ajuste","<?=$_tabla_general;?>",condicion,true);

		}else 

			$("#div-tabla").crear_tabla_general("tabla_ajuste","<?=$_tabla_general;?>",false,true);

	});

	

	$("body").on("change","[name='ajustes_input']",function(){

		var elemento = $(this);

		var iProductoId = $(this).attr("iProductoId");

		var iMinimo= $(this).attr("iMinimo");		

		var ExistenciaReal= $(this).val();		

		$(elemento).cargando("Ajustando...");

		$.post("./inventario/ajuste/modifica_existencia.php",{iProductoId:iProductoId,iMinimo:iMinimo,ExistenciaReal:ExistenciaReal},function(data){

			data = eval(data);

			if(data[1]>0) {

				$(elemento).cerrar_cargando();

				$(elemento).closest( "tr" ).find('td:eq(3)').html(data[0]);	

			} else {

				$(elemento).error(data[0]);

			}

		});		

	});

	



	

	

	



});



</script>



