<?
include("../../acceso/seguridad.php");
if($iUsuarioEmpresaId=="60"){
  $hide_m = "";
}else {
  $hide_m="hide";
}
$tabla = $_REQUEST['tabla'];
$condicion_ = $_REQUEST['condicion'];
if($condicion_=="" || $condicion_=="false") {
	$condicion = "iEstatus=0 and iControlInventario='1'";
}
else {
	$condicion = $_REQUEST['condicion']." and iEstatus=0 and iControlInventario='1'";
}
?>
<table class="table table-bordered table-striped" id="<?=$tabla?>">
  <thead>
    <th>
        Código      
    </th>
    <th class="hidden-xs">
        Producto
    </th>
    <th class="hidden-xs">
        Ubic. Física
    </th>
    <th>
        Cantidad Actual
    </th>
    <th class="hidden-xs">
        Exist. Mínima
    </th>
    <th no_visible="no" class="<?=$hide_m?>">
        Existencia Real en Inventario
    </th>
	</thead>
   <tbody>   
   <?
$datos = seleccionar("producto","iId,sCodigo,sNombre,sUbicacionFisica,iExistenciaMin",$condicion,false, false);                            
   foreach($datos as $renglones) {
    $exist= existencia($renglones['iId']);
	  if( $renglones['sUbicacionFisica'] =="") $ubicacion_fisica = "n/a";
	  else $ubicacion_fisica = $renglones['sUbicacionFisica'];
    if($exist>0 || $iUsuarioEmpresaId==60) {
      $texist +=$exist;
       echo '<tr>';
        echo '<td>'.$renglones['sCodigo'].'</td>';
        echo '<td  class="hidden-xs">'.$renglones['sNombre'].'</td>';
        echo '<td class="hidden-xs">'.$ubicacion_fisica.'</td>';
        echo '<td>'.inventario($exist,$renglones['iExistenciaMin']).'</td>';
        echo '<td class="hidden-xs">'.$renglones['iExistenciaMin'].'</td>';                                                
        echo '<td class="'.$hide_m.'"><input type="text" name="ajustes_input" class="form-control" value="'.$exist.'" iProductoId="'.$renglones['iId'].'" iMinimo="'.$renglones['iExistenciaMin'].'"/></td>';                                                		
       echo '</tr>';
     }
   }
   ?>                             
</tbody>
<tfoot><tr><td colspan="3" style='text-align: right;'><h3>Total</h3></td><td><h3><?php echo $texist;?></h3></td><td colspan="2"></td></tr></tfoot>
</table>