<?
include("../../acceso/seguridad.php");
$tabla = $_REQUEST['tabla'];
$condicion_ = $_REQUEST['condicion'];
if($condicion_=="" || $condicion_=="false") {
	$condicion = "iEstatus=0 and iControlInventario='1'";
}
else {
	$condicion = $_REQUEST['condicion']." and iEstatus=0 and iControlInventario='1'";
}
?>
<table class="table table-bordered table-striped" id="<?=$tabla?>">
  <thead>
    <th>
        Código      
    </th>
    <th class="hidden-xs">
        Producto
    </th>
    <th class="hidden-xs">
        Ubic. Física
    </th>
    <th>
        Cantidad Actual
    </th>
    <th class="hidden-xs">
        Exist. Mínima
    </th>
    <th no_visible="no">
        Existencia Real en Inventario
    </th>
	</thead>
   <tbody>   
   <?
$datos = seleccionar("producto","iId,sCodigo,sNombre,sUbicacionFisica,iExistenciaMin",$condicion,false, false);                            
   foreach($datos as $renglones) {
	  if( $renglones['sUbicacionFisica'] =="") $ubicacion_fisica = "n/a";
	  else $ubicacion_fisica = $renglones['sUbicacionFisica'];
       echo '<tr>';
        echo '<td>'.$renglones['sCodigo'].'</td>';
        echo '<td  class="hidden-xs">'.$renglones['sNombre'].'</td>';
        echo '<td class="hidden-xs">'.$ubicacion_fisica.'</td>';
        echo '<td>'.inventario(existenciaMatriz($renglones['iId'],1),$renglones['iExistenciaMin']).'</td>';
        echo '<td class="hidden-xs">'.$renglones['iExistenciaMin'].'</td>';                                                
        echo '<td><input type="text" name="ajustes_input" class="form-control" value="'.existenciaMatriz($renglones['iId'],1).'" iProductoId="'.$renglones['iId'].'" iMinimo="'.$renglones['iExistenciaMin'].'"/></td>';                                                		
       echo '</tr>';
   }
   ?>                             
</tbody>
</table>