<?
include("../../acceso/seguridad.php");
$tabla = $_REQUEST['tabla'];
$condicion_ = $_REQUEST['condicion'];
if($condicion_=="" || $condicion_=="false") {
	$condicion = "iEstatus=0 and iUsuarioEmpresaId='".$iUsuarioEmpresaId."' and iControlInventario='1'";
}
else {
	$condicion = $_REQUEST['condicion']." and iEstatus=0 and iUsuarioEmpresaId='".$iUsuarioEmpresaId."' and iControlInventario='1'";
}
?>
<table class="table table-bordered table-striped" id="<?=$tabla?>">
  <thead>
    <th>
        Código      
    </th>
    <th class="hidden-xs">
        Producto
    </th>
   	<th>
        Cantidad Actual
    </th>
    <th>
        Precio de Compra
    </th>
    <th>
    	Precio de Venta
    </th>
    <th>
    	Capital Activo
    </th>
    
	</thead>
   <tbody>   
   <?
$datos = seleccionar("producto","iId,sCodigo,sNombre,fPrecioCompra,fPrecioVenta",$condicion,false, false);                            
   foreach($datos as $renglones) {
    $t_precioventa +=$renglones['fPrecioVenta'];
    $t_preciocompra +=$renglones['fPrecioCompra'];
    $t_cantidad += existencia($renglones['iId']);
    $t_total += existencia($renglones['iId'])*$renglones['fPrecioVenta'];
	   echo '<tr>';
        echo '<td>'.$renglones['sCodigo'].'</td>';
        echo '<td  class="hidden-xs">'.$renglones['sNombre'].'</td>';
        echo '<td>'.inventario(existencia($renglones['iId']),$renglones['iExistenciaMin']).'</td>';
		echo '<td>$'.number_format($renglones['fPrecioCompra'],2).'</td>';
		echo '<td>$'.number_format($renglones['fPrecioVenta'],2).'</td>';
		echo '<td>$'.number_format(existencia($renglones['iId'])*$renglones['fPrecioVenta'],2).'</td>';
       echo '</tr>';
   }
   ?>                             
</tbody>
<tfoot>
<tr>
    <th colspan="2">
      Totales
    </th>
    <th>
        <?=number_format($t_cantidad,0);?>
    </th>
    <th>
      $<?=number_format($t_preciocompra,2);?>
    </th>
    <th>
      $<?=number_format($t_precioventa,2);?>
    </th>
    <th>
       $<?=number_format($t_total,2);?>
    </th>
   
</tr>
  </tfoot>
</table>