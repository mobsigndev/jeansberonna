<?
include_once("./acceso/seguridad.php");

$_tabla_general = "./inventario/devolucionclienteanterior/tabla.php";

?>

<div class="row">

  <div class="col-lg-3">



          <label>Categoría:</label>

	<select class="form-control" name='iCategoriaId' no_catalogo="no">

		<option value="0">Todas</option>    

		<?

		$select = seleccionar("categoria","iId,sCodigo,sNombre","iEstatus=0 and iTipoCategoria=1",false, false);

		foreach($select as $dato) {

			?>

			<option value="<?=$dato['iId'];?>"><?=$dato['sCodigo']."-".$dato['sNombre'];?></option>

			<?

		}

		?>

	</select>

  </div> 

   <div class="col-lg-3"></div>

	<? include("./clases/exportacion.php"); ?>

</div>



<div class="row">

    <div class="col-lg-12">    		

      	<div id="div-tabla"></div>    


	</div>

	<div class="col-lg-6">
	<input type="text" id="iClienteId" style="display:none;" value="<?php echo $_REQUEST['c'];?>">
      	<h3>Lista de devolución</h3>
      	<table id="listaregresar" class="table">
      		<thead><tr><th width="60" class="text-center"><strong>Cantidad</strong></th><th><strong>Producto</strong></th><th  width="200" class="text-center"><strong>Total</strong></th><th></th></tr></thead>
      		<tbody></tbody>
      		<tfoot><tr><td></td><td class="text-right">Total</td><td id='total_' class="text-center"></td><td></td></tr></tfoot>
      		      	</table>
      	<button id='realizardevolucion' class='btn btn-success'>REALIZAR DEVOLUCIÓN</button>
   	</div>

</div>        



<script>

$(document).ready(function () {

	$("#div-tabla").crear_tabla_general("tabla_ajuste","<?=$_tabla_general;?>",false,false);

	

	$("[name='iCategoriaId']").change(function(){

		if($(this).val()!="0") {

			var condicion = "iCategoriaId="+$(this).val();

			$("#div-tabla").crear_tabla_general("tabla_ajuste","<?=$_tabla_general;?>",condicion,true);

		}else 

			$("#div-tabla").crear_tabla_general("tabla_ajuste","<?=$_tabla_general;?>",false,true);

	});

	var total_=0;
	$("body").on("click",".quita",function(){
		$(this).closest("tr").remove();
		total_ -= limpia_formato($(this).attr("data-total"));
		$("#total_").html(number_format(total_,2));

	})
	
	
	$("body").on("click","#realizardevolucion",function(){
		var elemento0 = $(this);
		
		$(elemento0).confirmar("¿Seguro que desea realizar devolución","Si","No",function(respuesta){
	if(respuesta) {
		$(elemento0).attr("disabled","disabled");
		$(elemento0).cargando("Espere...");
		var cantidad_=$('#listaregresar > tbody  > tr').length;
		var c=0;
		var fMonto = $("#total_").html();
		fMonto=limpia_formato(fMonto);
		$('#listaregresar > tbody  > tr').each(function() {

			//console.log($(this).attr("data-iProductoId")+","+$(this).attr("data-cantidad"));
			var elemento = $(this);
				var precio = $(this).attr("data-precio");
		var iClienteId = $("#iClienteId").val();
			$.post("./inventario/devolucionclienteanterior/realizar_devolucion.php",{iProductoId:$(this).attr("data-iProductoId"),
				ExistenciaReal:$(this).attr("data-cantidad"),fPrecio:precio,iClienteId:iClienteId},function(data){
				c++;
				$(elemento).css("color","green");
				if(c==cantidad_){
						$.post("./inventario/devolucionclienteanterior/descuenta_devolucion.php",{iClienteId:iClienteId,fMonto:fMonto},function(data){
							$.post("./inventario/devolucionclienteanterior/ticket.php",{iClienteId:iClienteId},function(data) {
								printIt(data);
									$(elemento0).cerrar_cargando();
									$(elemento0).removeAttr("disabled");
									$('#listaregresar > tbody').html("");
									$('#total_').html("");

									$(elemento0).confirmar("Desea ir a pantalla de venta","Si","No",function(respuesta){
										if(respuesta) {
											window.location.href = './index.php?m=ventas&s=venta&icliente='+iClienteId;									
										}
									},'right');
							});	
						});
				}
			});
		});
			}},"right");

	});
	


	$("body").on("change","[name='ajustes_input_regresar']",function(){

		var elemento = $(this);
		var iProductoId = $(this).attr("data-iProductoId");
		var nombre = $(this).attr("data-nombre");
		var codigo = $(this).attr("data-codigo");
		var precio = $(this).attr("data-precio");
		var iClienteId = $("#iClienteId").val();
		var cantidad =$(this).val();
		if(cantidad=="" || cantidad=="0") return false;
		var total =cantidad*precio;
		total=number_format(total,2);
		total_ += limpia_formato(total);
		$("#total_").html(number_format(total_,2));
		$("#listaregresar tbody").append("<tr data-iProductoId='"+iProductoId+"' data-precio='"+precio+"' data-cantidad='"+cantidad+"'><td class='text-center'>"+cantidad+"</td><td> "+nombre+"</td><td class='text-center'>"+total+"</td><td><i class='fa fa-times quita' style='cursor:pointer;' data-total='"+total+"' > Quitar</i></td></tr>");
		$(this).val("0");
		//$(elemento).cargando("Agregando en lista de devolución...");

		/*$.post("./inventario/devolucionclienteanterior/modifica_existencia.php",{iProductoId:iProductoId,iMinimo:iMinimo,ExistenciaReal:ExistenciaReal},function(data){

			data = eval(data);

			if(data[1]>0) {

				$(elemento).cerrar_cargando();

				$(elemento).closest( "tr" ).find('td:eq(3)').html(data[0]);	

			} else {

				$(elemento).error(data[0]);

			}

		});	*/

	});

	



	

	

	



});



</script>



