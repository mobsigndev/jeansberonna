<?
include("../../acceso/seguridad.php");
$tabla = $_REQUEST['tabla'];
$condicion_ = $_REQUEST['condicion'];
if($condicion_=="" || $condicion_=="false") {
	$condicion = "iEstatus=0 and iControlInventario='1'";
}
else {
	$condicion = $_REQUEST['condicion']." and iEstatus=0 and iControlInventario='1'";
}
?>
<table class="table table-bordered table-striped" id="<?=$tabla?>">
  <thead>
    <th>
        Código      
    </th>
    <th class="hidden-xs">
        Producto
    <th no_visible="no" >
       Cantidad a Regresar
    </th>
	</thead>
   <tbody>   
   <?
$datos = seleccionar("producto","iId,sCodigo,sNombre,fPrecioVenta,sUbicacionFisica,iExistenciaMin",$condicion,false, false);                            
   foreach($datos as $renglones) {
	  if( $renglones['sUbicacionFisica'] =="") $ubicacion_fisica = "n/a";
	  else $ubicacion_fisica = $renglones['sUbicacionFisica'];
       echo '<tr>';
        echo '<td>'.$renglones['sCodigo'].'</td>';
        echo '<td>'.$renglones['sNombre'].'</td>';
        echo '<td ><input type="text" data-precio="'.$renglones['fPrecioVenta'].'" name="ajustes_input_regresar" class="form-control" value="0" data-codigo="'.$renglones['sCodigo'].'" data-nombre="'.$renglones['sNombre'].'" data-iProductoId="'.$renglones['iId'].'" iMinimo="'.$renglones['iExistenciaMin'].'"/></td>';                                                		
       echo '</tr>';
   }
   ?>                             
</tbody>
</table>