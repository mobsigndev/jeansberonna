<?
if(isset($_REQUEST['reutiliza'])){
	$archivo = "inventario/ingreso/".$_REQUEST['reutiliza'].".json";
	$archivo_solo = $_REQUEST['reutiliza'].".json";
	$iclienter = $_REQUEST['cl'];
	$itipotransaccion = $_REQUEST['tp'];
	$reutiliza=true;
}
else{
	$archivo="inventario/ingreso/".$iCodigo.".json";	
	$reutiliza=false;
	$iclienter=0;
}

?>
<!--<script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>-->
<script type="text/javascript">
var totalsolicitudf;
var totalsolicitud;
var productos=0;
var modal=true;
var tipodocumento;
var tipopago;
var tipo_pago_real=1;
var credito=0;

$(document).ready(function () {


		$.ajaxSetup({ cache: false });	
		$("body").on("click","#id_de_retorno",function(e){
			if($(this).attr("tipo")=="cliente")
				carga_cliente($(this).val());
			if($(this).attr("tipo")=="producto"){
				agregar_producto($(this).val(),function(){});
			}
		}); 
		  
		
		$("body").on("click","[name='sel-producto']",function(e){			
		    agregar_producto($(this).attr("iid"),function(){});
		});
		
		
		
		$("body").on("click","#categorias-menu ul li a",function(e){
			e.preventDefault();
			var iProductoId = $(this).attr("href");
			var elemento = $(this);
			$(elemento).cargando("Agregando");
		    agregar_producto(iProductoId,function(){
				$(elemento).cerrar_cargando();
			});
			return false;
		});
	

		//carga_acceso_directo(1,828);
		function carga_acceso_directo(tipo,idtransaccion){
			if(tipo=="1") { var titulo="Venta"; }
			if(tipo=="2") { var titulo="Factura"; }
			if(tipo=="3") { var titulo="Cotización"; }
			var forms="<div class='row'><div class='col-lg-12 text-center'><h3>¿Que acción desea realizar?</h3>";
			
			forms+="<button type='button' class='btn btn-primary' tipo='"+tipo+"' id_transaccion='"+idtransaccion+"' id='imprimir' data-dismiss='modal'><i class='fa fa-floppy-o'></i> PDF</button>";
			forms+="<button type='button' class='btn btn-success' id_transaccion='"+idtransaccion+"' id='enviar_correo' style='margin:0 20px 0 20px;'><i class='fa fa-share'></i> Enviar por correo</button>";
			forms+="<button type='button' class='btn btn-warning' tipo='"+tipo+"' id_transaccion='"+idtransaccion+"' id='imprimir_ticket' style='margin:0 20px 0 0' data-dismiss='modal'><i class='fa fa-print'></i> Ticket </button>";
			forms+="<button type='button' class='btn btn-default' data-dismiss='modal'><i class='fa fa-times'></i> Ninguna</button></div></div>";			
			var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";
			vermodal(titulo+" Registrada",forms,"");

		}

		$("body").on("click","#imprimir_ticket",function(e){  
				var iTransaccionId = $(this).attr("id_transaccion");
				var tipo = $(this).attr("tipo");				
				$.post("./ventas/reporteventas/ticket.php",{iId:iTransaccionId},function(data) {
					printIt(data);
				});	
			e.preventDefault();
	    });



		
	 	
	 	$("body").on("click","#enviar_correo",function(e){  
				var iTransaccionId = $(this).attr("id_transaccion");
				var tipo = $(this).attr("tipo");				

				$.post("./clases/formulario_correo.php",{iTransaccionId:iTransaccionId},function(data){
					vermodal("Envio de Archivos",data,"");
				});		
			e.preventDefault();
	    });
	    	 $("body").on("click","#enviar_archivo",function(e){  
				var elemento_ = $(this);
				var iTransaccionId = $(this).attr("iTransaccionId");
				var sAsunto = $("[name='sAsunto']").val();
				var sDestinatario=$("[name='sDestinatario']").val();
				var sComentario = $("[name='sComentario']").val();
				var sDocumento =  $(this).attr("sDocumento");
				var url = "./reporte_pdf.php?iId="+iTransaccionId;
				var pdf = $("[id='xml_']").val();
				var xml = $("[id='pdf_']").val();
				var iEmpresaId = $("[id='iEmpresaId']").val();
				if(sDestinatario==""){
					$(elemento_).error("Especifique destinatario")
				} else {
					$(elemento_).cargando("Enviando correo");
					$.post("./ventas/reporteventas/envio_archivos.php",{url:url,sAsunto:sAsunto,sDestinatario:sDestinatario,sComentario:sComentario,sDocumento:sDocumento,xml:xml,pdf:pdf,iTransaccionId:iTransaccionId,iEmpresaId:iEmpresaId},function(data){
					console.log(data);
					$(elemento_).correcto(data);
					//$(elemento_).cerrar_cargando();
					});		
				}
				e.preventDefault();
	    });

$("body").on("click","#imprimir",function(e){  
				var iTransaccionId = $(this).attr("id_transaccion");
				$.post("./inventario/ingreso/ver_tipo.php",{iTransaccionId:iTransaccionId},function(data){
					data = eval(data);
					if(data[0]=="factura")
					 window.open("./facturacionE/empresa_xml32.php?id="+iTransaccionId+"&timbre=false","_blank");
             		else
                     window.open("./clases/a_pdf.php?ver=si&url=../ventas/reporteventas/reporte_pdf.php?iId="+iTransaccionId,"_blank");

				});				
				    				/*}
				else{			
					window.open('./ventas/reporteventas/reporte.php?iId='+iTransaccionId,'_blank');
				}*/
				e.preventDefault();
});




		$('#busqueda-producto').typeahead([{                                
		    name: 'producto',
		    displayKey: 'name',    
		    prefetch:{
		    	ttl: 0,	
			        url: './inventario/ingreso/productos.php',
				        filter: function(res){
							var items = res.productos;
							var mapped = [];
							if(items) {
								for(var i = 0; i < items.length; i++) {
									var item = items[i];
									mapped[i] = {
										value: item.sCodigo+" "+item.sNombre,
										id: item.iId,
									};
								}
							}
							return mapped;
				        }
		            },
		 	header:"<h2>Productos</h2>"             
		}]).on('typeahead:selected typeahead:autocompleted', function(e, producto) {
			agregar_producto(producto.id,function(){
				$("#busqueda-producto").typeahead('setQuery', '');
			});
		});

		$("#busqueda-producto").focus();
		
		

		function agregar_producto(id,listo){
			if(id){
				var cantidad_producto=1;
				var precio_diferente="";
				tipodocumento=$("#tipo_transaccion_").attr("tipo");
				if("<?=$iIncluyeCantidadProductos?>"=="1")
				cantidad_producto = prompt("Cuantos productos agregara?", "1");				
				if("<?=$iIncluyeSolicitudPrecio?>"=="1")
				precio_diferente = prompt("Si desea especificar otro precio escribalo aqui, de lo contrario deje vacio.", "");									
				$.post( "./inventario/ingreso/pedido.php", { archivo:"<?=$archivo_solo;?>",idproducto: id,accion:"agregarproducto",tipodocumento:tipodocumento,cantidad_producto:cantidad_producto,precio_diferente:precio_diferente }, function( data ) {
				data=jQuery.parseJSON(data);				
				//&& tipodocumento!=3
				if(data.codigo==1){
					var datos=data.datos[0];

					
					if(datos.existencia=="0"){
					    var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";            			
						vermodal("Error","Producto agotado!",botones);
					}else{
					    var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";            			
						vermodal("Error","Cantidad de producto disponible: "+data.datos[0].existencia,botones);						
					}

				}
				if(data.codigo==2){
					cargar_pedido();
				}
				listo();
				});
			}
		}

	
		cargar_pedido();	
		function cargar_pedido(){
			$("#tabla tbody").html(null);

			
			
			

			$.getJSON("<?=$archivo;?>", function( data ) {

			  	if (!(data.productos.length == 1 && data.productos[0] == "")){
				  $.each( data.productos, function( key, producto ) {				  							
							var renglon="<tr class=\"producto\" id="+producto.iId+" contador=\""+producto.contador+"\"><td class=\"hidden-xs hidden-sm\" width='50'>"+producto.contador+"</td><td class=\"hidden-xs hidden-sm\" width='50'>"+producto.sUnidad+"</td><td width='50'>"+producto.cantidad+"</td><td>"+producto.sNombre+"</td></tr>";															
						$("#tabla tbody").append(renglon);
						productos++;			  	
				  });

				

		
			  }					
			  if(data.comentarioventa!=""){
			  	$("#comentarioventajson").text(data.comentarioventa);
			  }
			  if(data.comentariodescuento!=""){
			  	$("#comentariodescuentojson").text(data.comentariodescuento);
			  }
			  $('[data-toggle="tooltip"]').tooltip();
			  $("#busqueda-producto").focus();
			});
		}



 
		$("body #tabla").on("click","tbody > tr.producto",function(){
		

			if($(this).find('input').length){ }
			else {
				var index=$(this).index();
				var cantidad=$(this).find('td:eq(2)').html();				
				var contador=$(this).attr("contador");
				var id=$(this).attr("id");
				if($('#tabla > tbody > tr').eq(index+1).hasClass("producto") || typeof $('#tabla > tbody > tr').eq(index+1).attr('class') === "undefined"){
					var renglon="<tr  class=\"editar\" id="+id+" contador="+contador+"><td><span class=\"btn btn-danger btn-sm\" id=\"eliminarProducto\" data-toggle=\"tooltip\" title=\"Eliminar\"><i class=\"fa fa-trash-o fa-lg\"></i></span></td><td></td><td><span class=\"label label-warning\">Cantidad</span> <input type=\"text\" value=\""+cantidad+"\" style='width:60px;' id=\"cantidadProducto\" /></td><td></td></tr>";			
					$('#tabla > tbody > tr').eq(index).after(renglon);
					$("#cantidadProducto").focus();
				}
				else if($('#tabla > tbody > tr').eq(index+1).attr('class')==="editar"){
					$('#tabla > tbody > tr').eq(index+1).remove();
				}		
			}	
		});	



		$("body").on("click","#eliminarProducto",function(){		
			var id=$(this).closest("tr").attr("id");
			var contador=$(this).closest("tr").attr("contador");
			$.post( "./inventario/ingreso/pedido.php", { archivo:"<?=$archivo_solo;?>",idproducto:id,accion:"eliminar",contador:contador }, function( data ) {
				productos--;
				cargar_pedido();
			});
		});

		$("body").on("keypress","#cantidadProducto, #descuentoProducto",function(e){
			if (e.which == 13) {
				var id=$(this).closest("tr").attr("id");
				var contador=$(this).closest("tr").attr("contador");
				var cantidad=$("#cantidadProducto").val();
				var descuento=$("#descuentoProducto").val();
				tipodocumento=$("#tipo_transaccion_").attr("tipo");
				if(cantidad>0){
					$.post( "./inventario/ingreso/pedido.php", { archivo:"<?=$archivo_solo;?>",contador:contador,idproducto:id,accion:"modificar",tipodocumento:tipodocumento,cantidad:cantidad,descuento:descuento}, function( data ) {
						data=jQuery.parseJSON(data);
						if(data.codigo==3){
							var datos=data.datos[0];
						    var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";            			
							vermodal("Error","Cantidad de producto disponible: "+data.datos[0].existencia,botones);
						}else{cargar_pedido();}
					});
				}else{
					$.post( "./inventario/ingreso/pedido.php", { archivo:"<?=$archivo_solo;?>",idproducto:id,accion:"eliminar",contador:contador }, function( data ) {
						productos--;
						cargar_pedido();
					});
				}
				return false;
		    }

		});


			
		
	

		$("body").on("click","#almacenarSolicitud",function(){

			var elemento = $(this);
			if(productos>0){
			var cliente=$("#iCliente").attr("iclienteid");
			tipodocumento=$("#tipo_transaccion_").attr("tipo");
			enriquecido=$("#enriquecido").val();
			var titulo="Registrar alta";	
			var botones="<button type='button' class='btn btn-default' data-dismiss='modal' id=\"cancelarPago\">Cancelar</button>";
			botones+="<button type='button' class='btn btn-primary' id=\"aceptarPago\">Aceptar</button>";
			var html="";			
			html+="<div class=\"row\"><div class=\"col-lg-12\"><label>Comentarios</label> <input type=text id=\"sComentarios\" class=\"form-control\" value=\"\"></div></div>";
			html+="<div class=\"row\"><div class=\"col-lg-4\"><label>Referencia Factura</label><input type=text id=\"sReferenciaFactura\" class=\"form-control\" value=\"\"></div></div>";
			html+="<div class=\"row\"><div class=\"col-lg-4\"><label>Fecha de Factura</label><input type=text id=\"dFechaFactura\" name='dFechaFactura' data-date-format='dd-mm-yyyy' size='14'  class=\"form-control\"></div></div>";
						
			vermodal(titulo,html,botones);								
			
            if(modal){
				$("body").on("click","#cancelarPago",function(e){
					cerrarmodal();
				});

	
			$('[name="dFechaFactura"]').datepicker({
		 		todayBtn: "linked",
	   	 		language: "es",   	 
		    	autoclose: true,
	    		todayHighlight: true
			 });

				
				$("body").on("click","#aceptarPago",function(e){
					aceptar_pedido();
				});				
				$('#ventana-emergente').on('hidden.bs.modal', function () {
				  $(this).removeData('bs.modal');
				  modal=false;
				});
			}								
		  

		  }else{
			$(elemento).error("¡No ha agregado ning&uacute;n producto!","right");
		  }		
		  
		});	

		$("body").on("click","#eliminarSolicitud",function(){
			var elemento = $(this);
			productos=0;
			$.post( "./inventario/ingreso/pedido.php", { archivo:"<?=$archivo_solo;?>",accion:"eliminarventa"}, function( data ) {
				$(elemento).correcto("Eliminado");
				cargar_pedido();
			});				
		});
		$("body").on("click","#pendienteSolicitud",function(){
			var elemento = $(this);
			$.post( "./inventario/ingreso/pedido.php", { archivo:"<?=$archivo_solo;?>",accion:"guardarpendiente"}, function( data ) {
				$(elemento).correcto("Enviado a pendientes");
				solicitudes_pendientes();
				cargar_pedido();
			});	
		});

		$("body").on("click","#a_pendientes ul li a",function(e){
			var elemento = $(this);
			$(elemento).cargando("Cargando Solicitud");
			e.preventDefault();
			var archivo_p=$(this).closest('li').attr("id");
			$.post( "./inventario/ingreso/pedido.php", { accion:"cargarpendiente",archivo_p:archivo_p}, function( data ) {
				//$(elemento).correcto("Cargada correctamente");
				solicitudes_pendientes();
				cargar_pedido();
			});				
		});

		$("body").on("click","#a_pendientes ul li i",function(){
			var elemento = $(this);
			var archivo=$(this).closest('li').attr("id");
			$.post( "./inventario/ingreso/pedido.php", { accion:"eliminarpendiente",archivo:archivo}, function( data ) {
				$(elemento).correcto("Eliminada");
				solicitudes_pendientes();
			});				
		});	
		solicitudes_pendientes();
		function solicitudes_pendientes(){
			$.post( "./inventario/ingreso/pedido.php", { accion:"listapendientes"}, function( data ) {
				data=jQuery.parseJSON(data);
				var lista="<ul class=\"nav nav-second-level in fa-ul\" style=\"height: auto\">";
                $.each( data, function( key, archivo ) {
					lista+="<li id=\""+archivo.nombre+"\"><a href=\"\">"+archivo.fecha+"</a><i class=\"fa-li fa fa-times fa-lg\"></i></li>";
                });
                lista+="<ul>";
                $("#a_pendientes").html(lista);
			});	
		}

		 
		function aceptar_pedido(){			
			var sComentarios=$("#sComentarios").val();
			var dFechaFactura=$("#dFechaFactura").val();
			var sReferenciaFactura=$("#sReferenciaFactura").val();
					
			$.post( "./inventario/ingreso/pedido.php", { accion:"guardar",archivo:"<?=$archivo_solo;?>",dFechaFactura:dFechaFactura,sReferenciaFactura:sReferenciaFactura,sComentarios:sComentarios}, function( data ) {
				data=jQuery.parseJSON(data);				
				var iTransaccion= data.iTransaccionId;
				if(data.codigo==6){									
					cargar_pedido();
					productos=0;																							
					carga_acceso_directo(tipodocumento,iTransaccion);
				}
				if(data.codigo==5){
					$.each( data.datos, function( key, producto ) {
						$('#tabla > tbody > tr').eq(producto.contador-1).addClass('alert-danger');
					});
				}
				if(data.codigo==0){
					$("#aceptarPago").error("No se registro alta en inventario","right");
				}
			});				
		}																				
	});
</script>
<style>	
table {
    table-layout:fixed;
}
ul.fa-ul{
	color:#ccc;
}
ul.fa-ul li:hover{
	color:#808080;
}
#tabla-body{
  min-height:100px;
  max-height:300px;
  overflow-y:auto;
}
.typeahead, .tt-query, .tt-hint {
	border: 2px solid #CCCCCC;
	border-radius: 8px;
	font-size: 24px;
	height: 30px;
	line-height: 30px;
	outline: medium none;
	padding: 8px 12px;
	width: 396px;
}
.typeahead {
	background-color: #FFFFFF;
}
.typeahead:focus {
	border: 2px solid #0097CF;
}
.tt-hint {
  .form-control();
}
span.twitter-typeahead {
  width: 100%;
}
.tt-dropdown-menu {
	background-color: #FFFFFF;
	border: 1px solid rgba(0, 0, 0, 0.2);
	border-radius: 8px;
	box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	margin-top: 12px;
	padding: 8px 0;
	width: 422px;
}
.tt-suggestion {
	font-size: 24px;
	line-height: 24px;
	padding: 3px 20px;
}
.tt-suggestion.tt-is-under-cursor {
	background-color: #0097CF;
	color: #FFFFFF;
}
.tt-suggestion p {
	margin: 0;
}
#comentarioventa:hover{
color:#808080;
}
#comentariodescuento{
	color:#ccc;
}
#comentariodescuento:hover{
color:#808080;
}
</style>

<div class="row" style='margin-top:15px;'>
	<div class="col-lg-8">
		<div class="row">
			<div class="col-lg-9" style='margin-top:5px;'>    
				<div class="input-group"> 
				      <input type="text" class="form-control" id="busqueda-producto" autofocus>
					  <span class="input-group-addon" id='catalogo-producto2'><li class='fa fa-search' style='font-size:16px;'></li></span>
			    </div>
		   </div>
      
		    <div class="col-lg-3" style='margin-top:5px;'>
			   
			</div> 
           
		</div>
		<div class="row">	
		    <div class="col-lg-12">
				<table class="table">
			      <thead>
			        <tr>
			          <th class="hidden-xs hidden-sm" width="50">#</th>
			          <th class="hidden-xs hidden-sm" width="50">Unidad</th>
			           <th width="50">Cant.</th>	
			          <th>Producto</th>			          		          			          	         			         			          
			        </tr>
			      </thead>			        			        
			    </table>
			</div> 
		    <div class="col-lg-12" id="tabla-body">
				<table class="table table-hover table-condensed" id="tabla">
			      <tbody>
		        
			        </tbody>			        			        
			    </table>
			</div> 
		    <div class="col-lg-12">
				<div class="row">

					<div class="col-lg-2" style="text-align:left;color:#ccc">						
					</div>

					<div class="col-lg-6">  

							<button type="button" class="btn btn-default btn-sm" id="eliminarSolicitud"><i class="fa fa-trash-o"></i> Eliminar</button>
  			             	<button type="button" class="btn btn-default btn-sm" id="pendienteSolicitud"> <i class="fa fa-tasks"></i> Pendiente</button>			          	  			              			          	
  			              	<button type="button" class="btn btn-primary" id="almacenarSolicitud"><i class="fa fa-usd"></i> Registrar alta</button>

  			        </div>

					<div class="col-lg-4">

						
						

					</div>								
 				</div>						 		

		</div>

	</div>
    
	</div>

	<div class="col-lg-4">    
		<ul class="nav nav-tabs">
		  <li class="active"><a href="#categorias_productos_" data-toggle="tab">Productos</a></li>		
		  <li><a href="#a_pendientes" data-toggle="tab"><i class="fa fa-tasks"></i></a></li>
		</ul>
		<div class="tab-content">
		  <div class="tab-pane active" id="categorias_productos_" style="min-height: 500px; border: 1px solid #ccc; border-top:none">
				<? include("./inventario/ingreso/categorias_productos.php"); ?>
		  </div>        		  
		  <div class="tab-pane" id="a_pendientes" style="min-height: 500px; padding:15px; border: 1px solid #ccc; border-top:none"></div>
		</div>		
   </div>	
</div>
<!--<div class="row" style='margin-top:15px;'>
    <div class="col-lg-12">   
    <textarea  id="comentario" width="680" height="300"></textarea>
    </div>
</div>-->
<div class="row" style='margin-top:15px;'>
	<div class="col-lg-12">
    <form action="./clases/pdf.php" id="reporte_pdf" method="post" target="_blank" style='display:none;'>
	    
    </form>		
	</div>
</div>









