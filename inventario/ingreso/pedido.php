<?php   
//error_reporting(E_ALL);   
//ini_set('display_errors', '1');   
    include("../../acceso/seguridad.php"); 
@$idProducto=$_REQUEST['idproducto']; 
$accion=$_REQUEST['accion']; 
$cant=$_REQUEST['cant'];
$archivo=$iCodigo.".json"; 
if(trim($_REQUEST['archivo'])!=""){
    $archivo= $_REQUEST['archivo'];
}
  
  
  
if($accion==="cargarpendiente"){ 

  $archivo_p=$_REQUEST['archivo_p']; 

  $json = file_get_contents($archivo_p); 
  
  if($json!=""){ 
  
    $venta = json_decode($json, true);   
  
    file_put_contents($archivo,json_encode($venta)); 
  
    unset($venta); 
  
    unlink($archivo_p);      
  
    echo "ok";                                       
  
  } 
  
}    
  
  
  
if($accion==="eliminarpendiente"){ 
  
    $archivo=$_REQUEST['archivo']; 
  
    unlink($archivo); 
  
    echo "ok";     
  
} 
  
  
  
if($accion==="listapendientes"){ 
  
    $archivos=array(); 
  
    foreach (glob("$iCodigo.*.json") as $archivo) { 
  
        $arch=array(); 
  
        $arch["nombre"]=$archivo; 
  
        $arch["fecha"]=date("d-M-Y H:i:s", filemtime($archivo)); 
  
        array_push($archivos,$arch);  
  
    } 
  
    echo json_encode($archivos);     
  
} 
  
  
  
if($accion==="guardarpendiente"){ 
  
  $json = file_get_contents($archivo); 
  
  if($json!=""){ 
  
    $venta = json_decode($json, true);   
  
    $archivo_pendiente=$iCodigo.".".time().".json"; 
  
    file_put_contents($archivo_pendiente,json_encode($venta)); 
  
    $venta=array(); 
  
    $venta["productos"]=array(); 
  
    $venta["operaciones"]=calculosVenta(array()); 
  
    file_put_contents($archivo,json_encode($venta)); 
  
    unset($venta);       
  
    echo "ok";                                       
  
  } 
  
}    
  
  
  
if($accion==="eliminarventa"){ 
  
      $json = file_get_contents($archivo); 
  
      if($json!=""){     
  
        $venta["productos"]=array(); 
        
        
  
        file_put_contents($archivo,json_encode($venta)); 
  
        echo "ok";                                           
  
      } 
  
} 
    
  
if($accion==="comentarventa"){ 
  
    $comentario=$_REQUEST['comentario']; 
  
    if(file_exists($archivo)) { 
  
      $json = file_get_contents($archivo); 
  
      if($json!="" && $comentario!=""){ 
  
        $venta = json_decode($json, true); 
  
        $venta["comentarioventa"]=$comentario;       
  
        file_put_contents($archivo,json_encode($venta)); 
  
        unset($venta);                                   
  
      } 
  
    } 
  
        echo "ok";     
  
} 
  

  
if($accion==="guardar"){ 
  
    $estado=array(); 
    $dFechaFactura=$_REQUEST['dFechaFactura']; 
  
    $sReferenciaFactura=$_REQUEST['sReferenciaFactura']; 

    $sComentarios=$_REQUEST['sComentarios']; 
  	
    $estado["codigo"]=0;         
  
    if(file_exists($archivo)) { 
  
      $estado["codigo"]=1;   
  
      if(true){ 
  
        $estado["codigo"]=2;     
  
        $producto=array(); 
  
        $json = file_get_contents($archivo); 
  
        if($json!=""){ 
  
            $estado["codigo"]=3; 
  
            $venta = json_decode($json, true);   
  
            $productos=$venta["productos"];   
  
            $cantidadproductos=count($productos); 
              
  
       
  
            if($cantidadproductos>0){ 
  
                $estado["codigo"]=4; 
                          
  
                $folio=seleccionar("altainventario","COALESCE(MAX(iFolio),0)+1 as folio","iUsuarioEmpresaId=$iUsuarioEmpresaId",false,false); 
  
                $iFolio=$folio[0]["folio"]; 
  
                } 
                if($dFechaFactura!=""){
                    $v = explode("-",$dFechaFactura);     
                    $d__ = date("Y-m-d", mktime(0,0,0,$v[1],$v[0],$v[2]));
                    $dFechaFactura = "'".$d__."'";
                }else $dFechaFactura="null";
                
                $campos = "iUsuarioEmpresaId,iEmpleadoId,dtFechaAlta,sReferenciaFactura,sComentarios,iEstatus,dFechaFactura,iFolio";
                $valores="'".$iUsuarioEmpresaId."','".$iEmpleadoId."',CURRENT_TIMESTAMP(),'".$sReferenciaFactura."','".$sComentarios."','0','".$dFechaFactura."','".$iFolio."'"; 
                
                $idtransaccion=insertar("altainventario",$campos,$valores,true);                                   
  
                //DETALLE DE VENTA 
  
                foreach ($productos as $key => $value)  
  
                { 
  
                    $campos="iAltaInventarioId,iProductoId,fCantidad"; 
  
                    $valores="'".$idtransaccion."','".$value['iId']."','".$value['cantidad']."'"; 
  
                    insertar("detallealtainventario",$campos,$valores,false); 
  
					/* Actualizacion 27/05/2014 
					insertar en inventario si es una factura */
                      
                        $campos="iTransaccionId,iEmpleadoId,iProductoId,fCantidad,dtFecha,iTipo"; 
  
                        $valores="'".$idtransaccion."','".$iEmpleadoId."','".$value['iId']."','".$value['cantidad']."',CURRENT_TIMESTAMP(),'2'"; 
  
                        insertar("inventario",$campos,$valores,false);                       
  
                } 
   
            
                $venta=array(); 
  
                $venta["productos"]=array(); 
  
             
                file_put_contents($archivo,json_encode($venta)); 
  
                $estado["codigo"]=6;  
				$estado["iTransaccionId"]=$idtransaccion;			
  
            } 
  
        unset($venta);       
  
        } 
  
      } 
    
    echo json_encode($estado); 
  
} 
  
if($accion==="insertarproducto"){ 
  
    $nombre=$_REQUEST['nombre']; 
  
    $precio=(float)$_REQUEST['precio']; 
  
    $unidad=$_REQUEST['unidad']; 
  
    $iva=(float)$_REQUEST['iva']; 
  
    $cantidad=(float)$_REQUEST['cantidad']; 
  
    if(file_exists($archivo)) { 
  
      $json = file_get_contents($archivo); 
  
      if($json!="" && $nombre!="" && $precio!="" && $cantidad!=""){ 
  
        $producto=array(); 
  
        $venta = json_decode($json, true); 
  
        $ultimoproducto=end(array_keys($venta["productos"])); 
  
        $producto["sNombre"]=$nombre; 
  
        $producto["fPrecioVenta"]=$precio; 
        $producto["fPrecioVentaf"]=number_format($precio,2); 
  
        $producto["fIva"]=$iva; 
  
        $producto["cantidad"]=$cantidad; 
  
        $producto["sUnidad"]=$unidad; 
  
        $producto["contador"]=$venta["productos"][$ultimoproducto]["contador"]+1; 
  
        $producto["operaciones"]=calcular($precio,$cantidad,$iva); 
  
        array_push($venta["productos"], $producto); 
  
        $venta["operaciones"]=calculosVenta($venta["productos"]); 
  
        file_put_contents($archivo,json_encode($venta)); 
  
        unset($venta);                                   
  
      } 
  
    } 
  
} 
  
if($accion==="eliminar"){ 
  
    $contador=$_REQUEST['contador'];     
  
    if(file_exists($archivo)) { 
  
      $json = file_get_contents($archivo); 
  
      if($json!=""){ 
  
        $venta = json_decode($json, true); 
  
        $productos=$venta["productos"];   
  
        $n=1;    
  
        foreach ($productos as $key => $value)  
  
        {  
  
            if ($value['contador'] == $contador) { 
  
                unset($productos[$key]); 
  
            }else{ 
  
                $productos[$key]['contador']=$n; 
  
                $n++; 
  
            }  
  
        } 
  
        $venta["productos"]=$productos; 
  
        
  
        file_put_contents($archivo,json_encode($venta)); 
  
        unset($venta);   
  
        echo "ok";                               
  
      } 
  
    } 
  
} 
  
 
  
if($accion==="modificar"){ 
  
    $cantidad=$_REQUEST['cantidad']; 
  
    $editar=$_REQUEST['editar']; 

    $tipo=$_REQUEST['tipo']; 

    $descuento=$_REQUEST['descuento']; 
  
    $contador=$_REQUEST['contador']; 
  
    $estado=array(); 
  
    $estado["codigo"]=0;     
  
    if(file_exists($archivo)) { 
  
      $json = file_get_contents($archivo); 
  
      if($json!=""){ 
  
        $venta = json_decode($json, true); 
  
        $productos=$venta["productos"];      
  
        foreach ($productos as $key => $value)  
  
        {  
  
            $estado["codigo"]=1; 
  
            if ($value['contador'] == $contador) { 
                if($tipo!=""){
                    $estado["codigo"]=2; 
                    $productos[$key][$tipo]=$editar;                                             
                                     
                    $venta["productos"]=$productos;
                    file_put_contents($archivo,json_encode($venta));  
                    break;

                }
                    else
                {
                    $estado["codigo"]=2; 
      
                    if($productos[$key]["iControlInventario"]==1 && $_REQUEST['tipodocumento']!="3"){ 
      
                        $array_productos = array(); 
      
                        array_push($array_productos, $productos[$key]); 
      
                        $array_productos[0]["cantidad"]=(float)$cantidad; 
      
                        $existencia=existencia_producto($array_productos); 
      
                        if(count($existencia)>0){ 
      
                            $estado["codigo"]=3; 
      
                            $estado["datos"]=$existencia; 
      
                            echo json_encode($estado); 
      
                            exit(); 
      
                        } 
      
                    }    
      
                    $productos[$key]["cantidad"]=(float)$cantidad; 
      
            
      
                    $venta["productos"]=$productos; 
      
                    
      
                    file_put_contents($archivo,json_encode($venta)); 
      
                    $estado["codigo"]=4;                 
      
                    break; 
                }   
  
            }  
  
        } 
  
       unset($venta);            
  
      } 
  
    } 
  
    echo json_encode($estado); 
  
} 
  
  
if($accion==="agregarproducto"){ 
  
    $estado=array(); 
  
    $tabla ="producto"; 
  
    $condicion ="iId=$idProducto"; 
	
  
    $_c="iId,sNombre,fPrecioVenta,fIva,sUnidad,iControlInventario,sImagen1"; 
  
    $respuesta_json; 
  
    $venta=array(); 
  
    $estado["codigo"]=0; 
	if (isset($cant)) {
	$cantidad=$_REQUEST['cantidad'];
	}  
    else if(isset($_REQUEST['cantidad_producto']))
    {
        $cantidad = $_REQUEST['cantidad_producto'];
    }
    else {
	
    $cantidad=1; 
	}
	
	
    $producto = json_decode(seleccionar($tabla,$_c,$condicion,false,true),true); 
  
    if($producto) { 
  
        if(file_exists($archivo)) { 
  
          $json = file_get_contents($archivo); 
  
          if($json!=""){ 
  
                $venta = json_decode($json, true); 
  
                $existe=false; 
  
                $subtotal=0.0; 
  
                $productos=$venta["productos"]; 
  
                $contador=0; 
  
  
                foreach ($productos as $key => $value)  
  
                {  
                    
                    if ($value['iId'] == $producto[0]["iId"]) { 
  
						$cantidad=$cantidad+$value['cantidad'];						
                        $productos[$key]["cantidad"]=$cantidad; 
						                      
                        $existe=true;    
  
                        break; 
  
                    } 
  
                    $contador++; 
  
                } 
  
                if(!$existe){ 
                 
					
					if (isset($cant)) {
					$producto[0]["cantidad"]=$_REQUEST['cantidad'];
					
					} else {
					$producto[0]["cantidad"]=$cantidad;  
					}
                      $producto[0]["contador"]=$contador+1; 
                    #$producto[0]["cantidad"]=1; 
					
					/* PEDIDO EN LINEA*/ 
                   
                    array_push($productos,$producto[0]); 
  
                    $existe=false; 
  
                } 
              
  
                $venta["productos"]=$productos;                 
  
                file_put_contents($archivo,json_encode($venta));     
                unset($venta);           
  
          }else{ 
  
            #producto_unico($producto,$archivo); 
             $estado["codigo"]=2; 
  
          } 
  
        }else{ 
  
           # producto_unico($producto,$archivo); 
             $estado["codigo"]=2; 
  
        } 
  
        unset($producto); 
  
        $estado["codigo"]=2; 
  
    }else{ 
  
        $estado["codigo"]=3; 
  
    } 
  
echo json_encode($estado);   
  
} 
  
  
    
function existencia_producto($productos_venta){ 
  
    $productos=array(); 
  
    $producto=array(); 
  
    foreach ($productos_venta as $key => $value)  
  
    { 
  
     if($value["iControlInventario"]==1){ 
  
        $nproducto=existencia($value["iId"]); 
  
        if($nproducto<$value["cantidad"]){ 
  
            $producto["iId"]=$value["iId"]; 
  
            $producto["existencia"]=$nproducto; 
  
            $producto["disponible"]=false; 
  
            $producto["contador"]=$value["contador"]; 
  
            array_push($productos,$producto); 
  
        } 
  
     } 
  
    } 
  
    return $productos; 
  
} 
  
?>