<?
include("../../acceso/seguridad.php");
$_tabla = "inventario";
$_formulario = "./general/empleados/formulario.php";
$_pre_agregar = "./general/empleados/verifica_existencias.php";
$_post_agregar = "./general/empleados/genera_codigo.php";;
?>
<div class="row">
 
  <div class="col-lg-2">
      <label>Empleado</label>
	<select class="form-control" name='iEmpleadoId' no_catalogo="no">
		<option value="0">Todos</option>    
		<?
		$select = seleccionar("empleado","iId,iCodigo,sNombre","iEstatus=0 and iUsuarioEmpresaId=$iUsuarioEmpresaId",false, false);
		foreach($select as $dato) {
			?>
			<option value="<?=$dato['iId'];?>"><?=$dato['iCodigo']."-".$dato['sNombre'];?></option>
			<?
		}
		?>
	</select>
    </div>   
    <div class="col-lg-2">
        <label>Movimiento</label>
   		<select class="form-control" name='iMovimiento' no_catalogo="no">
            <option value="0">Todos</option>               
                <option value="1">Entradas</option>
                <option value="2">Salidas</option>                
		</select>	
	</div> 
        <div class="col-lg-2">
            <label>Desde fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Desde" name="desde" size="14" type="text">
		</div> 
         <div class="col-lg-2">
             <label>Hasta fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Hasta" name="hasta" size="14" type="text">
		</div> 
	<div class="col-lg-2">
    
    </div>
   	<div class="col-lg-2">
    
    </div>
</div>

<div class="row" style='margin-top:3px;'>

	<div class="col-lg-6">
    <button id='filtro_movimientos' type="button" class="btn btn-primary" titulo='Filtrar'>Aplicar Filtros en Reporte</button>
    </div>
	<? include("./clases/exportacion.php"); ?>

</div>

<div class="row">
    <div class="col-lg-12">    		
      	<div id="div-tabla-lista"></div>    
	</div>
</div>        

<script>
$(document).ready(function () {
		$('[name="desde"]').datepicker({
			todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true
			 });
 		 $('[name="hasta"]').datepicker({
			todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true	 
		});
	
var genera_tabla = {
  columnas: [ 
    {columna:'producto.sNombre', titulo: 'Producto',class:false,widthpdf:'17%',visible:true},
    {columna:'inventario.fCantidad', titulo: 'Cantidad',class:false,widthpdf:'17%',visible:true},
    {columna:'empleado.sNombre as NombreEmpleado', titulo: 'Empleado',class:"hidden-xs",widthpdf:'20%',visible:true},
    {columna:'inventario.fCantidad as movimiento', titulo: 'Movimiento',class:"hidden-xs",widthpdf:'20%',visible:true,label:'movimiento'},	
    {columna:'inventario.dtFecha', titulo: 'Fecha',class:"hidden-xs",widthpdf:'20%',visible:true},	
   ]   
};
<? $_tabla="inventario INNER JOIN producto ON inventario.iProductoId = producto.iId INNER JOIN empleado ON inventario.iEmpleadoId = empleado.iId"; ?>

	$("#div-tabla-lista").crear_tabla_lista('<?=$_tabla;?>',false,genera_tabla,"inventario.dtFecha>=CURRENT_DATE() and empleado.iUsuarioEmpresaId='<?=$iUsuarioEmpresaId;?>' and producto.iUsuarioEmpresaId='<?=$iUsuarioEmpresaId?>'",false);


$("#filtro_movimientos").click(function(){
	var elemento = $(this);
	$(elemento).cargando("Generando reporte","bottom");
	var empleado = $("[name='iEmpleadoId']").val();
	var movimiento= $("[name='iMovimiento']").val();	
	var desde = $("[name='desde']").val();	 
	var hasta = $("[name='hasta']").val();		
	var condicion = "empleado.iUsuarioEmpresaId='<?=$iUsuarioEmpresaId;?>' and producto.iUsuarioEmpresaId='<?=$iUsuarioEmpresaId?>'";
	if(empleado!="0") { condicion += " and empleado.iId ='"+empleado+"'"; }
	if(movimiento!="0") { 
		if(movimiento==1)
		condicion += " and inventario.fCantidad > 0"; 
		else
		condicion += " and inventario.fCantidad < 0"; 
	}
	if(desde!="") { condicion += " and inventario.dtFecha>="+fechahora(desde+" 00:00:00"); }
	if(hasta!="")	{ condicion += " and inventario.dtFecha<="+fechahora(hasta+" 23:59:59"); }	

	$("#div-tabla-lista").crear_tabla_lista('<?=$_tabla;?>',false,genera_tabla,condicion,false,true,function(data){
		$(elemento).cerrar_cargando();
	});
	
	
});



});

</script>
