<?
include_once("./acceso/seguridad.php");
$_tabla_general = "./inventario/reportealtas/tabla.php";
?>
<div class="row">
        <div class="col-lg-2">
            <label>Desde fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Desde" name="desde" size="14" type="text">
		</div> 
         <div class="col-lg-2">
             <label>Hasta fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Hasta" name="hasta" size="14" type="text">
		</div> 
    <div class="col-lg-2 hide">
      	<label>Empleado</label>
		<select class="form-control" name='iEmpleadoId' no_catalogo="no">
            <option value="0">Todos</option>    
            <?
            $select = seleccionar("empleado","iId,iCodigo,sNombre","iEstatus=0 and iUsuarioEmpresaId=$iUsuarioEmpresaId",false, false);
            foreach($select as $dato) {
                ?>
                <option value="<?=$dato['iId'];?>"><?=$dato['iCodigo']."-".$dato['sNombre'];?></option>
                <?
            }
            ?>
		</select>
  	</div>   
      <div class="col-lg-2">
      	<label>Estatus</label>
		<select class="form-control" name='iEstatus' no_catalogo="no">
            <option value="2">Todos</option>    
                <option value="0">Vigentes</option>
                <option value="1">Canceladas</option>                
		</select>
  	</div>   
	<div class="col-lg-4">
    </div>
</div>

<div class="row" style='margin-top:3px;'>
	<div class="col-lg-6">
    <button id='filtro_movimientos' type="button" class="btn btn-primary" titulo='Filtrar'>Aplicar Filtros en Reporte</button>
    </div>
	<? include("./clases/exportacion.php"); ?>

</div>
<div class="row">
    <div class="col-lg-12">    		
      	<div id="div-tabla"></div>    
	</div>
</div>        
<script>

$(document).ready(function () {

		 $("[name='desde']").val("<?=date("d-m-Y");?>");

		 $('[name="desde"]').datepicker({
	 		todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true
			 });

 		 $('[name="hasta"]').datepicker({
			todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true	 
		});

		$("body").on("click",".enviar_cancelar",function(e){  

			var elemento_ = $(this).closest("div");

			var id = $(this).attr("href");
			
	    	$(elemento_).confirmar("Seguro que desea cancelar alta de inventario?","Si","No",function(respuesta){

				if(respuesta) {
					
					$(elemento_).cargando("Cancelando...");										
					
						$.post("./inventario/reportealtas/cancela.php",{id:id},function(data){
							data = eval(data);
							if(data[1]==1) {
								$(elemento_).closest( "tr" ).find('td:eq(5)').html(data[0]);	
								$(elemento_).cerrar_cargando();													
							}else if(data[1]==0) {
								$(elemento_).error(data[0]);						
							}			
						});
					

				}							

			}); 
			e.preventDefault();
	    });
				


		var tipo="default";
		var cliente_seleccionado="";
		var abre_=false;
	


	//condicion, refresh ultimos dos
	 var condicion_ = " altainventario.dtFechaAlta>="+fechahora("<?=date("d-m-Y");?> 00:00:00")+""; 
	 condicion_ += " and altainventario.dtFechaAlta<="+fechahora("<?=date("d-m-Y");?> 23:59:59")+""; 	
	 
	$("#div-tabla").crear_tabla_general("tabla_altas","<?=$_tabla_general;?>",condicion_,false);
	


	

$("#filtro_movimientos").click(function(){
	var elemento = $(this);
	var condicion="1=1";
	$(elemento).cargando("Generando reporte","bottom");
	var empleado = $("[name='iEmpleadoId']").val();
	var desde = $("[name='desde']").val();	
	var hasta = $("[name='hasta']").val();			
	var iestatus = $("[name='iEstatus']").val();
	if(iestatus!="2") { condicion += " and altainventario.iEstatus ='"+iestatus+"'"; }		
	if(empleado!="0") { condicion += " and empleado.iId ='"+empleado+"'"; }
	if(desde!="") { condicion += " and altainventario.dtFechaAlta>="+fechahora(desde+" 00:00:00")+""; }
	if(hasta!="")	{ condicion += " and altainventario.dtFechaAlta<="+fechahora(hasta+" 23:59:59")+""; }	
	$("#div-tabla").crear_tabla_general("tabla_altas","<?=$_tabla_general;?>",condicion,true,function(){		
		$(elemento).cerrar_cargando();
	});	

});

});
</script>