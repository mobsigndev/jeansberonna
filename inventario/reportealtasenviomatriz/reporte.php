<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
if(file_exists("../acceso/seguridad.php")){
include("../acceso/seguridad.php");
$p = "..";
}
if(file_exists("../../acceso/seguridad.php")){
include("../../acceso/seguridad.php");
$p = "../..";
}

if(file_exists("./acceso/seguridad.php")){
include("./acceso/seguridad.php");
$p = ".";
}
$id=$_REQUEST['iId'];
$renglones=conceptosaltainventario((int)$id);
$datos=datos_altainventario((int)$id);

$sTipoTransaccion="Alta en Inventario";

if($datos["iEstatus"]==1)
	$estatus = "<div><label style='color:red;'>Cancelado</label></div>";
else 
	$estatus = "";
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" type="text/css" href="../../css/estilo_cotizacion.css" />
</head>
<style> 
	@page { margin: 0px 20px 0px 20px; color:#666;}
    #header {  text-align: center; border-bottom:2px solid #ccc;}
    #footer { position: fixed; left: 0px; bottom: 0px; right: 0px; height: 80px; width:785px; background:#ccc; font-size:10px;color:#666;}
	@media print {
  html, body {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
	color:#666;
  }
  #footer {
    position: absolute;
    bottom: 0;
	color:#666;
  }
}
</style>

<div style='font-family:Verdana, Geneva, sans-serif; color:#666;'>
	
    <!--ENCABEZADO -->
    <div style='width:780px; border-top:1px solid #fff;'>
    	
        <div style='width:220px; background-color:#fff; display:inline-block; border-right:2px solid #CCC; display:none;'>
           
            <?php if($datos["sLogotipo"]!=""){
				if(file_exists("$p/logotipos/".$datos["sLogotipo"])) { 
					              
                	list($anchura, $altura, $type, $attr) = getimagesize("$p/logotipos/".$datos["sLogotipo"]);
					
					 if($anchura>$altura) {
						
						#Mas ancho       
						if(($altura*3.33)>$anchura) { $yy=60; $xx=0;}
						else { 
							$xx=200;
				
						}
					}
					else {
					   
					   if(($anchura/3.33)>$altura) { $xx=200; $yy=0; }
					   else { $yy=90; } 
					}
						#echo '<img src="'.$p.'/logotipos/'.$datos["sLogotipo"].'" width='.$xx.' height='.$yy.' alt="Logotipo" />'; 
				  ?>
                   <img src="<?=$p;?>/logotipos/<?php echo $datos["sLogotipo"]; ?>" width="<?=$xx?>" height="<?=$yy?>" alt="Logotipo" />
				<? }
			}
        ?>       
        </div>
         
        <div style="width:560px; height:66px; display:inline-block; text-align:left;">
            <div style='margin-left:20px; margin-top:15px; font-size:24px; color:#444; vertical-align:top;'>
            <?php echo $sTipoTransaccion; ?>
			</div>
            <div style='margin-left:20px; display:none; margin-top:8px; font-size:13px; color:#444;vertical-align:top;'>
            Alta por:<?php echo $datos["Empleado"]; ?>
			</div>                       
           
        </div>
        
		<div style="width:180px; height:80px; border:2px solid #ccc; display:inline-block;">
         	<div style='margin-top:10px;margin-left:10px;padding-bottom:1px;color:#444;font-size:19px;text-align: left;'>
            <strong>Folio:</strong><? echo $datos["iFolio"]; ?>
            </div>
            <div style='margin-left:10px; margin-top:8px; font-size:12px; color:#444;vertical-align:top; text-align:left;'>
            <strong>Fecha:</strong> <? echo $datos["dtFechaAlta"]; ?>
            </div>
		</div>
        <?= $estatus; ?>
    </div>
    <!--ENCABEZADO#-->
    

    
    <!--CUERPO DE REPORTE -->
    <div style="width:780px;padding-bottom:30px;background-color:#fff; border-top:2px solid #CCC; margin-top:5px;">    	
      <h4 style='width:780px; margin-top:10px;	margin-left:10px;	padding-bottom:1px;	background-color:#;	font-size:15px;text-align:justify;	color:#333;'><?php echo $datos["sComentarioTransaccion"]; ?></h4>                        
     
      <table border="1" style='border:1px solid #999; width:780px; font-size:11px; border-collapse:collapse;color:#666;'>
          <tr> 
            <th width="50px"><strong>CANT</strong>.</th>
            <th width="60px" ><strong>UNIDAD</strong></th> 
            <th width="390px"><strong>CONCEPTO</strong></th>
        	    
                  
          </tr>
          <?php echo $renglones; ?>
      </table>

        	

     <? if($datos['sComentarios']!="" ){ ?>
     <div style='font-size:12px; margin-top:15px; text-align:justify;'>Comentarios: <?=$datos['sComentarios'];?> .</div>
      <? } ?>
     
  </div>
    <!--CUERPO DE REPORTE#-->
    
    
         
    </div>
    
   <!--PIE DE REPORTE -->
  <div id="footer" style="display:none;">
    	<p class="page">
        <center>
        <div style='margin-top:7px;'><?php echo $datos['sRazonSocial']; ?></div>
    	<div><?php echo utf8_encode($datos["sCalle"])." ".$datos["sNumeroExterior"]." Col.".$datos["sColonia"]." ".$datos["sCiudad"].", ".$datos["sEstado"].", ".$datos["sPais"]; ?></div>
        <div>Tel&eacute;fono <? echo $datos["sTelefonoContacto"]; ?></div>
    	<div><? echo $datos["sCorreo"]; ?></div>
        </center>
    	</p>
         
    </div>	
	<!--PIE DE REPORTE#-->
    
  

</html>