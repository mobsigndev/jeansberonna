<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
$id=$_REQUEST['iId'];
if(file_exists("../acceso/seguridad.php")){
include("../acceso/seguridad.php");
$p = "..";
}
if(file_exists("../../acceso/seguridad.php")){
include("../../acceso/seguridad.php");
$p = "../..";
}

if(file_exists("./acceso/seguridad.php")){
include("./acceso/seguridad.php");
$p = ".";
}
$id=$_REQUEST['iId'];
$operaciones=totalesTransaccion((int)$id);
$datos=datos_transaccion((int)$id);

if($datos["iTipo"]=="1"){
	$tipo="Efectivo";
}
if($datos["iTipo"]=="2"){
	$tipo="Tarj. Credito";
}
if($datos["iTipo"]=="3"){
	$tipo="Tarj. Debito";
}
if($datos["iTipo"]=="4"){
	$tipo="Cheque";
}
if($datos["iTipo"]=="5"){
	$tipo="Transferencia";
}
else {
	$tipo="No Identificado";
}

if($datos["iTipoTransaccion"]=="1"){
 $sTipoTransaccion= "Nota de Venta";
}
else if($datos['iTipoTransaccion'] == "2" ) {
 $sTipoTransaccion="Nota de Venta";
}
else if($datos['iTipoTransaccion'] == "3" ) {
 $sTipoTransaccion="Cotización";
}
?>
<html>

<div style='font-family:Verdana, Geneva, sans-serif; color:#666;'>
	
<?
	$transaccion =  $sTipoTransaccion; 
	$cliente = $datos["sNombre"];
	$folio = $datos["iFolio"];
	$serie = $datos["sSerie"];
	$fecha = $datos["dFecha"];
	$subtotalf =  $operaciones["subtotalf"];
	$descuento =  $operaciones["descuento"];
	$descuentof =  $operaciones["descuentof"];
	$ivaf = $operaciones["ivaf"];
	$totalf = $operaciones["totalf"]; 

	$html ="Folio $serie"."$folio";
	escribe($fp,$html);
	$html ="<br \>Fecha $fecha";
	escribe($fp,$html);
	$html="";
	$html ="<br \>Tipo de Pago: $tipo";
	escribe($fp,$html);
	$html="<br \>-------------------------------";
	escribe($fp,$html);	

$html="<br \>Cliente: ".utf8_decode($cliente);
escribe($fp,$html);

$html="<br \> -------------------------------";
escribe($fp,$html);	


$html="<br \> Cant. P.U. Producto &nbsp;&nbsp;&nbsp;&nbsp; Total<br \>";
escribe($fp,$html); 

 $t= $totalf;


$datos2 = seleccionar("detalletransaccion","sConcepto,fCantidad,fPrecio,fIva,fDescuento,sUnidad","iTransaccionId='$id'",false, false);                            
   foreach($datos2 as $renglones) {
	$valores=calcular($renglones['fPrecio'],$renglones['fCantidad'],$renglones['fIva']);	
	
	$pu = $renglones['fPrecio'];
	$t__ = $valores["subtotal"];
	
	


		escribe($fp,"\n");	
		$conc = $renglones['sConcepto'];
		$lon_c = strlen($conc);


		if($lon_c<12) {
			$html= "<br \> ".espacios($renglones['fCantidad'],"4").espacios(number_format($pu,2),"4").strtolower($conc)." ".number_format($t__,2);
			escribe($fp,$html);
		}else {
			$ciclo = $lon_c/11;
			for($i=0;$i<$ciclo;$i++) {
				$e=$i*11;
				$conc2= substr($conc, $e, 11);
				if($i==0)
				$html= "<br \>".espacios($renglones['fCantidad'],"4").espacios(number_format($pu,2),"4").strtolower($conc2)." ".number_format($t__,2);	
				else 
				$html= "<br \>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".strtolower($conc2)."&nbsp;&nbsp;&nbsp;&nbsp;";	
				escribe($fp,$html);	
			}
		}  
  	}

  $html="<br \> -------------------------------";
escribe($fp,$html);	
  $html="<br \>&nbsp;&nbsp;&nbsp;&nbsp;
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  &nbsp;&nbsp;
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
escribe($fp,$html);

$html ="Subtotal $".$subtotalf;
escribe($fp,$html);

if($descuento>0){
$html="<br \>&nbsp;&nbsp;&nbsp;&nbsp;
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  &nbsp;&nbsp;
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
escribe($fp,$html);
	$html ="Desc. &nbsp;&nbsp;&nbsp;&nbsp;$".$descuentof;
	escribe($fp,$html);
}
$html="<br \>&nbsp;&nbsp;&nbsp;&nbsp;
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  &nbsp;&nbsp;
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
escribe($fp,$html);
$html ="IVA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$".$ivaf;
escribe($fp,$html);

	$datos_ = array();
    $transaccion= array();
    $datos2 = seleccionar("transaccionimpuesto","iTipo,fCantidad,sImpuesto,fPorcentaje","iTransaccionId='$id'",false, false);
	   foreach($datos2 as $renglones) {
			$datos_['sImpuesto']	= 	$renglones['sImpuesto'];
			$datos_['cantidad']		= 	$renglones['fCantidad'];  
			$datos_['porcentaje']		= 	$renglones['fPorcentaje'];       
			array_push($transaccion,$datos_);
			
				 $impuesto_ = $datos_["sImpuesto"]." (".$datos_['porcentaje'].")%"; 
				 $cantidad_ = "$".$datos_["cantidad"]; 
				$html ="$impuesto_		$".$cantidad_;
				escribe($fp,$html);
	   }
           


  	$html="<br \>&nbsp;&nbsp;&nbsp;&nbsp;
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  &nbsp;&nbsp;
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
escribe($fp,$html);
  $html ="Total &nbsp;&nbsp;&nbsp; $".$totalf;
  escribe($fp,$html);	




$html="<br \> <br \>".$datos['sRazonSocial'];
escribe($fp,$html);
$html ="<br \> ". utf8_encode($datos["sCalle"])." ".$datos["sNumeroExterior"]." ".$datos["sCiudad"];
escribe($fp,$html);
$html ="<br \> Telefonos: ".$datos["sTelefonoContacto"];
escribe($fp,$html);
$html ="<br \> Correo: ".$datos["sCorreo"];
escribe($fp,$html);
$html ="<br \>";

//Output a line of the file until the end is reached
 
 
function escribe($fp,$texto) {
echo("<span style='font-size:8px;'>".$texto."</span>");
}
function espacios($cadena,$espacios) {
	$lon = strlen($cadena);
	if($lon<$espacios) {
		$llenar = $espacios-$lon;
	}
	for($i=0;$i<=$llenar;$i++) {
		$c.="&nbsp;";
	}
	return $cadena.$c;
}

function corta($cadena,$fp) {
	$lon = strlen($cadena);
	if($lon>12) {
		for($j=0;$j<12;$j++){
			$cadnueva .=$cadena[$j];
		}
		
		escribe($fp,$cadnueva);
	}	
}
?>


