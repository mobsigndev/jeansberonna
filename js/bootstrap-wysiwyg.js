/* http://github.com/mindmup/bootstrap-wysiwyg */
/*global jQuery, $, FileReader*/
/*jslint browser:true*/
(function ($) {
	'use strict';

	var readFileIntoDataUrl = function (fileInfo) {
		var loader = $.Deferred(),
			fReader = new FileReader();
		fReader.onload = function (e) {
			//loader.resolve(e.target.result);
			loader.resolve({url: e.target.result});
		};
		fReader.onerror = loader.reject;
		fReader.onprogress = loader.notify;
		fReader.readAsDataURL(fileInfo);
		return loader.promise();
	};
	var uploadFileToServer = function (fileInfo, script, options) {
		var imgbutton= $("#pictureBtn").html();	
		$("#pictureBtn").html("Cargando...").attr("disabled","disabled");	
		var loader = $.Deferred(),
			fReader = new FileReader();
		fReader.onload = function (e) {
			var blob = new Blob([e.target.result], {type: "application/octet-stream"}),
				data = new FormData();
			data.append("image", blob);
                        data.append("filename", fileInfo.name);
			$.each(options, function(name, value) {
				data.append(name, value)
			});
			$.ajax({
				url: script,
				type: "POST",
				data: data,
				dataType:"json",
				cache: false,
				contentType: false,
				processData: false
			}).done(function(data) {
				loader.resolve(data);
				$("#pictureBtn").html(imgbutton).removeAttr("disabled");
			}).fail(function(xhr, error) {
				loader.reject({xhr: xhr, error: error});
				$("#pictureBtn").html(imgbutton).removeAttr("disabled");
			});
		};
		fReader.onerror = loader.reject;
		fReader.onprogress = loader.notify;
		fReader.readAsArrayBuffer(fileInfo);
		return loader.promise();
        }
	$.fn.cleanHtml = function () {
		var html = $(this).html();
		return html && html.replace(/(<br>|\s|<div><br><\/div>|&nbsp;)*$/, '');
	};
	
	$.fn.sizeimage = function(pos){
		var imageresize=$(this);
		var himagereal=$(imageresize).attr("height");
		var wimagereal=$(imageresize).attr("width");
		console.log(himagereal);
		 $(this).popover('destroy');			
		 var content__='<div><strong>Dimensiones</strong><br>';
		 	content__+='<span class="glyphicon glyphicon-resize-vertical"></span><input id="heightimage_" type="text" style="width:50px;"/>px<br>';
		 	content__+='<span class="glyphicon glyphicon-resize-horizontal"></span><input id="widthimage_" type="text" style="width:50px;"/>px<br>';
		 	content__+='<div class="text-center" style="margin-top:4px;"><button class="btn btn-primary" id="okimage_">OK</button></div>';
		 
		 var pop_ = $(this).popover({
		 	 content: 	content__,
			 html: 		true,
			 trigger:	'manual'			
		 }).on('shown.bs.popover', function () {
		 	   console.log("cargado");
		      var left = pos.pageX;
		      var top = pos.pageY;		    
		  	  $(".popover").css("left", left - 180);
		  	  $(".popover").css("top",top - 180);	
		  	  $("#widthimage_").val(wimagereal);
		  	  $("#heightimage_").val(himagereal);
		  });
	 		
	 		pop_.popover('show');

	  	  $("#okimage_").click(function(){	
	  
	  	  	$(imageresize).attr("height",$("#heightimage_").val());
	  	  	$(imageresize).attr("width",$("#widthimage_").val());
	  	  	pop_.popover('hide');	
	  	  });
	  	  

	};

	$.fn.wysiwyg = function (userOptions) {
		var editor = this,
			selectedRange,
			options,
			toolbarBtnSelector,
			updateToolbar = function () {
				if (options.activeToolbarClass) {
					$(options.toolbarSelector).find(toolbarBtnSelector).each(function () {
						//var command = $(this).data(options.commandRole);
						//if (document.queryCommandState(command)) {
						var command = $(this).data(options.commandRole),
						format = command.match(/formatBlock (.+)/);
						if (document.queryCommandState(command) || (format && (format[1] == document.queryCommandValue("formatBlock")))) {
							$(this).addClass(options.activeToolbarClass);
						} else {
							$(this).removeClass(options.activeToolbarClass);
						}
					});
				}
			},
			execCommand = function (commandWithArgs, valueArg) {
				var commandArr = commandWithArgs.split(' '),
					command = commandArr.shift(),
					args = commandArr.join(' ') + (valueArg || '');
				document.execCommand(command, 0, args);
				updateToolbar();
			},
			bindHotkeys = function (hotKeys) {
				$.each(hotKeys, function (hotkey, command) {
					editor.keydown(hotkey, function (e) {
						if (editor.attr('contenteditable') && editor.is(':visible')) {
							e.preventDefault();
							e.stopPropagation();
							execCommand(command);
						}
					}).keyup(hotkey, function (e) {
						if (editor.attr('contenteditable') && editor.is(':visible')) {
							e.preventDefault();
							e.stopPropagation();
						}
					});
				});
			},
			getCurrentRange = function () {
				var sel = window.getSelection();
				if (sel.getRangeAt && sel.rangeCount) {
					return sel.getRangeAt(0);
				}
			},
			saveSelection = function () {
				selectedRange = getCurrentRange();
			},
			restoreSelection = function () {
				var selection = window.getSelection();
				if (selectedRange) {
					try {
						selection.removeAllRanges();
					} catch (ex) {
						document.body.createTextRange().select();
						document.selection.empty();
					}

					selection.addRange(selectedRange);
				}
			},
			//insertFiles = function (files) {
				insertFiles = function (files, uploadScript, uploadOptions) {
                                var uploadFunction = uploadScript ? uploadFileToServer : readFileIntoDataUrl;
				editor.focus();
				$.each(files, function (idx, fileInfo) {
					if (/^image\//.test(fileInfo.type)) {
						//$.when(readFileIntoDataUrl(fileInfo)).done(function (dataUrl) {
						//	execCommand('insertimage', dataUrl);
						$.when(uploadFunction(fileInfo, uploadScript, uploadOptions)).done(function (data) {
							execCommand('insertimage', data.url);
						}).fail(function (e) {
							options.fileUploadError("file-reader", e);
						});
					} else {
						options.fileUploadError("unsupported-file-type", fileInfo.type);
					}
				});
			},
			markSelection = function (input, color) {
				restoreSelection();
				if (document.queryCommandSupported('hiliteColor')) {
					document.execCommand('hiliteColor', 0, color || 'transparent');
				}
				saveSelection();
				input.data(options.selectionMarker, color);
			},
			bindToolbar = function (toolbar, options) {
				toolbar.find(toolbarBtnSelector).click(function () {
					restoreSelection();
					editor.focus();
					execCommand($(this).data(options.commandRole));
					saveSelection();
				});
				toolbar.find('[data-toggle=dropdown]').click(restoreSelection);

				toolbar.find('input[type=text][data-' + options.commandRole + ']').on('webkitspeechchange change', function () {
					var newValue = this.value; /* ugly but prevents fake double-calls due to selection restoration */
					this.value = '';
					restoreSelection();
					if (newValue) {
						editor.focus();
						execCommand($(this).data(options.commandRole), newValue);
					}
					saveSelection();
				}).on('focus', function () {
					var input = $(this);
					if (!input.data(options.selectionMarker)) {
						markSelection(input, options.selectionColor);
						input.focus();
					}
				}).on('blur', function () {
					var input = $(this);
					if (input.data(options.selectionMarker)) {
						markSelection(input, false);
					}
				});
				toolbar.find('input[type=file][data-' + options.commandRole + ']').change(function () {
					restoreSelection();
					if (this.type === 'file' && this.files && this.files.length > 0) {
						//insertFiles(this.files);
						insertFiles(this.files, options.uploadScript, options.uploadOptions);
					}
					saveSelection();
					this.value = '';
				});
			},
			//initFileDrops = function () {
				initFileDrops = function (options) {
				editor.on('dragenter dragover', false)
					.on('drop', function (e) {
						var dataTransfer = e.originalEvent.dataTransfer;
						e.stopPropagation();
						e.preventDefault();
						if (dataTransfer && dataTransfer.files && dataTransfer.files.length > 0) {
							//insertFiles(dataTransfer.files);
							insertFiles(dataTransfer.files, options.uploadScript, options.uploadOptions);
						}
					});
			};
		options = $.extend({}, $.fn.wysiwyg.defaults, userOptions);
		toolbarBtnSelector = 'a[data-' + options.commandRole + '],button[data-' + options.commandRole + '],input[type=button][data-' + options.commandRole + ']';
		bindHotkeys(options.hotKeys);
		if (options.dragAndDropImages) {
			//initFileDrops();
			initFileDrops(options);
		}
		bindToolbar($(options.toolbarSelector), options);
		editor.attr('contenteditable', true)
			.on('mouseup keyup mouseout', function () {
				saveSelection();
				updateToolbar();
			});
		$(window).bind('touchend', function (e) {
			var isInside = (editor.is(e.target) || editor.has(e.target).length > 0),
				currentRange = getCurrentRange(),
				clear = currentRange && (currentRange.startContainer === currentRange.endContainer && currentRange.startOffset === currentRange.endOffset);
			if (!clear || isInside) {
				saveSelection();
				updateToolbar();
			}
		});
		return this;
	};
	$.fn.wysiwyg.defaults = {
		hotKeys: {
			'ctrl+b meta+b': 'bold',
			'ctrl+i meta+i': 'italic',
			'ctrl+u meta+u': 'underline',
			'ctrl+z meta+z': 'undo',
			'ctrl+y meta+y meta+shift+z': 'redo',
			'ctrl+l meta+l': 'justifyleft',
			'ctrl+r meta+r': 'justifyright',
			'ctrl+e meta+e': 'justifycenter',
			'ctrl+j meta+j': 'justifyfull',
			'shift+tab': 'outdent',
			'tab': 'indent'
		},
		toolbarSelector: '[data-role=editor-toolbar]',
		commandRole: 'edit',
		activeToolbarClass: 'btn-info',
		selectionMarker: 'edit-focus-marker',
		selectionColor: 'darkgrey',
		dragAndDropImages: true,
		//fileUploadError: function (reason, detail) { console.log("File upload error", reason, detail); }
		fileUploadError: function (reason, detail) { console.log("File upload error", reason, detail); },
                uploadScript: '',
		uploadOptions: {}
	};

     
	
}(window.jQuery));