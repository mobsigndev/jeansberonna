jQuery(function($){

    $(document).ready(function () {

 		/*

	    # =============================================================================

	    #   Formularios

	    # =============================================================================

		*/

			$("body").on("click","#mas-letra",function(){

	  	

		var actual= $("table").css("font-size");

		actual = actual.replace("px","");		

		actual = parseInt(actual)+1; 

		$("table").css("font-size",actual+"px");		

		return false;

	});

	$("body").on("click","#menos-letra",function(){

		var actual= $("table").css("font-size");

		actual = actual.replace("px","");		

		actual = parseInt(actual)-1; 

		$("table").css("font-size",actual+"px");		

		return false;

	});

		$("body").on("click","#eliminar-seleccionados",function(e){			  

			var elemento = $(this).parent().parent().parent();

			var tabla=$(this).attr("tabla");	

			var seleccionados =$(".table input[type=checkbox][name!='checkAll']:checked").length;

			if(seleccionados>0){

	    	$(elemento).confirmar("Seguro que desea eliminar los "+seleccionados+" registros","Si","No",function(respuesta){

				if(respuesta) {

				$(".table input[type=checkbox][name!='checkAll']:checked").each(function(){

						var id = $(this).attr("iId");						

						var elimina_renglon = $(this);

						$(elemento).cargando("Eliminando Registro");

						$.post("./clases/eliminar.php",{id:id,tabla:tabla},function(data){

						data = eval(data);

						if(data[1]==1) {

							$(elimina_renglon).closest("tr").remove();

							$(elemento).correcto(data[0]);

						}else if(data[1]==0) {

							$(elemento).error(data[0]);						

						}			

						});

				});

				}							

			});

			}

			e.preventDefault();

		});

    	$("body").on("click","#nuevo",function(){   

			var titulo=$(this).attr("titulo");

			var form=$(this).attr("formulario");

			var tabla=$(this).attr("tabla");

			var post_agregar=$(this).attr("post_agregar");														

			var pre_agregar=$(this).attr("pre_agregar");

			var retorna_id=$(this).attr("retorna_id");

			if(retorna_id!="si") retorna_id="no";													

	    	var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>"

            			+"<button type='button' class='btn btn-primary' id='guardar' post_agregar='"+post_agregar+"' pre_agregar='"+pre_agregar+"'  tabla='"+tabla+"' retorna_id='"+retorna_id+"'>Guardar</button>";						

			$.post(form,null,function(data){				

		    	vermodal(titulo,data,botones);

			});

	    });  

		function campos_obligatorios() {

			completar = true;			

			$(".select[obligatorio='si'], input[obligatorio='si'], textarea[obligatorio='si']").each(function(index) {				

					$(this).parent().removeClass("has-error"); 					

			});

			$(".select[obligatorio='si'], input[obligatorio='si'], textarea[obligatorio='si']").each(function(index) {

				if($(this).val()=="") { 

					$(this).attr("placeholder","*"); 

					$(this).parent().addClass("has-error"); 					

					completar = false; 

				} 

			});

			return completar;

		}

	    $("body").on("click","#guardar",function(){

				var elemento = $(this);

				var tabla=$(this).attr("tabla");

				var iId = $("#iId").val();		

				var post_agregar=$(this).attr("post_agregar");	

				var pre_agregar=$(this).attr("pre_agregar");	

				var retorna_id=$(this).attr("retorna_id");

				if(pre_agregar=='undefined') { var pre_agregar='./clases/sin_pre_agregar.php'; }

				if(post_agregar=='undefined') { var post_agregar=false; }				

				if(campos_obligatorios()) {	

					$(elemento).cargando("Registrando "+tabla);

					var todos_datos = $("#ventana-formulario input,#ventana-formulario select,#ventana-formulario textarea").serialize();

					var datos = $("#ventana-formulario input[no_catalogo!='no'],#ventana-formulario select[no_catalogo!='no'],#ventana-formulario textarea[no_catalogo!='no']").serialize();

					var procede=true;

					$.post(pre_agregar,{datos:datos},function(data2){

						data2 = eval(data2);

						if(data2[1]=="0") {

						}

						else {

							$(elemento).error(data2[0]);						

							procede=false;

						}						

						if(procede) {

							$.post("./clases/almacena.php",{datos:datos,tabla:tabla},function(data){

								data = eval(data);

								if(data[1]>0) {

									$(elemento).correcto(data[0]);

									if(retorna_id=="si") { 

											$( "#id_de_retorno" ).attr("tipo",tabla);

									  		$( "#id_de_retorno" ).trigger( "retorno", data[1] ); 

											}

									if(post_agregar!=""){

										$.post(post_agregar,{iId:data[1],todos_datos:todos_datos},function(data1){

											data1 = eval(data1);

											if(data1[1]>0) {

												$(elemento).correcto(data1[0]);

											}

											else {

												$(elemento).error(data1[0]);						

											}

										});

									}							

								}else if(data[1]==0) {

									$(elemento).error(data[0]);						

								}

							});

						}

					});

				}

	    });  

		    $("body").on("click","#actualizar",function(){

				var elemento = $(this);

				var tabla=$(this).attr("tabla");

				var iId = $("[name='iId']").val();		

				var post_agregar=$(this).attr("post_agregar");	

				var pre_agregar=$(this).attr("pre_agregar");					

					$(elemento).cargando("Actualizando "+tabla);

				var datos = $("#ventana-formulario input[no_catalogo!='no'],#ventana-formulario select[no_catalogo!='no'],#ventana-formulario textarea[no_catalogo!='no']").serialize();

				console.log(datos);

				$.post("./clases/actualiza.php",{datos:datos,tabla:tabla,iId:iId},function(data){

					data = eval(data);

					if(data[1]==1) {

						$(elemento).correcto(data[0]);

					}else if(data[1]==0) {

						$(elemento).error(data[0]);						

					}

					//almacenado

				//	$(elemento).cerrar_cargando();

				});

	    });  

		$("body").on("click",".action-buttons #eliminar-registro",function(e){  

			var elemento = $(this);

			var tabla=$(this).parent().attr("tabla");	

			var id = $(this).attr("href");			

	    	$(elemento).confirmar("Seguro que desea eliminar registro","Si","No",function(respuesta){

				if(respuesta) {

					$(elemento).cargando("Eliminando Registro");

					$.post("./clases/eliminar.php",{id:id,tabla:tabla},function(data){

					data = eval(data);

					if(data[1]==1) {

						$(elemento).closest("tr").remove();

						$(elemento).correcto(data[0]);

					}else if(data[1]==0) {

						$(elemento).error(data[0]);						

					}			

					});

				}							

			});

			e.preventDefault();

	    });

		$("body").on("click",".action-buttons #editar-registro",function(e){    		

			var titulo=$(this).attr("titulo");			

			var form=$(this).parent().attr("formulario");

			var tabla=$(this).parent().attr("tabla");	

			var post_agregar=$(this).parent().attr("post_agregar");

			var pre_agregar=$(this).parent().attr("post_agregar");							

			var id = $(this).attr("href");										

	    	var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>"

            			+"<button type='button' class='btn btn-primary' id='actualizar' pre_agregar='"+pre_agregar+"' post_agregar='"+post_agregar+"' tabla='"+tabla+"'>Guardar</button>";					

			$.post(form,null,function(data){			

		    	vermodal(titulo,data,botones);

				rellena_datos(id,tabla);	

			});

			e.preventDefault();

	    });

		$("body").on("click",".action-buttons #ver-registro",function(e){  

			var titulo=$(this).attr("titulo");			

			var form=$(this).parent().attr("formulario");

			var tabla=$(this).parent().attr("tabla");	

			var id = $(this).attr("href");														

	    	var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";            			

			$.post(form,null,function(data){				

		    	vermodal(titulo,data,botones);

				rellena_datos(id,tabla);	

			});

			e.preventDefault();

	    });

		function rellena_datos(id,tabla) {

			 var _c = "";

			  $("#ventana-formulario input,#ventana-formulario select,#ventana-formulario textarea").each(function( i, val ) {

				  if($(this).attr("no_catalogo")=="no"){}

				  else

				 _c += $(this).attr("name")+",";  

			  });

			$.post("./clases/select.php",{id:id,tabla:tabla,_c:_c},function(data) {

				data = eval(data);

				 $.each( data[0], function( i, val ) {					 				

				 	$("[name='"+i+"']").val(val);

				 });

				$("[name='iId']").click();

			});

//			

		}	  

		$( "input" ).on( "retorno", function( event, id ) {

		  $( this ).val( id);		  

		  $( this ).click();

		}); 

		/*

	    # =============================================================================

	    #   Tooltips, Alertas, Mensajes de Carga

	    # =============================================================================

	    */ 

		$('[data-toggle="tooltip"]').tooltip();

/*

   var hideAllPopovers = function() {

       $('.popover').each(function() {

            $(this).hide();

        });  

    };

	$("body").on('click',':not(a)', function(e) {

		console.log("click");

        hideAllPopovers();

    });*/

		$.fn.cargando = function(_msg,_posicion){

			var _posicion = _posicion || "left";

			_msg = _msg || "Procesando";

		 $(this).popover('destroy');

		 $(this).popover({

			 content: 	'<i class="fa fa-spinner fa-spin fa-3x"></i> '+_msg,

			 html: 		true,

			 trigger:	'manual',

			 placement:	_posicion

		 });

		  $(this).popover('show');

		}

		$.fn.cerrar_cargando= function(_msg){

    		 $(this).popover('destroy');

		}

		$.fn.correcto = function(_msg,_posicion){

			 $(this).popover('destroy');			

			_msg = _msg || "Correcto";

			var _posicion = _posicion || "left";

		 var pop_ = $(this).popover({

			 content: 	'<i class="fa fa-thumbs-o-up fa-3x"></i> '+_msg,

			 html: 		true,

			 trigger:	'manual',

			 placement:	_posicion

		 })

		  pop_.popover('show');

		  setTimeout(function () {

		    	    pop_.popover('hide');

			    }, 4000);

		}

		$.fn.advertencia= function(_msg,_posicion){

			 $(this).popover('destroy');			

			var _posicion = _posicion || "left";

			_msg = _msg || "Advertencia";

		 var pop_ = $(this).popover({

			 content: 	'<i class="fa fa-exclamation-triangle fa-3x"></i> '+_msg,

			 html: 		true,

			 trigger:	'manual',

			 placement:	_posicion			 

		 })

		 pop_.popover('show');

		  setTimeout(function () {

		    	    pop_.popover('hide');

			    }, 4000);

		}

		$.fn.error = function(_msg,_posicion){

			 $(this).popover('destroy');			

			_msg = _msg || "Error";

			var _posicion = _posicion || "left";

		var pop_= $(this).popover({

			 content: 	'<i class="fa fa-times-circle fa-3x"></i> '+_msg,

			 html: 		true,

			 trigger:	'manual',

			 placement:	_posicion

		 })

		 pop_.popover('show');

		  setTimeout(function () {

		    	    pop_.popover('hide');

			    }, 4000);

		}

		$.fn.confirmar= function(_msg,_ok,_cancelar,callback,position){

			position = position || "left";

			 $(this).popover('destroy');			

			_ok = _ok || "Si";

			_cancelar = _cancelar || "No";			

			_msg = _msg || "Confirme";

			var botones="<div style='margin:0 auto;'><button type='button' class='btn btn-primary' id='ok_confirm'>"+_ok+"</button>"+

			" <button type='button' class='btn btn-default' data-dismiss='popover' id='cancel_confirm'>"+_cancelar+"</button></div>";

				 var pop_ =$(this).popover({

					 content: 	_msg+' <i class="fa fa-question fa-2x"></i><br>'+botones,

					 html: 		true,

					 trigger:	'manual',

					 placement:	position

				 }).on('shown.bs.popover', function () {

   				 var $popup = $(this);

   				 $(this).next('.popover').find('button#ok_confirm').click(function (e) {

       				 $popup.popover('hide');

					 callback(true);

   					 });

   				 $(this).next('.popover').find('button#cancel_confirm').click(function (e) {

      		 		 $popup.popover('hide');

					 callback(false);

    				});

				});

				 pop_.popover('show');

		}

			if($("[name='iEmpresaSeleccionadaId']").val()=="0") {

				var cantidad_ = $("#sel_empresa").find("option").length;

				if(cantidad_>1) {

					var elemento = $("[name='iEmpresaSeleccionadaId']");

					$(elemento).advertencia("Seleccione Empresa","bottom");

				}else {

					$("#cat_menu").addClass("Active");

					$("#n_empresa").advertencia("Registre una Empresa","right");

				}

			}

		/*

	    # =============================================================================

	    #   Otros

	    # =============================================================================

	    */ 

		$(".ver_licencias_usuario").click(function(){

			var titulo="Mis Licencias";

			var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";					

			$.post("./licencias.php",null,function(data){			

		    	vermodal(titulo,data,botones);

			});

			return false;

		});

		$("#entrar").click(function(){		

			var elemento = $(this);

			$(elemento).cargando("Validando");

			var usuario = $("#usuario").val();

			var clave = $("#clave").val();	

			if( usuario==""){

				$(elemento).error("Ingrese usuario");	

			}

			else if(clave==""){

				$(elemento).error("Ingrese clave");	

			}

			else {

				$.post("./validador.php",{usuario:usuario,clave:clave},function(data){

					$(elemento).correcto(data);	

					

					//$(elemento).cerrar_cargando();

				});

			 }

			return false;

		});

		$("[name='iEmpresaSeleccionadaId']").change(function(){					

			var elemento = $(this);

			$(elemento).cargando("Cargando Empresa","bottom");

			var iUsuarioEmpresaId = $(elemento).val();

			$.post("./carga_empresa.php",{iUsuarioEmpresaId:iUsuarioEmpresaId},function(data){

				location.reload();

			});

			return false;		

		});

		$("body").on("click","#tipo_transaccion li a",function(e){
			console.log("prueba");

			e.preventDefault();

			var tipo_ = $(this).html();

			var itipo_ = $(this).attr("href");	

			$("#tipo_transaccion_").html(tipo_);

			$("#tipo_transaccion_").attr("tipo",itipo_);

			if(itipo_==1){

				$("#almacenarSolicitud").html("<i class=\"fa fa-usd\"></i> Procesar pago");

				$("#divenriquecido").toggle(false);

			}

			if(itipo_==2){

				$("#almacenarSolicitud").html("<i class=\"fa fa-file-o\"></i> Generar factura");

				$("#divenriquecido").toggle(false);

			}

			if(itipo_==3){

				$("#almacenarSolicitud").html("<i class=\"fa fa-file-o\"></i> Generar cotizaciÃ³n");

				$("#divenriquecido").toggle(true);

			}

		});
		
		

		$("body").on("click","#tipo_credito li a",function(e){

			e.preventDefault();

			var tipo_ = $(this).html();

			var itipo_ = $(this).attr("href");	

			var iMovimientoEstadoCuentaId_=$(this).attr("iMovimientoEstadoCuentaId");

			$("#tipo_credito_").html(tipo_);

			$("#tipo_credito_").attr("tipo",itipo_);

			$("#tipo_credito_").attr("iMovimientoEstadoCuentaId",iMovimientoEstadoCuentaId_);			

			$("#tipo_credito_boton").html(tipo_);

		});

		

		

		

		

    });

});

	/*

	    # =============================================================================

	    #   Impresion, PDF, Excel

	    # =============================================================================

	    */ 

		function imprime(datos,titulo)

		{				

			titulo = titulo || "Reporte";	

			

			var ventimp=window.open(' ','impresion adminbox');

			$.post("./clases/encabezado.php",{titulo:titulo},function(data){

				datos = data+datos;

				ventimp.document.write(datos);

				ventimp.document.close();

				setTimeout(function(){

					ventimp.print();

					//ventimp.close();	

				}, 1000);

				

			 });

		}	

		function pdf(datos,titulo)

		{

			//var datos="";

			titulo = titulo || "Reporte";

			$.post("./clases/encabezado_pdf.php",{titulo:titulo},function(data){

				console.log(data);

				datos = data+datos;

				datos = encodeURIComponent(datos);				

				$("#t_codigo").html("<div style='font-size:10px;'>"+datos+"</div>");

				$("#form_pdf").attr("action","./clases/pdf.php");			

				$("#form_pdf").submit();			

			});

		}	

		function excel(datos,titulo)

		{

			//var datos="";

			titulo = titulo || "Reporte";

			datos = encodeURIComponent(datos);			

			$("#t_codigo").html(datos);

			$("#form_pdf").attr("action","./clases/excel.php");			

			$("#form_pdf").submit();			

		}				

		   /*

	    # =============================================================================

	    #   DataTables

	    # =============================================================================

			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",

	    */    

 $.fn.crear_tabla= function(formulario,iId,tabla,genera_tabla,condicion,order_by,recarga,post_agregar,callback_){		

			//genera_tabla = JSON.stringify(genera_tabla);

			callback_ = callback_ || false;	

				recarga = recarga || false;

			var elemento = $(this);

					$.post("./clases/tabla.php",{tabla:tabla,genera_tabla:genera_tabla,formulario:formulario,condicion:condicion,order_by:order_by,post_agregar:post_agregar,iId:iId},function(data){

						$(elemento).html(data);							

						var tabla = $("#tabla").dataTable({

						"sDom": "t<'row-fluid'<'span12'pi>>",

						 "oLanguage": {

							"sLengthMenu": "_MENU_ registros p/pagina",

							 "sSearch": "<span class='fa fa-search' style='margin-right:4px; font-size:18px;'></span>",

							 "sInfoFiltered": "",

							 "sInfo": "_TOTAL_ registros",

							 "sEmptyTable": "No se encontraron registros",

							 "sInfoEmpty": "No se encontraron registros",

							 "sLoadingRecords": "Espere- cargando...",

							 "sProcessing": "Procesando",

							  "sNext": "Siguiente",

							  "sPrevious": "Atras",

							   "sZeroRecords": "No hay registros que mostrar"				   

							}

						});	

			if(!recarga) {			

 $("body").on("click","#pdf-tabla",function() {		

 	  	var titulo_ = $(this).attr("titulo");  

		  pdf(datos_tabla(),titulo_);		

	  });

	  $("body").on("click","#excel-tabla",function() {	

	  	  	var titulo_ = $(this).attr("titulo");	  

		  excel(datos_tabla(),titulo_);		

	  });

	  $("body").on("click","#imprimir-tabla",function() {

		  

	  	  	var titulo_ = $(this).attr("titulo");

		  imprime(datos_tabla(),titulo_);

	  });	  

	  function datos_tabla() {

		tabla= $('#tabla').dataTable();

		  var c=0;	

		var informacion_="<table cellspacing='0' border='1' class='datos__' style='width:780px; font-size:10px;' >";	 

		var columnas__ = $('#tabla thead tr');

		informacion_+="<thead>";

		for(var j = 0; j < columnas__.length; j++){

			informacion_+="<tr>";			

			var columna__ = columnas__[j];

			$('th',columna__).each(function() {

				if($(this).attr("no_visible")) {} else {

					var valor = $(this).attr("widthpdf");

					informacion_+="<th style='width:"+valor+";'>"+$(this).html()+"</th>";

				}

			});

			informacion_+="</tr>";

		}		

		informacion_+="</thead><tbody>";

		var datos__ = tabla.fnGetNodes();

		for(var i = 0; i < datos__.length; i++){

			informacion_+="<tr>";

			var renglon__ = datos__[i];

			c=0;

			$('td', renglon__).each(function() {

				if($('#tabla thead th:eq('+c+')').attr("no_visible")) {}

				else informacion_+="<td> "+$(this).html()+"</td>";					

				c++;

			});

			informacion_+="</tr>";

		}

			informacion_ +="</tbody></table>";

			return informacion_; 

	  }

	  // Sometime later - filter...

	  $("body").on("keyup","#busqueda-tabla",function() {

	  	  tabla= $('#tabla').dataTable();

		  tabla.fnFilter( $(this).val() );			  	

	  });

	    $('.table').each(function() {

	      return $(".table #checkAll").click(function() {

	        if ($(".table #checkAll").is(":checked")) {

	          return $(".table input[type=checkbox]").each(function() {

	            return $(this).prop("checked", true);

	          });

	        } else {

	          return $(".table input[type=checkbox]").each(function() {

	            return $(this).prop("checked", false);

	          });

	        }

	      });

	    });	 						

			}

		$('[data-toggle="tooltip"]').tooltip();	

		if(callback_) { callback_(true); }	

	});	

   

} 

 $.fn.crear_tabla_general = function(tabla_,url_tabla,condicion,recarga,callback_){

		    callback_ = callback_ || false;					

			recarga = recarga || false;

			var elemento = $(this);

			

 

				$.post(url_tabla,{tabla:tabla_,condicion:condicion},function(data){

						$(elemento).html(data);							

						var tabla = $("#"+tabla_).dataTable({

						"sDom": "t<'row-fluid'<'span12'pi>>",

						 "oLanguage": {

							"sLengthMenu": "_MENU_ registros p/pagina",

							 "sSearch": "<span class='fa fa-search' style='margin-right:4px; font-size:18px;'></span>",

							 "sInfoFiltered": "",

							 "sInfo": "_TOTAL_ registros",

							 "sEmptyTable": "No se encontraron registros",

							 "sInfoEmpty": "No se encontraron registros",

							 "sLoadingRecords": "Espere- cargando...",

							 "sProcessing": "Procesando",

							  "sNext": "Siguiente",

							  "sPrevious": "Atras",

							   "sZeroRecords": "No hay registros que mostrar"				   

							}

						});	

			if(!recarga) {			

	

 	$("body").on("click","#pdf-tabla",function() {		  

	  	var titulo_ = $(this).attr("titulo");

		  pdf(datos_tabla_general(tabla_),titulo_);		

	  });

	  $("body").on("click","#excel-tabla",function() {		  

	  	var titulo_ = $(this).attr("titulo");

		  excel(datos_tabla_general(tabla_),titulo_);		

	  });

	  $("body").on("click","#imprimir-tabla",function() {			

	  	var titulo_ = $(this).attr("titulo");

		  imprime(datos_tabla_general(tabla_),titulo_);

	  });	   

	  function datos_tabla_general(tabla_) {

		tabla=  $("#"+tabla_).dataTable();

		

 

		  var c=0;	

		var informacion_="<table cellspacing='0' border='1' class='datos__' style='width:100%; font-size:10px;' >";	 

		var columnas__ =  $("#"+tabla_+" thead tr");

			console.log(tabla_);

			console.log(columnas__);

		informacion_+="<thead>";

		for(var j = 0; j < columnas__.length; j++){

			informacion_+="<tr>";			

			var columna__ = columnas__[j];

			$('th',columna__).each(function() {

				if($(this).attr("no_visible")) {} else {

					var valor = $(this).attr("widthpdf");

					informacion_+="<th style='width:"+valor+";'>"+$(this).html()+"</th>";

				}

			});

			informacion_+="</tr>";

		}		

		informacion_+="</thead><tbody>";

		var datos__ = tabla.fnGetNodes();

		for(var i = 0; i < datos__.length; i++){

			informacion_+="<tr>";

			var renglon__ = datos__[i];

			c=0;

			$('td', renglon__).each(function() {

				if( $("#"+tabla_+" thead th:eq("+c+")").attr("no_visible")) {}

				else informacion_+="<td> "+$(this).html()+"</td>";					

				c++;

			});

			informacion_+="</tr>";

		}

			informacion_ +="</tbody></table>";

			return informacion_; 

	  }

	  // Sometime later - filter...

	  $("body").on("keyup","#busqueda-tabla",function() {

	  	  tabla= $("#"+tabla_).dataTable();

		  tabla.fnFilter( $(this).val() );			  	

	  });

			}

		if(callback_) { callback_(true); }

	});

		$('[data-toggle="tooltip"]').tooltip();		

	}

 $.fn.crear_tabla_lista= function(tabla,iId,genera_tabla,condicion,order_by,recarga,callback_){		

			callback_ = callback_ || false;		

			genera_tabla = genera_tabla;	

			recarga = recarga || false;

			var elemento = $(this);

					$.post("./clases/tabla_lista.php",{tabla:tabla,genera_tabla:genera_tabla,condicion:condicion,order_by:order_by,iId:iId},function(data){

						$(elemento).html(data);							

						var tabla = $("#tabla_lista").dataTable({

						"sDom": "t<'row-fluid'<'span12'pi>>",

						"bAutoWidth": false,

						 "oLanguage": {

							"sLengthMenu": "_MENU_ registros p/pagina",

							 "sSearch": "<span class='fa fa-search' style='margin-right:4px; font-size:18px;'></span>",

							 "sInfoFiltered": "",

							 "sInfo": "_TOTAL_ registros",

							 "sEmptyTable": "No se encontraron registros",

							 "sInfoEmpty": "No se encontraron registros",

							 "sLoadingRecords": "Espere- cargando...",

							 "sProcessing": "Procesando",

							  "sNext": "Siguiente",

							  "sPrevious": "Atras",

							   "sZeroRecords": "No hay registros que mostrar"				   

							}

						});	

								if(callback_) { callback_(true); }

					});

					if(!recarga) {	

	$("body").on("click","#pdf-tabla",function() {

		  var titulo_ = $(this).attr("titulo");  

		  pdf(datos_tabla(),titulo_);		

	  });

	  $("body").on("click","#excel-tabla",function() {	

	  	  var titulo_ = $(this).attr("titulo");	  	  

		  excel(datos_tabla(),titulo_);		

	  });

	  $("body").on("click","#imprimir-tabla",function() {			

	  	  var titulo_ = $(this).attr("titulo");

		  imprime(datos_tabla(),titulo_);

	  });	  

	  function datos_tabla() {

		tabla= $('#tabla_lista').dataTable();

		  var c=0;	

		var informacion_="<table cellspacing='0' border='1' class='datos__' style='width:100%; font-size:10px;' >";	 

		var columnas__ = $('#tabla_lista thead tr');

		informacion_+="<thead>";

		for(var j = 0; j < columnas__.length; j++){

			informacion_+="<tr>";			

			var columna__ = columnas__[j];

			$('th',columna__).each(function() {

				if($(this).attr("no_visible")) {} else {

					var valor = $(this).attr("widthpdf");

					informacion_+="<th style='width:"+valor+";'>"+$(this).html()+"</th>";

				}

			});

			informacion_+="</tr>";

		}		

		informacion_+="</thead><tbody>";

		var datos__ = tabla.fnGetNodes();

		for(var i = 0; i < datos__.length; i++){

			informacion_+="<tr>";

			var renglon__ = datos__[i];

			c=0;

			$('td', renglon__).each(function() {

				if($('#tabla_lista thead th:eq('+c+')').attr("no_visible")) {}

				else informacion_+="<td> "+$(this).html()+"</td>";					

				c++;

			});

			informacion_+="</tr>";

		}

			informacion_ +="</tbody></table>";

			return informacion_; 

	  }					

		 $("body").on("keyup","#busqueda-tabla",function() {

	  		  	tabla= $('#tabla_lista').dataTable();

		  		tabla.fnFilter( $(this).val() );			  			  

	  	});

					}

	 }

 	    function vermodal(titulo,contenido,botones,callback_){
			callback_ = callback_ || false;	
	    	$("#myModalLabel").html(null);

	    	$(".modal-body").html(null);

	    	$(".modal-footer").html(null);

	        $("#myModalLabel").html(titulo);

	    	$(".modal-body").html(contenido);

	    	$(".modal-footer").html(botones);

		   	if($('#ventana-emergente').hasClass('in')) { 
 	    			console.log("ya no se abrio otro .modal , ya estaba abierto");
 	    		}
 	    		else {
 	    			console.log("abrio modal nuevo");
 	    			$('#ventana-emergente').modal();
 	    		}
			if(callback_){ callback_(true); }	
			
			

		}

	    function cerrarmodal(){

	    	$('#ventana-emergente').modal('hide');

	    }   

		

		

		

		function datos_tabla_(tabla_) {

		tabla=  $("#"+tabla_).dataTable();

		  var c=0;	

		var informacion_="<table cellspacing='0' border='1' class='datos__' style='width:100%; font-size:10px;' >";	 

		var columnas__ =  $("#"+tabla_+" thead tr");

		informacion_+="<thead>";

		for(var j = 0; j < columnas__.length; j++){

			informacion_+="<tr>";			

			var columna__ = columnas__[j];

			$('th',columna__).each(function() {

				if($(this).attr("no_visible")) {} else {

					var valor = $(this).attr("widthpdf");

					informacion_+="<th style='width:"+valor+";'>"+$(this).html()+"</th>";

				}

			});

			informacion_+="</tr>";

		}		

		informacion_+="</thead><tbody>";

		var datos__ = tabla.fnGetNodes();

		for(var i = 0; i < datos__.length; i++){

			informacion_+="<tr>";

			var renglon__ = datos__[i];

			c=0;

			$('td', renglon__).each(function() {

				if( $("#"+tabla_+" thead th:eq("+c+")").attr("no_visible")) {}

				else informacion_+="<td> "+$(this).html()+"</td>";					

				c++;

			});

			informacion_+="</tr>";

		}

			informacion_ +="</tbody></table>";

			return informacion_; 

	  }

	  

	  

	  function datos_tabla_(tabla_) {

		tabla=  $("#"+tabla_).dataTable();

		  var c=0;	

		var informacion_="<table cellspacing='0' border='1' class='datos__' style='width:100%; font-size:10px;' >";	 

		var columnas__ =  $("#"+tabla_+" thead tr");

		informacion_+="<thead>";

		for(var j = 0; j < columnas__.length; j++){

			informacion_+="<tr>";			

			var columna__ = columnas__[j];

			$('th',columna__).each(function() {

				if($(this).attr("no_visible")) {} else {

					var valor = $(this).attr("widthpdf");

					informacion_+="<th style='width:"+valor+";'>"+$(this).html()+"</th>";

				}

			});

			informacion_+="</tr>";

		}		

		informacion_+="</thead><tbody>";

		var datos__ = tabla.fnGetNodes();

		for(var i = 0; i < datos__.length; i++){

			informacion_+="<tr>";

			var renglon__ = datos__[i];

			c=0;

			$('td', renglon__).each(function() {

				if( $("#"+tabla_+" thead th:eq("+c+")").attr("no_visible")) {}

				else informacion_+="<td> "+$(this).html()+"</td>";					

				c++;

			});

			informacion_+="</tr>";

		}

			informacion_ +="</tbody></table>";

			return informacion_; 

	  }

	  

	  function fecha(fecha) {

	  	return "STR_TO_DATE('"+fecha+"','%d-%m-%Y')";

	  }

	  function fechahora(fechahora) {

	  	return "STR_TO_DATE('"+fechahora+"','%d-%m-%Y %H:%i:%s')";

	  }
