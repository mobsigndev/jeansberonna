<?php 
  include("../../acceso/seguridad.php");
  $idproducto = $_REQUEST['idproducto'];
  $condicion = stripslashes(urldecode($_REQUEST['condicion']));

?>
<div id='ventana-formulario'>
<!-- Nav tabs -->
  <!--<input type="text" class="form-control" name="dRegistro" value="CURRENT_DATE()" style='display:none;'>-->
  <input type="text" class="form-control" name="iId" style='display:none;'>
  <div class="row">
	
  <div class="col-lg-12">
            <table class="table table-bordered table-condensed" id="<?=$tabla?>">
            <thead>
              <th> Transacción </th>
              <th> Fecha </th>
              <th>
                  Producto      
              </th>
               <th class="text-center" id="bvendidos">
                  Vendidos      
              </th>
               
              <th class="text-center" id="bvendidos">
                  Utilidad (%)      
              </th>
               <th class="text-center" id="bvendidos">
                  Utilidad ($)      
              </th>
              
            </thead>
             <tbody>   
             <?php
              $datos = select_query("select transaccion.iTipoTransaccion,transaccion.iFolio,transaccion.iId as idTransaccion,transaccion.dFecha,producto.iId,sConcepto,fPrecioCompra,sum(fCantidad) as vendidos,fPrecioCompra,sum(producto.fPrecioCompra*fCantidad) as inversion,sum(((fPrecio*fCantidad)*detalletransaccion.fIva)/100) as iva,
sum((((((fPrecio*fCantidad)*detalletransaccion.fIva)/100)+(fCantidad*fPrecio))*fDescuento/100)) as descuento,
sum((fCantidad*fPrecio)) as subtotal,
sum(((fCantidad*fPrecio)+((fPrecio*fCantidad)*detalletransaccion.fIva)/100)-(((((fPrecio*fCantidad)*detalletransaccion.fIva)/100)+(fCantidad*fPrecio))*fDescuento/100)) as total  from detalletransaccion inner join transaccion on transaccion.iId = detalletransaccion.iTransaccionId
              inner join producto on detalletransaccion.iProductoId = producto.iId where $condicion and producto.iId='$idproducto'  group by iProductoId,transaccion.dFecha order by sum(fCantidad) desc");
              $cantidad=0;
              $vendidos=0;  
              while($renglones = mysql_fetch_array($datos)) {
                   echo '<tr>';
               $cantidad++;
               $vendidos+=limpia_formato(number_format($renglones['vendidos'],2));
             
               $porcentaje+=limpia_formato(number_format(((limpia_formato(number_format($renglones['total'],2))*100)/limpia_formato(number_format($renglones['inversion'],2))-100),2));
               $dinero+=limpia_formato(number_format(limpia_formato(number_format($renglones['total'],2))-limpia_formato(number_format($renglones['inversion'],2)),2));



                  
                    echo '<td class="text-center">'.$renglones['iFolio'].'</td>';
                     echo '<td class="text-center">'.date_format(date_create($renglones['dFecha']),"d/m/Y H:i:s").'</td>';
                      echo '<td>'.$renglones['sConcepto'].'</td>';
                      echo '<td  class="text-center">'.number_format($renglones['vendidos'],2).'</td>';
                       echo '<td class="text-center success">'.number_format(((limpia_formato(number_format($renglones['total'],2))*100)/limpia_formato(number_format($renglones['inversion'],2))-100),2).'%</td>';
            echo '<td class="text-center success">$'.number_format(limpia_formato(number_format($renglones['total'],2))-limpia_formato(number_format($renglones['inversion'],2)),2).'</td>';
                        
                 echo '</tr>';
             
             }?>
             </tbody>
          <tfoot>
          <tr>
              <th>
                
              </th>
              <th class="text-center">
                
              </th>
              <th class="text-right">Total</th>
              <th class="text-center">
                  <?=number_format($vendidos,2);?>
              </th>
              <th class="text-center success">
                 <?=number_format($porcentaje/$cantidad,2);?>%
              </th>
                <th class="text-center success">
                 $<?=number_format($dinero,2);?>
              </th>
          </tr>
            </tfoot>
          </table>
  </div>
                
  </div>
  </div>
