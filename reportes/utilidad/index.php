<?

include_once("./acceso/seguridad.php");

$_tabla_general = "./reportes/utilidad/tabla.php";

?>

<div class="row">


 		<div class="col-lg-2">
            <label>Desde fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Desde" name="desde" size="14" type="text">
		</div> 
         <div class="col-lg-2">
             <label>Hasta fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Hasta" name="hasta" size="14" type="text">
		</div> 
 <div class="col-lg-4">
          <label>Categoría:</label>

	<select class="form-control" name='iCategoriaId' no_catalogo="no">

		<option value="0">Todas</option>    

		<?

		$select = seleccionar("categoria","iId,sCodigo,sNombre","iEstatus=0 and iUsuarioEmpresaId=$iUsuarioEmpresaId and iTipoCategoria=1",false, false);

		foreach($select as $dato) {

			?>

			<option value="<?=$dato['iId'];?>"><?=$dato['sCodigo']."-".$dato['sNombre'];?></option>

			<?

		}

		?>

	</select>

  </div> 
</div>
<div class="row" style='margin-top:3px;'>

	<div class="col-lg-6">
    <button id='filtro_movimientos' type="button" class="btn btn-primary" titulo='Filtrar'>Aplicar Filtros en Reporte</button>
    </div>
	<? include("./clases/exportacion.php"); ?>

</div>



<div class="row">

    <div class="col-lg-12">    		

      	<div id="div-tabla"></div>    

	</div>

</div>        



<script>

$(document).ready(function () {
	$("[name='desde']").val("<?=date("d-m-Y");?>");
		 $('[name="desde"]').datepicker({
			 		todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true
			 });
 		 $('[name="hasta"]').datepicker({
					todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true	 
		});
	 var condicion_ = " transaccion.dFecha>="+fechahora("<?=date("d-m-Y");?> 00:00:00")+""; 
	 condicion_ += " and transaccion.dFecha<="+fechahora("<?=date("d-m-Y");?> 23:59:59")+""; 
	$("#div-tabla").crear_tabla_general("utilidad","<?=$_tabla_general;?>",condicion_,false);

	$("body").on("click",".detalle",function(data){
		var idproducto = $(this).attr("data-idproducto");
		var condicion = $(this).attr("data-condicion");
		var elemento = $(this);
		$(elemento).cargando("Cargando");
		var titulo="Detalle de Utilidad";
		var form="./reportes/utilidad/detalle.php";
    	var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";            			
		$.post(form,{idproducto:idproducto,condicion:condicion},function(data){
			$(elemento).cerrar_cargando();				
	    	vermodal(titulo,data,botones);
		});

	});

	$("#filtro_movimientos").click(function(){
		var elemento = $(this);
		var condicion="1=1";
		var iCategoriaId = $("[name=iCategoriaId]").val();
		var desde = $("[name='desde']").val();	
		var hasta = $("[name='hasta']").val();
		var icliente = $("#iCliente").attr("iClienteId");
		var tipo_transaccion= $("#tipo_transaccion_").attr("tipo");	
		if(icliente!="0") { condicion += " and transaccion.iClienteId ='"+icliente+"'"; }
						if(tipo_transaccion!="0") { condicion += " and transaccion.iTipoTransaccion ='"+tipo_transaccion+"'"; }		
		$(elemento).cargando("Generando reporte","bottom");
		if(iCategoriaId!="0") 	condicion += "and iCategoriaId="+iCategoriaId;
		if(desde!="") { condicion += " and transaccion.dFecha>="+fechahora(desde+" 00:00:00")+""; }
		if(hasta!="")	{ condicion += " and transaccion.dFecha<="+fechahora(hasta+" 23:59:59")+""; }
		$("#div-tabla").crear_tabla_general("utilidad","<?=$_tabla_general;?>",condicion,true,function(){
			$(elemento).cerrar_cargando();
		});
		
	});

$("body").on("click","[name='sel-cliente']",function(e){	
				abre_=true;							
				cliente_seleccionado = $(this).attr("iid");
		});
	$('#ventana-emergente').on('hidden.bs.modal', function (e) {
		if(abre_){
		carga_cliente(cliente_seleccionado);
		abre_ = false;
		}else {
			var elemento = $("#filtro_movimientos");
			var condicion="1=1";
			var iCategoriaId = $("[name=iCategoriaId]").val();
			var desde = $("[name='desde']").val();	
			var hasta = $("[name='hasta']").val();
			var icliente = $("#iCliente").attr("iClienteId");
			var tipo_transaccion= $("#tipo_transaccion_").attr("tipo");	
			if(icliente!="0") { condicion += " and transaccion.iClienteId ='"+icliente+"'"; }
							if(tipo_transaccion!="0") { condicion += " and transaccion.iTipoTransaccion ='"+tipo_transaccion+"'"; }		
			$(elemento).cargando("Generando reporte","bottom");
			if(iCategoriaId!="0") 	condicion += "and iCategoriaId="+iCategoriaId;
			if(desde!="") { condicion += " and transaccion.dFecha>="+fechahora(desde+" 00:00:00")+""; }
			if(hasta!="")	{ condicion += " and transaccion.dFecha<="+fechahora(hasta+" 23:59:59")+""; }
			$("#div-tabla").crear_tabla_general("utilidad","<?=$_tabla_general;?>",condicion,true,function(){
				$(elemento).cerrar_cargando();
			});
		}
	});
	$("body").on("click","#seleccionar-cliente",function(e){
			tipo="default";
			var elemento = $(this);
			$(elemento).cargando("Cargando");
			var titulo="Selecciona Cliente"
			var form="./general/clientes/lista_clientes.php";
			var tabla2="usuario";
	    	var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";            			
			$.post(form,null,function(data){
				$(elemento).cerrar_cargando();				
		    	vermodal(titulo,data,botones);		    	
			});
			e.preventDefault();
		});


	$("body").on("click","#seleccionar-cliente-interior",function(e){
		    tipo="interior";
			var elemento = $(this);
			$(elemento).cargando("Cargando");
			var titulo="Selecciona Cliente"
			var form="./general/clientes/lista_clientes.php";
			var tabla="usuario";
	    	var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";   

			$.post(form,null,function(data){
								
		    	vermodal(titulo,data,botones,function(){		    		
		    		
		    		$(elemento).cerrar_cargando();
		    	});
		    	
			});
			e.preventDefault();
		}); 

	function carga_cliente(iClienteId){
		
			$.post( "./ventas/venta/carga_cliente.php", { iClienteId: iClienteId}, function( data ) {
				data = eval(data);		
				if(tipo=="default"){						
					$("#iCliente").html(data[1]);
					$("#iCliente").attr("iClienteId",data[0]);		
				}	
			});
			
	}


});



</script>



