<?
include("../../acceso/seguridad.php");
$tabla = $_REQUEST['tabla'];
$condicion__ = $_REQUEST['condicion'];
$condicion = stripslashes($_REQUEST['condicion']);

if($condicion=="" || $condicion=="false") {
  $condicion = "transaccion.iEstatus=0 and transaccion.iUsuarioEmpresaId='".$iUsuarioEmpresaId."'";
}
else {
  $condicion = $condicion." and transaccion.iEstatus=0 and transaccion.iUsuarioEmpresaId='".$iUsuarioEmpresaId."' and detalletransaccion.iTipo=1";
}

?>
<table class="table table-bordered table-condensed" id="<?=$tabla?>">
  <thead>
    <th></th>
    <th>
        Producto      
    </th>
     <th class="text-center">
        Vendidos      
    </th>
      <th class="text-center">
        Precio Compra (Unitario)      
    </th>
    <th class="text-center danger">
        Costo Compra
    </th>
    <th class="text-center">
        Subtotal
    </th>
    <th class="text-center">
        IVA
    </th>
    <th class="text-center">
        Descuentos
    </th>
    <th class="text-center warning">
    Total
    </th>
    <th class="text-center success">
    Utilidad (%)
    </th>
    <th class="text-center success">
    Utilidad ($)
    </th>
   
    
  </thead>
   <tbody>   
   <? 
$vendidos=0;
$inversion=0;
$subtotal=0;
$iva=0;
$descuento=0;
$total=0;
$porcentaje=0;
$dinero=0;

$datos = select_query("select producto.iId,sConcepto,fPrecioCompra,sum(fCantidad) as vendidos,sum(producto.fPrecioCompra*fCantidad) as inversion,sum(((fPrecio*fCantidad)*detalletransaccion.fIva)/100) as iva,
sum((((((fPrecio*fCantidad)*detalletransaccion.fIva)/100)+(fCantidad*fPrecio))*fDescuento/100)) as descuento,
sum((fCantidad*fPrecio)) as subtotal,
sum(((fCantidad*fPrecio)+((fPrecio*fCantidad)*detalletransaccion.fIva)/100)-(((((fPrecio*fCantidad)*detalletransaccion.fIva)/100)+(fCantidad*fPrecio))*fDescuento/100)) as total from detalletransaccion inner join transaccion on transaccion.iId = detalletransaccion.iTransaccionId
inner join producto on detalletransaccion.iProductoId = producto.iId where $condicion group by iProductoId");
$cantidad=0;  
while($renglones = mysql_fetch_array($datos)) {
     echo '<tr>';
     $cantidad++;
     $vendidos+=limpia_formato(number_format($renglones['vendidos'],2));
     $costo+=limpia_formato(number_format($renglones['fPrecioCompra'],2));
     $inversion+=limpia_formato(number_format($renglones['inversion'],2));
     $subtotal+=limpia_formato(number_format($renglones['subtotal'],2));
     $iva+=limpia_formato(number_format($renglones['iva'],2));
     $descuento+=limpia_formato(number_format($renglones['descuento'],2));
     $total+=limpia_formato(number_format($renglones['total'],2));

     $porcentaje+=limpia_formato(number_format(((limpia_formato(number_format($renglones['subtotal'],2))*100)/limpia_formato(number_format($renglones['inversion'],2))-100),2));
     $dinero+=limpia_formato(number_format(limpia_formato(number_format($renglones['subtotal'],2))-limpia_formato(number_format($renglones['inversion'],2)),2));
   echo '<td class="text-center"><button class="btn btn-success btn-xs detalle"  title="Ver Detalle" data-idproducto="'.$renglones['iId'].'" data-condicion="'.urlencode($condicion__).'"><i class="fa fa-list" style="cursor:pointer"></i></button></td>';
        echo '<td>'.$renglones['sConcepto'].'</td>';
         echo '<td  class="text-center">'.number_format($renglones['vendidos'],2).'</td>';
          echo '<td  class="text-center">$'.number_format($renglones['fPrecioCompra'],2).'</td>';
        echo '<td  class="text-center danger">$'.number_format($renglones['inversion'],2).'</td>';
        echo '<td  class="text-center">$'.number_format($renglones['subtotal'],2).'</td>';
    echo '<td class="text-center">$'.number_format($renglones['iva'],2).'</td>';
    echo '<td class="text-center">$'.number_format($renglones['descuento'],2).'</td>';
    echo '<td class="text-center warning">$'.number_format($renglones['total'],2).'</td>';
        echo '<td class="text-center success">'.number_format(((limpia_formato(number_format($renglones['subtotal'],2))*100)/limpia_formato(number_format($renglones['inversion'],2))-100),2).'%</td>';
            echo '<td class="text-center success">$'.number_format(limpia_formato(number_format($renglones['subtotal'],2))-limpia_formato(number_format($renglones['inversion'],2)),2).'</td>';
       echo '</tr>';
   }
   ?>                             
</tbody>
<tfoot>
<tr>
<th></th>
    <th>
      Totales
    </th>
    <th class="text-center">
        <?=number_format($vendidos,2);?>
    </th>
    <th class="text-center">
       -
    </th>
    <th class="text-center danger">
      $<?=number_format($inversion,2);?>
    </th>
    <th class="text-center">
      $<?=number_format($subtotal,2);?>
    </th>
    <th class="text-center">
       $<?=number_format($iva,2);?>
    </th>
     <th class="text-center">
       $<?=number_format($descuento,2);?>
    </th>
      <th class="text-center warning">
       $<?=number_format($total,2);?>
    </th>
      <th class="text-center success">
       <?=number_format($porcentaje/$cantidad,2);?>%
    </th>
      <th class="text-center success">
       $<?=number_format($dinero,2);?>
    </th>
</tr>
  </tfoot>
</table>