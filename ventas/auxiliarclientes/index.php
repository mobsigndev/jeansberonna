<?
include_once("./acceso/seguridad.php");
$_tabla_general = "./ventas/auxiliarclientes/tabla.php";
?>
<div class="row">
        <div class="col-lg-2 hide">
            <label>Desde fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Desde" name="desde" size="14" type="text">
		</div> 
         <div class="col-lg-2 hide">
             <label>Hasta fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Hasta" name="hasta" size="14" type="text">
		</div> 
    <div class="col-lg-2 hide">
      	<label>Empleado</label>
		<select class="form-control" name='iEmpleadoId' no_catalogo="no">
            <option value="0">Todos</option>    
            <?
            $select = seleccionar("empleado","iId,iCodigo,sNombre","iEstatus=0 and iUsuarioEmpresaId=$iUsuarioEmpresaId",false, false);
            foreach($select as $dato) {
                ?>
                <option value="<?=$dato['iId'];?>"><?=$dato['iCodigo']."-".$dato['sNombre'];?></option>
                <?
            }
            ?>
		</select>
  	</div>   
      <div class="col-lg-2 hide">
      	<label>Cobranza:</label>
		<select class="form-control" name='cobranza' no_catalogo="no">
            <option value="0">Todos</option>    
				<option value="15">15 Dias</option>
                <option value="30">30 Dias</option>
                <option value="45">45 Dias</option>
                <option value="60">60 Dias</option> 
                <option value="90">90 Dias</option> 
                <option value="120">120 Dias</option>                
		</select>
  	</div>   
	<div class="col-lg-4">
    </div>
</div>

<div class="row" style='margin-top:3px;'>
	<div class="col-lg-6">
    <button id='filtro_movimientos' type="button" class="btn btn-primary" titulo='Filtrar'>Aplicar Filtros en Reporte</button>
    </div>
	<? include("./clases/exportacion.php"); ?>

</div>
<div class="row">
    <div class="col-lg-12">    		
      	<div id="div-tabla"></div>    
	</div>
</div>        
<script>

$(document).ready(function () {

		 $("[name='desde']").val("<?=date("d-m-Y");?>");

		 $('[name="desde"]').datepicker({
	 		todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true
			 });

 		 $('[name="hasta"]').datepicker({
			todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true	 
		});

		
		$("body").on("click","[name='sel-cliente']",function(e){			
			carga_cliente($(this).attr("iid"));
		});




		$("body").on("click","#seleccionar-cliente",function(e){
			tipo="default";
			var elemento = $(this);
			$(elemento).cargando("Cargando");
			var titulo="Selecciona Cliente"
			var form="./general/clientes/lista_clientes.php";
			var tabla2="usuario";
	    	var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";            			
			$.post(form,null,function(data){
				$(elemento).cerrar_cargando();				
		    	vermodal(titulo,data,botones);		    	
			});
			e.preventDefault();
		});


	$("body").on("click","#seleccionar-cliente-interior",function(e){
		    tipo="interior";
			var elemento = $(this);
			$(elemento).cargando("Cargando");
			var titulo="Selecciona Cliente"
			var form="./general/clientes/lista_clientes.php";
			var tabla="usuario";
	    	var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";   

			$.post(form,null,function(data){
								
		    	vermodal(titulo,data,botones,function(){		    		
		    		
		    		$(elemento).cerrar_cargando();
		    	});
		    	
			});
			e.preventDefault();
		}); 

	//condicion, refresh ultimos dos
	 var condicion_ = " transaccion.dFecha>="+fechahora("<?=date("d-m-Y");?> 00:00:00")+""; 
	 condicion_ += " and transaccion.dFecha<="+fechahora("<?=date("d-m-Y");?> 23:59:59")+""; 	
	 condicion_ = "0|0|0";
	$("#div-tabla").crear_tabla_general("tabla_facturas","<?=$_tabla_general;?>",condicion_,false);
	


	function carga_cliente(iClienteId){
		
			$.post( "./ventas/venta/carga_cliente.php", { iClienteId: iClienteId}, function( data ) {
				data = eval(data);		
				if(tipo=="default"){						
					$("#iCliente").html(data[1]);
					$("#iCliente").attr("iClienteId",data[0]);		
				}	else
				{
				
					//$(elemento_).cargando("Cargando detalle...");
					$.post("./ventas/auxiliarclientes/detalle_pedido_linea.php",{iPedidoId:idpedido,iClienteId:data[0],Cliente:data[1]},function(data4){						
					//	$(elemento_).cerrar_cargando();	
						vermodal("Detalle de pedido en linea",data4,"");					
					});
					
				}
			});
			
	}


$("#filtro_movimientos").click(function(){
	var elemento = $(this);
	var condicion="1=1";
	$(elemento).cargando("Generando reporte","bottom");
	var empleado = $("[name='iEmpleadoId']").val();

	var icliente = $("#iCliente").attr("iClienteId");
	var cobranza = $("[name='cobranza']").val();

	condicion += "|0";
	condicion += "|"+icliente; 

	$("#div-tabla").crear_tabla_general("tabla_facturas","<?=$_tabla_general;?>",condicion,true,function(){		
		$(elemento).cerrar_cargando();
	});	

});

});
</script>