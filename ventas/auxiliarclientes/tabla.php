<?

include("../venta/registra_saldo_pendiente.php");
$tabla = $_REQUEST['tabla'];
$condicion_ = stripslashes($_REQUEST['condicion']);
$condicion_ = explode("|", $condicion_);
$condicion = $condicion_[0];
$cobranza = $condicion_[1];
$cliente = $condicion_[2];
$condicion = "transaccion.iUsuarioEmpresaId='".$iUsuarioEmpresaId."'";
if($cliente!="0" && $cliente!="") { 
  $condicion .= " and transaccion.iClienteId='$cliente'"; 
  $ccondicion = " and iId='$cliente'"; 
  $saldo15=0;$saldo30=0;$saldo45=0;$saldo60=0;$saldo90=0;$saldo120=0;$saldot=0;
}
?>
<table class="table table-bordered table-striped" id="<?=$tabla?>">
  <thead>
  <tr class="warning">
    <th>
        Nombre de Cliente
    </th>
    <th class="text-center">
        Saldo
    </th>      
 <th class="text-center">
        Límite
    </th> 
    <th class="text-center">
    	15 Dias
    </th>
    <th class="text-center">
     	 30 Dias
    </th>
    <th class="text-center">
       45 Dias
    </th>
    <th class="text-center">
	    60 Dias	
    </th>
    <th class="text-center">
      90 Dias 
    </th>
    <th class="text-center">
      120 Dias o mas
    </th>
    </tr>
	</thead>
   <tbody>   
   <?
    $datos_cliente = seleccionar("cliente","iId,fSaldoPendiente","iUsuarioEmpresaId='".$iUsuarioEmpresaId."' and iEstatus=0 $ccondicion",false, false);
    foreach($datos_cliente as $cliente) {    
      $idCliente =$cliente['iId']; 
      $fSaldoPendiente = $cliente['fSaldoPendiente'];
      if(($fSaldoPendiente*1)>0){
        registra_sp($idCliente);
      }
      $saldo=0;
      $total_ventas=0;
      $sNombre="";
      $limite=0;
      $s_ = saldo($idCliente);
      $saldo=$s_[0];
      $total_ventas=$s_[1];
      $sNombre=$s_[3];
      $limite=$s_[2];
      $procede=$s_[4];

    $datos_transaccion0 = seleccionar("pagotransaccion inner join transaccion on transaccion.iId = pagotransaccion.iTransaccionId ","max(dtFechaOperacion) as dtFecha,DATEDIFF(CURRENT_DATE(),max(dtFechaOperacion)) as ias","transaccion.iUsuarioEmpresaId='".$iUsuarioEmpresaId."' and transaccion.iClienteId='".$idCliente."' and pagotransaccion.fPago >0 and transaccion.iEstatus=0",false, false);
    foreach($datos_transaccion0 as $dato0) {   
    $dtFecha =$dato0['dtFecha']; 
    $dias_pago =$dato0['ias']; 
    }  
    if($dias_pago==""){ $dias_pago="-"; }



    $datos_transaccion = seleccionar("transaccion","iId,iFolioRaiz","transaccion.iUsuarioEmpresaId='".$iUsuarioEmpresaId."' and transaccion.iClienteId='".$idCliente."' and transaccion.iEstatus=0",false, false);
    foreach($datos_transaccion as $dato) {    
      $iRaizId_ =$dato['iFolioRaiz']; 
    }  
    $datos_transaccion2 = seleccionar("transaccion","dFecha as dtFecha,DATEDIFF(CURRENT_DATE(),dFecha) as ias","transaccion.iId='".$iRaizId_."'",false, false);
    foreach($datos_transaccion2 as $dato2) {    
      $dtFecha2 =$dato2['dtFecha']; 
      $dias =$dato2['ias']; 
    }
    $porcentaje_pago = number_format(100-($saldo*100/$total_ventas),0);
      
        $saldot+=$saldo;
          echo '<tr>';
          echo '<td>'.$sNombre.'</td>';
          echo '<td class="text-center">$'.number_format($saldo,0).'</td>';
          echo '<td class="text-center">$'.number_format($limite,0).'</td>';
          echo '<td class="text-center">'.(tabla_cobranza($dias,15,$saldo)).'</td>';
          echo '<td class="text-center">'.(tabla_cobranza($dias,30,$saldo)).'</td>';
          echo '<td class="text-center">'.(tabla_cobranza($dias,45,$saldo)).'</td>';
          echo '<td class="text-center">'.(tabla_cobranza($dias,60,$saldo)).'</td>';
          echo '<td class="text-center">'.(tabla_cobranza($dias,90,$saldo)).'</td>';
          echo '<td class="text-center">'.(tabla_cobranza($dias,120,$saldo)).'</td>';
          echo '</tr>';
      
   
  }

   ?>                             

</tbody>
<tfoot>
<tr class="warning">
    <th class="text-right">
      Totales
    </th>
    <th class="text-center">
       <?=number_format($saldot,0);?>
    </th>
     <th>
    </th>
     <th class="text-center">
       <?=number_format($saldo15,0);?>
    </th>
     <th class="text-center">
       <?=number_format($saldo30,0);?>
    </th>
     <th class="text-center">
       <?=number_format($saldo45,0);?>
    </th>
     <th class="text-center">
       <?=number_format($saldo60,0);?>
    </th>
     <th class="text-center">
       <?=number_format($saldo90,0);?>
    </th>
    <th class="text-center">
       <?=number_format($saldo120,0);?>
    </th>
</tr>
  </tfoot>
</table>

<?php

function  tabla_cobranza($dias,$cobranza,$saldo){
  global $saldo15,$saldo30,$saldo45,$saldo60,$saldo90,$saldo120;;
if($saldo=="") return "";
  if($cobranza=="15") {
    if($dias>=0 && $dias<=29) {
      $saldo15+=$saldo;
  
      return "$".number_format($saldo,0);
    }
    else return "";
  }
  else if($cobranza=="30") {   
    if($dias>29 && $dias<45) {
      $saldo30+=$saldo;
      return "$".number_format($saldo,0);
    }
    else return "";
  }
  else if($cobranza=="45") {
    if($dias>=45 && $dias<60){
      $saldo45+=$saldo;   
     return "$".number_format($saldo,0);
   }
    else return "";
  }
  else if($cobranza=="60") {
    if($dias>=60 && $dias<90) {
      $saldo60+=$saldo;
      return "$".number_format($saldo,0);
    }
    else return "";
  }
  else if($cobranza=="90") {
    if($dias>=90 && $dias<120) {
      $saldo90+=$saldo;
      return "$".number_format($saldo,0);
    }
    else return "";
  }
  else    if($cobranza=="120") {
    if($dias>=120) {
      $saldo120+=$saldo;
      return "$".number_format($saldo,0);
    }
    else return "";
  }
  else return "";
}
?>