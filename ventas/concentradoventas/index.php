<?
include_once("./acceso/seguridad.php");
$_tabla_general = "./ventas/concentradoventas/tabla.php";
//if(!isset($_REQUEST['d'])) die("Mantenimiento de modulo");
?>
<div class="row">
        <div class="col-lg-2 ">
            <label>Desde fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Desde" name="desde" size="14" type="text">
		</div> 
         <div class="col-lg-2 ">
             <label>Hasta fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Hasta" name="hasta" size="14" type="text">
		</div> 
</div>

<div class="row" style='margin-top:3px;'>
	<div class="col-lg-6">
    <button id='filtro_movimientos' type="button" class="btn btn-primary" titulo='Filtrar'>Aplicar Filtros en Reporte</button>
    <input type="checkbox" name="ocultarcliente" id="ocultarcliente" style="display:inline-block; padding:0px; margin-left:10px;"><label> Ocultar Clientes</label>
    </div>
	<? //include("./clases/exportacion.php"); ?>

</div>
<div class="row" style="margin-top:10px;">
    <div class="col-lg-12">    		
      	<div id="div-tabla"></div>    
	</div>
</div>        
<script>

$(document).ready(function () {

		 $("[name='desde']").val("<?=date("d-m-Y");?>");

		 $('[name="desde"]').datepicker({
	 		todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true
			 });

 		 $('[name="hasta"]').datepicker({
			todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true	 
		});

		
		$("body").on("click","[name='sel-cliente']",function(e){			
			carga_cliente($(this).attr("iid"));
		});



	$("body").on("click","#ocultarcliente",function(e){
		console.log("aqui");
		if($(this).prop("checked")){
			$(".cliente__").hide();

		}else {
			$(".cliente__").show();
		}
		});

		$("body").on("click","#seleccionar-cliente",function(e){
			tipo="default";
			var elemento = $(this);
			$(elemento).cargando("Cargando");
			var titulo="Selecciona Cliente"
			var form="./general/clientes/lista_clientes.php";
			var tabla2="usuario";
	    	var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";            			
			$.post(form,null,function(data){
				$(elemento).cerrar_cargando();				
		    	vermodal(titulo,data,botones);		    	
			});
			e.preventDefault();
		});


	$("body").on("click","#seleccionar-cliente-interior",function(e){
		    tipo="interior";
			var elemento = $(this);
			$(elemento).cargando("Cargando");
			var titulo="Selecciona Cliente"
			var form="./general/clientes/lista_clientes.php";
			var tabla="usuario";
	    	var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";   

			$.post(form,null,function(data){
								
		    	vermodal(titulo,data,botones,function(){		    		
		    		
		    		$(elemento).cerrar_cargando();
		    	});
		    	
			});
			e.preventDefault();
		}); 

	//condicion, refresh ultimos dos
	
	var desde = "<?=date("d-m-Y");?>";	
	var hasta = "<?=date("d-m-Y");?>";	
	var hasta_historico = "<?=date("d-m-Y");?>";	
	if(desde!="") { desde = fechahora(desde+" 00:00:00"); }
	if(hasta!="") { hasta = fechahora(hasta+" 23:59:59"); }	
	if(hasta!="") { hasta_historico = fechahora(hasta_historico+" 00:00:00"); }	

	$.post("./ventas/concentradoventas/tabla.php",{desde:desde,hasta:hasta,hasta_historico:hasta_historico},function(data4){
					$("#div-tabla").html(data4);
		});
	


	function carga_cliente(iClienteId){
		
			$.post( "./ventas/venta/carga_cliente.php", { iClienteId: iClienteId}, function( data ) {
				data = eval(data);		
				if(tipo=="default"){						
					$("#iCliente").html(data[1]);
					$("#iCliente").attr("iClienteId",data[0]);		
				}	else
				{
				
					//$(elemento_).cargando("Cargando detalle...");
					$.post("./ventas/concentradoventas/detalle_pedido_linea.php",{iPedidoId:idpedido,iClienteId:data[0],Cliente:data[1]},function(data4){						
					//	$(elemento_).cerrar_cargando();	
						vermodal("Detalle de pedido en linea",data4,"");					
					});
					
				}
			});
			
	}


$("#filtro_movimientos").click(function(){
	var elemento = $(this);
	var condicion="1=1";
	$(elemento).cargando("Generando reporte","bottom");

	var icliente = $("#iCliente").attr("iClienteId");
	
	var desde = $("[name='desde']").val();	
	var hasta = $("[name='hasta']").val();
	var hasta_historico = 	$("[name='hasta']").val();
	if(desde!="") { desde = fechahora(desde+" 00:00:00"); }
	if(hasta!="") { hasta = fechahora(hasta+" 23:59:59"); }	
	$.post("./ventas/concentradoventas/tabla.php",{desde:desde,hasta:hasta},function(data4){
		$(elemento).cerrar_cargando();
					$("#div-tabla").html(data4);
		});


});

});
</script>