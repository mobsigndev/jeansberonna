<?
//if(!isset($_REQUEST['d']))
//die("Modulo en Actualización");
include_once("./acceso/seguridad.php");
$_tabla_general = "./ventas/cortecaja/tabla.php";
?>
<div class="row">
  
    <div class="row">
        <div class="col-lg-2 ">
            <label>Desde fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Desde" name="desde" size="14" type="text">
		</div> 
         <div class="col-lg-2 ">
             <label>Hasta fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Hasta" name="hasta" size="14" type="text">
		</div> 
	</div>

             
        <div class="col-lg-4 hide">
      	<label>Empleado</label>
		<select class="form-control" name='iEmpleadoId' no_catalogo="no">
            <option value="0">Todos</option>    
            <?
            $select = seleccionar("empleado","iId,iCodigo,sNombre","iEstatus=0 and iUsuarioEmpresaId=$iUsuarioEmpresaId",false, false);
            foreach($select as $dato) {
                ?>
                <option value="<?=$dato['iId'];?>"><?=$dato['iCodigo']."-".$dato['sNombre'];?></option>
                <?
            }
            ?>
		</select>
  	</div>  
        
    
	<div class="col-lg-6">
    
    </div>
</div>

<div class="row" style='margin-top:3px;'>

	<div class="col-lg-6">
    <button id='filtro_movimientos' type="button" class="btn btn-primary" titulo='Filtrar'>Aplicar Filtros</button>
     <button id='imprimir' type="button" class="btn btn-warning" titulo='Imprimir'>Imprimir</button>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">    		
      	<div id="div-tabla"></div>          	
	</div>
</div>        
<script>
var transaccion_seleccionada=0;
var elemento_seleccionado="";
$(document).ready(function () {

$("#imprimir").click(function(){
	var data=$("#div-tabla").html();
	imprime(data,"Corte de Caja");
});

$("body").on("change",".fechasdeposito",function(){
	var datoregistrado = $(this).val();
	var fecha = $(this).attr("data-fecha");
	var elemento =$(this);
	$(elemento).cargando("Almacenando... espere","left");
	$.post("./ventas/cortecaja/registra.php",{fecha:fecha,datoregistrado:datoregistrado},function(data){
		$(elemento).correcto(data);
	});
});


	$("[name='desde']").val("<?=date("d-m-Y");?>");
		 $('[name="desde"]').datepicker({
			 		todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true
			 });

		 $("[name='hasta']").val("<?=date("d-m-Y");?>");
		 $('[name="hasta"]').datepicker({
			 		todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true
			 });
 			 
	//condicion, refresh ultimos dos
	 var condicion_ = " and pagotransaccion.dFecha>="+fechahora(<?=date("d-m-Y");?>+" 00:00:00")+""; 
	 condicion_ += " and pagotransaccion.dFecha<="+fechahora(<?=date("d-m-Y");?>+" 23:59:59")+""; 	
	
	$('.fechasdeposito').datepicker({
					 		todayBtn: "linked",
		   	 		language: "es",   	 
			    	autoclose: true,
		    		todayHighlight: true
				});
$("#filtro_movimientos").click(function(){
	var elemento = $(this);
	var condicion="1=1";
	$(elemento).cargando("Generando reporte","bottom");
	var desde = $("[name='desde']").val();		
		var hasta = $("[name='hasta']").val();		
	var empleado = $("[name='iEmpleadoId']").val();
	if(desde==""){
		$(elemento).error("Seleccione fecha","right");
	} else {
		if(empleado!="0") { condicion += " and empleado.iId ='"+empleado+"'"; }
		if(desde!="" && hasta!="") { condicion += " and (pagotransaccion.dFecha>="+fechahora(desde+" 00:00:00")+" and pagotransaccion.dFecha<="+fechahora(hasta+" 23:59:00")+")"; }
		
		$.post("<?=$_tabla_general;?>",{condicion:condicion,fecha:fecha,empleado:empleado},function(data){
			$("#div-tabla").html(data);
			$(elemento).cerrar_cargando();
			

		});
	}

});


});
 
</script>
