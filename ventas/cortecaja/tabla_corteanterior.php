<style>
@media print {
  table {
    width:100%;
    border:1px solid #ccc;
    border-collapse: collapse;
  }
  td,th,tr{
    border:1px solid #ccc;
    border-collapse: collapse;
  }
  h3,h4,h5{
    padding:0px;
    margin:2px;
  }
}
</style>
<?

include("../../acceso/seguridad.php");
$t_total_="0.00";
$tabla = $_REQUEST['tabla'];
$condicion_ = stripslashes($_REQUEST['condicion']);
$condicion = "transaccion.iUsuarioEmpresaId='".$iUsuarioEmpresaId."'";
if($condicion_!="" &&$condicion_!="false")
  $condicion = $condicion_." and ".$condicion;

?>
<br>
<div>
  <h5>Corte de Caja del dia <span style="font-weight:bold; text-decoration:underline;"><?=$_REQUEST['fecha'];?></span></h5>
</div>
<div class="hide">
  <h5>Empleado: <span style="font-weight:bold; text-decoration:underline;"><?
  if($_REQUEST['empleado']=="0") echo "Todos";
  else {

    $datos = seleccionar("empleado","sNombre","iId='".$_REQUEST['empleado']."'",false,false, "iId");
    echo $datos[0]['sNombre'];
  }
  ?>
  </span></h5>
</div>
<table class="table table-bordered table-striped" id="<?=$tabla?>">

  <thead>
    <th style="text-align:left;">

        <h4>Forma de Pago</h4>

    </th>

  <th style="text-align:center; width:220px;">

        <h4>Total</h4>

    </th>

  </thead>

   <tbody>   

   <?


      $totales_al_dia = seleccionar("pagotransaccion inner join transaccion on pagotransaccion.iTransaccionId = transaccion.iId","pagotransaccion.iTipoPago,sum(pagotransaccion.fPago) as total",$condicion,false,false,"pagotransaccion.iTipoPago");
      foreach($totales_al_dia as $total_dia){
        $tipo_pago = $total_dia['iTipoPago'];
        //Acumular totales dependiendo tipo de pagos.
        if($tipo_pago == "1" || $tipo_pago=="7") { $t_efectivo_+= $total_dia['total']; }
        else if($tipo_pago == "2" || $tipo_pago == "3" || $tipo_pago == "10" || $tipo_pago =="13" || $tipo_pago =="15" ||$tipo_pago=="16") { $t_tarjetas_+= $total_dia['total']; }

        $total_general+= $total_dia['total'];
      } 
      
   ?>                             
    <tr>
    <td>
      <h4><i class="fa fa-money"></i> Efectivo</h4>
    </td> 
    

    <td align="center"><h4>$<?=number_format($t_efectivo_,2);?></h4></td>
    </tr>  
    <tr><td><h4><i class="fa fa-credit-card"></i> Tarjetas</h4></td> 
    <td align="center"><h4>$<?=number_format($t_tarjetas_,2);?></h4></td>
    </tr>

      
</tbody>
<tfoot>
<tr>
    <th style="text-align:right;">

      <h4>TOTALES</h4>

    </th>
    <th style="text-align:center;">
    <h4>$<?=number_format(limpia_formato($total_general),2);?></h4>
    </th>
</tr>
  </tfoot>
</table>

<script>
$(document).ready(function () {
  $("body").on("click",".detalles_t i,#t0",function(){
    var id_ = $(this).attr("id");
    var datos_ = $(this).attr("datos");
    $(this).informativo(datos_);
  }); 
  


});

</script>