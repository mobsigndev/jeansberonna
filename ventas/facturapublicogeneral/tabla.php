<?

include("../../acceso/seguridad.php");

$tabla = $_REQUEST['tabla'];

$condicion_ = stripslashes($_REQUEST['condicion']);

$condicion = "transaccion.iUsuarioEmpresaId='".$iUsuarioEmpresaId."'";



if($condicion_!="" &&$condicion_!="false")

	$condicion = $condicion_." and ".$condicion;



?>

<table class="table table-bordered table-striped" id="<?=$tabla?>">

  <thead>
  <th class="check-header">

              
    <label><input id="checkAll" name="checkAll" type="checkbox"><span></span></label>
    </th>
    <th>

        Tipo      

    </th>

    <th>

        Folio

    </th>

    <th class="hidden-xs">

        Fecha

    </th>      

   
    <th  class="hidden-xs">

    	Cliente

    </th>

    <th>

        Subtotal

    </th>

    <th>

       Imp. Trasladados

    </th>

    <th>

       Imp. Retenidos

    </th>

    <th>

       Descuento

    </th>

    <th>

     	 Total

    </th>

    

	</thead>

   <tbody>   

   <?



$t_subtotal_=0;
$t_total_ =0;
$t_descuento_=0;
$t_trasladados_=0;
$t_retenidos_=0;



$datos = seleccionar("transaccion INNER JOIN cliente ON transaccion.iClienteId = cliente.iId INNER JOIN empleado ON transaccion.iEmpleadoId = empleado.iId LEFT JOIN detallefactura ON detallefactura.iTransaccionId = transaccion.iId","transaccion.iEstatus,transaccion.iId,transaccion.iClienteId,transaccion.iEmpleadoId,transaccion.dFecha,transaccion.iEstatus,transaccion.iTipoTransaccion,transaccion.sComentarioTransaccion,transaccion.sComentarioDescuento,cliente.sNombre,empleado.sNombre as Empleado,transaccion.sSerie,transaccion.iFolio,detallefactura.sTUUID,iLigaMostrador",$condicion,false, false);                            

   foreach($datos as $renglones) {
      $t_retenidos=0;
      $t_trasladados=0;
      $impuestos = seleccionar("transaccionimpuesto","sum(fCantidad) as cantidad,iTipo","iTransaccionId='".$renglones['iId']."'",false,false,"iTipo");
      foreach($impuestos as $t_impuestos){
        if($t_impuestos['iTipo']=='0') $t_retenidos = $t_impuestos['cantidad'];
        if($t_impuestos['iTipo']=='1') $t_trasladados = $t_impuestos['cantidad'];

      }
   $totales_ = totalesTransaccion($renglones['iId']);  

   #Sumatoria para totales
    $t_subtotal_+=$totales_["subtotal"];
    $t_trasladados_+=($totales_["iva"]+$t_trasladados);
    $t_total_+= $totales_["total"];
    $t_descuento_ +=$totales_["descuento"];
    $t_retenidos_ +=$t_retenidos_;
   #Termina sumatoria

      $iLigaMostrador=$renglones['iLigaMostrador'];

 	   $iEstatus=$renglones['iEstatus'];	    

	   $subtotal=$totales_['subtotalf'];

	   $iva=$totales_['ivaf'];	   

     $t_trasladados = number_format((str_replace(",", "", $iva)+$t_trasladados),2);
     $t_retenidos = number_format($t_retenidos,2);

	   $total=$totales_['totalf'];	   

	   $descuento=$totales_['descuentof'];	 
	   
	   //$foliofiscal=$totales_['sTUUID'];

if($iLigaMostrador!="0" && $iLigaMostrador!=""){
    $check_='<small>Ya facturó</small>';
}
      else {
        $check_='<label><input type="checkbox" id="'.$renglones['iId'].'" name="seleccionado_venta" /><span></span></label>';
      }

       echo '<tr>';
       echo '<td class="check">'.$check_.'</td>';

        echo '<td>'.transaccion($renglones['iTipoTransaccion']).'</td>';

        echo '<td>'.$renglones['sSerie'].''.$renglones['iFolio'].'</td>';

        echo '<td>'.$renglones['dFecha'].'</td>';

        

        echo '<td>'.$renglones['sNombre'].'</td>';

        echo '<td>'.$subtotal.'</td>';

        echo '<td>'.$t_trasladados.'</td>';

        echo '<td>'.$t_retenidos.'</td>';

        echo '<td>'.$descuento.'</td>';                                                

        echo '<td>'.$total.'</td>'; 

       
       echo '</tr>';

   }

   ?>                             

</tbody>
<tfoot>
<tr>
    <th colspan="5">

      Totales

    </th>

    <th>

        <?=number_format($t_subtotal_,2);?>

    </th>

    <th>

      <?=number_format($t_trasladados_,2);?>

    </th>

    <th>

      <?=number_format($t_retenidos_,2);?>

    </th>

    <th>

       <?=number_format($t_descuento_,2);?>

    </th>

    <th>

       <?=number_format($t_total_,2);?>

    </th>

 
</tr>
  </tfoot>

</table>