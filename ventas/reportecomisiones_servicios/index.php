<?
include_once("./acceso/seguridad.php");
$_tabla_general = "./ventas/reportecomisiones/tabla.php";
?>
<div class="row">
        <div class="col-lg-2">
            <label>Desde fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Desde" name="desde" size="14" type="text">
		</div> 
         <div class="col-lg-2">
             <label>Hasta fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Hasta" name="hasta" size="14" type="text">
		</div> 
    <div class="col-lg-2">
      	<label>Empleado</label>
		<select class="form-control" name='iEmpleadoId' no_catalogo="no">
            <option value="0">Todos</option>    
            <?
            $select = seleccionar("empleado","iId,iCodigo,sNombre","iEstatus=0 and iUsuarioEmpresaId=$iUsuarioEmpresaId",false, false);
            foreach($select as $dato) {
                ?>
                <option value="<?=$dato['iId'];?>"><?=$dato['iCodigo']."-".$dato['sNombre'];?></option>
                <?
            }
            ?>
		</select>
  	</div>   
      <div class="col-lg-2">
      	<label>Estatus</label>
		<select class="form-control" name='iEstatus' no_catalogo="no">
            <option value="2">Todos</option>    
                <option value="0">Vigentes</option>
                <option value="1">Canceladas</option>                
		</select>
  	</div>   
	<div class="col-lg-4">
    </div>
</div>

<div class="row" style='margin-top:3px;'>
	<div class="col-lg-6">
    <button id='filtro_movimientos' type="button" class="btn btn-primary" titulo='Filtrar'>Aplicar Filtros en Reporte</button>
    </div>
	<? include("./clases/exportacion.php"); ?>

</div>
<div class="row">
    <div class="col-lg-12">    		
      	<div id="div-tabla"></div>    
	</div>
</div>        
<script>

$(document).ready(function () {

		 $("[name='desde']").val("<?=date("d-m-Y");?>");

		 $('[name="desde"]').datepicker({
	 		todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true
			 });

 		 $('[name="hasta"]').datepicker({
			todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true	 
		});

		 $("body").on("click",".enviar_correo",function(e){  
				var iTransaccionId = $(this).attr("href");
				$.post("./clases/formulario_correo.php",{iTransaccionId:iTransaccionId},function(data){
					vermodal("Envio de Archivos",data,"");
				});		
			e.preventDefault();
	    });

		 $("body").on("click",".enviar_devoluciones",function(e){  
				var iTransaccionId = $(this).attr("href");
				$.post("./ventas/reportecomisiones/devoluciones.php",{iTransaccionId:iTransaccionId},function(data){
					vermodal("Devoluciones",data,"");
				});		
				e.preventDefault();
	    });

		$("body").on("change","#tabla_devoluciones tr>td>input",function(e){  
				var iDetalleTransaccionId = $(this).attr("id");
				var elemento_ = $(this);
				var ValorNuevo = $(this).val();				
				var ValorViejo = $(this).attr("antes");								
				var dev = ValorViejo-ValorNuevo;
				if(ValorNuevo>ValorViejo) {
					$(elemento_).error("No puede agregar mas productos, genere una venta nueva");	
					$(elemento_).val(ValorViejo);
				}else {
					$(elemento_).confirmar("Seguro que desea realizar devolución de "+dev+" productos","Si","No",function(respuesta){
					if(respuesta) {
						$(elemento_).cargando("Generando devolución de producto");
							$.post("./ventas/reportecomisiones/genera_devolucion.php",{ValorViejo:ValorViejo,ValorNuevo:ValorNuevo,iDetalleTransaccionId:iDetalleTransaccionId},function(data){
							data = eval(data);
							if(data[1]>0) 
								$(elemento_).correcto(data[0]);
							else
								$(elemento_).error(data[0]);
							});	
					}else {
						$(elemento_).val(ValorViejo);
					}
					});
				}
				e.preventDefault();
	    });

			$("body").on("click","#enviar_archivo",function(e){  
				var elemento_ = $(this);
				var iTransaccionId = $(this).attr("iTransaccionId");
				var sAsunto = $("[name='sAsunto']").val();
				var sDestinatario=$("[name='sDestinatario']").val();
				var sComentario = $("[name='sComentario']").val();
				var sDocumento =  $(this).attr("sDocumento");
				var url = "./reporte_pdf.php?iId="+iTransaccionId;
				var pdf = $("[id='xml_']").val();
				var xml = $("[id='pdf_']").val();
				var iEmpresaId = $("[id='iEmpresaId']").val();
				if(sDestinatario==""){
					$(elemento_).error("Especifique destinatario")
				} else {
					$(elemento_).cargando("Enviando correo");
					$.post("./ventas/reportecomisiones/envio_archivos.php",{url:url,sAsunto:sAsunto,sDestinatario:sDestinatario,sComentario:sComentario,sDocumento:sDocumento,xml:xml,pdf:pdf,iTransaccionId:iTransaccionId,iEmpresaId:iEmpresaId},function(data){
					console.log(data);
					$(elemento_).correcto(data);
					//$(elemento_).cerrar_cargando();
					});		
				}
				e.preventDefault();
	    });
$("body").on("click",".reutilizar_transaccion",function(e){
	var elemento = $(this);
	$(elemento).cargando("Obteniendo datos","left");
	var itransaccion_ = $(elemento).attr("href");
	$.post("./ventas/venta/reutilizar_transaccion.php",{transaccion:itransaccion_},function(data){
		data = eval(data);
		var archivo = data[0];
		var cl = data[1];
		var tp = data[2];
		$(elemento).cerrar_cargando();
		window.location = "./index.php?m=ventas&s=venta&reutiliza="+archivo+"&cl="+cl+"&tp="+tp;
		
	});
	return false;

});
 	$("body").on("click","#imprimir_ticket",function(e){  
				var iTransaccionId = $(this).attr("id_transaccion");
				//var tipo = $(this).attr("tipo");				
				$.post("./ventas/reportecomisiones/ticket.php",{iId:iTransaccionId},function(data) {
					printIt(data);
				});	
			e.preventDefault();
	    });

$("body").on("click",".reenviar_correo",function(e){  
				var elemento_ = $(this);
				var iTransaccionId = $(this).attr("href");
				$(elemento_).cargando("Obteniendo datos");
				$.post("./ventas/reportecomisiones/datos_pedido_linea.php",{iPedidoId:iTransaccionId},function(data){						
						data = eval(data);
						var comentarios = data[0];
						var correo = data[1];
						var sNoCliente = data[2];
								
					
			


				var url="../../tiendaenlinea/reporte_pdf.php?iId="+iTransaccionId;
				var sAsunto="Pedido realizado en la Tienda en linea de Adminbox";
				var sDestinatario=correo;
				var sDocumento="tiendavirtual_"+iTransaccionId;
				var sComentario="Ha realizado un pedido desde la Tienda en linea de Adminbox. <br>Se adjunta copia de su pedido <br>Comentarios:"+comentarios;
				if(sNoCliente!=""){ sComentario+="<br><b>No.Cliente</b> "+sNoCliente; }							
				if(sDestinatario==""){
					$(elemento_).error("Especifique destinatario")
				} else {
					$(elemento_).cargando("Enviando correo");
					$.post("./ventas/reportecomisiones/envio_archivos.php",{url:url,sAsunto:sAsunto,sDocumento:sDocumento,sDestinatario:sDestinatario,sComentario:sComentario,iTransaccionId:iTransaccionId},function(data){
					console.log(data);
					$(elemento_).correcto(data);
					//$(elemento_).cerrar_cargando();
					});		
				}
				
				});
				//e.preventDefault();
				return false;
	    });


		$("body").on("click",".enviar_cancelar",function(e){  

			var elemento_ = $(this).closest("div");

			var id = $(this).attr("href");
			var tipo=$(this).attr("tipo");
			
	    	$(elemento_).confirmar("Seguro que desea cancelar registro","Si","No",function(respuesta){

				if(respuesta) {
					
					$(elemento_).cargando("Cancelando...");
					// SI es cotizacion
					if(tipo==3){
							$.post("./ventas/reportecomisiones/cancela_cotizacion.php",{id:id},function(data){
								data = eval(data);
								if(data[1]==1) {
									$(elemento_).closest( "tr" ).find('td:eq(9)').html(data[0]);
									$(elemento_).cerrar_cargando();							
								}else if(data[1]==0) {
									$(elemento_).error(data[0]);						
								}			
							});
					} else if(tipo==2){
						$(elemento_).cargando("Cancelando factura ante SAT...");
						$.post("./sellos/cancela.php",{iTransaccionId:id},function(data){
							data = eval(data);
							if(data[1]==1) {								
								$(elemento_).correcto(data[0]);							

									$.post("./ventas/reportecomisiones/cancela.php",{id:id},function(data){
										data = eval(data);
										if(data[1]==1) {
											$(elemento_).closest( "tr" ).find('td:eq(9)').html(data[0]);	
											$(elemento_).cerrar_cargando();													
										}else if(data[1]==0) {
											$(elemento_).error(data[0]);						
										}			
									});

							}else if(data[1]==0) {
								$(elemento_).error(data[0]);						
							}			
						});
					}
					else {
						$.post("./ventas/reportecomisiones/cancela.php",{id:id},function(data){
							data = eval(data);
							if(data[1]==1) {
								$(elemento_).closest( "tr" ).find('td:eq(9)').html(data[0]);	
								$(elemento_).cerrar_cargando();													
							}else if(data[1]==0) {
								$(elemento_).error(data[0]);						
							}			
						});
					}

				}							

			}); 
			e.preventDefault();
	    });

		$("body").on("click",".enviar_venta",function(e){  
			var elemento_ = $(this).closest("ul");
			var id = $(this).attr("href");
			var condicion="1=1";
	    	$(elemento_).confirmar("Seguro que desea enviar a venta","Si","No",function(respuesta){
				if(respuesta) {
					$(elemento_).cargando("Vendiendo Cotizacion");
					$.post("./ventas/reportecomisiones/vender.php",{id:id},function(data){
					data = eval(data);
					if(data[1]==1) {
						$(elemento_).closest( "ul" ).find('td:eq(5)').html(data[0]);		
						alert("Cotizacion vendida");			
						$("#div-tabla").crear_tabla_general("tabla_facturas","<?=$_tabla_general;?>",condicion,true);
					}else if(data[1]==0) {
						$(elemento_).error(data[0]);						
					}			
					});
				}							
			}); 
			e.preventDefault();
	    });
		
		$("body").on("click",".vendida",function(e){  
			var elemento_ = $(this).closest("ul");
			var id = $(this).attr("href");
			var condicion="1=1";
	    	$(elemento_).confirmar("Marcar pedido como vendido?","Si","No",function(respuesta){
				if(respuesta) {
					$(elemento_).cargando("Vendiendo pedido en linea");
					$.post("./ventas/reportecomisiones/vender.php",{id:id},function(data){
					data = eval(data);
					if(data[1]==1) {
						$(elemento_).closest( "ul" ).find('td:eq(5)').html(data[0]);		
						alert("Pedido vendido");			
						$("#div-tabla").crear_tabla_general("tabla_facturas","<?=$_tabla_general;?>",condicion,true);
					}else if(data[1]==0) {
						$(elemento_).error(data[0]);						
					}			
					});
				}							
			}); 
			e.preventDefault();
	    });
		var idpedido=0;
		$("body").on("click",".detalle_pedido_linea",function(e){  
			var elemento_ = $(this).closest("ul");
			idpedido = $(this).attr("href");
			var condicion="1=1";	    	
					$(elemento_).cargando("Cargando detalle...");
					$.post("./ventas/reportecomisiones/detalle_pedido_linea.php",{iPedidoId:idpedido},function(data){						
						$(elemento_).cerrar_cargando();	
						vermodal("Detalle de pedido en linea",data,"");					
					});
				
			e.preventDefault();
	    });

		$("body").on("click",".enviar_facturar",function(e){ 
		//alert("Enviando a facturar");
		var condicion="1=1"; 
		$("#div-tabla").crear_tabla_general("tabla_facturas","<?=$_tabla_general;?>",condicion,true);
		});

		$("body").on("click",".enviar_facturar_cotizacion",function(e){ 
		//alert("Enviando a facturar");
		var condicion="1=1"; 
		$("#div-tabla").crear_tabla_general("tabla_facturas","<?=$_tabla_general;?>",condicion,true);
	    });

		var tipo="default";
		var cliente_seleccionado="";
		var abre_=false;
		$("body").on("click","[name='sel-cliente']",function(e){	
				abre_=true;							
				cliente_seleccionado = $(this).attr("iid");
		});

		$('#ventana-emergente').on('hidden.bs.modal', function (e) {
			if(abre_){
			carga_cliente(cliente_seleccionado);
			abre_ = false;
			}else {
				var elemento = $("#filtro_movimientos");
				var condicion="1=1";
				$(elemento).cargando("Generando reporte","bottom");
				var empleado = $("[name='iEmpleadoId']").val();
				var desde = $("[name='desde']").val();	
				var hasta = $("[name='hasta']").val();	
				var icliente = $("#iCliente").attr("iClienteId");
				var tipo_transaccion= $("#tipo_transaccion_").attr("tipo");	
				var iestatus = $("[name='iEstatus']").val();
				if(iestatus!="2") { condicion += " and transaccion.iEstatus ='"+iestatus+"'"; }
				if(icliente!="0") { condicion += " and transaccion.iClienteId ='"+icliente+"'"; }
				if(tipo_transaccion!="0") { condicion += " and transaccion.iTipoTransaccion ='"+tipo_transaccion+"'"; }	
				if(empleado!="0") { condicion += " and empleado.iId ='"+empleado+"'"; }
				if(desde!="") { condicion += " and transaccion.dFecha>="+fechahora(desde+" 00:00:00")+""; }
				if(hasta!="")	{ condicion += " and transaccion.dFecha<="+fechahora(hasta+" 23:59:59")+""; }	
				$("#div-tabla").crear_tabla_general("tabla_facturas","<?=$_tabla_general;?>",condicion,true,function(){
					$(elemento).cerrar_cargando();
				});	
			}
		});

		$("body").on("click","#seleccionar-cliente",function(e){
			tipo="default";
			var elemento = $(this);
			$(elemento).cargando("Cargando");
			var titulo="Selecciona Cliente"
			var form="./general/clientes/lista_clientes.php";
			var tabla2="usuario";
	    	var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";            			
			$.post(form,null,function(data){
				$(elemento).cerrar_cargando();				
		    	vermodal(titulo,data,botones);		    	
			});
			e.preventDefault();
		});


	$("body").on("click","#seleccionar-cliente-interior",function(e){
		    tipo="interior";
			var elemento = $(this);
			$(elemento).cargando("Cargando");
			var titulo="Selecciona Cliente"
			var form="./general/clientes/lista_clientes.php";
			var tabla="usuario";
	    	var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";   

			$.post(form,null,function(data){
								
		    	vermodal(titulo,data,botones,function(){		    		
		    		
		    		$(elemento).cerrar_cargando();
		    	});
		    	
			});
			e.preventDefault();
		}); 

	//condicion, refresh ultimos dos
	 var condicion_ = " transaccion.dFecha>="+fechahora("<?=date("d-m-Y");?> 00:00:00")+""; 
	 condicion_ += " and transaccion.dFecha<="+fechahora("<?=date("d-m-Y");?> 23:59:59")+""; 	
	 
	$("#div-tabla").crear_tabla_general("tabla_facturas","<?=$_tabla_general;?>",condicion_,false);
	


	function carga_cliente(iClienteId){
		
			$.post( "./ventas/venta/carga_cliente.php", { iClienteId: iClienteId}, function( data ) {
				data = eval(data);		
				if(tipo=="default"){						
					$("#iCliente").html(data[1]);
					$("#iCliente").attr("iClienteId",data[0]);		
				}	else
				{
				
					//$(elemento_).cargando("Cargando detalle...");
					$.post("./ventas/reportecomisiones/detalle_pedido_linea.php",{iPedidoId:idpedido,iClienteId:data[0],Cliente:data[1]},function(data4){						
					//	$(elemento_).cerrar_cargando();	
						vermodal("Detalle de pedido en linea",data4,"");					
					});
					
				}
			});
			
	}

$("#filtro_movimientos").click(function(){
	var elemento = $(this);
	var condicion="1=1";
	$(elemento).cargando("Generando reporte","bottom");
	var empleado = $("[name='iEmpleadoId']").val();
	var desde = $("[name='desde']").val();	
	var hasta = $("[name='hasta']").val();	
	var icliente = $("#iCliente").attr("iClienteId");
	var tipo_transaccion= $("#tipo_transaccion_").attr("tipo");	
	var iestatus = $("[name='iEstatus']").val();
	if(iestatus!="2") { condicion += " and transaccion.iEstatus ='"+iestatus+"'"; }
	if(icliente!="0") { condicion += " and transaccion.iClienteId ='"+icliente+"'"; }
	if(tipo_transaccion!="0") { condicion += " and transaccion.iTipoTransaccion ='"+tipo_transaccion+"'"; }	
	if(empleado!="0") { condicion += " and empleado.iId ='"+empleado+"'"; }
	if(desde!="") { condicion += " and transaccion.dFecha>="+fechahora(desde+" 00:00:00")+""; }
	if(hasta!="")	{ condicion += " and transaccion.dFecha<="+fechahora(hasta+" 23:59:59")+""; }	
	$("#div-tabla").crear_tabla_general("tabla_facturas","<?=$_tabla_general;?>",condicion,true,function(){		
		$(elemento).cerrar_cargando();
	});	

});

});
</script>