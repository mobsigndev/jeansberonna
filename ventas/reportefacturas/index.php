<?
#if(!isset($_REQUEST['prueba']))
#die("Modulo en Actualización");
include_once("./acceso/seguridad.php");
$_tabla_general = "./ventas/reportefacturas/tabla.php";
?>
<div class="row">
  
        <div class="col-lg-2">
            <label>Desde fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Desde" name="desde" size="14" type="text">
		</div> 
         <div class="col-lg-2">
             <label>Hasta fecha</label>
             <input class="form-control" data-date-format="dd-mm-yyyy" placeholder="Hasta" name="hasta" size="14" type="text">
		</div> 
        
        <div class="col-lg-2">
      	<label>Empleado</label>
		<select class="form-control" name='iEmpleadoId' no_catalogo="no">
            <option value="0">Todos</option>    
            <?
            $select = seleccionar("empleado","iId,iCodigo,sNombre","iEstatus=0 and iUsuarioEmpresaId=$iUsuarioEmpresaId",false, false);
            foreach($select as $dato) {
                ?>
                <option value="<?=$dato['iId'];?>"><?=$dato['iCodigo']."-".$dato['sNombre'];?></option>
                <?
            }
            ?>
		</select>
  	</div>  
        
      <div class="col-lg-2">
      	<label>Estatus</label>
		<select class="form-control" name='iEstatus' no_catalogo="no">
            <option value="3">Todos</option>    
                <option value="0">Pendiente de Pago</option>
                <option value="2">Pagadas</option>
                <option value="1">Canceladas</option>                
		</select>
  	</div>    
  	  <div class="col-lg-2">
      	<label>Forma de Pago</label>
		<select class="form-control" name='iTipoPago' no_catalogo="no">
            <option value="0">Todos</option>    
                <option value="1">Efectivo</option>
                <option value="2">Tarjeta Crédito</option>
                <option value="3">Tarjeta Débito</option>                
                <option value="4">Cheque</option>                
                <option value="5">Transferencia</option>                
                <option value="6">No Identificado</option>                
		</select>
  	</div>   
	<div class="col-lg-2">
    
    </div>
</div>

<div class="row" style='margin-top:3px;'>

	<div class="col-lg-6">
    <button id='filtro_movimientos' type="button" class="btn btn-primary" titulo='Filtrar'>Aplicar Filtros en Reporte</button>
    <input type="checkbox" style="display:inline;" id="mostrarConceptos" checked="checked"><label>Mostrar Columna de conceptos en ésta tabla</label>
    </div>
	<? include("./clases/exportacion.php"); ?>

</div>

<div class="row">
    <div class="col-lg-12">    		
      	<div id="div-tabla"></div>    
	</div>
</div>        
<script>
var transaccion_seleccionada=0;
var elemento_seleccionado="";
$(document).ready(function () {
	$("[name='desde']").val("<?=date("d-m-Y");?>");
		 $('[name="desde"]').datepicker({
			 		todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true
			 });
 		 $('[name="hasta"]').datepicker({
					todayBtn: "linked",
   	 		language: "es",   	 
	    	autoclose: true,
    		todayHighlight: true	 
		});
		 
		 function conceptos_(){
		 	 	if($("#mostrarConceptos").is(':checked')) {
			 		$(".mostrarConceptos").show();
			 	}else {
			 		$(".mostrarConceptos").hide();
			 	}
		 }
		$("body").on("click","#mostrarConceptos",function(e){  
			conceptos_();
	    });
		
		 $("body").on("click",".enviar_correo",function(e){  
				var iTransaccionId = $(this).attr("href");
				$.post("./clases/formulario_correo.php",{iTransaccionId:iTransaccionId},function(data){
					vermodal("Envio de Archivos",data,"");
				});		
			e.preventDefault();
	    });
		
		 $("body").on("click",".cambiar_estado",function(f){ 
		 		var elemento_ = $(this).closest("ul"); 

				var iTransaccionId = $(this).attr("href");
				transaccion_seleccionada = iTransaccionId;
				elemento_seleccionado = elemento_;
				var tipo = $(this).attr("tipo");
				$.post("./ventas/reportefacturas/estado.php",{iTransaccionId:iTransaccionId,tipo:tipo},function(data){
					vermodal("Estado de la factura",data,"");
				});		
			f.preventDefault();
	    });






			 $("body").on("click","#enviar_archivo",function(e){  
			 	var elemento_ = $(this);
				var iTransaccionId = $(this).attr("iTransaccionId");
				var sAsunto = $("[name='sAsunto']").val();
				var sDestinatario=$("[name='sDestinatario']").val();
				var sComentario = $("[name='sComentario']").val();
				var sDocumento =  $(this).attr("sDocumento");
				var url = "./reporte_pdf.php?iId="+iTransaccionId;
				var pdf = $("[id='xml_']").val();
				var xml = $("[id='pdf_']").val();
				var iEmpresaId = $("[id='iEmpresaId']").val();
				if(sDestinatario==""){
					$(elemento_).error("Especifique destinatario")
				} else {
					$(elemento_).cargando("Enviando correo");
					$.post("./ventas/reportefacturas/envio_archivos.php",{url:url,sAsunto:sAsunto,sDestinatario:sDestinatario,sComentario:sComentario,sDocumento:sDocumento,xml:xml,pdf:pdf,iTransaccionId:iTransaccionId,iEmpresaId:iEmpresaId},function(data){
						console.log(data);
						$(elemento_).correcto(data);
						//$(elemento_).cerrar_cargando();
					});		
				}
			e.preventDefault();
				
	    });
		
		$("body").on("click","[name='sel-cliente']",function(e){			
			carga_cliente($(this).attr("iid"));
		});
			 
		$("body").on("click","#seleccionar-cliente",function(e){
			var elemento = $(this);
			$(elemento).cargando("Cargando");
			var titulo="Selecciona Cliente"
			var form="./general/clientes/lista_clientes.php";
			var tabla="usuario"	
	    	var botones="<button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>";            			
			$.post(form,null,function(data){
				$(elemento).cerrar_cargando();				
		    	vermodal(titulo,data,botones);
			});
			e.preventDefault();
		});
	

	//condicion, refresh ultimos dos
	 var condicion_ = " transaccion.dFecha>="+fechahora("<?=date("d-m-Y");?> 00:00:00")+""; 
	 condicion_ += " and transaccion.dFecha<="+fechahora("<?=date("d-m-Y");?> 23:59:59")+""; 	
	 
	$("#div-tabla").crear_tabla_general("tabla_facturas","<?=$_tabla_general;?>",condicion_,false);

	function carga_cliente(iClienteId){
			$.post( "./ventas/venta/carga_cliente.php", { iClienteId: iClienteId}, function( data ) {
				data = eval(data);								
				$("#iCliente").html(data[1]);
				$("#iCliente").attr("iClienteId",data[0]);			
			});
	}

$("#filtro_movimientos").click(function(){
	var elemento = $(this);
	var condicion="1=1";
	$(elemento).cargando("Generando reporte","bottom");
	var desde = $("[name='desde']").val();	
	var hasta = $("[name='hasta']").val();	
	var icliente = $("#iCliente").attr("iClienteId");
	var tipo_transaccion= $("#tipo_transaccion_").attr("tipo");		
	var iestatus = $("[name='iEstatus']").val();
	var empleado = $("[name='iEmpleadoId']").val();
	var tipopago = $("[name='iTipoPago']").val();
	if(iestatus!="3") { 
		if(iestatus=="0" || iestatus=="2") { condicion += " and pagotransaccion.iEstatus ='"+iestatus+"' and transaccion.iEstatus='0'"; }
		if(iestatus=="1") { condicion += " and transaccion.iEstatus ='1'"; }
	}
 	if(icliente!="0") { condicion += " and transaccion.iClienteId ='"+icliente+"'"; }
 	if(tipopago!="0") { condicion += " and transaccion.iTipoPago ='"+tipopago+"'"; }
	if(tipo_transaccion!="0") { condicion += " and transaccion.iTipoTransaccion ='"+tipo_transaccion+"'"; }	else {
		condicion+=" and (transaccion.iTipoTransaccion=2 or transaccion.iTipoTransaccion=1)"
	}
	if(empleado!="0") { condicion += " and empleado.iId ='"+empleado+"'"; }
	if(desde!="") { condicion += " and transaccion.dFecha>="+fechahora(desde+" 00:00:00")+""; }
	if(hasta!="")	{ condicion += " and transaccion.dFecha<="+fechahora(hasta+" 23:59:59")+""; }	
	condicion += " and pagotransaccion.iEstatus<>1";
	$("#div-tabla").crear_tabla_general("tabla_facturas","<?=$_tabla_general;?>",condicion,true,function(){
		$(elemento).cerrar_cargando();
		conceptos_();
	});	
});
$('#ventana-emergente').on('hidden.bs.modal', function (e) {
	$(elemento_seleccionado).closest( "tr" ).find('td:eq(5)').html(labelcargando());
	$(elemento_seleccionado).closest( "tr" ).find('td:eq(6)').html(labelcargando());
	$(elemento_seleccionado).closest( "tr" ).find('td:eq(8)').html(labelcargando());
	$("#pagados_totales").html(labelcargando());
	$("#pendientes_totales").html(labelcargando());
	$.post( "./ventas/reportefacturas/abonos_total.php", { iTransaccionId:transaccion_seleccionada}, function( data ) {
		data = eval(data);								
		$(elemento_seleccionado).closest( "tr" ).find('td:eq(5)').html(data[0]);
		$(elemento_seleccionado).closest( "tr" ).find('td:eq(6)').html(data[1]);
		$(elemento_seleccionado).closest( "tr" ).find('td:eq(8)').html(data[2]);
		actualiza_totales();
		
	});
});

function actualiza_totales(){
	var t_pagados__ =0;
	var t_pendientes__ =0;
	$('.t_pagados').each(function() {
		t_pagados__+=limpia_formato($(this).html());
	});
	$('.t_pendientes').each(function() {
		t_pendientes__+=limpia_formato($(this).html());
	});
		$("#pagados_totales").html(number_format(t_pagados__,2));
		$("#pendientes_totales").html(number_format(t_pendientes__,2));
}

});
 
</script>
