<?

include("../../acceso/seguridad.php");

$tabla = $_REQUEST['tabla'];

$condicion_ = stripslashes($_REQUEST['condicion']);

$condicion = "transaccion.iUsuarioEmpresaId='".$iUsuarioEmpresaId."'";

if($condicion_!="" &&$condicion_!="false")

	$condicion = $condicion_." and ".$condicion;

?>

<table class="table table-bordered table-striped" id="<?=$tabla?>">

  <thead>
    <th>

        Tipo

    </th>

    <th>

        Folio

    </th>

    <th class="hidden-xs">

        Fecha

    </th>      

   
    <th  class="hidden-xs">

    	Cliente

    </th>
   

    <th>

     	 Total

    </th>

   
    
    <th class="hidden-xs">

    Pagado

    </th>
   <th class="hidden-xs">

      Pendiente

    </th>
     <th class="mostrarConceptos">

     Concepto 

    </th>

    <th>

      Estatus 

    </th>

    <th>

      Fecha de Pago 

    </th>
    <th>

      Forma de Pago 

    </th>

	<th no_visible="no" width="100">

    	

    </th>

	</thead>

   <tbody>   

   <?
$t_subtotal_=0;
$t_total_ =0;
$t_descuento_=0;
$t_trasladados_=0;
$t_retenidos_=0;
$datos = seleccionar("transaccion INNER JOIN cliente ON transaccion.iClienteId = cliente.iId INNER JOIN empleado ON transaccion.iEmpleadoId = empleado.iId LEFT JOIN detallefactura ON detallefactura.iTransaccionId = transaccion.iId LEFT JOIN pagotransaccion ON pagotransaccion.iTransaccionId = transaccion.iId
","transaccion.iTipoPago,sum(pagotransaccion.fPago) as pago,transaccion.iId,transaccion.iClienteId,transaccion.iEmpleadoId,transaccion.sSerie,transaccion.dFecha,transaccion.iEstatus,transaccion.iTipoTransaccion,transaccion.sComentarioTransaccion,transaccion.sComentarioDescuento,cliente.sNombre,empleado.sNombre as Empleado,transaccion.iFolio,detallefactura.sTUUID,pagotransaccion.iEstatus as estado,max(pagotransaccion.dFecha) as FechaPago",$condicion,false,false, "transaccion.iId");
#echo "select transaccion.iId,transaccion.iClienteId,transaccion.iEmpleadoId,transaccion.dFecha,transaccion.iEstatus,transaccion.iTipoTransaccion,transaccion.sComentarioTransaccion,transaccion.sComentarioDescuento,cliente.sNombre,empleado.sNombre as Empleado,transaccion.iFolio,detallefactura.sTUUID,pagotransaccion.iEstatus as estado from transaccion INNER JOIN cliente ON transaccion.iClienteId = cliente.iId INNER JOIN empleado ON transaccion.iEmpleadoId = empleado.iId LEFT JOIN detallefactura ON detallefactura.iTransaccionId = transaccion.iId LEFT JOIN pagotransaccion ON pagotransaccion.iTransaccionId = transaccion.iId where $condicion";                            
  $c=0;
   foreach($datos as $renglones) {
    $c++;
      $t_retenidos=0;
      $t_trasladados=0;
      $impuestos = seleccionar("transaccionimpuesto","sum(fCantidad) as cantidad,iTipo","iTransaccionId='".$renglones['iId']."'",false,false,"iTipo");
      foreach($impuestos as $t_impuestos){
        if($t_impuestos['iTipo']=='0') $t_retenidos = $t_impuestos['cantidad'];
        if($t_impuestos['iTipo']=='1') $t_trasladados = $t_impuestos['cantidad'];

      }
    $totales_ = totalesTransaccion($renglones['iId']);  



    $datos22 = seleccionar("detalletransaccion","iProductoId,iTipo,sConcepto,fCantidad,fPrecio,fIva,fDescuento,sUnidad","iTransaccionId='".$renglones['iId']."'",false, false);                            
  $c=0;
  $conceptos="";
   $detalle_productos ="<table style='font-size:12px;'><tr><td>-</td><td><strong>Productos</strong></td></tr>";
   foreach($datos22 as $renglones2) {
    $c++;
    $conceptos .= $c."- ".ucfirst(strtolower(strip_tags($renglones2['sConcepto'])))."<br>";
    $detalle_productos .="<tr><td style='vertical-align:top;'>-</td><td>".ucfirst(strtolower(strip_tags(str_replace('"',"'",$renglones2['sConcepto']))))."</td></tr>";
    }
    $detalle_productos .="</table>";
    if(strlen($conceptos)>100) { $conceptos = substr($conceptos, 0, 100).'... <i class="fa fa-search-plus" style="cursor:pointer;" title="Informativo" datos="'.$detalle_productos.'"></i>'; }

    #1-efectivo, 2- tc, 3-tdebito, 4-cheque, 5-transfe, 6-no identificado
    $tipo_pago = $renglones['iTipoPago'];
    if($tipo_pago == "1") { $FormaPago = "Efectivo"; }
    else if($tipo_pago == "2") { $FormaPago = "Tar. Crédito"; }
    else if($tipo_pago == "3") { $FormaPago = "Tar. Débito"; }
    else if($tipo_pago == "4") { $FormaPago = "Cheque"; }
    else if($tipo_pago == "5") { $FormaPago = "Transferencia"; }
    else if($tipo_pago == "6") { $FormaPago = "No Identificado"; }
   

      else if($tipo_pago=="01") $FormaPago = "01 Efectivo";
      else if($tipo_pago=="02") $FormaPago = "02 Cheque nominativo";
      else if($tipo_pago=="03") $FormaPago = "03 Transferencia de fondos";
      else if($tipo_pago=="04") $FormaPago = "04 Tarjeta de credito";
      else if($tipo_pago=="05") $FormaPago = "05 Monedero electronico";
      else if($tipo_pago=="06") $FormaPago = "06 Dinero electronico";
      else if($tipo_pago=="07") $FormaPago = "07 Tarjetas digitales";
      else if($tipo_pago=="08") $FormaPago = "08 Vales de despensa";
      else if($tipo_pago=="28") $FormaPago = "28 Tarjeta de debito";      
      else if($tipo_pago=="29") $FormaPago = "29 Tarjeta de servicios";  
      else $FormaPago="99 Otros";

   #Sumatoria para totales
    $t_subtotal_+=$totales_["subtotal"];
    $t_trasladados_+=($totales_["iva"]+$t_trasladados);
    $t_total_+= $totales_["total"];
    $t_descuento_ +=$totales_["descuento"];
    $t_retenidos_ +=$t_retenidos_;
   
        $detallet = "<table style='width:300px;'>";
        $detallet .= "<tr><td style='font-weight:bold;'>Subtotal</td><td>$".number_format($t_subtotal_,2)."</td></tr>";

        $detallet .= "<tr><td style='font-weight:bold;'>Imp. Trasladados</td><td>$".number_format($t_trasladados_,2)."</td></tr>";

        $detallet .= "<tr><td style='font-weight:bold;'>Imp. Retenidos</td><td>$".number_format($t_retenidos_,2)."</td></tr>";

        $detallet .= "<tr><td style='font-weight:bold;'>Descuentos</td><td>$".number_format($t_descuento_,2)."</td></tr>"; 
        $detallet .= '</table>';

   #Termina sumatoria

 	   $iEstatus=$renglones['iEstatus'];
	   $estado=$renglones['estado'];	    
     $pagado =$renglones['pago'];

     
     if($estado=="2" )$FechaPago = $renglones['FechaPago'];
     else $FechaPago = "-";

    if($iEstatus=="1"){
      $pagado = limpia_formato($pagado);
      $FechaPago = "-";
     }

    


 

	   $subtotal=$totales_['subtotalf'];

	   $iva=$totales_['ivaf'];	   

     $t_trasladados = number_format((str_replace(",", "", $iva)+$t_trasladados),2);
     $t_retenidos = number_format($t_retenidos,2);

	   $total=$totales_['totalf'];	   

	   $descuento=$totales_['descuentof'];	 

    #Si es pagado poner pendiente a 0, por las transacciones realizadas antes de este modulo
     
     if($estado=="2" && $iEstatus!="1") {
        $pendiente = number_format("0",2);
        $pagado = $total;
     } 
     else {
        $pendiente = limpia_formato($total)-limpia_formato($pagado);
        if($iEstatus=="1"){
          $pendiente=0;
        }

        $t_pendiente_ +=$pendiente;
        $pendiente = number_format($pendiente,2);
	   }


	   //$foliofiscal=$totales_['sTUUID'];

        echo '<tr>';

        echo '<td>'.transaccion($renglones['iTipoTransaccion']).'</td>';

        echo '<td>'.$renglones['sSerie']."".$renglones['iFolio'].'</td>';

        echo '<td>'.$renglones['dFecha'].'</td>';

        echo '<td>'.$renglones['sNombre'].'</td>';


        $detalle = "<table style='width:300px;'>";
        $detalle .= "<tr><td style='font-weight:bold;'>Subtotal</td><td>$".$subtotal."</td></tr>";

        $detalle .= "<tr><td style='font-weight:bold;'>Imp. Trasladados</td><td>$".$t_trasladados."</td></tr>";

        $detalle .= "<tr><td style='font-weight:bold;'>Imp. Retenidos</td><td>$".$t_retenidos."</td></tr>";

        $detalle .= "<tr><td style='font-weight:bold;'>Descuentos</td><td>$".$descuento."</td></tr>"; 
        $detalle .= '</table>';

        echo '<td class="detalles_t" width="90"><i class="fa fa-search-plus" id="'.$c.'" style="cursor:pointer;" title="Informativo" datos="'.$detalle.'"></i> '.$total.'</td>'; 
        if($iEstatus=="1"){ $pagado = number_format("0",2); }
        echo '<td class="t_pagados">'.$pagado.'</td>';
        
        echo '<td class="t_pendientes">'.$pendiente.'</td>';

        echo '<td class="mostrarConceptos"><small>'.$conceptos.'</small></td>';

        if($iEstatus=="1"){
          echo '<td>'.label($iEstatus,"avanzado").'</td>'; 
        } else {
          echo '<td>'.label($estado,"estado_factura").'</td>'; 
        }		

        echo '<td>'.$FechaPago.'</td>';
        if($renglones['sTUUID']=="") { 
            if($renglones['iTipoTransaccion']=="2") $folio_fiscal = "Sin Timbrar";
            if($renglones['iTipoTransaccion']=="1") $folio_fiscal = "<center>-</center>";
        }
		//echo '<td>'.$folio_fiscal.'</td>';
		  echo '<td>'.$FormaPago.'</td>';
		echo '<td>';
 $t_pagado_ +=limpia_formato($pagado);
		 ?>

         <div class="btn-group" style=''>

                  <button type="button" class="btn btn-info btn-sm">Acciones</button>

                  <button type="button" class="btn btn-info dropdown-toggle btn-sm" data-toggle="dropdown">

                    <span class="caret"></span>

                    <span class="sr-only"></span>

                  </button>

                  <ul class="dropdown-menu" role="menu" id='accion_seleccionada'>                   

                
                   <li><a href="<?=$renglones['iId'];?>" class='cambiar_estado' tipo="1">Abonar</a></li>
                   <li><a href="<?=$renglones['iId'];?>" class='cambiar_estado' tipo="2">Liquidar</a></li>
                   <li><a href="<?=$renglones['iId'];?>" class='cambiar_estado' tipo="3">Historial de Pago</a></li>
                  <li class="divider"></li>
                  <li><a href="./ventas/reportefacturas/recibo.php?iId=<?=$renglones['iId'];?>" target="_blank">Pagaré</a></li>
                    <!--<?php if($iEstatus=="0")  { ?>
                      <li class="divider"></li>
                      <li><a href="<?=$renglones['iId'];?>" tipo="<?=$renglones['iTipoTransaccion'];?>" class='cancelar_factura'>Cancelar Factura</a></li>
                    <?php } ?>-->

                   
                    <li class="divider"></li>
                    <li><a href="./ventas/reportefacturas/reporte.php?iId=<?=$renglones['iId'];?>" target="_blank">Previa</a></li>                    
                    				
				  </ul>

          </div>

          <?		

		echo '</td>';                                               		

       echo '</tr>';

   }

   ?>                             

</tbody>
<tfoot>
<tr>
    <th colspan="4">

      Totales

    </th>
   

    <th>
        <i class="fa fa-search-plus" id="t0" style="cursor:pointer;" title="Informativo" datos="<?=$detallet;?>"></i> 
       <?=number_format($t_total_,2);?>

    </th>
  <th id="pagados_totales">

        <?=number_format($t_pagado_,2);?>

    </th>
   
    <th id="pendientes_totales">      <?=number_format($t_pendiente_,2);?> 
    </th>
       <th>      
    </th>
    <th>      
    </th>
    <th class="mostrarConceptos">      
    </th>
    <th>      
    </th>
  <th no_visible="no">

      

    </th>
</tr>
  </tfoot>
</table>

<script>
$(document).ready(function () {
  $("body").on("click",".detalles_t i,#t0,.mostrarConceptos i",function(){
    var id_ = $(this).attr("id");
    var datos_ = $(this).attr("datos");
    $(this).informativo(datos_);
  }); 
  
//$(".enviar_facturar").click(function(){
	/*
	var href = $(this).attr("href");
	var id = $(this).attr("id");
	
	window.location = 'index.php';
	return false;
	
	*/
	
	/*
$('#myModal').on('show', function() {
    var id = $(this).data('id'),
        removeBtn = $(this).find('.danger');
})

$('.confirm-delete').on('click', function(e) {
    e.preventDefault();

    var id = $(this).data('id');
    $('#myModal').data('id', id).modal('show');
});

$('#btnYes').click(function() {
    // handle deletion here
  	var id = $('#myModal').data('id');
  	$('[data-id='+id+']').remove();
  	$('#myModal').modal('hide');
});
	
//});

//return false;
*/

});

</script>