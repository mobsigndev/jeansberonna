<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
if(file_exists("../acceso/seguridad.php")){
include("../acceso/seguridad.php");
$p = "..";
}
if(file_exists("../../acceso/seguridad.php")){
include("../../acceso/seguridad.php");
$p = "../..";
}

if(file_exists("./acceso/seguridad.php")){
include("./acceso/seguridad.php");
$p = ".";
}
$id=$_REQUEST['iId'];

if(permiso("Tintoreria",$iUsuarioEmpresaId)){
  $tintoreria=true;
  $renglones=conceptosTransaccion((int)$id,false,false,true);
 }
 else {
  $tintoreria=false;
  $renglones=conceptosTransaccion((int)$id);
 }
$operaciones=totalesTransaccion((int)$id);
$datos=datos_transaccion((int)$id);
if($datos["iTipoTransaccion"]=="1"){
 $sTipoTransaccion= "Nota de Venta";
}
else if($datos['iTipoTransaccion'] == "2" ) {
 $sTipoTransaccion="Factura";
}
else if($datos['iTipoTransaccion'] == "3" ) {
 $sTipoTransaccion="Cotización";
}
if($datos["iEstatus"]==1)
	$estatus = "<div><label style='color:red;'>Cancelado</label></div>";
else 
	$estatus = "";



?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" type="text/css" href="../../css/estilo_cotizacion.css" />
</head>
<style> 
	@page { margin: 0px 20px 0px 20px; color:#666;}
    #header {  text-align: center; border-bottom:2px solid #ccc;}
    #footer { position: fixed; left: 0px; bottom: 0px; right: 0px; height: 80px; width:785px; background:#ccc; font-size:10px;color:#666;}
	@media print {
  html, body {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
	color:#666;
  }
  #footer {
    position: absolute;
    bottom: 0;
	color:#666;
  }
}
</style>

<div style='font-family:Verdana, Geneva, sans-serif; color:#666;'>
	
    <!--ENCABEZADO -->
    <div style='width:780px; border-top:1px solid #fff;'>
    	
        <div style='width:220px; background-color:#fff; display:inline-block; border-right:2px solid #CCC;'>
           
            <?php if($datos["sLogotipo"]!=""){
				if(file_exists("$p/logotipos/".$datos["sLogotipo"])) { 
					              
                	list($anchura, $altura, $type, $attr) = getimagesize("$p/logotipos/".$datos["sLogotipo"]);
					
					 if($anchura>$altura) {
						
						#Mas ancho       
						if(($altura*3.33)>$anchura) { $yy=60; $xx="auto";}
						else { 
							$xx=200;
				
						}
					}
					else {
					   
					   if(($anchura/3.33)>$altura) { $xx=200; $yy="auto"; }
					   else { $yy=90; } 
					}
						
				  ?>

                   <img src="<?=$p;?>/logotipos/<?=$datos["sLogotipo"];?>" width="<?=$xx?>" height="<?=$yy?>" alt="Logotipo" />
				<? }
			}
        ?>       
        </div>
         
        <div style="width:360px; height:106px; display:inline-block; text-align:left;">
            <div style='margin-left:20px; margin-top:15px; font-size:24px; color:#444; vertical-align:top;'>
            <?php echo $sTipoTransaccion; ?>
			</div>
            <div style='margin-left:20px; margin-top:8px; font-size:13px; color:#444;vertical-align:top;'>
            De: <?php echo $datos["sRazonSocial"]; ?>
			</div>
           
            <div style='margin-left:20px; margin-top:8px; font-size:13px; color:#444;vertical-align:top;'>
            Para: <? echo $datos["sNombre"]; ?>
            </div>

           <? if($tintoreria) { ?>
            <div style='margin-left:20px; margin-top:8px; font-size:13px; color:red;vertical-align:top;'>
            <strong>Fecha de Entrega: <? echo $datos["dtFechaEntrega"]; ?></strong>
            </div>
            <? } ?>
            
        </div>
        
		<div style="width:180px; height:110px; border:2px solid #ccc; display:inline-block;">
         	<div style='margin-top:10px;margin-left:10px;padding-bottom:1px;color:#444;font-size:19px;text-align: left;'>
            <strong>Folio:</strong><? echo  $datos["sSerie"].$datos["iFolio"]; ?>
            </div>
            <div style='margin-left:10px; margin-top:8px; font-size:12px; color:#444;vertical-align:top; text-align:left;'>
            <strong>Fecha:</strong> <? echo $datos["dFecha"]; ?>
            </div>
            <div style='margin-left:10px; margin-top:8px; font-size:12px; color:#444;vertical-align:top; text-align:left;'>
            <strong>Lugar:</strong> <? echo $datos["sCiudad"].", ".$datos["sEstado"].", ".$datos["sPais"]; ?>
            </div>
		</div>
        <?= $estatus; ?>
    </div>
    <!--ENCABEZADO#-->
    

    
    <!--CUERPO DE REPORTE -->
    <div style="width:780px;padding-bottom:30px;background-color:#fff; border-top:2px solid #CCC; margin-top:5px;">    	
      <h4 style='width:780px; margin-top:10px;	margin-left:10px;	padding-bottom:1px;	background-color:#;	font-size:15px;text-align:justify;	color:#333;'><?php echo $datos["sComentarioTransaccion"]; ?></h4>                        
     
      <table border="1" style='border:1px solid #999; width:780px; font-size:11px; border-collapse:collapse;color:#666;'>
          <tr> 
            <? if($tintoreria) { ?>
              <th width="120px"><strong>TIPO</strong></th>
              <th width="320px"><strong>CONCEPTO</strong></th>
            <? } else { ?>
            <th width="440px"><strong>CONCEPTO</strong></th>
            <? } ?>
        	<th width="60px" ><strong>UNIDAD</strong></th>
            <th width="70px"><strong>PRECIO</strong></th>
            <!--<th width="50px"><strong>IVA</strong></th>-->
            <th width="50px"><strong>CANT</strong>.</th>
            <th width="140px"><strong>TOTAL</strong></th>
          </tr>
          <?php echo $renglones; ?>
      </table>

      <br><br>    
      <div style='width:310px; margin-left:470px; font-weight:bold;margin-top:10px; color:#666;	'>
          <div style='width:160px; display:inline-block; text-align:right; '>Subtotal</div>
          <div style='width:140px;  display:inline-block; text-align:right;border-bottom:1px solid #ccc;'>$<?php echo $operaciones["subtotalf"]; ?></div>
		
    <? if($operaciones["descuento"]>0){ ?>
          <br>
          <div style='width:160px;display:inline-block; text-align:right;'>Descuento</div>
          <div style='width:140px;  display:inline-block; text-align:right;border-bottom:1px solid #ccc;'>-$<?php echo $operaciones["descuentof"]; ?></div>
    <? } ?>
		<br>
          <div style='width:160px;display:inline-block; text-align:right;'>I.V.A.</div>
          <div style='width:140px;  display:inline-block; text-align:right;border-bottom:1px solid #ccc;'>$<?php echo $operaciones["ivaf"]; ?></div>
		<br>
        	<? 
			$datos_ = array();
            $transaccion= array();
            $datos2 = seleccionar("transaccionimpuesto","iTipo,fCantidad,sImpuesto,fPorcentaje","iTransaccionId='$id'",false, false);
			   foreach($datos2 as $renglones) {
					$datos_['sImpuesto']	= 	$renglones['sImpuesto'];
					$datos_['cantidad']		= 	number_format($renglones['fCantidad'],2);  
					$datos_['porcentaje']		= 	number_format($renglones['fPorcentaje'],2);       
					array_push($transaccion,$datos_);
					?>
						<div style='width:160px;display:inline-block; text-align:right; font-size:14px;'><?php echo $datos_["sImpuesto"]." (".$datos_['porcentaje'].")%"; ?></div>
						<div style='width:140px;  display:inline-block; text-align:right;border-bottom:1px solid #ccc;'>$<?php echo $datos_["cantidad"]; ?></div>
						<br> <?
			   }
           ?>
        
          <div style='width:160px;display:inline-block; text-align:right;'>TOTAL</div>          
          <div style='width:140px;  display:inline-block;text-align:right;border-bottom:1px solid #ccc;'>$<?php echo $operaciones["totalf"]; ?></div>
     </div>

     <? if($datos['dLimitePago']!="" && $datos['fInteresPagare']!="" && $datos['fInteresPagare']>0 ){ ?>
     <div style='font-size:8px; margin-top:15px; text-align:justify;'>Debo(emos) y pagare(mos) incondicionalmente, a la vista y a la orden de <?=$datos['sRazonSocial'];?> la cantidad anotada en esta factura el dia <?=$datos['dLimitePago'];?>, en la ciudad de <?=$datos["sCiudad"];?>, si no fuere pagado satisfactoriamente este pagaré me(nos) obligo(amos) a pagar durante todo el tiempo que permaneciera total o parcialmente insoluto, intereses moratorios a razón del <?=$datos['fInteresPagare'];?>% mensual sin que por esto considere prorrogado el plazo fijado para cumplir esta obligación. LA FIRMA DE ESTE COMPROBANTE DARA COMO ACEPTADO ESTE PAGARE.</div>
      <? } ?>
  </div>
    <!--CUERPO DE REPORTE#-->
    
    
         
    </div>
    
   <!--PIE DE REPORTE -->
  <div id="footer">
    	<p class="page">
        <center>
        <div style='margin-top:7px;'><?php echo $datos['sRazonSocial']; ?></div>
    	<div><?php echo utf8_encode($datos["sCalle"])." ".$datos["sNumeroExterior"]." Col.".$datos["sColonia"]." ".$datos["sCiudad"].", ".$datos["sEstado"].", ".$datos["sPais"]; ?></div>
        <div>Tel&eacute;fono <? echo $datos["sTelefonoContacto"]; ?></div>
    	<div><? echo $datos["sCorreo"]; ?></div>
        </center>
    	</p>
         
    </div>	
	<!--PIE DE REPORTE#-->
      <div style='text-align:justify; width:780px;'>
    <?
     if($datos['iTipoTransaccion'] == "3" ) {
		    echo ($datos['sEnriquecido']);
	   }
	?>
</div>
    


</html>