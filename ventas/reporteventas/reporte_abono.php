<?php
if(file_exists("../acceso/seguridad.php")){
include("../acceso/seguridad.php");
$p = "..";
}
if(file_exists("../../acceso/seguridad.php")){
include("../../acceso/seguridad.php");
$p = "../..";
}

if(file_exists("./acceso/seguridad.php")){
include("./acceso/seguridad.php");
$p = ".";
}
$id=$_REQUEST['iId'];

$montoabono = verabono((int)$id);
$iTransaccionId=$montoabono['iTransaccionId'];
$fecha_abono = $montoabono['dFecha'];
$fPago = $montoabono['fPago'];
$renglones=conceptosTransaccion((int)$iTransaccionId);
$operaciones=totalesTransaccion((int)$iTransaccionId);
$datos=datos_transaccion((int)$iTransaccionId);
$detalleabonos_ = detalleabonos((int)$iTransaccionId,(int)$id);
$detalleabonos =$detalleabonos_[0];
$saldo_anterior=$detalleabonos_[1];
$saldo_anterior = limpia_formato($operaciones['totalf'])-limpia_formato($saldo_anterior);
$saldo_actual = $saldo_anterior-$fPago;
$saldo_anterior = number_format($saldo_anterior,2);
$saldo_actual = number_format($saldo_actual,2);

if($datos["iTipoTransaccion"]=="1"){
 $sTipoTransaccion= "Nota de Venta";
}
else if($datos['iTipoTransaccion'] == "2" ) {
 $sTipoTransaccion="Factura";
}
else if($datos['iTipoTransaccion'] == "3" ) {
 $sTipoTransaccion="Cotización";
}
if($datos["iEstatus"]==1)
	$estatus = "<div><label style='color:red;'>Cancelado</label></div>";
else 
	$estatus = "";

?>
<html>

<head> 
	<link rel="stylesheet" type="text/css" href="../css/estilo_cotizacion.css" />
</head>
<style> 
@page { margin: 160px 20px 120px 20px; }
    #header { position: fixed; left: 0px; top: -140px; right: 0px; height: 140px; text-align: center; border-bottom:2px solid #ccc;}
    #footer { position: fixed; left: 0px; bottom: -110px; right: 0px; height: 80px; background:#ccc; font-size:10px;}
</style>

<div style='font-family:Verdana, Geneva, sans-serif; color:#666;'>
	
    <!--ENCABEZADO -->
    <div style='width:780px; border-top:1px solid #fff;' id="header">
    	
        <div style='width:220px; background-color:#fff; display:inline-block; border-right:2px solid #CCC;'>
           
            <?php if(true){
                if(file_exists("$p/imagen/logotipo.jpg")) { 
                
                	list($anchura, $altura, $type, $attr) = getimagesize("$p/imagen/logotipo.jpg");
					
					if($anchura>$altura) {
						#Mas ancho       
						if(($altura*3.33)>$anchura) { $yy=60; $xx=0; }
						else { 
							$xx=200; $yy=0; 
				
						}
					}
					else {
					   
					   if(($anchura/3.33)>$altura) { $xx=200; $yy=0; }
					   else { $xx=0; $yy=60; }
					}
						
				  ?>
                  <img src="<?=$p;?>/imagen/logotipo.jpg" width="<?=$xx?>" height="<?=$yy?>" alt="Logotipo" />
                  
				<? } 
			}
        ?>                
          
        </div>
         
        <div style="width:360px; height:106px; display:inline-block; text-align:left;">
            <div style='margin-left:20px; margin-top:15px; font-size:24px; color:#444; vertical-align:top;'>
            PAGO
			</div>
            <div style='margin-left:20px; margin-top:8px; font-size:13px; color:#444;vertical-align:top;'>
            Cliente: <? echo $datos["sNombre"]; ?>
			</div>
            <div style='margin-left:20px; margin-top:8px; font-size:13px; color:#444;vertical-align:top; display:none;'>
            Para: <?php echo $datos['sRazonSocial']; ?>
            </div>
             <div style='margin-left:20px; margin-top:8px; font-size:12px; color:#444;vertical-align:top; text-align:left;'>
            <strong>Fecha PAGO:</strong> <? echo $fecha_abono; ?>
            </div>
        </div>
        
		<div style="width:180px; height:110px; border:2px solid #ccc; display:inline-block;">
         	<div style='margin-top:10px;margin-left:10px;padding-bottom:1px;color:#444;font-size:19px;text-align: left;'>
            <strong>Folio <?=$sTipoTransaccion;?>:</strong><? echo $datos["sSerie"].$datos["iFolio"]; ?>
            </div>
            <div style='margin-left:10px; margin-top:8px; font-size:12px; color:#444;vertical-align:top; text-align:left;'>
            <strong>Fecha <?=$sTipoTransaccion;?>:</strong> <? echo $datos["dFecha"]; ?>
            </div>
          <!--  <div style='margin-left:10px; margin-top:8px; font-size:12px; color:#444;vertical-align:top; text-align:left;'>
            <strong>Lugar <?=$sTipoTransaccion;?>:</strong> <? echo $datos["sCiudad"].", ".$datos["sEstado"].", ".$datos["sPais"]; ?>
            <div style='margin-top:10px;margin-left:10px;padding-bottom:1px;color:#444;font-size:19px;text-align: left;'>            
            </div>
           
            </div>-->
		</div>
        <?= $estatus; ?>
    </div>
    <!--ENCABEZADO#-->
    
        <!--PIE DE REPORTE -->
  <div id="footer">
    	<p class="page">
        <center>
        <div style='margin-top:7px;'><?php echo $datos['sRazonSocial']; ?></div>
    	<div><?php echo utf8_encode($datos["sCalle"])." ".$datos["sNumeroExterior"]." Col.".$datos["sColonia"]." ".$datos["sCiudad"].", ".$datos["sEstado"].", ".$datos["sPais"]; ?></div>
        <div>Tel&eacute;fono <? echo $datos["sTelefonoContacto"]; ?></div>
    	<div><? echo $datos["sCorreo"]; ?></div>
        </center>
    	</p>
         
    </div>	
	<!--PIE DE REPORTE#-->
    
    
    <!--CUERPO DE REPORTE -->
    <div style="width:780px;padding-bottom:30px;background-color:#fff;">    	    
      <br>
    <table border="1" style='border:none; width:780px; font-size:11px; border-collapse:collapse;'>
      <tr><td width="80" style="font-weight:bold; border:none;">Recibimos de:</td><td style='border-bottom:1px solid #ccc;'><? echo $datos["sNombre"]; ?></td></tr>
      <tr><td width="80" style="font-weight:bold; border:none;">La cantidad de:</td><td style='border-bottom:1px solid #ccc;'><? echo $fPago; ?></td></tr>
      <tr><td width="80" style="font-weight:bold; border:none;">Por concepto de:</td><td style='border-bottom:1px solid #ccc;'>Pago a  <?=$sTipoTransaccion;?> con FOLIO <?=$datos["sSerie"].$datos["iFolio"];?></td></tr>
    </table>
     
     <div style="clear:both;"></div>     
     

     <br><br>
   <div style='width:300px; margin-left:470px; font-weight:bold;margin-top:10px;'>
          <div style='width:160px; display:inline-block; text-align:right; '>Saldo Anterior</div>
          <div style='width:140px;  display:inline-block; text-align:right;border-bottom:1px solid #ccc;'>$<?php echo $saldo_anterior; ?></div>
    <br>
          <div style='width:160px;display:inline-block; text-align:right;'>Este Pago</div>
          <div style='width:140px;  display:inline-block; text-align:right;border-bottom:1px solid #ccc;'>-$<?php echo $fPago; ?></div>
    <br>
          <div style='width:160px;display:inline-block; text-align:right;'>Saldo Actual</div>
          <div style='width:140px;  display:inline-block; text-align:right;border-bottom:1px solid #ccc;'>$<?php echo $saldo_actual; ?></div>              
     </div>
     <br><br>
  <div style="border-top:1px solid #ccc; width:780px; height:1px;"></div>
  <label> Historial de Pagos a <?=$sTipoTransaccion;?></label>

  <br><br>
  <table border="1" style='border:none; width:400px; font-size:12px; border-collapse:collapse;'>
          <tr> 
            <th width="80px" style='border:none; text-align:left;'><strong>FECHA</strong></th>
            <th width="120px"  style='border:none;'><strong>PAGO</strong></th>            
          </tr>
          <?php echo utf8_decode($detalleabonos); ?>
      </table>

      <br><br>  


  </div>
    <!--CUERPO DE REPORTE#-->
    
    
    
    </div>



</div>
</html>