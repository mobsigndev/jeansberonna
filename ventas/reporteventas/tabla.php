<?
include("../../acceso/seguridad.php");
$tabla = $_REQUEST['tabla'];
$condicion_ = stripslashes($_REQUEST['condicion']);
$condicion_ = explode("|", $condicion_);
$condicion = $condicion_[0];
$cobranza = $condicion_[1];
$cliente = $condicion_[2];
$condicion = "transaccion.iUsuarioEmpresaId='".$iUsuarioEmpresaId."'";
if($cliente!="0" && $cliente!="") { 
  $condicion .= " and transaccion.iClienteId='$cliente'"; 
  $ccondicion = " and iId='$cliente'"; 
}
?>
<table class="table table-bordered table-striped" id="<?=$tabla?>">
  <thead>
    <th>
        Cobranza
    </th>
    <th>
        Fecha último pago
    </th>      
 <th>
        % de Pago
    </th> 
    <th>
    	Cliente
    </th>
    <th>
     	 Saldo
    </th>
    <th>
	    Estatus	
    </th>
	</thead>
   <tbody>   
   <?
    $datos_cliente = seleccionar("cliente","iId","iUsuarioEmpresaId='".$iUsuarioEmpresaId."' and iEstatus=0 $ccondicion",false, false);
    foreach($datos_cliente as $cliente) {    
      $idCliente =$cliente['iId']; 
      $saldo=0;
      $total_ventas=0;
      $sNombre="";
      $limite=0;
      $s_ = saldo($idCliente);
      $saldo=$s_[0];
      $saldot+=$saldo;
      $total_ventas=$s_[1];
      $sNombre=$s_[3];
      $limite=$s_[2];
      $procede=$s_[4];

    $datos_transaccion0 = seleccionar("pagotransaccion inner join transaccion on transaccion.iId = pagotransaccion.iTransaccionId ","max(dtFechaOperacion) as dtFecha,DATEDIFF(CURRENT_DATE(),max(dtFechaOperacion)) as ias","transaccion.iUsuarioEmpresaId='".$iUsuarioEmpresaId."' and transaccion.iClienteId='".$idCliente."' and pagotransaccion.fPago >0 and transaccion.iEstatus=0",false, false);
    foreach($datos_transaccion0 as $dato0) {   
    $dtFecha =$dato0['dtFecha']; 
    $dias_pago =$dato0['ias']; 
    }  
    if($dias_pago==""){ $dias_pago="-"; }



    $datos_transaccion = seleccionar("transaccion","iId,iFolioRaiz","transaccion.iUsuarioEmpresaId='".$iUsuarioEmpresaId."' and transaccion.iClienteId='".$idCliente."' and transaccion.iEstatus=0",false, false);
    foreach($datos_transaccion as $dato) {    
      $iRaizId_ =$dato['iFolioRaiz']; 
    }  
    $datos_transaccion2 = seleccionar("transaccion","dFecha as dtFecha,DATEDIFF(CURRENT_DATE(),dFecha) as ias","transaccion.iId='".$iRaizId_."'",false, false);
    foreach($datos_transaccion2 as $dato2) {    
      $dtFecha2 =$dato2['dtFecha']; 
      $dias =$dato2['ias']; 
    }
    $porcentaje_pago = number_format(100-($saldo*100/$total_ventas),2);
        if(tabla_cobranza($dias,$cobranza)) {
          $muestra=true;
        }
        else {
          $muestra = false;
        }
      if($muestra && $porcentaje_pago<100){
          echo '<tr>';
          echo '<td class="text-center">'.label($dias,"cobranza").'</td>';
          echo '<td>'.$dtFecha.'</td>';
          echo '<td class="text-center">'.number_format($porcentaje_pago,0).'%</td>';
          echo '<td>'.$sNombre.'</td>';
          echo '<td>'.number_format($saldo,2).'</td>'; 
          echo '<td>';
          if($porcentaje_pago>40 && $dias>=15) {
            echo "<label class='label label-success'> CLIENTE APTO PARA NUEVA VENTA </label>";
          }  
          else if($dias<=15) {
            echo "<label class='label label-success'> CLIENTE EN TIEMPO PARA CAMBIOS  </label>";
          }
          else {
            echo "<label class='label label-danger'> NO ES APTO PARA VENTA NI CAMBIOS, TIENE QUE REALIZAR UN ABONO</label>";
          }
          echo '</td>'; 		
          echo '</tr>';
      }
   
  }

   ?>                             

</tbody>
<tfoot>
<tr>
    <th colspan="4" class="text-right">
      Totales
    </th>
    <th>
       <?=number_format($saldot,2);?>
    </th>
    <th>
    </th>
</tr>
  </tfoot>
</table>

<?php

function  tabla_cobranza($dias,$cobranza){
  if($cobranza=="15") {
    if($dias>=0 && $dias<=29) {
      return true;
    }
    else return false;
  }
  else if($cobranza=="30") {
    if($dias>29 && $dias<45) return true;
    else return false;
  }
  else if($cobranza=="45") {
    if($dias>=45 && $dias<60) return true;
    else return false;
  }
  else if($cobranza=="60") {
    if($dias>=60 && $dias<90) return true;
    else return false;
  }
  else if($cobranza=="90") {
    if($dias>=90 && $dias<120) return true;
    else return false;
  }
  else    if($cobranza=="120") {
    if($dias>=120) return true;
    else return false;
  }
  else return true;
}
?>