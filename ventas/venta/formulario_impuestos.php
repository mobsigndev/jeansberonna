<? include("../../acceso/seguridad.php");?>
<div id='ventana-formulario'>
<!-- Nav tabs -->
<ul class="nav nav-pills">
  <!--<li class='active'><a href="#general" data-toggle="tab">Agregar Impuestos</a></li>-->
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="general">
  <!--<input type="text" class="form-control" name="dRegistro" value="CURRENT_DATE()" style='display:none;'>-->
  <input type="text" class="form-control" name="iId" style='display:none;'>
  <div class="row">
	
  <div class="col-lg-12">
      
      	 <label><h4>Impuestos Retenidos</h4></label><br />
         <label>Federales</label><br />
         <div class="btn-group" data-toggle="buttons">
     		    <?
                $select = seleccionar("impuesto","fPorcentaje,sNombre,iId,iBaseGravable,iTipo","iEstatus=0 and iTipo=0 and iNivel=0 and iUsuarioEmpresaId='$iUsuarioEmpresaId'",false, false);
                foreach($select as $dato) {
                ?>                  
                  <input type="button" nombre_impuesto="<?=$dato['sNombre'];?>" impuesto="<?=$dato['fPorcentaje'];?>" tipo="<?=$dato['iTipo'];?>" gravable="<?=$dato['iBaseGravable'];?>" id_impuesto="<?=$dato['iId'];?>" value="<?=$dato['sNombre']." (".$dato['fPorcentaje']."%)";?>" name="impuestos_" tipo='0' class="btn btn-primary">
                <?
                }
                ?>
                </div>
                
                 <br /><label>Estatales (locales)</label><br />
         <div class="btn-group" data-toggle="buttons">
     		    <?
                $select = seleccionar("impuesto","fPorcentaje,sNombre,iId,iBaseGravable,iTipo","iEstatus=0 and iTipo=0 and iNivel=1 and iUsuarioEmpresaId='$iUsuarioEmpresaId'",false, false);
                foreach($select as $dato) {
                ?>                  
                  <input type="button" nombre_impuesto="<?=$dato['sNombre'];?>" impuesto="<?=$dato['fPorcentaje'];?>" tipo="<?=$dato['iTipo'];?>" gravable="<?=$dato['iBaseGravable'];?>" id_impuesto="<?=$dato['iId'];?>" value="<?=$dato['sNombre']." (".$dato['fPorcentaje']."%)";?>" name="impuestos_" tipo='0' class="btn btn-primary">
                <?
                }
                ?>
                </div> 
                
     		<br /><label><h4>Impuestos Trasladados</h4></label><br />
            <label>Federales</label><br />
            <div class="btn-group" data-toggle="buttons">
     		    <?
                $select = seleccionar("impuesto","fPorcentaje,sNombre,iId,iBaseGravable,iTipo","iEstatus=0 and iTipo=1 and iNivel=0 and iUsuarioEmpresaId='$iUsuarioEmpresaId'",false, false);
                foreach($select as $dato) {
                ?>                               	
                     <input type="button" nombre_impuesto="<?=$dato['sNombre'];?>" impuesto="<?=$dato['fPorcentaje'];?>" tipo="<?=$dato['iTipo'];?>" gravable="<?=$dato['iBaseGravable'];?>" id_impuesto="<?=$dato['iId'];?>" value="<?=$dato['sNombre']." (".$dato['fPorcentaje']."%)";?>" name="impuestos_" tipo='1' class="btn btn-primary">
                <?
                }
                ?>
            </div>
            <br /><label>Estatales (locales)</label><br />
         <div class="btn-group" data-toggle="buttons">
     		    <?
                $select = seleccionar("impuesto","fPorcentaje,sNombre,iId,iBaseGravable,iTipo","iEstatus=0 and iTipo=1 and iNivel=1 and iUsuarioEmpresaId='$iUsuarioEmpresaId'",false, false);
                foreach($select as $dato) {
                ?>                  
                  <input type="button" nombre_impuesto="<?=$dato['sNombre'];?>" impuesto="<?=$dato['fPorcentaje'];?>" tipo="<?=$dato['iTipo'];?>" gravable="<?=$dato['iBaseGravable'];?>" id_impuesto="<?=$dato['iId'];?>" value="<?=$dato['sNombre']." (".$dato['fPorcentaje']."%)";?>" name="impuestos_" tipo='1' class="btn btn-primary">
                <?
                }
                ?>
                </div> 
            
      </div>
    </div>
  		
  </div> 
  
  <div class="tab-pane" id="otro">
	<div class="row">  
	   <div class="col-lg-4">
			         		
      </div>
	 </div>      
  </div>

</div>
</div>